from django.contrib.auth.models import User
import csv

headers = ['username', 'password', 'email', 'phone_number', 'first_name', 'last_name', 'address']


def migrate_user():
    with open('user.csv', 'w') as csvfile:
        writer = csv.writer(csvfile)
        writer.writerow(headers)
        for user in User.objects.all():
            writer.writerow([user.username,
                             user.password,
                             user.email if user.email else user.profile.email,
                             user.profile.phone_number,
                             user.first_name,
                             user.last_name,
                             user.profile.address])


if __name__ == '__main__':
    migrate_user()
