import re
from user_module.models import UserProfile

change = (
    (r'^0169', '039'),
    (r'^0168', '038'),
    (r'^0167', '037'),
    (r'^0166', '036'),
    (r'^0165', '035'),
    (r'^0164', '034'),
    (r'^0163', '033'),
    (r'^0162', '032'),
    (r'^0120', '070'),
    (r'^0121', '079'),
    (r'^0122', '077'),
    (r'^0126', '076'),
    (r'^0128', '078'),
    (r'^0124', '084'),
    (r'^0127', '081'),
    (r'^0129', '082'),
    (r'^0123', '083'),
    (r'^0125', '085'),
    (r'^0186', '056'),
    (r'^0188', '058'),
    (r'^0199', '059'),
)


def update_change():
    profiles = UserProfile.objects.filter(phone_number__isnull=False)
    for pro in profiles:
        for ch in change:
            if re.match(ch[0], pro.phone_number):
                print(f'OLD : {pro.phone_number}')
                pro.phone_number = re.sub(ch[0], ch[1], pro.phone_number)
                pro.save()
                print(f'NEW : {pro.phone_number}')
                continue


if __name__ == '__main__':
    update_change()
