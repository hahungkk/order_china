from shop_module.models import TrackingChecker, ShipmentPack, OrderItem

order_items = OrderItem.objects.filter(shipment_pack__isnull=False)
list_sp = []

for order in order_items:
    sp = ShipmentPack.objects.filter(tracking_code=order.shipment_pack)
    if sp and sp not in list_sp:
        list_sp.append(sp)
        TrackingChecker.objects.get_or_create(tracking_code=sp.first(), version=1)
        print("Created %s" % order.shipment_pack)

print("Done")
