# from shop_module.constants import DefaultConfigure
from shop_module.models import Order
from shop_module.controllers.OrderController import calculate_service_charge2
# from system_configure.controllers import SystemConfigureController
import datetime

date_cal = datetime.datetime.now()
date_cal = date_cal.replace(month=7, day=31)

list_orders = Order.objects.filter(created__gte=date_cal)
# service_charge_data = SystemConfigureController.getConfigure('service_charge_data',
#                                                              default=DefaultConfigure.service_charge_data)
for order in list_orders:
    print('====== Order %s ' % order.id)
    for order_item in order.orderitem_set.all():
        order_item.service_charge, order.insurance_charge = calculate_service_charge2(
            None, order.total_item_cost, None, None, order_item.price, order_item.quantity,
            order_item.weight, None, order_item.insurance, order_item.bargain, order.customer)
        order_item.save()
print('======= DONE =======')
