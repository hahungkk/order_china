from openpyxl import load_workbook
from user_module.models import UserProfile, User


wb = load_workbook('ID_khach_hang.xlsx')
ws = wb['Sheet1']
for row in ws.values:
    if row[2] is not None:
        try:
            user = UserProfile.objects.get(user__username=row[1])
            data = str(row[2])
            user.facebook_uid = data
            user.save()
        except Exception as e:
            print(e)

