import os
import django
from shop_module.models import TrackingChecker, ShipmentPack

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "order_china.settings")
django.setup()


def run():
    print("START")
    for ship in ShipmentPack.objects.all():
        check = TrackingChecker.objects.filter(tracking_code=ship).exists()
        if not check:
            TrackingChecker.objects.get_or_create(tracking_code=ship, version=1)
            print(ship.tracking_code)
    print("====== DONE =========")


if __name__ == '__main__':
    run()
