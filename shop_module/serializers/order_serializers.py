from django.db.models import Sum
from rest_framework import serializers
from shop_module.api.Payment_RestfulAPI import CustomerPaymentSerializer
from shop_module.constants import STATUS_COLOR_MAPPING
from shop_module.models import OrderItem, ShipmentPack, ShipmentStatus, Order, Status, CustomerPayment, \
    MONEY_MAX_DIGITS, MONEY_DECIMAL_PLACES
from user_module.models import UserProfile


class ShipmentStatusSerializer(serializers.ModelSerializer):
    class Meta:
        fields = '__all__'
        model = ShipmentStatus


class OrderItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderItem
        fields = ('id', 'shopping_domain', 'vendor', 'customer_note', 'name', 'item_url', 'checked_quantity',
                  'image_url', 'options_selected', 'quantity', 'currency', 'exchange_rate', 'fast_order',
                  'price', 'shipping', 'service_charge', 'insurance_charge', 'tax_cost', 'fragile', 'insurance',
                  'status', 'status_value', 'shipment', 'shipment_all', 'fast_order_charge', 'bargain_charge')

    status = serializers.SlugRelatedField(slug_field='label', queryset=Status.objects.all())
    status_value = serializers.SerializerMethodField(read_only=True)
    shipment = serializers.SerializerMethodField(read_only=True)
    shipment_all = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_status_value(order):
        return order.status.value

    @staticmethod
    def get_shipment(order_item):
        if order_item.shipment_pack:
            sp = ShipmentPack.objects.filter(tracking_code=order_item.shipment_pack).first()
            if sp:
                return ShipmentStatusSerializer(sp.status_set.first()).data
            return None
        return None

    @staticmethod
    def get_shipment_all(order_item):
        if order_item.shipment_pack:
            sp = ShipmentPack.objects.filter(tracking_code=order_item.shipment_pack).first()
            if sp:
                return ShipmentStatusSerializer(sp.status_set.all(), many=True).data
            return None
        return None


class OrderItemAdminSerializer(OrderItemSerializer):
    status = serializers.SlugRelatedField(slug_field='value', queryset=Status.objects.all())
    status_label = serializers.SerializerMethodField()
    status_tag = serializers.SerializerMethodField()
    customer = serializers.SerializerMethodField(read_only=True)
    exporter = serializers.SlugRelatedField(slug_field='username', read_only=True)
    importer = serializers.SlugRelatedField(slug_field='username', read_only=True)
    count_item = serializers.SerializerMethodField()
    count_order_quantity = serializers.SerializerMethodField()

    @staticmethod
    def get_count_order_quantity(obj):
        if obj.shipment_pack is not None:
            count = OrderItem.objects.filter(shipment_pack=obj.shipment_pack)\
                .exclude(status__value='Failed')\
                .aggregate(count=Sum('quantity'))
            return count['count']
        return 0

    @staticmethod
    def get_customer(item):
        return item.order.customer.username

    @staticmethod
    def get_count_item(item):
        return item.order.orderitem_set.count()

    @staticmethod
    def get_status_label(item):
        return item.status.label

    @staticmethod
    def get_status_tag(order):
        value = order.status.value
        r = STATUS_COLOR_MAPPING.get(value)
        return r if r else 'lime'

    class Meta:
        model = OrderItem
        fields = ('image_inline_tag', 'image_url', 'item_url',
                  'id', 'vendor', 'name', 'order', 'status_label',
                  'option_selected_tag', 'customer',
                  'quantity', 'price', 'shipping', 'shipvn',
                  'status', 'order_code', 'shipment_pack',
                  'customer_note', 'order_employee', 'checked_quantity',
                  'note', 'shipment', 'exporter', 'exported_date', 'importer', 'imported_date',
                  'weight', 'fragile', 'insurance', 'fast_order',
                  'currency', 'exchange_rate', 'count_order_quantity',
                  'order_quantity', 'order_price', 'order_shipping',
                  'paid_quantity', 'paid_price', 'paid_shipping', 'count_item',
                  'service_charge', 'insurance_charge', 'fast_order_charge', 'tax_cost',
                  'total_order_currency_tag', 'total_tag', 'status_tag')


class OrderItemAdminSerializerSP(serializers.ModelSerializer):
    customer = serializers.SerializerMethodField(read_only=True)
    exporter = serializers.SerializerMethodField(read_only=True)
    exported_date = serializers.SerializerMethodField(read_only=True)
    weight = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_exporter(item):
        return item.exporter.username if item.exporter else None

    @staticmethod
    def get_customer(item):
        return item.order.customer.username

    @staticmethod
    def get_exported_date(item):
        return item.exported_date.strftime('%d/%m/%Y %H:%M')

    @staticmethod
    def get_weight(item):
        sp = ShipmentPack.objects.get(tracking_code=item.shipment_pack)
        return sp.weight

    class Meta:
        model = OrderItem
        fields = ('shipment_pack', 'customer', 'order', 'weight', 'quantity', 'exported_date', 'exporter')


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order

        fields = (
            'id', 'created', 'modified', 'status', 'currency', 'exchange_rate', 'extra_charge',
            'customer_note', 'prepaid_percent', 'facebook_name', 'delivery_method', 'delivery_address',
            'city', 'district', 'street', 'receiver_name', 'receiver_phone', 'tax_cost', 'grant_total',
            'sum_payment_transaction', 'total_cost', 'count_item', 'total_item_cost',
            'sum_item_service_charge', 'sum_item_tax_cost', 'order_weight',
            'sum_cost_by_weight', 'payment_left_tag', 'sum_payment_transaction_tag',
            'redude_cost_tag')

        read_only_fields = (
            'id', 'created', 'modified', 'status', 'currency', 'exchange_rate', 'extra_charge',
            'tax_cost', 'grant_total', 'sum_payment_transaction', 'total_cost', 'count_item',
            'total_item_cost', 'sum_item_service_charge', 'sum_item_tax_cost',
            'order_weight', 'cost_by_weight', 'payment_left_tag', 'sum_payment_transaction_tag',
            'redude_cost_tag')

    status = serializers.SlugRelatedField(slug_field='label', read_only=True)
    facebook_name = serializers.SerializerMethodField()

    @staticmethod
    def get_facebook_name(order):
        if order.customer.profile:
            return order.customer.profile.user_facebook
        return None


class OrderSerializerAdmin(serializers.ModelSerializer):
    class Meta:
        model = Order

        fields = ['id', 'customer', 'count_item', 'order_cost', 'total_cost_tag', 'status', 'created_by_id',
                  'sum_payment_transaction_tag', 'sum_item_service_charge_orig_tag', 'created', 'created_by',
                  'modified_by', 'latest_transaction_date', 'ordered_date', 'shiptovn_date', 'status_tag']

        read_only_fields = ['id', 'customer', 'count_item', 'order_cost', 'total_cost_tag', 'status',
                            'sum_payment_transaction_tag', 'sum_item_service_charge_orig_tag', 'created',
                            'modified_by', 'latest_transaction_date', 'ordered_date', 'shiptovn_date', 'status_tag']

    status = serializers.SlugRelatedField(slug_field='label', read_only=True)
    status_tag = serializers.SerializerMethodField()
    customer = serializers.SlugRelatedField(slug_field='username', read_only=True)
    created_by = serializers.SlugRelatedField(slug_field='username', read_only=True)
    modified_by = serializers.SlugRelatedField(slug_field='username', read_only=True)

    @staticmethod
    def get_status_tag(order):
        value = order.status.value
        if order.orderitem_set.filter(fast_order=True).exists():
            return "lime"
        r = STATUS_COLOR_MAPPING.get(value)
        return r if r else 'info'


class OrderDetailAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order

        fields = (
            'sum_by_currency', 'created_by_id', 'created_by',
            'id', 'created', 'modified', 'status', 'currency', 'exchange_rate', 'extra_charge',
            'customer_note', 'facebook_name', 'prepaid_percent', 'redude_cost_tag',
            'tax_cost', 'grant_total_tag', 'sum_payment_transaction', 'total_cost',
            'count_item', 'total_item_cost', 'sum_item_service_charge', 'sum_item_tax_cost',
            'order_weight', 'sum_cost_by_weight', 'customer', 'ship_to_vn', 'phone_tag',
            'insurance_charge', 'sum_item_service_charge_tag', 'sum_item_shipping_tag',
            'sum_item_cost_tag', 'status_tag', 'sum_payment_transaction_tag', 'payment_left_tag',
            'sum_cost_by_weight_tag', 'payment', 'grant_total', 'created_username')

        read_only_fields = (
            'sum_by_currency',
            'id', 'created', 'modified', 'currency', 'exchange_rate', 'extra_charge',
            'tax_cost', 'grant_total_tag', 'sum_payment_transaction', 'total_cost', 'count_item',
            'total_item_cost', 'sum_item_service_charge', 'sum_item_tax_cost',
            'order_weight', 'cost_by_weight', 'customer', 'ship_to_vn', 'redude_cost_tag',
            'phone_tag', 'insurance_charge', 'sum_item_service_charge_tag', 'sum_item_shipping_tag',
            'sum_item_cost_tag', 'status_tag', 'sum_payment_transaction_tag', 'payment_left_tag',
            'sum_cost_by_weight_tag', 'payment', 'grant_total', 'created_username')

    payment = CustomerPaymentSerializer(read_only=True, many=True)
    created_username = serializers.SerializerMethodField()
    status = serializers.SlugRelatedField(slug_field='value', queryset=Status.objects.all())
    facebook_name = serializers.SerializerMethodField()
    customer = serializers.SlugRelatedField(slug_field='username', read_only=True)
    status_tag = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_status_tag(order):
        value = order.status.value
        r = STATUS_COLOR_MAPPING.get(value)
        return r if r else 'info'

    @staticmethod
    def get_created_username(order):
        return order.created_by.username if order.created_by else None

    @staticmethod
    def get_facebook_name(order):
        if order.customer.profile:
            return order.customer.profile.user_facebook
        return None


class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order

        fields = (
            'id', 'created', 'modified', 'status', 'currency', 'exchange_rate', 'extra_charge',
            'customer_note', 'prepaid_percent', 'facebook_name', 'grant_total', 'sum_total',
            'status_value',
            'delivery_method', 'delivery_address', 'city', 'district', 'street', 'receiver_name',
            'receiver_phone', 'tax_cost', 'grant_total', 'sum_payment_transaction', 'total_cost',
            'count_item', 'total_item_cost', 'sum_item_service_charge', 'sum_item_tax_cost',
            'orderitem_set', 'order_weight', 'sum_cost_by_weight')

        read_only_fields = (
            'id', 'created', 'modified', 'status', 'currency', 'exchange_rate', 'extra_charge',
            'sum_total', 'status_value',
            'tax_cost', 'grant_total', 'sum_payment_transaction', 'total_cost', 'count_item',
            'total_item_cost', 'sum_item_service_charge', 'sum_item_tax_cost',
            'orderitem_set', 'order_weight', 'cost_by_weight', 'grant_total')

    orderitem_set = OrderItemSerializer(many=True, read_only=True)
    status = serializers.SlugRelatedField(slug_field='label', read_only=True)
    status_value = serializers.SerializerMethodField()
    facebook_name = serializers.SerializerMethodField()

    @staticmethod
    def get_facebook_name(order):
        if order.customer.profile:
            return order.customer.profile.user_facebook
        return None

    @staticmethod
    def get_status_value(order):
        return order.status.value


class OrderCountSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        super(OrderCountSerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        super(OrderCountSerializer, self).create(validated_data)

    date = serializers.DateTimeField()
    count = serializers.IntegerField()


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = 'label'


class ItemHistorySerializer(serializers.Serializer):
    history_date = serializers.DateTimeField()
    history_user = serializers.SerializerMethodField()
    history_type = serializers.SerializerMethodField()
    status = serializers.SerializerMethodField()
    quantity = serializers.IntegerField()
    order_quantity = serializers.IntegerField()
    fragile = serializers.BooleanField()
    insurance = serializers.BooleanField()
    fast_order = serializers.BooleanField()
    service_charge = serializers.DecimalField(max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES)
    price = serializers.DecimalField(max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES)

    def update(self, instance, validated_data):
        super(ItemHistorySerializer, self).update(instance, validated_data)

    def create(self, validated_data):
        super(ItemHistorySerializer, self).create(validated_data)

    @staticmethod
    def get_status(history):
        return history.status.label

    @staticmethod
    def get_history_user(history):
        return history.history_user.username

    @staticmethod
    def get_history_type(history):
        if history.history_type == '+':
            return 'Created'
        return 'Changed'
