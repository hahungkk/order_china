from chatterbot import ChatBot
from chatterbot.trainers import ListTrainer

chatbot = ChatBot("order_china Bot")

conversation = [
    "Xin chào",
    "Hế lô",
    "Bạn có khỏe không ?",
    "Tôi khỏe.",
    "Rất vui khi được nghe vậy",
    "Cảm ơn",
    "Không có gì !"
]

chatbot.set_trainer(ListTrainer)
chatbot.train(conversation)
