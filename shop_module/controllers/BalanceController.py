from django.db import IntegrityError
from . import PaymentController
from ..models import BalanceAccount, BalanceType


def create_customer_account_balance(user):
    account = BalanceAccount()
    account.account_holder = user
    account.type = BalanceType.user_credit
    account.amount = 0
    account.currency = PaymentController.get_default_currency()
    account.name = "%s - credit balance" % user
    account.save()
    return account


def get_default_user_balance(user):
    try:
        balance = BalanceAccount.objects.get(account_holder=user, currency=PaymentController.get_default_currency())
    except BalanceAccount.DoesNotExist:
        balance = create_customer_account_balance(user)
    return balance


def chargeBalance(balance_id, amount):
    """ Increase of decrease balance
    Remember to do this in a SQL transaction
    """
    balance = BalanceAccount.objects.get(pk=balance_id)
    balance.amount += amount
    if balance.amount < 0:
        raise IntegrityError("Insufficient during transaction")
    else:
        balance.save()
    return balance


def transferBalance(from_balance_id, to_balance_id, withdraw_amount, deposit_amount):
    """ Transfer money from one balance to another
    Remember to do this in a SQL transaction
    """

    if withdraw_amount <= 0:
        raise IntegrityError("BalanceController.transferBalance: amount must >0")
    from_balance = BalanceAccount.objects.get(pk=from_balance_id)
    to_balance = BalanceAccount.objects.get(pk=to_balance_id)

    from_balance.amount -= withdraw_amount
    if from_balance.amount < 0:
        raise IntegrityError("Insufficient during transaction")
    else:
        from_balance.save()

    to_balance.amount += deposit_amount
    to_balance.save()
    return from_balance, to_balance

# def revertTransaction(transactionList,detail=None): count=0; for transactionLog in transactionList: if
# transactionLog.transaction_status == TransactionStatus.auto and transactionLog.balance and transactionLog.amount !=
#  0: if chargeBalance(transactionLog.balance.id, -transactionLog.amount): revertTransactionLog = TransactionLog();
# revertTransactionLog.transaction_type = transactionLog.transaction_type revertTransactionLog.balance =
# transactionLog.balance revertTransactionLog.invoice_bill = transactionLog.invoice_bill revertTransactionLog.amount
# = -transactionLog.amount revertTransactionLog.transaction_status = TransactionStatus.revert if detail:
# revertTransactionLog.data = json.dumps({ 'detail': detail }); revertTransactionLog.save(); count+=1; return count
