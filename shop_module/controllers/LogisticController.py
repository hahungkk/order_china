import decimal
from django.utils.translation import ugettext_lazy as _
from ..models import ShipmentItem, ShipmentPackage, Status, ShipmentLogistics


def get_default_shipment_status():
    status = Status.objects.filter(default=True, is_shipmentstatus=True).first()
    if not status:
        status = Status.objects.all().first()
    return status


def createShipmentPackageForOrders(order_list):
    """ Create a shimpemt package from a list of order

    :param order_list:
    :type order_list: List<Order>
    :return:
    """
    package = ShipmentPackage()
    package.status = get_default_shipment_status()
    package.weight = 0
    package.note = _("Automatic created from order") + ": " + ','.join(str(order) for order in order_list)

    orderitem_list = []
    for order in order_list:
        for orderitem in order.orderitem_set.all():
            package.weight += orderitem.weight
            orderitem_list.append(orderitem)

    if len(order_list) == 1:
        package.order = order_list[0]

    package.save()
    for orderitem in orderitem_list:
        ShipmentItem(order_item=orderitem, shipment_package=package, quantity=orderitem.quantity).save()

    return package


def createShipmentPackageForOrderItems(order_item_list):
    package = ShipmentPackage()
    package.status = get_default_shipment_status()
    package.weight = 0
    package.note = _("Automatic created from %s items" % len(order_item_list))

    package.save()
    order_id_list = []
    for orderitem in order_item_list:
        if orderitem.order_id not in order_id_list:
            order_id_list.append(orderitem.order_id)
        package.weight += orderitem.weight * orderitem.quantity
        ShipmentItem(order_item=orderitem, shipment_package=package, quantity=orderitem.quantity).save()

    if len(order_id_list) == 1:
        package.order_id = order_id_list[0]

    package.save()

    return package


def createShipmentLogisticsForPackages(packageList):
    """ Create a shimpemt package from a list of order

    :param packageList:
    :type packageList: List<ShipmentPackage>
    :return:
    """
    container = ShipmentLogistics()
    container.status = get_default_shipment_status()
    container.weight = 0
    container.note = _("Automatic created for package") + ": " + ','.join(str(package) for package in packageList)

    for package in packageList:
        container.weight += package.weight
    container.save()

    for package in packageList:
        container.shipmentpackage_set.add(package)

    return container


def calculate_shipment_shipment_charge(weight, length, width, height):
    tmp_weight = (length * width * height) / 6000
    if weight > decimal.Decimal(tmp_weight):
        tmp_weight = weight
    return 0
