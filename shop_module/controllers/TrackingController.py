import json
import logging
import requests
from system_configure.controllers.Tool import parse_data_to_munch

logger = logging.getLogger(__name__)


HEADERS = {
    'Accept-Encoding': 'gzip, deflate, br',
    'Accept-Language': 'en-US,en;q=0.8,vi;q=0.6',
    'User-Agent': "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/61.0.3163.79 "
                  "Safari/537.36",
    'Accept': '*/*',
    'Connection': 'keep-alive',
    'X-Requested-With': 'XMLHttpRequest',
    'Host': 'www.kuaidi100.com',
    'Referer': 'https://www.kuaidi100.com/'
}

GUESS = 'http://m.kuaidi100.com/autonumber/auto?{0}'
QUERY = 'http://m.kuaidi100.com/query?{0}'


def get_status(code):
    com_code_url = 'https://www.kuaidi100.com/autonumber/autoComNum?text=' + code
    headers = {'Origin': "https://www.kuaidi100.com",
               'Accept-Encoding': "gzip, deflate, br",
               'Accept-Language': "en-US,en;q=0.8,zh-CN;q=0.6,zh;q=0.4,ja;q=0.2,zh-TW;q=0.2,uz;q=0.2,vi;q=0.2",
               "User-Agent": "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36"
               "(KHTML, like Gecko) Chrome/59.0.3071.115 Safari/537.36"}
    c_r = requests.post(com_code_url, headers=headers)
    c_r = json.loads(c_r.text)
    try:
        com_code = c_r['auto'][0]['comCode']
        get_url = 'https://www.kuaidi100.com/query?type=' + com_code + '&postid=' + code +\
                  '&id=1&valicode=&temp=0.21830105590577142'
        req = requests.post(get_url, headers=headers)
        mres = parse_data_to_munch(req.text)
        if mres.message == 'ok':
            return mres
        else:
            return None
    except Exception as e:
        logger.info(e)
        return None


# def get_data_from_kuaidi(code: str, quite: bool=False) -> any:
#     params = urlencode({'num': code})
#     guess_url = GUESS.format(params)
#
#     res = json.loads(urlopen(guess_url).read().decode('utf-8'))
#     possible_company_name = [company['comCode'] for company in res]
#
#     if not quite:
#         print('Possible company:', ', '.join(possible_company_name))
#
#     for company_name in possible_company_name:
#         if not quite:
#             print('Try', company_name, '...', end='')
#
#         params = urlencode({
#             'type': company_name,
#             'postid': code,
#             'id': 1,
#             'valicode': '',
#             'temp': random.random()
#         })
#
#         req = requests.get(QUERY.format(params), headers={'Referer': guess_url})
#         print("+++++++++++++++++++++++++++++++++++")
#         print(req.text)

