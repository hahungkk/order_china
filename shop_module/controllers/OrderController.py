import json
import os
import logging
import decimal
from urllib.parse import urlparse, parse_qs

from django.utils.text import slugify
from django.conf import settings
from system_configure.controllers import SystemConfigureController
from ..models import OrderItem, Status, Currency, CartItem, Product, Category, ProductOption, Vendor
from user_module.models import UserProfile
from ..api.Serializer import FullCartItemSerializer
from ..enums import VipStatus
import requests
from lxml import html


def get_default_order_status():
    status = Status.objects.filter(default=True, is_orderstatus=True).first()
    if not status:
        status = Status.objects.all().first()
    return status


def get_default_category_parent():
    cat, created = Category.objects.get_or_create(code='', defaults={
        'code': '',
        'label': 'uncategorized'
    })
    return cat


def save_cartitem_in_session(request):
    data_list = [request.session[key] for key, value in list(request.session.items()) if 'cartItem_' in key]
    if data_list:
        list_slz = FullCartItemSerializer(data=data_list, many=True)
        list_slz.is_valid(raise_exception=True)
        logging.info(list_slz.data)
        list_slz.save(customer=request.user.username)
        [request.session.pop(key) for key, value in list(request.session.items()) if 'cartItem_' in key]


def update_or_create_new_product_from_cartitem(cart_item):
    if cart_item.product_id > 0:
        try:
            product = Product.objects.get(id=cart_item.product_id)
        except Product.DoesNotExist:
            logging.debug("Some one submit order with cartItem.product_id does not exist")
        else:
            cart_item.name = product.name
            cart_item.price = product.price
            cart_item.currency = product.currency
            cart_item.shipping = product.shipping
            cart_item.weight = product.weight
            cart_item.shopping_domain = product.shopping_domain
            cart_item.sku = product.sku
            cart_item.vendor = product.vendor
            return product

    category_list = []
    for cat in cart_item.category_list.split(','):
        cat_code = cat.strip()
        category, created = Category.objects.get_or_create(code=cat_code, defaults={
            'code': cat_code, 'label': cat_code, 'parent': get_default_category_parent()
        })
        category_list.append(category.pk)

    try:
        option_data = json.loads(cart_item.options_selected)
    except Exception as e:
        logging.error(e)
        option_data = {}

    product_option_list = []
    if isinstance(option_data, dict):
        for opt_name in option_data:
            try:
                data = {
                    'name': opt_name.strip(),
                    'value': option_data[opt_name].strip(),
                    'label': '',
                    'translated': ''
                }
            except Exception as e:
                logging.error(e)
                continue
            option, created = ProductOption.objects.get_or_create(name=data['name'], value=data['value'], defaults=data)
            product_option_list.append(option.pk)

    vendor, created = Vendor.objects.get_or_create(name=cart_item.vendor.strip(),
                                                   shopping_domain=cart_item.shopping_domain,
                                                   defaults={'name': cart_item.vendor.strip(),
                                                             'shopping_domain': cart_item.shopping_domain}
                                                   )

    slug_string = slugify(cart_item.name + cart_item.sku)

    if len(cart_item.html) > 255:
        html_cache_path = slug_string + '.html'
        with open(os.path.join(settings.HTML_CACHE_FOLDER, html_cache_path), 'w') as f:
            f.write(cart_item.html.encode('utf-8', 'ignore'))
    else:
        html_cache_path = None

    cutted_slug_string = (slug_string[:250] + '..') if len(slug_string) > 255 else slug_string

    try:
        product, created = Product.objects.update_or_create(sku=cart_item.sku, defaults={
            'slug': cutted_slug_string,
            'shopping_domain': cart_item.shopping_domain,
            'vendor': vendor,
            'detail_url': cart_item.detail_url,
            'name': cart_item.name,
            'sku': cart_item.sku,
            'image_url': cart_item.image_url,
            'short_description': cart_item.short_description,
            'currency': cart_item.currency,
            'price': cart_item.price,
            'shipping': cart_item.shipping,
            'weight': cart_item.weight,
            'inventory_quantity': SystemConfigureController.get_configure('default_inventory_quantity', default=0),
            'options_metadata': cart_item.options_metadata,
            'html_cache_path': html_cache_path
        })
        if category_list:
            product.categories.clear()
            product.categories.add(*category_list)
        if product_option_list:
            product.options_selected.clear()
            product.options_selected.add(*product_option_list)

        return product
    except Exception as e:
        logging.error(e)
        return None


def calculate_service_charge2(total_item_cost, price, quantity, insurance, fast_order, bargain, customer=None):

    """
        please note that all item related charge here using CNY and all order related charge using VND
        special offer for large quantity item
        calculate service charge using ratio tier
    """
    insurance_service_charge = 0
    fast_order_charge = 0
    five_dot = decimal.Decimal(0.05)
    # 5% service charge
    bargain_charge_ratio, insurance_charge_ratio, fast_order_charge_ratio = five_dot, five_dot, five_dot
    sale_ratio = 0
    bargain_charge = 0
    vip_reduce_charge_ratio = 0
    sale = SystemConfigureController.get_public_config('khuyenmai')
    if sale:
        sale_ratio = decimal.Decimal(int(sale.value)/100)
        if int(sale.value) >= 100:
            sale_ratio = decimal.Decimal(1)
    if fast_order:
        fast_order_charge = (price * fast_order_charge_ratio) * quantity
    if bargain:
        bargain_charge = (price * bargain_charge_ratio) * quantity
    if insurance:
        insurance_service_charge = (price * insurance_charge_ratio) * quantity

    if customer:
        if customer.customerProfile.vip_status == VipStatus.VIP1:
            vip_reduce_charge_ratio = 0.05  # 5% service charge reduce
        if customer.customerProfile.vip_status == VipStatus.VIP2:
            vip_reduce_charge_ratio = 0.10  # 10% service charge reduce
        if customer.customerProfile.vip_status == VipStatus.VIP3:
            vip_reduce_charge_ratio = 0.20  # 20% service charge reduce

    if quantity >= 50:
        charge = 0
        if sale:
            charge = charge - charge * sale_ratio
        return charge, insurance_service_charge, fast_order_charge, bargain_charge

    free_by_date = SystemConfigureController.get_public_config('free')
    if free_by_date:
        value = decimal.Decimal(free_by_date.value)
        if 0 < value <= total_item_cost:
            return 0, insurance_service_charge, fast_order_charge, bargain_charge

    # calculate service charge by price
    elif price < 12:  # item price in CNY
        service_charge_ratio = 0.1  # 10% service charge
        charge = price * decimal.Decimal(service_charge_ratio)
        charge = charge * quantity
        fcharge = charge - (charge * decimal.Decimal(vip_reduce_charge_ratio))
        if sale:
            fcharge = fcharge - fcharge * sale_ratio
        return fcharge, insurance_service_charge, fast_order_charge, bargain_charge

    elif price >= 350:  # item price in CNY
        service_charge_ratio = 0.05  # 5% service charge
        if customer:
            if customer.customerProfile.vip_status == VipStatus.VIP1:
                service_charge_ratio = 0.040  # 4.0% service charge
            if customer.customerProfile.vip_status == VipStatus.VIP2:
                service_charge_ratio = 0.035  # 3.5% service charge
            if customer.customerProfile.vip_status == VipStatus.VIP3:
                service_charge_ratio = 0.030  # 3.0% service charge
        charge = price * decimal.Decimal(service_charge_ratio)
        charge = charge * quantity
        if sale:
            charge = charge - charge * sale_ratio
        return charge, insurance_service_charge, fast_order_charge, bargain_charge

    elif 12 <= price < 350:  # item price in CNY
        # charge by quantity using CNY but the next step currency is VND
        service_charge_quantity = 1.5
        # big hand here because missing context
        if decimal.Decimal(1000000) <= total_item_cost <= decimal.Decimal(10000000):
            service_charge_quantity = 1.2
        elif decimal.Decimal(10000000) <= total_item_cost <= decimal.Decimal(20000000):
            service_charge_quantity = 1.0
        elif decimal.Decimal(20000000) <= total_item_cost <= decimal.Decimal(60000000):
            service_charge_quantity = 0.5
        elif total_item_cost > decimal.Decimal(60000000):
            return 0, insurance_service_charge, fast_order_charge, bargain_charge

        charge = quantity * decimal.Decimal(service_charge_quantity)
        fcharge = charge - (charge * decimal.Decimal(vip_reduce_charge_ratio))
        if sale:
            fcharge = fcharge - fcharge * sale_ratio
        return fcharge, insurance_service_charge, fast_order_charge, bargain_charge


def calculate_sale_serivce_charge(service_charge, sale):
    sale_ratio = decimal.Decimal(int(sale.value) / 100)
    if int(sale.value) >= 100:
        sale_ratio = decimal.Decimal(1)
    charge = service_charge - service_charge * sale_ratio
    return charge


def build_orderitem_from_cart(order, vip_status):
    order_item_list = []

    sum_item_cost = 0
    sum_item_shipping = 0
    cur = Currency.objects.get(pk='CNY')
    for cartItem in CartItem.objects.filter(customer=order.customer.username, checked=True):
        cartItem.currency = cur
        # convert cartitem to orderitem
        item = OrderItem()
        # remove prodcut model because do not use yet
        # item.product = update_or_create_new_product_from_cartitem(cartItem)
        item.name = cartItem.name
        item.item_url = cartItem.detail_url
        item.vendor = cartItem.vendor
        item.currency = cartItem.currency
        item.exchange_rate = cartItem.currency.exchange_rate
        item.options_selected = cartItem.options_selected
        item.status = order.status  # default to order status
        item.quantity = cartItem.quantity
        item.price = cartItem.price
        item.shipping = cartItem.shipping
        # default to item_url
        item.order_item_url = cartItem.detail_url
        item.image_url = cartItem.image_url

        item.order_quantity = 0
        item.paid_quantity = None
        item.order_price = cartItem.price  # default to price
        item.paid_price = None
        item.order_shipping = cartItem.shipping  # default to shipping
        item.paid_shipping = None
        item.fast_order = cartItem.fast_order
        item.weight = cartItem.weight
        item.fragile = cartItem.fragile
        item.insurance = cartItem.insurance
        item.bargain = cartItem.bargain
        item.customer_note = cartItem.note
        item.http_referer = cartItem.http_referer

        # forein key
        item.order = order
        order_item_list.append(item)

        sum_item_cost += item.price * item.quantity * item.exchange_rate
        sum_item_shipping += item.shipping * item.exchange_rate

    total_item_cost = sum_item_cost + sum_item_shipping

    # customer is order.customer
    for item in order_item_list:
        item.service_charge, item.insurance_charge, item.fast_order_charge, item.bargain_charge = calculate_service_charge2(
            total_item_cost, item.price, item.quantity,
            item.insurance, item.fast_order, item.bargain, order.customer)

    # add item to order
    # print(order_item_list)
    for item in order_item_list:
        item.save()
        order.orderitem_set.add(item)
    # clear cart
    CartItem.objects.filter(customer=order.customer.username, checked=True).delete()
    # clear who created this order because its created automatically
    order.created_by = None
    order.modified_by = None
    order.save()

    # Auto save info to profiles
    changed = False
    customer_profile = order.customer.customerProfile
    if not customer_profile.city:
        customer_profile.city = order.city
        changed = True
    if not customer_profile.district:
        customer_profile.district = order.district
        changed = True
    if not customer_profile.street:
        customer_profile.street = order.street
        changed = True
    if changed:
        customer_profile.save()

    changed = False
    user_profile = order.customer.profile
    if not user_profile.address:
        user_profile.address = order.delivery_address
        changed = True
    if not user_profile.full_name:
        user_profile.full_name = order.receiver_name
        changed = True
    if not user_profile.phone_number:
        user_profile.phone_number = order.receiver_phone
        changed = True
    if changed:
        user_profile.save()


def calculate_order_cost_by_weight(order):
    weight = order.order_weight
    int_weight = int(order.order_weight)
    if weight == int_weight:
        pass
    elif weight < int_weight + 0.5:
        weight = int_weight + 0.5
    elif weight > int_weight + 0.5:
        weight = int_weight + 1
    if weight <= 20:
        return weight * 25000
    elif 20 < weight < 40:
        return weight * 20000
    elif 40 < weight < 60:
        return weight * 19000
    elif 60 < weight < 100:
        return weight * 18000
    elif weight > 100:
        return weight * 17000


def update_facebook_name_from_order(facebook_name, user):
    update_facebook_user = UserProfile.objects.filter(user_id=user.id)
    if update_facebook_user:
        update_facebook_user[0].user_facebook = facebook_name
        update_facebook_user[0].save()
        # logging.debug("============= SAVED ==============")
    else:
        logging.debug("can not find any userprofile")


HEADERS = {
    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) '
                  'Chrome/70.0.3538.77 Safari/537.36',
}


def get_sku(url):
    par = urlparse(url)
    return parse_qs(par.query).get('id')[0]


def taobao(item):
    """ https://item.taobao.com/item.htm?spm=a21bo.7929913.198967.15.c31f4174yjRU1U&id=564064938812 """
    item.sku = get_sku(item.detail_url)
    if 'm.intl.taobao.com' in item.detail_url:
        url = 'https://item.taobao.com/item.htm?id='
        item.detail_url = url + item.sku
    response = requests.get(url=item.detail_url, headers=HEADERS)
    tree = html.fromstring(response.text)
    item.shopping_domain = 'Taobao'
    try:
        name = tree.xpath('//h3[@class="tb-main-title"]/text()')[0].strip()
    except Exception as e:
        print(e)
        name = 'chua lay duoc ten'
    item.name = name
    try:
        image_url = tree.xpath('//img[@id="J_ImgBooth"]/@src')[0].strip()
    except Exception as e:
        print(e)
        image_url = None
    item.image_url = image_url
    return item


def tmall(item):
    """ https://item.taobao.com/item.htm?spm=a21bo.7929913.198967.15.c31f4174yjRU1U&id=564064938812 """
    item.sku = get_sku(item.detail_url)
    response = requests.get(url=item.detail_url, headers=HEADERS)
    tree = html.fromstring(response.text)
    item.shopping_domain = 'tmall'
    try:
        name = tree.xpath('//div[@class="tb-detail-hd"]/h1/text()')[0].strip()
    except Exception as e:
        print(e)
        name = 'chua lay duoc ten'
    item.name = name
    try:
        image_url = tree.xpath('//img[@id="J_ImgBooth"]/@src')[0].strip()
    except Exception as e:
        print(e)
        image_url = None
    item.image_url = image_url
    return item


def other1688(item):
    """ https://detail.1688.com/offer/581659697964.html?spm=a2604.8117111.ji00a0ac.3.55a53ea7OcsPj9 """
    # reg = r'(\d+)\.html'
    item.sku = None
    response = requests.get(url=item.detail_url, headers=HEADERS)
    tree = html.fromstring(response.text)
    item.name = tree.xpath('//h1[@class="d-title"]/text()')[0].strip()
    item.shopping_domain = '1688'
    item.image_url = tree.xpath('//a[@class="box-img"]/img/@src')[0].strip()
    return item


def build_item_manual(item):
    if 'taobao.com' in item.detail_url:
        item = taobao(item)
    elif 'tmall' in item.detail_url:
        item = tmall(item)
    elif '1688.com' in item.detail_url and 'm.1688.com' not in item.detail_url:
        item = other1688(item)
    else:
        item.sku = ''
        item.name = ''
        item.shopping_domain = 'other'
        item.image_url = ''
    return item

