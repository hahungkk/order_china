#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  PaymentController.py
#
#
#  Created by TVA on 11/4/15.
#  Copyright (c) 2015 order_china. All rights reserved.
from django.conf import settings
from django.db import transaction, IntegrityError
from ..models import Transaction, Currency
from . import BalanceController


def get_default_currency():
    currency = None
    main_currency_code = getattr(settings, 'MAIN_CURRENCY_CODE')
    if main_currency_code:
        currency, created = Currency.objects.get_or_create(code=main_currency_code, defaults=dict(
            code=main_currency_code, label=main_currency_code, exchange_rate=1
        ))
    if not currency:
        currency = Currency.objects.filter(exchange_rate=1).first()
    if not currency:
        currency = currency.objects.all().first()

    return currency


def atomic_transaction_for_payment(payment, from_balance, to_balance, currency, amount, exchange_rate, detail, user):
    """ Make transaction atomically

    :param payment:
    :param from_balance:
    :param to_balance:
    :param currency:
    :param amount:
    :param exchange_rate:
    :param detail:
    :param user:
    :return:
    """
    common_amount = currency.convert(amount, exchange_rate)
    if not exchange_rate:
        exchange_rate = currency.exchange_rate
    withdraw_amount = 0
    deposit_amount = 0
    if from_balance and to_balance:
        if from_balance.id == to_balance.id:
            raise IntegrityError(
                "PaymentController.atomic_transaction_for_payment: from_balance and to_balance must be different")

    if from_balance:
        if from_balance.currency == currency:
            withdraw_amount = amount
        else:
            withdraw_amount = from_balance.currency.invert(common_amount)

    if to_balance:
        if to_balance.currency == currency:
            deposit_amount = amount
        else:
            deposit_amount = to_balance.currency.invert(common_amount)

    with transaction.atomic():
        if from_balance and to_balance:
            from_balance, to_balance = BalanceController.transferBalance(from_balance.id, to_balance.id,
                                                                         withdraw_amount, deposit_amount)
        else:
            if from_balance:
                from_balance = BalanceController.chargeBalance(from_balance.id, -withdraw_amount)
            elif to_balance:
                to_balance = BalanceController.chargeBalance(to_balance.id, deposit_amount)
            else:
                raise IntegrityError(
                    "PaymentController.atomic_transaction_for_payment:\
                     from_balance and to_balance can not both be null")

        trans = Transaction(from_balance=from_balance,
                            to_balance=to_balance,
                            from_balance_history=str(from_balance) if from_balance else "",
                            to_balance_history=str(to_balance) if to_balance else "",
                            currency=currency,
                            exchange_rate=exchange_rate,
                            amount=amount,
                            detail=detail,
                            cashier=user
                            )
        trans.save()
        if payment:
            payment.transaction = trans
            payment.save()
