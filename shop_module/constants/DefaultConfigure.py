service_charge_data = {
    'www.amazon.com': {
        'service_charge_ratio': 0.05,
        'insurance_cost_ratio': 0.03,
        'USD': {
            'service_charge_by_quantity': 0,
        }
    },
    'default': {
        'service_charge_ratio': 0.02,
        'insurance_cost_ratio': 0.05,
        'CNY': {
            'service_charge_by_quantity': 1.5,
        }
    }
}
