#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  __init__.py
#  
#
#  Created by TVA on 2/27/16.
#  Copyright (c) 2016 order_china. All rights reserved.
#
from .DefaultConfigure import *
from .DefaultSettings import *

STATUS_COLOR_MAPPING = {
    'ShipToVN': 'info',
    'ShipToCN': 'amber',
    'Ordered': 'primary',
    'Failed': 'danger',
    'Completed': 'success',
    'WaitingForPrePaid': 'warning',
}
