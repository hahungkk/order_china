import decimal
import json
import logging
import re
import pytz
from audit_log.models.fields import CreatingUserField, LastUserField
from concurrency.fields import IntegerVersionField
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator, MaxValueValidator
from django.db import models
from django.db.models import Sum, F, Count
from django.utils.html import format_html
from django.utils.translation import ugettext_lazy as _
from .shop_models import Currency
from .customer_models import CustomerProfile
from ..constants.DefaultSettings import *
from ..enums import *
import datetime
from simple_history.models import HistoricalRecords
from .tracking_models import ShipmentPack


TZ = pytz.timezone('Asia/Ho_Chi_Minh')

CALCULATE_DATE = datetime.datetime.now().replace(month=8, day=1, hour=00, minute=00)


class Order(models.Model):
    class Meta:
        verbose_name = _("Order")
        verbose_name_plural = _("Order")

    version = IntegerVersionField()
    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"),
                                   limit_choices_to={'is_staff': True},
                                   editable=True, blank=True,
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), limit_choices_to={'is_staff': True},
                                related_name="modified_%(app_label)s_%(class)s_set")

    status = models.ForeignKey("Status", verbose_name=_("status"), limit_choices_to={'is_orderstatus': True},
                               on_delete=models.PROTECT)
    customer = models.ForeignKey(User,
                                 verbose_name=_("customer"),
                                 related_name="order_set",
                                 on_delete=models.PROTECT)
    customer_note = models.TextField(verbose_name=_("customer_note"), blank=True)
    note = models.TextField(verbose_name=_("note"), blank=True)
    delivery_method = models.PositiveSmallIntegerField(verbose_name=_("delivery_method"),
                                                       choices=DeliveryMethod.ChoiceList(),
                                                       default=DeliveryMethod.store_pickup, db_index=True)
    delivery_address = models.CharField(verbose_name=_("delivery_address"), blank=True, max_length=512,
                                        db_index=True)
    city = models.CharField(verbose_name=_("city"), blank=True, max_length=512, db_index=True)
    district = models.CharField(verbose_name=_("district"), blank=True, max_length=512, db_index=True)
    street = models.CharField(verbose_name=_("street"), blank=True, max_length=512, db_index=True)
    receiver_name = models.CharField(verbose_name=_("receiver_name"), blank=True, max_length=512,
                                     db_index=True)
    receiver_phone = models.CharField(verbose_name=_("receiver_phone"), blank=True, max_length=512,
                                      db_index=True)

    prepaid_percent = models.PositiveSmallIntegerField(verbose_name=_("prepaid_percent"), default=0,
                                                       db_index=True, validators=[MinValueValidator(0),
                                                       MaxValueValidator(100)])
    currency = models.ForeignKey("Currency", verbose_name=_("currency"), on_delete=models.PROTECT)
    exchange_rate = models.DecimalField(verbose_name=_("exchange_rate"), default=decimal.Decimal(0),
                                        max_digits=XRATE_MAX_DIGITS, decimal_places=XRATE_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)])
    extra_charge = models.DecimalField(verbose_name=_("extra_charge"), default=decimal.Decimal(0),
                                       max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                       validators=[MinValueValidator(0)], db_index=True)
    tax_cost = models.DecimalField(verbose_name=_("tax_cost"), default=decimal.Decimal(0),
                                   max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                   validators=[MinValueValidator(0)], db_index=True)
    order_weight = models.DecimalField(verbose_name=_("order_weight"), default=decimal.Decimal(0),
                                       max_digits=9, decimal_places=2)
    cost_by_weight = models.DecimalField(verbose_name=_("cost_by_weight"), default=decimal.Decimal(0),
                                         max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                         validators=[MinValueValidator(0)], db_index=True)
    cost_per_weight = models.DecimalField(verbose_name=_("cost_per_weight"), default=decimal.Decimal(0),
                                          max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                          validators=[MinValueValidator(0)], db_index=True)

    ordered_date = models.DateTimeField(verbose_name=_("Ordered Date"), null=True, blank=True)
    shiptovn_date = models.DateTimeField(verbose_name=_("ShipToVN Date"), null=True, blank=True)

    history = HistoricalRecords()

    def __str__(self):
        try:
            return str("#%s (%s)" % (self.id, str(self.status)))
        except Exception as e:
            print(e)
            return str("#%s" % self.id)

    @property
    def address_tag(self):
        return str("%s %s, %s, %s" % (self.delivery_address, self.street, self.district, self.city))

    @property
    def extra_charge_tag(self):
        return self.currency.display(self.extra_charge)

    @property
    def tax_cost_tag(self):
        return self.currency.display(self.tax_cost)

    def calculate_order_item(self):
        # ::type: dict
        decimal.getcontext().rounding = decimal.ROUND_UP
        result = self.orderitem_set.all().exclude(status__logic_step=LogicStep.failed).aggregate(
            _count_item=Count(F('id')),
            _sum_item_cost=Sum(F('price') * F('quantity') * F('exchange_rate')),
            _sum_item_cost_orig=Sum(F('price') * F('quantity'), output_field=models.DecimalField()),
            _sum_item_shipping=Sum(F('shipping') * F('exchange_rate')),
            _sum_item_service_charge=Sum(F('service_charge') * F('exchange_rate')),
            _sum_bargain_charge=Sum(F('bargain_charge') * F('exchange_rate')),
            _sum_insurance_charge=Sum(F('insurance_charge') * F('exchange_rate')),
            _sum_fast_order_charge=Sum(F('fast_order_charge') * F('exchange_rate')),
            _sum_item_service_charge_orig=Sum('service_charge'),
            _sum_insurance_charge_orig=Sum('insurance_charge'),
            _sum_bargain_charge_orig=Sum('bargain_charge'),
            _sum_item_tax_cost=Sum(F('tax_cost') * F('exchange_rate')),
            _sum_item_weight=Sum(F('weight')),
        )

        for key, value in list(result.items()):
            setattr(self, key, value)

    def calculate_sum_by_currency(self):
        sum_by_currency = self.orderitem_set.all().exclude(status__logic_step=LogicStep.failed).values(
            'currency').annotate(
            sum_item_cost=Sum(F('price') * F('quantity'), output_field=models.DecimalField()),
            sum_shipping=Sum(F('shipping'), output_field=models.DecimalField()),
            sum_order_item_cost=Sum(F('order_price') * F('order_quantity'),
                                    output_field=models.DecimalField()),
            sum_order_shipping_cost=Sum(F('order_shipping'), output_field=models.DecimalField()),
            sum_service_charge=Sum(F('service_charge'), output_field=models.DecimalField()),
            sum_insurance_charge=Sum(F('insurance_charge'), output_field=models.DecimalField()),
            sum_fast_order_charge=Sum(F('fast_order_charge'), output_field=models.DecimalField()),
            sum_bargain_charge=Sum(F('bargain_charge'), output_field=models.DecimalField()),
            sum_tax_cost=Sum(F('tax_cost'), output_field=models.DecimalField()),
        )
        try:
            for sum_cur in sum_by_currency:
                sum_cur['sum_total'] = sum_cur['sum_item_cost'] + \
                                       sum_cur['sum_shipping'] + \
                                       sum_cur['sum_service_charge'] + \
                                       sum_cur['sum_insurance_charge'] + \
                                       sum_cur['sum_fast_order_charge'] + \
                                       sum_cur['sum_bargain_charge'] + \
                                       sum_cur['sum_tax_cost']
                sum_cur['sum_order_cost'] = sum_cur['sum_order_item_cost'] + sum_cur['sum_order_shipping_cost']
        except Exception as e:
            logging.debug('SQL annotate error: %s' % e)
            self._sum_by_currency = []
        else:
            self._sum_by_currency = sum_by_currency

    @property
    def count_item(self):
        if not hasattr(self, '_count_item'):
            self.calculate_order_item()
        try:
            return int(self._count_item)
        except Exception as e:
            print(e)
            return 0

    @property
    def sum_item_cost(self):
        if not hasattr(self, '_sum_item_cost'):
            self.calculate_order_item()
        return Currency.do_invert(self._sum_item_cost, self.exchange_rate)

    @property
    def sum_item_shipping(self):
        if not hasattr(self, '_sum_item_shipping'):
            self.calculate_order_item()
        return Currency.do_invert(self._sum_item_shipping, self.exchange_rate)

    @property
    def sum_item_service_charge(self):
        if not hasattr(self, '_sum_item_service_charge'):
            self.calculate_order_item()
        return Currency.do_invert(self._sum_item_service_charge, self.exchange_rate)

    @property
    def sum_item_insurance_charge(self):
        if not hasattr(self, '_sum_insurance_charge'):
            self.calculate_order_item()
        return Currency.do_invert(self._sum_insurance_charge, self.exchange_rate)

    @property
    def sum_fast_order_charge(self):
        if not hasattr(self, '_sum_fast_order_charge'):
            self.calculate_order_item()
        return Currency.do_invert(self._sum_fast_order_charge, self.exchange_rate)

    @property
    def sum_bargain_charge(self):
        if not hasattr(self, '_sum_bargain_charge'):
            self.calculate_order_item()
        return Currency.do_invert(self._sum_bargain_charge, self.exchange_rate)

    def sum_bargain_charge_tag(self):
        return self.currency.display(self.sum_bargain_charge)
    sum_bargain_charge_tag.short_description = _('Sum Bargain Charge')

    def sum_fast_order_charge_tag(self):
        return self.currency.display(self.sum_fast_order_charge)
    sum_fast_order_charge_tag.short_description = _('Fast Order Charge')

    @property
    def sum_item_tax_cost(self):
        if not hasattr(self, '_sum_item_tax_cost'):
            self.calculate_order_item()
        return Currency.do_invert(self._sum_item_tax_cost, self.exchange_rate)

    @property
    def sum_by_currency(self):
        if not hasattr(self, '_sum_by_currency'):
            self.calculate_sum_by_currency()
        return self._sum_by_currency

    def count_item_tag(self):
        return str(self.count_item)
    count_item_tag.short_description = _("Count Item")

    @property
    def sum_cost_by_weight(self):
        return self.order_weight * (self.cost_per_weight * 1000)

    def sum_cost_by_weight_tag(self):
        return self.currency.display(self.order_weight * (self.cost_per_weight * 1000))
    sum_cost_by_weight_tag.short_description = _('Sum Cost By Weight')

    def sum_item_cost_tag(self):
        return self.currency.display(self.sum_item_cost)
    sum_item_cost_tag.short_description = _('Sum item cost')

    def sum_item_shipping_tag(self):
        return self.currency.display(self.sum_item_shipping)
    sum_item_shipping_tag.short_description = _('Sum item shipping')

    def sum_item_service_charge_tag(self):
        return self.currency.display(self.sum_item_service_charge)
    sum_item_service_charge_tag.short_description = _('Sum item service')

    def sum_item_service_charge_orig_tag(self):
        return str(self._sum_item_service_charge_orig) + ' CNY'
    sum_item_service_charge_orig_tag.short_description = _('Sum item service origin')

    @property
    def insurance_charge_property(self):
        if not hasattr(self, '_sum_insurance_charge'):
            self.calculate_order_item()
        return self._sum_insurance_charge if self._sum_insurance_charge else 0

    def insurance_charge(self):
        return self.currency.display(self.insurance_charge_property)
    insurance_charge.short_description = _('Insurance Charge')

    @property
    def total_item_cost(self):
        return self.sum_item_cost + \
            self.sum_item_shipping + \
            self.sum_item_service_charge + \
            self.insurance_charge_property + \
            self.sum_item_tax_cost + \
            self.sum_fast_order_charge + \
            self.sum_bargain_charge

    def total_item_cost_tag(self):
        return self.currency.display(self.total_item_cost)
    total_item_cost_tag.short_description = _('Total item cost')

    @property
    def total_cost(self):
        return self.total_item_cost + self.extra_charge

    def total_cost_tag(self):
        return self.currency.display(self.total_cost)
    total_cost_tag.short_description = _('Total cost')

    @property
    def order_vip_status(self):
        customer = CustomerProfile.objects.get(user_id=self.customer.id)
        return customer.vip_status

    @property
    def reduce_cost(self):
        if self.created < CALCULATE_DATE.replace(tzinfo=self.created.tzinfo):
            return 0
        extras = self.tax_cost + self.sum_cost_by_weight
        if self.order_vip_status == 0:
            return 0
        elif self.order_vip_status == 1:
            return extras * 5 / 100
        elif self.order_vip_status == 2:
            return extras * 10 / 100
        return extras * 20 / 100

    @property
    def extras(self):
        extras = self.tax_cost + self.sum_cost_by_weight
        return extras - self.reduce_cost

    @property
    def grant_total(self):
        # todo: calculate ShipmentPackage shipping_cost
        return self.total_cost + self.extras

    @property
    def prepaid(self):
        return self.total_cost * self.prepaid_percent / 100

    def prepaid_tag(self):
        return self.currency.display(self.prepaid)
    prepaid_tag.short_description = _('Prepaid')

    def redude_cost_tag(self):
        return ' -' + self.currency.display(self.reduce_cost)
    redude_cost_tag.short_description = _('VIP')

    def grant_total_tag(self):
        return self.currency.display(self.grant_total)
    grant_total_tag.short_description = _('Grant total')

    @property
    def order_cost(self):
        result = 0
        for sum_cur in self.sum_by_currency:
            result += sum_cur['sum_order_cost']
        return result

    @property
    def latest_transaction(self):
        result = self.payment.first()
        return result.created if result else None

    def latest_transaction_date(self):
        return self.latest_transaction
    latest_transaction_date.short_description = _('Transaction Date')

    def latest_transaction_export(self):
        if self.latest_transaction:
            return self.latest_transaction.strftime('%d/%m/%Y')
        return None

    latest_transaction_date.short_description = _('Transaction Date')

    def order_cost_tag(self):
        result = ''
        for sum_cur in self.sum_by_currency:
            result += '%s%s ' % (sum_cur['sum_order_cost'], sum_cur['currency'])
        return result
    order_cost_tag.short_description = _('Order Cost')

    def sum_total(self):
        sum_total = 0
        for sum_cur in self.sum_by_currency:
            sum_total += sum_cur['sum_total']
        return sum_total

    def calculate_payments(self):
        result = self.payment.all()
        total_amount_transaction = 0
        for r in result:
            amount_t = r.transaction.amount * r.transaction.exchange_rate
            if r.transaction.from_balance:
                total_amount_transaction -= amount_t
            else:
                total_amount_transaction += amount_t
        setattr(self, '_sum_payment_transaction', total_amount_transaction)
        setattr(self, '_count_payment', result.count())

    def ship_to_vn(self):
        return str(self.orderitem_set.filter(status__value__in=['ShipToVN', 'Completed']).count()) \
               + '/' +\
               str(self.orderitem_set.all().exclude(status__value='Failed').count())
    ship_to_vn.short_description = _("ShipToVN")

    @property
    def count_payment(self):
        if not hasattr(self, '_count_payment'):
            self.calculate_payments()
        return self._count_payment

    @property
    def sum_payment_transaction(self):
        if not hasattr(self, '_sum_payment_transaction'):
            self.calculate_payments()
        return Currency.do_invert(self._sum_payment_transaction, self.exchange_rate)

    def sum_payment_transaction_tag(self):
        return self.currency.display(self.sum_payment_transaction)
    sum_payment_transaction_tag.short_description = _('Payment total')

    @property
    def payment_left(self):
        return self.grant_total - self.sum_payment_transaction

    def payment_left_tag(self):
        return self.currency.display(self.payment_left)
    payment_left_tag.short_description = _('Payment left')

    def user_facebook_tag(self):
        user_profile = self.customer.profile
        if user_profile.user_facebook:
            return user_profile.user_facebook
        return None
    user_facebook_tag.short_description = _('Facebook Address')

    def phone_tag(self):
        return self.customer.profile.phone_number
    phone_tag.short_description = _('Phone Number')

    @property
    def calculate_order_weight(self):
        list_item_tracking = []
        for item in self.orderitem_set.all():
            list_item_tracking.append(item.shipment_pack)
        list_track = ShipmentPack.objects.filter(tracking_code__in=list_item_tracking)
        sum_weight = list_track.aggregate(weight=Sum('weight'))
        return sum_weight['weight']

    def order_weight_by_track(self):
        return f'{self.calculate_order_weight} Kg'
    order_weight_by_track.short_description = _('Order Weight By Tracking')


class OrderItem(models.Model):
    class Meta:
        verbose_name = _("OrderItem")
        verbose_name_plural = _("OrderItem")

    version = IntegerVersionField()
    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"),
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"),
                                related_name="modified_%(app_label)s_%(class)s_set")

    order = models.ForeignKey("Order", verbose_name=_("order"), on_delete=models.CASCADE)
    shopping_domain = models.CharField(verbose_name=_("shopping_domain"), blank=True, max_length=255,
                                       db_index=True)
    vendor = models.CharField(verbose_name=_("vendor"), blank=True, max_length=255, db_index=True)
    name = models.CharField(verbose_name=_("name"), blank=True, max_length=512, db_index=True)
    image_url = models.CharField(verbose_name=_("image_url"), max_length=2048, blank=True)
    image_url_origin = models.CharField(verbose_name=_("image_url_origin"), max_length=2048, blank=True, null=True)
    item_url = models.CharField(verbose_name=_("item_url"), blank=True, max_length=2048)
    currency = models.ForeignKey("Currency", verbose_name=_("currency"), blank=True, null=True,
                                 on_delete=models.PROTECT)
    exchange_rate = models.DecimalField(verbose_name=_("exchange_rate"), default=decimal.Decimal(0),
                                        max_digits=XRATE_MAX_DIGITS, decimal_places=XRATE_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)])
    options_selected = models.CharField(verbose_name=_("options_selected"), max_length=512, blank=True)
    status = models.ForeignKey("Status", verbose_name=_("status"), limit_choices_to={'is_orderstatus': True},
                               on_delete=models.PROTECT)

    quantity = models.PositiveIntegerField(verbose_name=_("customer_quantity"), default=1, db_index=True)
    checked_quantity = models.PositiveIntegerField(verbose_name=_("checked_quantity"), default=0, db_index=True)
    price = models.DecimalField(verbose_name=_("customer_price"), default=decimal.Decimal(0),
                                max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                validators=[MinValueValidator(0)], db_index=True)
    shipping = models.DecimalField(verbose_name=_("customer_shipping"), default=decimal.Decimal(0),
                                   max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                   validators=[MinValueValidator(0)], db_index=True)

    order_item_url = models.CharField(verbose_name=_("order_item_url"), blank=True, max_length=2048)

    order_quantity = models.PositiveIntegerField(verbose_name=_("order_quantity"), default=0, db_index=True)
    paid_quantity = models.PositiveIntegerField(verbose_name=_("paid_quantity"), null=True,
                                                blank=True, default=None, db_index=True)
    order_price = models.DecimalField(verbose_name=_("order_price"), default=decimal.Decimal(0),
                                      max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                      validators=[MinValueValidator(0)], db_index=True)
    paid_price = models.DecimalField(verbose_name=_("paid_price"), null=True, blank=True, default=None,
                                     max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                     validators=[MinValueValidator(0)], db_index=True)
    order_shipping = models.DecimalField(verbose_name=_("order_shipping"), default=decimal.Decimal(0),
                                         max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                         validators=[MinValueValidator(0)], db_index=True)
    paid_shipping = models.DecimalField(verbose_name=_("paid_shipping"), null=True, blank=True, default=None,
                                        max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)], db_index=True)

    service_charge = models.DecimalField(verbose_name=_("service_charge"), default=decimal.Decimal(0),
                                         max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                         validators=[MinValueValidator(0)], db_index=True)
    tax_cost = models.DecimalField(verbose_name=_("tax_cost"), default=decimal.Decimal(0),
                                   max_digits=MONEY_MAX_DIGITS,
                                   decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)],
                                   db_index=True)
    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0),
                                 max_digits=9, decimal_places=2)
    fragile = models.BooleanField(verbose_name=_("fragile"), default=False)
    insurance = models.BooleanField(verbose_name=_("insurance"), default=False)

    customer_note = models.TextField(verbose_name=_("customer_note"), blank=True)
    note = models.TextField(verbose_name=_("note"), blank=True)

    order_employee = models.ForeignKey(User, verbose_name=_("order_employee"),
                                       limit_choices_to={'is_staff': True},
                                       null=True, blank=True, on_delete=models.SET_NULL)
    http_referer = models.CharField(verbose_name=_("http_referer"), max_length=2048, blank=True)
    shipment_pack = models.CharField(max_length=255, verbose_name=_('shipment_pack'), null=True, blank=True)
    company = models.CharField(verbose_name=_("provider"), choices=Company.ChoiceList(),
                               default=Company.taobao, db_index=True, max_length=255)
    order_code = models.CharField(max_length=255, verbose_name=_('order_code'), null=True, blank=True)

    insurance_charge = models.DecimalField(verbose_name=_("insurance_charge"), default=decimal.Decimal(0),
                                           max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                           validators=[MinValueValidator(0)], db_index=True)
    bargain = models.BooleanField(verbose_name=_("bargain"), default=False)
    bargain_charge = models.DecimalField(verbose_name=_("bargain_charge"), default=decimal.Decimal(0),
                                         max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                         validators=[MinValueValidator(0)], db_index=True)
    fast_order = models.BooleanField(verbose_name=_("fast_order"), default=False)
    fast_order_charge = models.DecimalField(verbose_name=_("fast_order_charge"), default=decimal.Decimal(0),
                                            max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                            validators=[MinValueValidator(0)], db_index=True)
    exporter = models.ForeignKey(User, verbose_name=_("exporter"), related_name="exporter_item_set",
                                 on_delete=models.PROTECT, null=True, blank=True)
    exported_date = models.DateTimeField(null=True, blank=True)

    importer = models.ForeignKey(User, verbose_name=_("importer"), related_name="importer_item_set",
                                 on_delete=models.PROTECT, null=True, blank=True)
    imported_date = models.DateTimeField(null=True, blank=True)

    history = HistoricalRecords()

    def __str__(self):
        try:
            return str("<%s> (%s) %s" % (self.pk, str(self.status), self.importer))
        except Exception as e:
            print(e)
            return str("<%s> %s" % (self.pk, self.name))

    def option_selected_tag(self):
        try:
            option_selected = json.loads(self.options_selected)
        except ValueError:
            return re.sub(r"u'", "'", re.sub(r'\s*,\s*u?', '\n', self.options_selected)).lstrip('{').rstrip('}')
        else:
            return '\n'.join([key + ':' + value for key, value in list(option_selected.items())])

    option_selected_tag.allow_tags = True
    option_selected_tag.short_description = _("Option Selected")

    def image_tag(self):
        return format_html(
            '<a href="{}" target="blank" data-toggle="tooltip" title="{}"><img src="{}" '
            'style="width:128px;height:128px;" class="toggle-enlarge"/> </a>',
            self.order_item_url if self.order_item_url else self.item_url,
            self.option_selected_tag(), self.image_url, self.image_url)

    image_tag.allow_tags = True
    image_tag.short_description = _("Image")

    def image_inline_tag(self):
        return format_html(
            '<a href="{}" target="blank" data-toggle="tooltip" title="{}"><img src="{}" '
            'style="width:500px;height:500px;" class="toggle-enlarge"/> </a>',
            self.order_item_url if self.order_item_url else self.item_url,
            self.option_selected_tag(),
            self.image_url_origin if self.image_url_origin else self.image_url,
            self.image_url_origin if self.image_url_origin else self.image_url
        )

    image_inline_tag.allow_tags = True
    image_inline_tag.short_description = _("Image")

    def item_url_tag(self):
        return format_html('<a href="{}" target="blank">{}</a>',
                           self.order_item_url if self.order_item_url else self.item_url, self.name)

    item_url_tag.allow_tags = True
    item_url_tag.short_description = _("Item url")

    def price_tag(self):
        return self.currency.display(self.price)

    price_tag.short_description = _("Price")

    def order_price_tag(self):
        return self.currency.display(self.order_price)

    order_price_tag.short_description = _("Order Price")

    @property
    def value(self):
        return self.price * self.quantity

    def value_tag(self):
        return self.currency.display(self.value)

    value_tag.short_description = _("Value")

    @property
    def total(self):
        return self.value + \
               self.shipping + \
               self.service_charge + \
               decimal.Decimal(self.tax_cost) + \
               self.fast_order_charge + \
               self.bargain_charge + \
               self.insurance_charge

    def total_tag(self):
        return self.currency.display(self.total)
    total_tag.short_description = _("Total")

    def total_order_currency_tag(self):
        value = decimal.Decimal(self.total)
        return self.order.currency.display(
            self.order.currency.invert(value * decimal.Decimal(self.exchange_rate), self.order.exchange_rate))

    total_order_currency_tag.short_description = _("Total")

    def shipvn(self):
        history = self.history.filter(status__value="ShipToVN")
        if history.exists():
            date = history.last().history_date.astimezone(TZ)
            return history.last().history_user.username + ' ' + date.strftime("%H:%M %d/%m/%Y")
        return None
    shipvn.short_description = "ShiptoVN Checker"
