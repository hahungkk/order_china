#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  logistic_models.py
#
#
#  Created by TVA on 3/28/16.
#  Copyright (c) 2016 ordercn. All rights reserved.
#


import decimal
from audit_log.models.fields import CreatingUserField, LastUserField
from concurrency.fields import IntegerVersionField
from django.contrib.auth.models import User
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from ..constants.DefaultSettings import *


class ShipmentService(models.Model):
    class Meta:
        verbose_name = _("ShipmentService")
        verbose_name_plural = _("ShipmentService")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"), related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), related_name="modified_%(app_label)s_%(class)s_set")

    name = models.CharField(verbose_name=_("name"), max_length=512)
    label = models.CharField(verbose_name=_("label"), max_length=512)
    contact = models.TextField(verbose_name=_("contact"), blank=True)
    metadata = models.TextField(verbose_name=_("metadata"), blank=True)
    is_local = models.BooleanField(verbose_name=_("is_local"), default=False, db_index=True)

    def __str__(self):
        return str(self.label)


class ShipmentLocation(models.Model):
    class Meta:
        verbose_name = _("ShipmentLocation")
        verbose_name_plural = _("ShipmentLocation")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)

    name = models.CharField(verbose_name=_("name"), max_length=512)
    note = models.TextField(verbose_name=_("note"), blank=True)
    address = models.CharField(verbose_name=_("address"), blank=True, max_length=512)
    country = models.CharField(verbose_name=_("country"), blank=True, max_length=512)
    state = models.CharField(verbose_name=_("state"), blank=True, max_length=512)
    city = models.CharField(verbose_name=_("city"), blank=True, max_length=512)
    zipcode = models.CharField(verbose_name=_("zipcode"), blank=True, max_length=512)

    order_index = models.PositiveIntegerField(verbose_name=_("order_index"), null=True, db_index=True)

    def __str__(self):
        return str(self.name)


class ShipmentItem(models.Model):
    class Meta:
        verbose_name = _("ShipmentItem")
        verbose_name_plural = _("ShipmentItem")

    order_item = models.ForeignKey('OrderItem',
                                   verbose_name=_("order_item"), on_delete=models.CASCADE)
    shipment_package = models.ForeignKey('ShipmentPackage',
                                         verbose_name=_("shipment_package"), on_delete=models.CASCADE)
    quantity = models.PositiveIntegerField(verbose_name=_("quantity"), default=0, db_index=True)
    arrived_quantity = models.PositiveIntegerField(verbose_name=_("arrived_quantity"), null=True, blank=True,
                                                   default=None, db_index=True)

    def __str__(self):
        return "%s x %s" % (self.quantity, str(self.order_item)[:100])


class ShipmentPackage(models.Model):
    class Meta:
        verbose_name = _("ShipmentPackage")
        verbose_name_plural = _("ShipmentPackage")

    version = IntegerVersionField()
    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"), related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), related_name="modified_%(app_label)s_%(class)s_set")

    departed = models.DateTimeField(verbose_name=_("departed"), blank=True, null=True)
    arrived = models.DateTimeField(verbose_name=_("arrived"), blank=True, null=True)
    delivered = models.DateTimeField(verbose_name=_("delivered"), blank=True, null=True)

    orderitem_set = models.ManyToManyField('OrderItem', verbose_name=_("orderitem_set"), through='ShipmentItem')

    status = models.ForeignKey("Status", verbose_name=_("status"), limit_choices_to={'is_shipmentstatus': True},
                               on_delete=models.PROTECT)

    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0), max_digits=9, decimal_places=2)
    tracking_code = models.CharField(verbose_name=_("tracking_code"), blank=True, max_length=512, db_index=True)

    shipment_service = models.ForeignKey("ShipmentService", verbose_name=_("shipment_service"), null=True, blank=True,
                                         on_delete=models.PROTECT)
    shipment_method = models.CharField(verbose_name=_("shipment_method"), blank=True, max_length=512, db_index=True)

    location = models.ForeignKey("ShipmentLocation", verbose_name=_("location"), related_name='departed_packages',
                                 blank=True, null=True, on_delete=models.PROTECT)
    destination = models.ForeignKey("ShipmentLocation", verbose_name=_("destination"), related_name='arrived_packages',
                                    blank=True, null=True, on_delete=models.PROTECT)

    currency = models.ForeignKey("Currency", verbose_name=_("currency"), null=True, blank=True,
                                 on_delete=models.PROTECT)
    exchange_rate = models.DecimalField(verbose_name=_("exchange_rate"), default=decimal.Decimal(0),
                                        max_digits=XRATE_MAX_DIGITS, decimal_places=XRATE_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)])
    shipping_cost = models.DecimalField(verbose_name=_("shipping_cost"), default=decimal.Decimal(0),
                                        max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)], db_index=True)

    inventory_employee = models.ForeignKey(User, verbose_name=_("inventory_employee"),
                                           limit_choices_to={'is_staff': True}, null=True, blank=True,
                                           on_delete=models.SET_NULL)

    note = models.TextField(verbose_name=_("note"), blank=True)
    vendor_payment = models.ForeignKey("VendorPayment", verbose_name=_("vendor_payment"), null=True, blank=True,
                                       on_delete=models.SET_NULL)
    shipment_logistics = models.ForeignKey("ShipmentLogistics", verbose_name=_("shipment_logistics"), null=True,
                                           blank=True, on_delete=models.SET_NULL)
    order = models.ForeignKey("Order", verbose_name=_("order"), null=True, blank=True, on_delete=models.SET_NULL)
    customer = models.ForeignKey(User, verbose_name=_("customer"), related_name='customer_packages', null=True,
                                 blank=True, on_delete=models.SET_NULL)
    customer_payment = models.ForeignKey("CustomerPayment", verbose_name=_("customer_payment"), null=True, blank=True,
                                         on_delete=models.SET_NULL)

    def __str__(self):
        return str("[%s] (%s)" % (self.pk, str(self.status)))


class ShipmentLogistics(models.Model):
    class Meta:
        verbose_name = _("ShipmentLogistics")
        verbose_name_plural = _("ShipmentLogistics")

    version = IntegerVersionField()
    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"), related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), related_name="modified_%(app_label)s_%(class)s_set")

    departed = models.DateTimeField(verbose_name=_("departed"), blank=True, null=True)
    arrived = models.DateTimeField(verbose_name=_("arrived"), blank=True, null=True)
    delivered = models.DateTimeField(verbose_name=_("delivered"), blank=True, null=True)

    status = models.ForeignKey("Status", verbose_name=_("status"), limit_choices_to={'is_shipmentstatus': True},
                               on_delete=models.PROTECT)

    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0), max_digits=9, decimal_places=2)
    tracking_code = models.CharField(verbose_name=_("tracking_code"), blank=True, max_length=512, db_index=True)

    shipment_service = models.ForeignKey("ShipmentService", verbose_name=_("shipment_service"), null=True, blank=True,
                                         on_delete=models.PROTECT)
    shipment_method = models.CharField(verbose_name=_("shipment_method"), blank=True, max_length=512, db_index=True)

    location = models.ForeignKey("ShipmentLocation", verbose_name=_("location"), related_name='departed_container',
                                 blank=True, null=True, on_delete=models.PROTECT)
    destination = models.ForeignKey("ShipmentLocation", verbose_name=_("destination"), related_name='arrived_container',
                                    blank=True, null=True, on_delete=models.PROTECT)

    currency = models.ForeignKey("Currency", verbose_name=_("currency"), null=True, blank=True,
                                 on_delete=models.PROTECT)
    exchange_rate = models.DecimalField(verbose_name=_("exchange_rate"), default=decimal.Decimal(0),
                                        max_digits=XRATE_MAX_DIGITS, decimal_places=XRATE_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)])
    shipping_cost = models.DecimalField(verbose_name=_("shipping_cost"), default=decimal.Decimal(0),
                                        max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)], db_index=True)
    tax_cost = models.DecimalField(verbose_name=_("tax_cost"), default=decimal.Decimal(0), max_digits=MONEY_MAX_DIGITS,
                                   decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)],
                                   db_index=True)

    inventory_employee = models.ForeignKey(User, verbose_name=_("inventory_employee"),
                                           limit_choices_to={'is_staff': True}, null=True, blank=True,
                                           on_delete=models.SET_NULL)

    note = models.TextField(verbose_name=_("note"), blank=True)
    logistics_payment = models.ForeignKey("LogisticsPayment", verbose_name=_("logistics_payment"), null=True,
                                          blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return str("{%s} (%s)" % (self.pk, str(self.status)))
