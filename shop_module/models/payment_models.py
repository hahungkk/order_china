#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  payment_models.py
#
#
#  Created by TVA on 3/28/16.
#  Copyright (c) 2016 ordercn. All rights reserved.
#

import decimal
from audit_log.models.fields import CreatingUserField, LastUserField
from django.contrib.auth.models import User
from django.contrib.contenttypes.fields import GenericForeignKey
from django.contrib.contenttypes.models import ContentType
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from ..constants.DefaultSettings import *
from ..enums import *


# ================= Payment ====================

class PaymentType(models.Model):
    class Meta:
        verbose_name = _("PaymentType")
        verbose_name_plural = _("PaymentType")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    value = models.CharField(verbose_name=_("value"), max_length=255, primary_key=True, unique=True)
    label = models.CharField(verbose_name=_("label"), max_length=512)

    is_customerpayment = models.BooleanField(verbose_name=_("is_customerpayment"), default=True,
                                             db_index=True)
    is_vendorpayment = models.BooleanField(verbose_name=_("is_vendorpayment"), default=False, db_index=True)
    is_logisticspayment = models.BooleanField(verbose_name=_("is_logisticspayment"), default=False,
                                              db_index=True)

    order_index = models.PositiveIntegerField(verbose_name=_("order_index"), null=True, db_index=True)

    def __str__(self):
        return str(self.label)


def migrate_get_first_user_id():
    first_user = User.objects.all().first()
    if first_user:
        return first_user.pk
    else:
        return first_user


class CustomerPayment(models.Model):
    class Meta:
        verbose_name = _("CustomerPayment")
        verbose_name_plural = _("CustomerPayment")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"),
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"),
                                related_name="modified_%(app_label)s_%(class)s_set")

    # customer = models.ForeignKey(User, verbose_name=_("customer"), default=migrate_getFirstUserID,
    #                              on_delete=models.PROTECT)
    order = models.ForeignKey("Order", verbose_name=_("order"), null=True, blank=True,
                              on_delete=models.PROTECT, related_name='payment')
    type = models.ForeignKey("PaymentType", verbose_name=_("PaymentType"),
                             limit_choices_to={'is_customerpayment': True}, on_delete=models.PROTECT)
    transaction = models.OneToOneField("Transaction", verbose_name=_("transaction"), on_delete=models.PROTECT)
    note = models.TextField(verbose_name=_("note"), blank=True)  # payment note

    def __str__(self):
        return str(self.transaction) if self.transaction_id else ""

    def amount_tag(self):
        return self.transaction.currency.display(self.transaction.amount)

    def customer_tag(self):
        return self.order.customer.username


class VendorPayment(models.Model):
    class Meta:
        verbose_name = _("VendorPayment")
        verbose_name_plural = _("VendorPayment")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"),
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"),
                                related_name="modified_%(app_label)s_%(class)s_set")

    vendor = models.ForeignKey("Vendor", verbose_name=_("vendor"), on_delete=models.PROTECT)
    type = models.ForeignKey("PaymentType", verbose_name=_("PaymentType"),
                             limit_choices_to={'is_vendorpayment': True}, on_delete=models.PROTECT)
    transaction = models.OneToOneField("Transaction", verbose_name=_("transaction"), on_delete=models.PROTECT)
    note = models.TextField(verbose_name=_("note"), blank=True)  # payment note

    def __str__(self):
        return str(self.transaction) if self.transaction_id else ""


class LogisticsPayment(models.Model):
    class Meta:
        verbose_name = _("LogisticsPayment")
        verbose_name_plural = _("LogisticsPayment")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"),
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"),
                                related_name="modified_%(app_label)s_%(class)s_set")

    shipment_service = models.ForeignKey("ShipmentService", verbose_name=_("shipment_service"),
                                         on_delete=models.PROTECT)
    type = models.ForeignKey("PaymentType", verbose_name=_("PaymentType"),
                             limit_choices_to={'is_logisticspayment': True}, on_delete=models.PROTECT)
    transaction = models.OneToOneField("Transaction", verbose_name=_("transaction"), on_delete=models.PROTECT)
    note = models.TextField(verbose_name=_("note"), blank=True)  # payment note

    def __str__(self):
        return str(self.transaction) if self.transaction_id else ""


class GenericPayment(models.Model):
    class Meta:
        verbose_name = _("General Payment")
        verbose_name_plural = _("General Payment")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"),
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"),
                                related_name="modified_%(app_label)s_%(class)s_set")

    content_type = models.ForeignKey(ContentType, verbose_name=_("content_type"), on_delete=models.CASCADE)
    object_id = models.PositiveIntegerField(verbose_name=_("object_id"), )
    content_object = GenericForeignKey('content_type', 'object_id')

    type = models.ForeignKey("PaymentType", verbose_name=_("type"), on_delete=models.PROTECT)
    transaction = models.OneToOneField("Transaction", verbose_name=_("transaction"), on_delete=models.PROTECT)
    note = models.TextField(verbose_name=_("note"), blank=True)  # payment note

    def __str__(self):
        return str(self.transaction) if self.transaction_id else ""


class Transaction(models.Model):
    class Meta:
        verbose_name = _("Transaction")
        verbose_name_plural = _("Transaction")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    from_balance = models.ForeignKey("BalanceAccount", verbose_name=_("from_balance"),
                                     related_name='from_transactions',
                                     null=True, blank=True,
                                     on_delete=models.PROTECT)
    to_balance = models.ForeignKey("BalanceAccount", verbose_name=_("to_balance"),
                                   related_name='to_transactions', null=True, blank=True,
                                   on_delete=models.PROTECT)
    from_balance_history = models.CharField(verbose_name=_("from_balance_history"),
                                            default="", max_length=512, editable=False)
    to_balance_history = models.CharField(verbose_name=_("to_balance_history"), default="", max_length=512,
                                          editable=False)
    currency = models.ForeignKey("Currency", verbose_name=_("currency"), on_delete=models.PROTECT)
    exchange_rate = models.DecimalField(verbose_name=_("exchange_rate"), default=decimal.Decimal(0),
                                        max_digits=XRATE_MAX_DIGITS,
                                        decimal_places=XRATE_DECIMAL_PLACES,
                                        validators=[MinValueValidator(0)])
    amount = models.DecimalField(verbose_name=_("amount"), default=decimal.Decimal(0),
                                 max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                 validators=[MinValueValidator(0)], db_index=True)
    detail = models.TextField(verbose_name=_("detail"), blank=True)
    cashier = models.ForeignKey(User, verbose_name=_("cashier"),
                                limit_choices_to={'is_staff': True}, null=True,
                                blank=True, on_delete=models.PROTECT)

    def __str__(self):
        return str(_("Transaction") + " %s : %s%s" % (self.pk,
                                                      "-" if self.from_balance and not self.to_balance else "",
                                                      self.currency.display(self.amount)))


class BalanceAccount(models.Model):
    class Meta:
        verbose_name = _("BalanceAccount")
        verbose_name_plural = _("BalanceAccount")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"),
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"),
                                related_name="modified_%(app_label)s_%(class)s_set")

    name = models.CharField(verbose_name=_("name"), max_length=512)
    type = models.PositiveSmallIntegerField(verbose_name=_("type"), choices=BalanceType.ChoiceList(),
                                            default=BalanceType.bank, db_index=True)
    currency = models.ForeignKey("Currency", verbose_name=_("currency"), on_delete=models.PROTECT)
    amount = models.DecimalField(verbose_name=_("amount"), default=decimal.Decimal(0),
                                 max_digits=MONEY_MAX_DIGITS,
                                 decimal_places=MONEY_DECIMAL_PLACES,
                                 validators=[MinValueValidator(0)], db_index=True)

    account_holder = models.ForeignKey(User, verbose_name=_("account_holder"), null=True, blank=True,
                                       on_delete=models.PROTECT)
    metadata = models.TextField(verbose_name=_("metadata"), blank=True)
    info = models.TextField(verbose_name=_("info"), blank=True)

    def amount_tag(self):
        return self.currency.display(self.amount)
    amount_tag.short_description = _("Amount")

    def __str__(self):
        return self.name
