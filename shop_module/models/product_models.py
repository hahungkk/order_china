#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  product_models.py
#
#
#  Created by TVA on 3/28/16.
#  Copyright (c) 2016 ordercn. All rights reserved.
#

import decimal
from audit_log.models.fields import CreatingUserField, LastUserField
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from mptt.models import MPTTModel, TreeForeignKey

from ..constants.DefaultSettings import *


class Category(MPTTModel):
    class Meta:
        verbose_name = _("Category")
        verbose_name_plural = _("Category")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"), related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), related_name="modified_%(app_label)s_%(class)s_set")

    code = models.CharField(verbose_name=_("code"), unique=True, max_length=255)
    label = models.CharField(verbose_name=_("label"), db_index=True, max_length=255)
    metadata = models.TextField(verbose_name=_("metadata"), blank=True)

    order_index = models.PositiveIntegerField(verbose_name=_("order_index"), null=True, default=0,
                                              db_index=True)  # TODO: remove null=True

    class MPTTMeta:
        order_insertion_by = ['order_index']

    parent = TreeForeignKey('self', null=True, blank=True, related_name='children',
                            db_index=True, on_delete=models.CASCADE)

    def __str__(self):
        return str("%s" % (self.label if self.label else self.code))


class ProductOption(models.Model):
    class Meta:
        verbose_name = _("ProductOption")
        verbose_name_plural = _("ProductOption")

        unique_together = ('value', 'name')

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"), related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), related_name="modified_%(app_label)s_%(class)s_set")

    name = models.CharField(verbose_name=_("name"), max_length=255)
    value = models.CharField(verbose_name=_("value"), max_length=255)
    label = models.CharField(verbose_name=_("label"), db_index=True, blank=True, max_length=255)
    translated = models.CharField(verbose_name=_("translated"), db_index=True, blank=True, max_length=255)

    def __str__(self):
        return str('%s: %s' % (self.label if self.label else self.name,
                               self.translated if self.translated else self.value))


class Product(models.Model):
    class Meta:
        verbose_name = _("Product")
        verbose_name_plural = _("Product")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"), related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), related_name="modified_%(app_label)s_%(class)s_set")

    is_hidden = models.BooleanField(verbose_name=_("is_hidden"), default=False)
    slug = models.SlugField(verbose_name=_("slug"), max_length=255, unique=True, db_index=True)
    categories = models.ManyToManyField("Category", verbose_name=_("categories"), blank=True)

    shopping_domain = models.CharField(verbose_name=_("shopping_domain"), blank=True, max_length=255, db_index=True)
    vendor = models.ForeignKey("Vendor", verbose_name=_("vendor"), null=True, blank=True, on_delete=models.PROTECT)
    detail_url = models.CharField(verbose_name=_("detail_url"), max_length=2048)
    name = models.CharField(verbose_name=_("name"), max_length=512, db_index=True)
    sku = models.CharField(verbose_name=_("sku"), blank=True, max_length=255, unique=True, db_index=True)
    image_url = models.CharField(verbose_name=_("image_url"), max_length=2048, blank=True)
    short_description = models.TextField(verbose_name=_("short_description"), blank=True)
    html_cache_path = models.CharField(verbose_name=_("html_cache_path"), max_length=2048, blank=True, null=True)
    currency = models.ForeignKey("Currency", verbose_name=_("currency"), on_delete=models.PROTECT)
    price = models.DecimalField(verbose_name=_("price"), default=decimal.Decimal(0), max_digits=MONEY_MAX_DIGITS,
                                decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)], db_index=True)
    shipping = models.DecimalField(verbose_name=_("shipping"), default=decimal.Decimal(0), max_digits=MONEY_MAX_DIGITS,
                                   decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)],
                                   db_index=True)
    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0), max_digits=9, decimal_places=2)
    inventory_quantity = models.PositiveIntegerField(verbose_name=_("inventory_quantity"), default=0)
    options_selected = models.ManyToManyField("ProductOption", verbose_name=_("options_selected"), blank=True)
    options_metadata = models.TextField(verbose_name=_("options_metadata"), blank=True)

    def __str__(self):
        return str("%s: %s" % (self.shopping_domain, self.name))

    def image_tag(self):
        return '<img src="%s" style="width:64px;height:64px;" />' % self.image_url

    image_tag.allow_tags = True
    image_tag.short_description = _("Image")


class CartItem(models.Model):
    class Meta:
        ordering = ['-id']
        verbose_name = _("CartItem")
        verbose_name_plural = _("CartItem")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    customer = models.CharField(verbose_name=_("customer"), blank=True, null=True, max_length=255, db_index=True)
    shopping_domain = models.CharField(verbose_name=_("shopping_domain"), blank=True, max_length=255, db_index=True)
    vendor = models.CharField(verbose_name=_("vendor"), blank=True, max_length=255, db_index=True)
    detail_url = models.CharField(verbose_name=_("detail_url"), blank=True, max_length=2048)
    name = models.CharField(verbose_name=_("name"), blank=True, max_length=512, db_index=True)
    sku = models.CharField(verbose_name=_("sku"), blank=True, max_length=512, db_index=True)
    image_url = models.CharField(verbose_name=_("image_url"), blank=True, max_length=2048)
    short_description = models.TextField(verbose_name=_("short_description"), blank=True)
    currency = models.CharField(verbose_name=_("currency"), max_length=255, db_index=True)
    price = models.DecimalField(verbose_name=_("price"), default=decimal.Decimal(0), max_digits=MONEY_MAX_DIGITS,
                                decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)], db_index=True)
    shipping = models.DecimalField(verbose_name=_("shipping"), default=decimal.Decimal(0), max_digits=MONEY_MAX_DIGITS,
                                   decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)],
                                   db_index=True)
    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0), max_digits=9, decimal_places=2)
    quantity = models.PositiveIntegerField(verbose_name=_("quantity"), default=1)
    options_selected = models.TextField(verbose_name=_("options_selected"), blank=True)
    options_metadata = models.TextField(verbose_name=_("options_metadata"), blank=True)
    category_list = models.CharField(verbose_name=_("category_list"), max_length=512, blank=True)
    fragile = models.BooleanField(verbose_name=_("fragile"), default=False)
    checked = models.BooleanField(verbose_name=_("checked"), default=True)
    insurance = models.BooleanField(verbose_name=_("insurance"), default=False)
    fast_order = models.BooleanField(verbose_name=_("fast_order"), default=False)
    bargain = models.BooleanField(verbose_name=_("bargain"), default=False)
    note = models.CharField(verbose_name=_("note"), max_length=1024, blank=True)
    html = models.TextField(verbose_name=_("html"), blank=True)

    product_id = models.PositiveIntegerField(verbose_name=_("product_id"), default=0)
    http_referer = models.CharField(verbose_name=_("http_referer"), max_length=2048, blank=True)

    def __str__(self):
        return str("%s: %s" % (self.shopping_domain, self.name))
