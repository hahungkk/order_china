import decimal
from django.db import models
from django.utils.translation import ugettext_lazy as _
from audit_log.models.fields import CreatingUserField, LastUserField


class Package(models.Model):

    class Meta:
        db_table = 'package'

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    created_by = CreatingUserField(verbose_name=_("created by"), limit_choices_to={'is_staff': True},
                                   editable=True, blank=True,
                                   related_name="created_%(app_label)s_%(class)s_set")
    modified_by = LastUserField(verbose_name=_("modified by"), limit_choices_to={'is_staff': True},
                                related_name="modified_%(app_label)s_%(class)s_set")
    code = models.CharField(max_length=250, null=True, blank=True, unique=True)
    version = models.PositiveIntegerField(default=1)
    label = models.CharField(max_length=250, null=True, default=True)
    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0), max_digits=9, decimal_places=2)

    @property
    def count_item(self):
        return self.shipment_set.count()


class TrackingChecker(models.Model):
    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    version = models.IntegerField(default=0)
    checked = models.BooleanField(default=False)
    tracking_code = models.ForeignKey('ShipmentPack', verbose_name=_("tracking_code"),
                                      related_name='checking_set', on_delete=models.SET_NULL,
                                      null=True, blank=True)


class VietNamTracking(models.Model):
    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    version = models.IntegerField(default=0)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    length = models.IntegerField(default=0)
    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0),
                                 max_digits=9, decimal_places=2)
    tracking_code = models.ForeignKey('ShipmentPack', verbose_name=_("tracking_code"),
                                      related_name='vietnam_tracking_set',
                                      on_delete=models.SET_NULL, null=True, blank=True)


class ShipmentStatus(models.Model):
    time = models.DateTimeField(verbose_name=_("time"), null=True, blank=True)
    context = models.CharField(verbose_name=_("context"), blank=True, null=True, max_length=512)
    location = models.CharField(verbose_name=_("context"), blank=True, null=True, max_length=512)
    shipment_pack = models.ForeignKey('ShipmentPack', related_name='status_set', null=True,
                                      blank=True, on_delete=models.SET_NULL)

    def __str__(self):
        return self.context


class ShipmentPack(models.Model):
    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    tracking_code = models.CharField(max_length=255, verbose_name=_('tracking_code'),
                                     primary_key=True)
    note = models.TextField(verbose_name=_("note"), blank=True, null=True)
    width = models.IntegerField(default=0)
    height = models.IntegerField(default=0)
    length = models.IntegerField(default=0)
    weight = models.DecimalField(verbose_name=_("weight"), default=decimal.Decimal(0),
                                 max_digits=9, decimal_places=2)
    package = models.ForeignKey('Package', null=True, blank=True,
                                related_name='shipment_set',
                                on_delete=models.SET_NULL)

    def __str__(self):
        return self.tracking_code
