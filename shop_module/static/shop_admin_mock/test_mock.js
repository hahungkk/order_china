/**
 * Created by TVA on 10/19/15.
 */



$(function () {
    $('p.datetime > br').remove();

    $('fieldset.module textarea.vLargeTextField')
        .attr('rows',3)
        .attr('cols',30);

    //swap functionality of 2 buttons
    $('div.inner-right-column > div > div > button.btn.btn-high.btn-info')
        .text('Save')
        .removeClass('btn-info')
        .addClass('btn-warning');
    $('div.inner-right-column > div > div > button:nth-child(2)')
        .text('Save and edit')
        .addClass('btn-info');

    Suit.after_inline.register('my_unique_func_name', function(inline_prefix, row){
        // Your code here
        console.info(inline_prefix);
        console.info(row);
        row.find('textarea.vLargeTextField')
            .attr('rows',1)
            .attr('cols',30);
    });
});