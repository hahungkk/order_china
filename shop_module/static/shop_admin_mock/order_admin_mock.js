/**
 * Created by TVA on 11/9/15.
 */


function newPrepaidPaymentAdded(selectElement, reload){

    var obj_id = selectElement.value;
    var obj_unicode = selectElement.options[selectElement.selectedIndex].innerHTML;
    addNewCustomerPaymentInlineRow(obj_id, obj_unicode, "PrePaid", "");
    if(reload){
        $('#order_form').find('> div.inner-right-column > div > div.submit-row > button').prop('disabled', true);
        window.location.reload(true);
    }
}


function addNewCustomerPaymentInlineRow(obj_id, obj_unicode, paymentType, paymentNote){
    //obj_id = 10;
    //obj_unicode = 'test ttttttt';
    var row_prefix = $('#payment-group').find('> div > fieldset > table > tbody > tr.form-row').length - 1;
    var row_form = $('#payment-empty')[0];
    var form_row_html = row_form.outerHTML.replace(/set-__prefix__|set-empty/g,'set-'+row_prefix);

    $(form_row_html).insertBefore('#payment-empty');
    $('#payment-'+row_prefix)
        .removeClass("empty-form")
        .addClass("dynamic-payment has_original");

    $('#id_customerpayment_set-'+row_prefix+'-id').attr("value",obj_id);
    $('#payment-'+row_prefix+' > td.field-__unicode__ > p').html(obj_unicode);

    if(paymentType)$('#id_customerpayment_set-'+row_prefix+'-type').val(paymentType);
    if(paymentNote)$('#id_customerpayment_set-'+row_prefix+'-note').val(paymentNote);

    $('#payment-'+row_prefix+' > td.text-center').append('<p><a href="/adl/shop_module/customerpayment/'+obj_id+'/" class="inlinechangelink">Change</a></p>')
}

$(function () {
$(document).ready(function($) {
    $("#searchbar").removeAttr('value');
    var nonNumberCharRegex = new RegExp('[^0-9.]', 'g'); //flag g (global) is always needed
    var totalCost = Number($('div.control-group.form-row.field-total_cost_tag > div > div.controls > span').html().replace(nonNumberCharRegex, ''));
    var prepaid_percent = Number($('#id_prepaid_percent').attr('value'));
    var prepaid_payment_cost = totalCost / 100 * prepaid_percent;
    var postpaid_payment_cost = Number($('div.control-group.form-row.field-payment_left_tag > div > div.controls > span').html().replace(nonNumberCharRegex, ''));
    var order_id = $('div.control-group.form-row.field-id > div > div.controls > span').text();
    var customer_id = $('div.control-group.form-row.field-customer_id > div > div.controls > span').text();
    var setgroup = $('#payment-group');
    var currency = $('#id_currency').val();
    var exchange_rate = $('#id_exchange_rate').val();

    var addPrepaidPaymentURL = '/adl/shop_module/customerpayment/add/?_to_field=id&_popup=1&amount=' + prepaid_payment_cost.toFixed(2) +'&customer='+customer_id+'&order=' + order_id + '&currency=' + currency + '&exchange_rate=' + exchange_rate;
    setgroup.find('> div > fieldset > table > tbody').append(
        '<tr class="add-row-custom"><td colspan="5"><div>' +
        '<select id="id_customerpayment1" name="customerpayment1" onchange="newPrepaidPaymentAdded(this)" style="display:None;"></select>' +
        '<a class="related-widget-wrapper-link" id="add_id_customerpayment1" href="' + addPrepaidPaymentURL + '">Add Prepaid payment</a></div></td></tr>'
    );

    var addPostpaidPaymentURL = '/adl/shop_module/customerpayment/add/?_to_field=id&_popup=1&amount='+postpaid_payment_cost.toFixed(2) +'&customer='+customer_id+'&order='+order_id+'&currency='+currency+'&exchange_rate='+exchange_rate;
    setgroup.find('> div > fieldset > table > tbody').append(
        '<tr class="add-row-custom"><td colspan="5"><div>' +
        '<select id="id_customerpayment2" name="customerpayment2" onchange="newPrepaidPaymentAdded(this)" style="display:None;"></select>' +
        '<a class="related-widget-wrapper-link" id="add_id_customerpayment2" href="' + addPostpaidPaymentURL + '">Add Complete payment</a></div></td></tr>'
    );

    var addHalfCompletePaidPayment = $(
        '<tr class="add-row-custom"><td colspan="5"><div>' +
        '<select id="id_customerpayment3" name="customerpayment3" onchange="newPrepaidPaymentAdded(this, true)" style="display:None;"></select>' +
        '<a id="add_id_customerpayment3" href="javascript:void(0);">Add Half-Complete payment</a></div></td></tr>'
    );

    addHalfCompletePaidPayment.find('a').click(function (event) {
        //console.log($('div.inline-group.suit-tab.suit-tab-OrderItem div.inline-related > h3'))
        var totalCostOfSelectedItem = 0;
        var orderitemIdList = [];
        $.each(
            $('div.inline-group.suit-tab.suit-tab-OrderItem div.inline-related').has('h3 > span.check-select > input:checked')
        ,function(i,element){
            totalCostOfSelectedItem+=Number($(element).find('div.field-total_order_currency_tag span')[0].innerText.replace(nonNumberCharRegex, ''));
            orderitemIdList.push( $(element).children('input')[0].value )
            }
        );
        var halfPaid_payment_cost = totalCostOfSelectedItem /100 * (100-prepaid_percent);
        var paramOrderItemStr = orderitemIdList.join('&orderitem_id=');

        if (this.href && halfPaid_payment_cost>0) {
            this.href = '/adl/shop_module/customerpayment/add/?_to_field=id&_popup=1&orderitem_id='+paramOrderItemStr+'&amount='+halfPaid_payment_cost.toFixed(2) +'&customer='+customer_id+'&order='+order_id+'&currency='+currency+'&exchange_rate='+exchange_rate;
            showRelatedObjectPopup(this);
        }else{
            this.href = "javascript:void(0);";
            alert("No item is selected");
        }
        event.preventDefault();
    });

    setgroup.find('> div > fieldset > table > tbody').append(addHalfCompletePaidPayment);

    $.each($('div.inline-group.suit-tab.suit-tab-OrderItem div.inline-related > h3'), function(i,e){
        var newElement = $('<span class="check-select"><input id="id_orderitem_set-'+i+'-SELECT" name="orderitem_set-'+i+'-SELECT" type="checkbox"> <label class="vCheckboxLabel inline" for="id_orderitem_set-'+i+'-SELECT">Select</label></span>')[0];
        //e.replaceChild(newElement,e.firstChild)
        $(e).find('b:first-child').replaceWith(newElement)
    });

    $('img.toggle-enlarge').css({width: "320px", height: "320px"});
    console.log("its should work.");

    Suit.after_inline.register('order_admin_inline', function (inline_prefix, row) {
        // Your code here
    });
});
});