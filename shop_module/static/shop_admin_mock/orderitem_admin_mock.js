/**
 * Created by TVA on 11/30/15.
 */

$(function () {
    // var lastVendor = '';
    var vendorList = [];
    var classList = ['error', 'info', 'success']; //'warning'
    $.each($('#result_list').find('> tbody > tr'), function (i, e) {
            var vendor = $(e).find('td.field-vendor').text();
            var index = vendorList.indexOf(vendor);
            if (index === -1) {
                index = vendorList.length;
                vendorList.push(vendor);
            }
            $(e).addClass(classList[index % classList.length])
        }
    );
    //enable tooltip
    // $('[data-toggle="tooltip"]').tooltip();

    //$('img.toggle-enlarge').hover(
    //    function () {
    //        $(this).stop().animate({width: "400px", height: "400px"}, 500);
    //    },
    //    function() {
    //        $(this).stop().animate({width: "64px", height: "64px"}, 500);
    //    }
    //);

    Suit.after_inline.register('my_unique_func_name', function (inline_prefix, row) {
        // Your code here
        console.info(inline_prefix);
        console.info(row)
    });
});

$(function () {
    $(document).ready(function ($) {
        $("#searchbar").removeAttr('value');
    });
});