from django.db.models.signals import pre_save
from django.dispatch import receiver

from ..models import ShipmentPackage, ShipmentLogistics

print("Connecting %s success" % __name__)


@receiver(pre_save, sender=ShipmentPackage)
def preSaveShipmentPackage(sender, instance=ShipmentPackage(), **kwargs):
    if kwargs['raw']: return  # disable this handler during fixture loading
    if instance.exchange_rate == 0 and instance.currency:
        instance.exchange_rate = instance.currency.exchange_rate

    if not instance.departed:
        instance.departed = instance.created

    if not instance.inventory_employee and instance.arrived and instance.modified_by and instance.modified_by.is_staff:
        instance.inventory_employee = instance.modified_by


@receiver(pre_save, sender=ShipmentLogistics)
def preSaveShipmentLogistics(sender, instance=ShipmentLogistics(), **kwargs):
    if kwargs['raw']: return  # disable this handler during fixture loading
    if instance.exchange_rate == 0 and instance.currency:
        instance.exchange_rate = instance.currency.exchange_rate

    if not instance.departed:
        instance.departed = instance.created

    if not instance.inventory_employee and instance.arrived and instance.modified_by and instance.modified_by.is_staff:
        instance.inventory_employee = instance.modified_by
