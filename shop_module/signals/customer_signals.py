#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  customer_signals.py
#  
#
#  Created by TVA on 4/14/16.
#  Copyright (c) 2016 order_china. All rights reserved.
#

from django.contrib.auth.signals import user_logged_in
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from ..controllers import BalanceController
from ..models import User, CustomerProfile, EmployeeProfile, EmployeePosition

print(("Connecting %s success" % __name__))


def create_customer_profile(user, referer=None):
    new_profile = CustomerProfile()
    new_profile.user = user
    new_profile.id = user.id
    if referer:
        print(f"referrer is {referer}")
        referer_profile = CustomerProfile.objects.get(user__username=referer)
        new_profile.referrer = referer_profile
    try:
        new_profile.save()
    except Exception as e:
        print(e)


def create_employee_profile(user):
    new_profile = EmployeeProfile()
    new_profile.user = user
    new_profile.id = user.id
    new_profile.save()


def compare_and_sync_employee_profile(employee_profile, employee_position):
    # check for sync
    sync_group = True

    current_group_permission_list = [gr for gr in employee_profile.user.groups.all()]
    current_group_permission_list.sort(key=lambda g: g.pk)
    position_group_permission_list = [gr for gr in employee_position.permission_groups.all()]
    position_group_permission_list.sort(key=lambda g: g.pk)
    if len(current_group_permission_list) == len(position_group_permission_list):
        for i in range(len(current_group_permission_list)):
            if current_group_permission_list[i].pk != position_group_permission_list[i].pk: break
        else:
            sync_group = False

    if sync_group:  # synchronize group
        employee_profile.user.groups.clear()
        employee_profile.user.groups.add(*position_group_permission_list)


@receiver(post_save, sender=EmployeeProfile)
def post_save_employee_profile(sender, instance=EmployeeProfile(), created=False, **kwargs):
    if kwargs['raw']:
        return  # disable this handler during fixture loading
    if instance.position:
        compare_and_sync_employee_profile(instance, instance.position)


@receiver(post_save, sender=EmployeePosition)
def post_save_employee_position(sender, instance=EmployeePosition(), created=False, **kwargs):
    if kwargs['raw']:
        return  # disable this handler during fixture loading
    for employeeProfile in instance.employeeprofile_set.all():
        compare_and_sync_employee_profile(employeeProfile, instance)


@receiver(pre_save, sender=User)
def pre_save_user(sender, instance=User(), **kwargs):
    if kwargs['raw']:
        return  # disable this handler during fixture loading
    if instance.id:  # user already created, edit user

        # try:
        #     if instance.customerProfile:
        #         pass  # user during login, last_login changes
        # except CustomerProfile.DoesNotExist:
        #     create_customer_profile(instance)

        if instance.is_staff:
            try:
                if instance.employeeProfile:
                    pass
            except EmployeeProfile.DoesNotExist:
                create_employee_profile(instance)


# @receiver(post_save, sender=User)
# def post_save_user(sender, instance=User(), created=False, **kwargs):
#     if kwargs['raw']:
#         return  # disable this handler during fixture loading
#     if created:
#         if instance.customerProfile:
#             pass  # user during login, last_login changes
#         # automaticaly create profile when a new User is saved
#         else:
#             create_customer_profile(instance)


@receiver(user_logged_in, sender=User)
def user_logged_in(sender, user, **kwargs):
    try:
        if user.customerProfile:
            pass
    except CustomerProfile.DoesNotExist:
        create_customer_profile(user)

    BalanceController.get_default_user_balance(user)

    if user.is_staff:
        try:
            if user.employeeProfile:
                pass
        except EmployeeProfile.DoesNotExist:
            create_employee_profile(user)
