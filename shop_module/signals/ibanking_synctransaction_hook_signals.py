#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  ibanking_synctransaction_hook_signals.py
#  
#
#  Created by TVA on 4/14/16.
#  Copyright (c) 2016 order_china. All rights reserved.
#
import logging
import re
from django.conf import settings
from django.db.models.signals import post_save
from django.dispatch import receiver

from ibanking_synctransaction.models import Transaction as IbankingTransaction
from system_configure.controllers import Tool
from ..controllers import PaymentController
from ..enums import *
from ..models import Order, BalanceAccount, CustomerPayment, PaymentType, Currency, Status

print("Connecting %s success" % __name__)


@receiver(post_save, sender=IbankingTransaction)
@Tool.disable_for_loaddata
def postSaveIbankingTransaction(sender, instance=IbankingTransaction(), created=False, **kwargs):
    if settings.DEBUG is False and created is False:
        return
    # should do no thing and return if edit when debug if off

    if instance.in_amount > 0:
        m = re.search(r'(\w{32})#(\d+)', instance.description)
        if m:
            payment_uid = m.group(1)
            order_id = int(m.group(2))
        else:
            m = re.search(r'\.\s*(\w{32})\W', instance.description)
            if m:
                payment_uid = m.group(1)
                order_id = None
            else:
                return  # ignore due to not found trace of order payment_uid

        logging.info("Detect incoming transaction from ibanking:%s" % instance)

        if payment_uid and order_id:
            paidOrder = None
            if order_id:
                paidOrder = Order.objects.filter(id=order_id, payment_uid=payment_uid).first()
            if not paidOrder:
                paidOrder = Order.objects.filter(payment_uid=payment_uid).first()
            if not paidOrder:
                logging.info("Couldn't find Order for this IbankingTransaction:%s" % instance)
                return
            amount_need_to_be_prepaid = (
                                        paidOrder.grant_total - paidOrder.sum_payment_transaction) * paidOrder.prepaid_percent / 100
            logging.debug('amount_need_to_be_prepaid=%s' % amount_need_to_be_prepaid)

            bank_payment_uid = instance.balance_account.detail
            balance = BalanceAccount.objects.filter(payment_uid=bank_payment_uid, type=BalanceType.bank).first()
            if not balance:
                logging.error(
                    "Couldn't find balance account link to this bank using detail=payment_uid:%s" % bank_payment_uid)
                return
            payment = CustomerPayment()
            payment.customer = paidOrder.customer
            payment.order = paidOrder
            payment.type = PaymentType.objects.filter(is_customerpayment=True).first()
            currency = Currency.objects.get(pk=instance.balance_account.currency)
            try:
                PaymentController.atomic_transaction_for_payment(payment, None, balance, currency, instance.in_amount,
                                                                 currency.exchange_rate, detail="",
                                                                 user=None)
            except Exception as e:
                logging.error("Unable to complete transaction: %s" % e)
                return

            if paidOrder.status.logic_step == LogicStep.pending:
                # TODO: might contain logic bug here cause sum_payment_transaction may contain this transaction already
                if instance.in_amount >= amount_need_to_be_prepaid:
                    paidOrder.status = Status.objects.filter(is_orderstatus=True, logic_step=LogicStep.approved).first()
                    paidOrder.save()
        elif payment_uid:
            payment = CustomerPayment()
            payment.customer = payment.customer
            payment.order = None
            payment.type = PaymentType.objects.filter(is_customerpayment=True).first()
            balance = BalanceAccount.objects.filter(payment_uid=payment_uid).first()
            currency = Currency.objects.get(pk=instance.balance_account.currency)
            try:
                PaymentController.atomic_transaction_for_payment(payment, None, balance, currency, instance.in_amount,
                                                                 currency.exchange_rate,
                                                                 detail="Automatic payment by scaning ibanking",
                                                                 user=balance.account_holder)
            except Exception as e:
                logging.error("Unable to complete transaction: %s" % e)
                return

            # add another payment to the system balance
            bank_payment_uid = instance.balance_account.detail
            system_balance = BalanceAccount.objects.filter(payment_uid=bank_payment_uid, type=BalanceType.bank).first()
            if not system_balance:
                logging.error(
                    "Couldn't find balance account link to this bank using detail=payment_uid:%s" % bank_payment_uid)
                return
            try:
                PaymentController.atomic_transaction_for_payment(payment, None, system_balance, currency,
                                                                 instance.in_amount, currency.exchange_rate,
                                                                 detail="Automatic payment by scaning ibanking",
                                                                 user=balance.account_holder)
            except Exception as e:
                logging.error("Unable to complete transaction: %s" % e)
                return
