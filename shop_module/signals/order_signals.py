#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  order_signals.py
#
#
#  Created by TVA on 4/14/16.
#  Copyright (c) 2016 order_china. All rights reserved.
#
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from django.utils import timezone
from django.conf import settings
from orderbot.task import send_message_task
from shop_module import enums
from system_configure.controllers.SystemConfigureController import get_public_config
from ..controllers.OrderController import calculate_service_charge2, calculate_sale_serivce_charge
from ..models import Currency, ShipmentPack
from ..models import Order, OrderItem
from ..task import update_order_when_exchange_rate_changed
import re

print("Connecting %s success" % __name__)


@receiver(pre_save, sender=Order)
def presave_order(sender, instance, **kwargs):
    if instance.exchange_rate <= 0 and instance.currency:
        instance.exchange_rate = instance.currency.exchange_rate
    if instance.id:
        if instance.status.value == enums.ORDERED_STATUS and not instance.ordered_date:
            instance.ordered_date = timezone.localtime(timezone.now())
        if instance.status.value == enums.SHIP_TO_VN_STATUS and not instance.shiptovn_date:
            instance.shiptovn_date = timezone.localtime(timezone.now())


@receiver(post_save, sender=Order)
def post_save_order(sender, instance, created, **kwargs):
    if not created:
        sale = get_public_config('khuyenmai')
        # if instance.status.value == 'Completed':
        #     add_referrer_credit.delay(instance.customer.id, instance.sum_item_cost)
        if instance.status.value == enums.PREPAID_STATUS and sale is not None:
            for item in instance.orderitem_set.all():
                charge = calculate_sale_serivce_charge(item.service_charge, sale)
                OrderItem.objects.filter(id=item.id).update(service_charge=charge)
        if instance.status.value == enums.SHIP_TO_VN_STATUS and settings.FACEBOOK_BOT:
            send_message_task(instance).delay()


@receiver(pre_save, sender=OrderItem)
def presave_order_item(sender, instance, **kwargs):
    instance.image_url_origin = re.sub(r'\.150x150|\.jpg_150x150q90|\.jpg_150x150', '', instance.image_url)
    if instance.id:
        old_order_item = OrderItem.objects.get(pk=instance.id)
        order = instance.order
        if instance.service_charge == old_order_item.service_charge:
            if instance.insurance != old_order_item.insurance or instance.quantity != old_order_item.quantity or \
                    instance.fast_order != old_order_item.fast_order:
                instance.service_charge, \
                    instance.insurance_charge, \
                    instance.fast_order_charge, \
                    instance.bargain_charge = \
                    calculate_service_charge2(order.total_item_cost,
                                              instance.price,
                                              instance.quantity,
                                              instance.insurance,
                                              instance.fast_order,
                                              instance.bargain,
                                              order.customer)

        if instance.shipment_pack != old_order_item.shipment_pack:
            try:
                ShipmentPack.objects.get_or_create(tracking_code=instance.shipment_pack, weight=0)
            except Exception as e:
                print(e)
                pass


# @receiver(pre_save, sender=Currency)
# def presave_currency(sender, instance, **kwargs):
#     if instance.code == 'CNY':
#         old_ex_rate = Currency.objects.get(code=instance.code).exchange_rate
#         # Checking change exchange rate
#         if old_ex_rate != instance.exchange_rate:
#             update_order_when_exchange_rate_changed.delay()


@receiver(post_save, sender=Currency)
def post_save_currency(sender, instance, created, **kwargs):
    update_order_when_exchange_rate_changed.delay()
