# created by Cherry 25/09/2017
from django.db.models.signals import pre_save, post_save
from django.dispatch import receiver
from ..models import ShipmentPack

print("Connecting %s success" % __name__)


@receiver(pre_save, sender=ShipmentPack)
def presave_shipment_pack(sender, instance=ShipmentPack(), **kwargs):
    pass


# @receiver(post_save, sender=ShipmentPack)
# def presave_shipment_pack(sender, instance=ShipmentPack, created=False, **kwargs):
#     if created:
#         if instance.tracking_code:
#             get_shipment_status.delay(instance.tracking_code)
