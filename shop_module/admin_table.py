import django_tables2 as tables
from import_export import resources, fields
from shop_module.api.Order_RestfulAPI import ExportCSVDateFormat
from shop_module.models import Order
from django.utils.translation import ugettext_lazy as _


class OrderResource(resources.ModelResource):
    class Meta:
        model = Order
        fields = ['id', 'customer__username', 'count_item', 'order_cost', 'total_cost_CNY',
                  'total_cost', 'payment_total', 'service_charge', 'fast_order_charge', 'insurance_charge',
                  'status__label', 'created', 'created_by', 'modified_by', 'ordered_date', 'paid_date']

        export_order = ['id', 'customer__username', 'count_item', 'order_cost', 'total_cost_CNY',
                        'total_cost', 'payment_total', 'service_charge', 'fast_order_charge', 'insurance_charge',
                        'status__label', 'created', 'created_by', 'modified_by', 'ordered_date', 'paid_date']

        widgets = {
            'created': {'format': ExportCSVDateFormat},
            'ordered_date': {'format': ExportCSVDateFormat}
        }

    total_cost = fields.Field(attribute='total_cost', readonly=True)
    total_cost_CNY = fields.Field(attribute='sum_total', readonly=True)
    count_item = fields.Field(attribute='count_item', readonly=True)
    order_cost = fields.Field(attribute='order_cost', readonly=True)
    service_charge = fields.Field(attribute='_sum_item_service_charge_orig', readonly=True)
    insurance_charge = fields.Field(attribute='insurance_charge_property', readonly=True)
    fast_order_charge = fields.Field(attribute='sum_fast_order_charge', readonly=True)
    payment_total = fields.Field(attribute='sum_payment_transaction', readonly=True)
    paid_date = fields.Field(attribute='latest_transaction_export', readonly=True)

    # @staticmethod
    # def dehydrate_paid_date(item):
    #     return item.weight
    #


class TrackingTable(tables.Table):
    time = tables.Column(orderable=False, verbose_name=_('Time'))
    context = tables.Column(orderable=False, verbose_name=_('Status'))

    class Meta:
        attrs = {'class': 'table table-bordered table-hover'}


class SumByCurrencyTable(tables.Table):
    currency = tables.Column(orderable=False, verbose_name=_('Currency'))
    sum_item_cost = tables.Column(orderable=False, verbose_name=_('Sum Item Cost'))
    sum_shipping = tables.Column(orderable=False, verbose_name=_('Sum Shipping'))
    sum_service_charge = tables.Column(orderable=False, verbose_name=_('Sum Service Charge'))
    sum_insurance_charge = tables.Column(orderable=False, verbose_name=_('Sum Insurance Charge'))
    sum_fast_order_charge = tables.Column(orderable=False, verbose_name=_('Sum Fast Order Charge'))
    sum_bargain_charge = tables.Column(orderable=False, verbose_name=_('Sum Bargain Charge'))
    sum_tax_cost = tables.Column(orderable=False, verbose_name=_('Sum Tax Cost'))
    sum_total = tables.Column(orderable=False, verbose_name=_('Sum Total'))
    sum_order_cost = tables.Column(orderable=False, verbose_name=_('Sum Order Cost'))

    class Meta:
        attrs = {'class': 'table table-bordered table-hover'}


class SumByCurrencyTableChecker(tables.Table):
    currency = tables.Column(orderable=False, verbose_name=_('Currency'))
    sum_item_cost = tables.Column(orderable=False, verbose_name=_('Sum Item Cost'))
    sum_shipping = tables.Column(orderable=False, verbose_name=_('Sum Shipping'))
    sum_service_charge = tables.Column(orderable=False, verbose_name=_('Sum Service Charge'))
    sum_insurance_charge = tables.Column(orderable=False, verbose_name=_('Sum Insurance Charge'))
    sum_fast_order_charge = tables.Column(orderable=False, verbose_name=_('Sum Fast Order Charge'))
    sum_bargain_charge = tables.Column(orderable=False, verbose_name=_('Sum Bargain Charge'))
    sum_tax_cost = tables.Column(orderable=False, verbose_name=_('Sum Tax Cost'))
    sum_total = tables.Column(orderable=False, verbose_name=_('Sum Total'))

    class Meta:
        attrs = {'class': 'table table-bordered table-hover'}
