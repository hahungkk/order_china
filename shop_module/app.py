#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  app.py
#
#
#  Created by TVA

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MyAppConfig(AppConfig):
    name = 'shop_module'
    verbose_name = _("Shop Module")

    def ready(self):
        from . import signals
