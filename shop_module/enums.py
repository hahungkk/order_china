#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  enums.py
#
#
#  Created by TVA


from system_configure.enums import EnumBase


class CustomerGender(EnumBase):
    unknown = 0
    male = 1
    female = 2


class VipStatus(EnumBase):
    Normal = 0
    VIP1 = 1
    VIP2 = 2
    VIP3 = 3


class DeliveryMethod(EnumBase):
    store_pickup = 0
    address_shipping = 1


class BalanceType(EnumBase):
    cash = 0
    bank = 1
    organization = 2
    funding = 3
    user_credit = 4


class LogicStep(EnumBase):
    pending = 0
    approved = 1
    processing = 2
    completed = 3
    # not_completed = 5
    failed = 4


class Company(EnumBase):
    taobao = "Taobao"
    m1688 = "1688"
    # tmall = 2


ITEM_CHECKER_PERM = 'kiểm hàng'
ACCOUNTANT_CE_PERM = 'kế toán (sửa người tạo)'
ACCOUNTANT_PERM = 'kế toán'
ORDERER_PERM = 'đặt hàng'

WAITING_PREPAID_STATUS = 'WaitingForPrePaid'
PREPAID_STATUS = 'PrePaid'
SHIP_TO_VN_STATUS = 'ShipToVN'
COMPLETED_STATUS = 'Completed'
ORDERED_STATUS = 'Ordered'

