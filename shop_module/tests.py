from django.urls import reverse_lazy
from drf_tweaks.test_utils import DatabaseAccessLintingApiTestCase


class TestOrderAdminAPI(DatabaseAccessLintingApiTestCase):
    def test_order_admin(self):
        self.client.get(reverse_lazy('OrderAdmin-list'))

    def test_order_item_admin(self):
        self.client.get(reverse_lazy('OrderItem-list'))
