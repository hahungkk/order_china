#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  Product_RestfulAPI.py
#  
#
#  Created by TVA on 11/11/15.
#  Copyright (c) 2015 order_china. All rights reserved.

import re

import django_filters
from django.conf import settings
from rest_framework import serializers, generics, viewsets, filters
from django_filters.rest_framework import DjangoFilterBackend
from system_configure.controllers import Tool
from ..models import Product


class ProductSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product

        fields = [
            'id', 'name', 'slug', 'image_url',
            'short_description',
            'categories',
            'currency', 'price',
            'shopping_domain',
        ]

    categories = serializers.StringRelatedField(many=True, read_only=True)


class ProductDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = Product
        fields = '__all__'
    categories = serializers.PrimaryKeyRelatedField(many=True, read_only=True)


class ProductListFilter(django_filters.FilterSet):
    min_price = django_filters.NumberFilter(name="price", lookup_type='gte')
    max_price = django_filters.NumberFilter(name="price", lookup_type='lte')

    class Meta:
        model = Product
        fields = ['categories', 'currency', 'shopping_domain', 'min_price', 'max_price']


class ProductCollectionClientAPIView(viewsets.GenericViewSet, generics.ListAPIView):
    """ A view that returns data in JSON.
    """

    serializer_class = ProductSerializer

    permission_classes = []

    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = ProductListFilter
    search_fields = ('name', 'short_description')
    ordering_fields = ('name', 'price')

    def get_queryset(self):
        return Product.objects.filter(is_hidden=False)


class ProductDetailClientAPIView(viewsets.GenericViewSet, generics.RetrieveAPIView):
    serializer_class = ProductDetailSerializer

    permission_classes = []

    def get_queryset(self):
        return Product.objects.filter(is_hidden=False)

    def retrieve(self, request, *args, **kwargs):
        pk = kwargs['pk'];
        if settings.DEBUG and re.match(r'\d+',
                                       pk):  # TODO: disable this in production to prevent crawler easy get product info
            instance = self.get_object()
        else:
            try:
                instance = self.get_queryset().get(slug=pk);
            except Product.DoesNotExist:
                return Tool.Response(status=404)

        serializer = self.get_serializer(instance)
        return Tool.Response(serializer.data)
