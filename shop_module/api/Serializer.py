#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  serializers.py
#
#
#  Created by TVA on 12/1/15.
#  Copyright (c) 2015 order_china. All rights reserved.
from rest_framework import serializers

from shop_module.models import Status
from ..models import CartItem, MONEY_MAX_DIGITS, MONEY_DECIMAL_PLACES


class CartItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItem
        fields = ('id', 'created', 'modified', 'customer', 'shopping_domain', 'vendor', 'detail_url',
                  'name', 'sku', 'image_url', 'product_id', 'http_referer', 'note', 'quantity',
                  'options_selected', 'fragile', 'insurance', 'fast_order', 'bargain', 'short_description',
                  'html', 'currency', 'price', 'shipping', 'weight', 'options_metadata', 'category_list', 'checked')
        read_only_fields = ('id', 'created', 'modified', 'customer', 'shopping_domain', 'vendor', 'detail_url',
                            'name', 'sku', 'image_url', 'product_id', 'http_referer', 'short_description',
                            'html', 'currency', 'price', 'shipping', 'weight', 'options_metadata', 'category_list')


class FullCartItemSerializer(serializers.ModelSerializer):
    class Meta:
        model = CartItem
        fields = '__all__'

    service_charge = serializers.DecimalField(MONEY_MAX_DIGITS, MONEY_DECIMAL_PLACES, read_only=True)
    # currency_label = serializers.CharField(label=_('currency'), read_only=True) #source='currency.label',


class StatusSerializer(serializers.ModelSerializer):
    class Meta:
        model = Status
        fields = ['value', 'label', 'logic_step']
