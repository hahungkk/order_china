from rest_framework import serializers, generics, permissions, viewsets

from ..models import CustomerProfile


class CustomerProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerProfile
        fields = ('customerProfile_url', 'full_name', 'address', 'phone_number', 'city', 'district', 'street',
                  'vip_status', 'receive_sms_notify', 'receive_email_notify', 'gender', 'about',
                  'supporter_employee', 'is_referrer', 'vip_status', 'refererlink')
        read_only_fields = ('supporter_employee', 'is_referrer', 'refererlink')

    customerProfile_url = serializers.HyperlinkedIdentityField(view_name='CustomerProfile-detail', read_only=True)
    supporter_employee = serializers.StringRelatedField()


class CustomerProfileDetailClientAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateAPIView):
    """A view that returns the count of active users in JSON.
    """

    serializer_class = CustomerProfileSerializer

    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return CustomerProfile.objects.filter(user=self.request.user)
