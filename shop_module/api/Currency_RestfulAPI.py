#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  Currency_RestfulAPI.py

from rest_framework import serializers, viewsets, generics
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAdminUser
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from ..models import Currency


class CurrencySerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ('code', 'exchange_rate', 'label')
        read_only_fields = ('code', 'exchange_rate', 'label')


class CurrencyUpdateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Currency
        fields = ('code', 'exchange_rate', 'label')
        read_only_fields = ('code', 'label')


class CurrencyClientAPIView(viewsets.GenericViewSet, generics.ListAPIView):
    """ A view that returns all currency data in JSON.
    """

    serializer_class = CurrencySerializer
    permission_classes = []
    authentication_classes = [JSONWebTokenAuthentication, SessionAuthentication]
    queryset = Currency.objects.all()


class CurrencyUpdateClientAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateAPIView):
    """ A view that returns all currency data in JSON.
    """
    serializer_class = CurrencyUpdateSerializer
    permission_classes = [IsAdminUser]
    authentication_classes = [JSONWebTokenAuthentication, SessionAuthentication]
    queryset = Currency.objects.all()
