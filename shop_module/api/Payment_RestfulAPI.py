import django_filters
from munch import Munch
from django.utils import timezone
from utilities.django_sql_execfile import sql_execfile
from rest_framework import serializers, generics, permissions, viewsets, status, filters
from rest_framework.decorators import action
from system_configure.controllers import Tool
from ..models import CustomerPayment, Transaction, BalanceAccount, Order
from django_filters.rest_framework import DjangoFilterBackend, FilterSet
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework.authentication import SessionAuthentication
import decimal


class CustomerAccountBalanceSerializer(serializers.ModelSerializer):
    class Meta:
        model = BalanceAccount

        fields = ('created', 'account_holder', 'type', 'name', 'currency', 'amount',)

    account_holder = serializers.StringRelatedField()


class CustomerAccountBalanceCollectionClientAPIView(viewsets.GenericViewSet, generics.ListAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CustomerAccountBalanceSerializer

    def get_queryset(self):
        if self.request.user.is_staff:
            return BalanceAccount.objects.all()
        return BalanceAccount.objects.filter(account_holder=self.request.user)


class PaymentAdminStatisticFilterForm(serializers.Serializer):
    def update(self, instance, validated_data):
        super(PaymentAdminStatisticFilterForm, self).update(instance, validated_data)

    def create(self, validated_data):
        super(PaymentAdminStatisticFilterForm, self).create(validated_data)

    from_date = serializers.DateTimeField(default=timezone.localtime(timezone.now()) - timezone.timedelta(days=30))
    to_date = serializers.DateTimeField(default=timezone.localtime(timezone.now()))


class TransactionSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction

        fields = ('from_balance', 'to_balance', 'currency', 'exchange_rate', 'amount', 'cashier', 'detail')

    cashier = serializers.StringRelatedField()


class CustomerPaymentSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerPayment

        fields = ('created', 'order', 'type', 'transaction', 'note')

    type = serializers.StringRelatedField()
    transaction = TransactionSerializer(many=False, read_only=True)


class PaymentFilter(FilterSet):
    range_amount = django_filters.RangeFilter(name="transaction__amount")

    class Meta:
        model = CustomerPayment
        fields = ['range_amount', 'transaction__from_balance', 'transaction__to_balance']


# noinspection PyTypeChecker,PyUnusedLocal
class CustomerPaymentAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CustomerPaymentSerializer

    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = PaymentFilter
    search_fields = ['note', ]
    ordering_fields = ['created', ]

    def get_queryset(self):
        return CustomerPayment.objects.filter(order__customer=self.request.user)

    @action(detail=False,
            methods=['GET', 'POST'],
            permission_classes=[permissions.IsAuthenticated, permissions.IsAdminUser],
            serializer_class=PaymentAdminStatisticFilterForm)
    def getstats(self, request, *args, **kwargs):
        data = request.data
        if request.method == 'GET':
            data = request.GET
        form_data = PaymentAdminStatisticFilterForm(data=data)
        if not form_data.is_valid():
            return Tool.error_response_restful(form_data.errors, code=status.HTTP_400_BAD_REQUEST)
        # ::type: AdminStatisticFilterForm
        data = Munch(form_data.data)

        # test raw_queryfile
        # qs = raw_queryfile(Order, 'datagrip/order_rawquery.sql')
        # for order in qs[:]:
        # 	logging.debug('payment_left:%s'%order.payment_left);

        return Tool.success_response_restful(sql_execfile('datagrip/payment_transaction_stats.sql',
                                                          params=data,
                                                          map_result_to_dict=True,
                                                          include_description=True))


class TransactionAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction

        fields = ('id', 'from_balance', 'to_balance', 'currency', 'exchange_rate', 'amount', 'cashier', 'is_deposite')

    cashier = serializers.StringRelatedField(read_only=True)
    from_balance = serializers.PrimaryKeyRelatedField(
        queryset=BalanceAccount.objects.filter(account_holder__is_staff=True), allow_null=True)
    to_balance = serializers.PrimaryKeyRelatedField(
        queryset=BalanceAccount.objects.filter(account_holder__is_staff=True), allow_null=True)
    amount = serializers.SerializerMethodField()
    is_deposite = serializers.SerializerMethodField()

    @staticmethod
    def get_amount(tran):
        sign = "-" if tran.from_balance and not tran.to_balance else ""
        return decimal.Decimal(f"{sign}{tran.amount}")

    @staticmethod
    def get_is_deposite(tran):
        return True if tran.from_balance and not tran.to_balance else False


class TransactionCreatSerializer(serializers.ModelSerializer):
    class Meta:
        model = Transaction

        fields = ('id', 'from_balance', 'to_balance', 'currency', 'exchange_rate', 'amount', 'cashier',)

    cashier = serializers.StringRelatedField(read_only=True)
    from_balance = serializers.PrimaryKeyRelatedField(
        queryset=BalanceAccount.objects.filter(account_holder__is_staff=True), allow_null=True)
    to_balance = serializers.PrimaryKeyRelatedField(
        queryset=BalanceAccount.objects.filter(account_holder__is_staff=True), allow_null=True)

    def create(self, validated_data):
        instance = super(TransactionCreatSerializer, self).create(validated_data)
        instance.cashier = self.context['request'].user
        instance.save()
        return instance


class CustomerPaymentAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerPayment

        fields = ('id', 'created', 'order', 'type', 'transaction', 'note', 'customer_tag')

    type = serializers.StringRelatedField()
    order = serializers.PrimaryKeyRelatedField(queryset=Order.objects.filter(
        created__gte=timezone.localtime(timezone.now())-timezone.timedelta(days=30)))
    transaction = TransactionAdminSerializer(many=False)


class CustomerPaymentCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomerPayment

        fields = ('id', 'created', 'order', 'type', 'transaction', 'note')


class CustomerPaymentAdminAPIView(CustomerPaymentAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = CustomerPaymentAdminSerializer
    authentication_classes = [JSONWebTokenAuthentication, SessionAuthentication]
    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = PaymentFilter
    search_fields = ['id', ]
    ordering_fields = ['created', ]

    def get_queryset(self):
        now = timezone.localtime(timezone.now())
        delta_time = now - timezone.timedelta(days=30)
        if self.request.user.is_staff:
            return CustomerPayment.objects.all().order_by('-created')
        return CustomerPayment.objects.filter(order__customer=self.request.user,
                                              order__created__gte=delta_time)

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return CustomerPaymentCreateSerializer
        return CustomerPaymentAdminSerializer


class TransactionAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TransactionAdminSerializer
    authentication_classes = [JSONWebTokenAuthentication, SessionAuthentication]

    def get_queryset(self):
        return Transaction.objects.all().order_by('-created')

    def get_serializer_class(self):
        if self.request.method == 'POST':
            return TransactionCreatSerializer
        return TransactionAdminSerializer
