#!/usr/bin/python
# -*- coding: utf-8 -*-

#  Created by Cherry on 12/26/17.

from datetime import datetime
from django.db import transaction
from django.db.models import DateTimeField, Sum
from django.db.models.functions import Trunc
from django.utils import timezone
from django_filters import ModelChoiceFilter
from django_filters.rest_framework import FilterSet, DjangoFilterBackend
from drf_tweaks.optimizator import AutoOptimizeMixin
from rest_framework import generics, permissions, viewsets, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import action
from rest_framework.filters import SearchFilter
from rest_framework.generics import ListAPIView
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from rest_framework_extensions.cache.decorators import cache_response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from shop_module.api.Order_RestfulAPI import OrderDetailClientAPIView, \
    TinySetPagination, OrderFilter, LargeResultsSetPagination
from shop_module.api.Serializer import StatusSerializer
from shop_module.serializers.order_serializers import OrderSerializerAdmin, OrderItemAdminSerializer, \
    OrderDetailAdminSerializer, OrderItemAdminSerializerSP
from system_configure.controllers import Tool
from ..models import Order, Status
from ..models import OrderItem


class StaffOnlyPermission(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.is_staff


class OrderItemFilter(FilterSet):
    status = ModelChoiceFilter(queryset=Status.objects.all())

    class Meta:
        model = OrderItem
        fields = {
            'id': ['exact'],
            'shipment_pack': ['exact'],
        }


class OrderCollectionAdminAPI(AutoOptimizeMixin, viewsets.GenericViewSet, ListAPIView):
    permission_classes = (StaffOnlyPermission,)
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = OrderSerializerAdmin
    pagination_class = TinySetPagination
    filter_class = OrderFilter
    filter_backends = (SearchFilter, DjangoFilterBackend)
    search_fields = ('customer__username', 'id')
    filter_fields = ('created', 'created_by')

    def get_queryset(self):
        return Order.objects.select_related('created_by',
                                            'status',
                                            'modified_by',
                                            'customer') \
            .prefetch_related('orderitem_set').all().order_by('-created')


class OrderItemCollectionAdminAPI(AutoOptimizeMixin, viewsets.GenericViewSet, generics.ListAPIView):
    permission_classes = (StaffOnlyPermission,)
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = OrderItemAdminSerializer
    pagination_class = TinySetPagination
    filter_class = OrderItemFilter
    filter_backends = (SearchFilter, DjangoFilterBackend)
    search_fields = ('order__id', 'order__customer__username', 'shipment_pack', 'order_code')

    def get_queryset(self):
        fr = self.request.query_params.get('from', None)
        to = self.request.query_params.get('to', None)
        if fr and to:
            fr_date = datetime.strptime(fr, "%Y-%m-%d")
            to_date = datetime.strptime(to, "%Y-%m-%d").replace(hour=23, minute=59, second=59)
            return OrderItem.objects.filter(exported_date__gte=fr_date,
                                            exported_date__lte=to_date,
                                            status='Completed')
        if fr and not to:
            fr_date = datetime.strptime(fr, "%Y-%m-%d")
            to_date = timezone.localtime(timezone.now()).replace(hour=23, minute=59, second=59)
            return OrderItem.objects.filter(exported_date__gte=fr_date,
                                            exported_date__lte=to_date,
                                            status='Completed')
        return OrderItem.objects.all().order_by('-created')

    def get_serializer_class(self):
        fr = self.request.query_params.get('from', None)
        if fr:
            return OrderItemAdminSerializerSP
        return super(OrderItemCollectionAdminAPI, self).get_serializer_class()

    @cache_response(60 * 60)
    @action(detail=False, methods=['GET', 'POST'],
            authentication_classes=[JSONWebTokenAuthentication, SessionAuthentication])
    def stats(self, request, pk):
        resp = {'date': [], 'price': []}
        price = []
        data = OrderItem.objects.filter(created__gte=timezone.localtime(timezone.now()) - timezone.timedelta(days=15)) \
            .annotate(created_date=Trunc('created', 'day', output_field=DateTimeField())) \
            .values('created_date').annotate(price=Sum('price'))
        for da in data:
            resp['date'].append(da['created_date'].strftime("%d/%m/%y"))
            price.append(da['price'])
        resp['price'].append(price)
        return Tool.success_response_restful(resp)


class OrderItemExportAdminAPI(OrderItemCollectionAdminAPI):
    pagination_class = LargeResultsSetPagination


class OrderDetailAdminAPI(OrderDetailClientAPIView):
    serializer_class = OrderDetailAdminSerializer
    authentication_classes = [JSONWebTokenAuthentication, SessionAuthentication]
    permission_classes = [StaffOnlyPermission]

    def get_queryset(self):
        return Order.objects.all()

    def destroy(self, request, *args, **kwargs):
        instance = self.get_object()
        with transaction.atomic():
            try:
                instance.orderitem_set.all().delete()
                instance.customerpayment_set.all().delete()
                self.perform_destroy(instance)
                return Response(status=status.HTTP_204_NO_CONTENT)
            except Exception as e:
                print(e)
                return Response(data={"error": "Error"}, status=status.HTTP_400_BAD_REQUEST)

    @action(detail=True, methods=['GET'], permission_classes=[permissions.IsAuthenticated],
            serializer_class=OrderItemAdminSerializer)
    def items(self, request, pk):
        obj = self.get_object()
        items = obj.orderitem_set.all()
        return Response(OrderItemAdminSerializer(items, many=True).data)


class StatusAPIView(viewsets.ModelViewSet):
    queryset = Status.objects.all().order_by('logic_step')
    serializer_class = StatusSerializer
    permission_classes = [StaffOnlyPermission]
    authentication_classes = [SessionAuthentication, JSONWebTokenAuthentication]
