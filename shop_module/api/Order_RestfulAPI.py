from collections import OrderedDict
import datetime
import logging
import openpyxl
import pytz
from django.conf import settings
from django.db import transaction
from django.db.models import DateTimeField, Count
from django.db.models.functions import Trunc
from django.utils import timezone
from django.utils import translation
from django.utils.translation import ugettext as __, ugettext_lazy as _
from django_filters.rest_framework import FilterSet, ModelChoiceFilter, DateFromToRangeFilter
from import_export import resources, fields
from rest_framework import renderers
from rest_framework import serializers, generics, permissions, exceptions, viewsets
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.parsers import FileUploadParser
from rest_framework.permissions import BasePermission
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from shop_module.serializers.order_serializers import OrderSerializer, OrderDetailSerializer, \
    OrderItemAdminSerializer, ItemHistorySerializer
from system_configure.controllers import Tool
from user_module.controllers.EmailController import send_email_notify_ordered
from utilities.VietnameseDateTimeFormat import convert_to_vietnamese_datetime
from ..controllers import OrderController, PaymentController, BalanceController
from ..enums import *
from ..models import Order, CartItem, Status, CustomerPayment, PaymentType
from ..models import OrderItem, CustomerProfile


class StaffOnlyPermission(BasePermission):
    """
    Allows access only to authenticated users.
    """

    def has_permission(self, request, view):
        return request.user and request.user.is_authenticated and request.user.is_staff


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 10000


class ExtraLargeResultsSetPagination(PageNumberPagination):
    page_size = 5000
    page_size_query_param = 'page_size'
    max_page_size = 10000


class TinySetPagination(PageNumberPagination):
    page_size = 50
    page_size_query_param = 'page_size'
    max_page_size = 100


# admin form
class AdminStatisticFilterForm(serializers.Serializer):
    def create(self, validated_data):
        pass

    def update(self, instance, validated_data):
        pass

    from_date = serializers.DateTimeField(default=timezone.localtime(timezone.now()) - timezone.timedelta(days=30))
    to_date = serializers.DateTimeField(default=timezone.localtime(timezone.now()))


class OrderClientAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderSerializer
    pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        return Order.objects.filter(customer=self.request.user).order_by('-created')

    def perform_create(self, serializer):
        OrderController.save_cartitem_in_session(self.request)
        cp = CustomerProfile.objects.get(user_id=self.request.user.id)
        facebook_name = self.request.data.get('facebook_name', None)
        if facebook_name:
            OrderController.update_facebook_name_from_order(facebook_name, self.request.user)
        if not CartItem.objects.filter(customer=self.request.user.username).exists():
            raise exceptions.ValidationError('Your cart is empty!')

        currency = PaymentController.get_default_currency()

        with transaction.atomic():
            # try:
            order = serializer.save(customer=self.request.user, currency=currency,
                                    status=OrderController.get_default_order_status())
            OrderController.build_orderitem_from_cart(order, cp.vip_status)
        # except Exception as e:
        # 	raise exceptions.NotAcceptable(str(e))
        return order

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        order = self.perform_create(serializer)
        order = Order.objects.get(pk=order.pk)  # reload order after build
        tz = pytz.timezone('Asia/Ho_Chi_Minh')
        time_created = convert_to_vietnamese_datetime(datetime.datetime.now(tz))
        if not settings.DEBUG:
            send_email_notify_ordered.delay(to_address=order.customer.email,
                                            full_name=order.customer.username,
                                            order_id=order.id,
                                            created=time_created,
                                            order_cost=order.total_cost_tag(),
                                            prepaid=order.prepaid_tag())
        serializer = self.get_serializer(instance=order)
        headers = self.get_success_headers(serializer.data)
        return Tool.Response(serializer.data, status=201, headers=headers)

    @action(detail=False, methods=['GET', ],
            authentication_classes=[JSONWebTokenAuthentication, SessionAuthentication])
    def stats(self, request, *args, **kwargs):
        resp = {'date': [], 'count': []}
        count = []
        data = Order.objects.filter(created__gte=timezone.localtime(timezone.now()) - timezone.timedelta(days=15)) \
            .annotate(created_date=Trunc('created', 'day', output_field=DateTimeField())) \
            .values('created_date').annotate(count=Count('id')).order_by('created_date')
        for da in data:
            resp['date'].append(da['created_date'].strftime("%d/%m/%y"))
            count.append(da['count'])
        resp['count'].append(count)
        return Tool.success_response_restful(resp)

    @action(detail=False, methods=['GET', ],
            authentication_classes=[JSONWebTokenAuthentication, SessionAuthentication])
    def money(self, request, *args, **kwargs):
        temp = OrderedDict()
        data = Order.objects.filter(created__gte=timezone.localtime(timezone.now()) - timezone.timedelta(days=15)) \
            .annotate(created_date=Trunc('created', 'day', output_field=DateTimeField())).order_by('created')
        for da in data:
            if not hasattr(temp, da.created_date.strftime("%d/%m/%y")):
                temp[da.created_date.strftime("%d/%m/%y")] = round(da.total_cost)
            else:
                temp[da.created_date.strftime("%d/%m/%y")] += round(da.total_cost)
        resp = {'date': temp.keys(), 'total': [temp.values()]}
        return Tool.success_response_restful(resp)


ExportCSVDateFormat = '%d/%m/%Y'


class StatusFilter(FilterSet):
    class Meta:
        model = Status
        fields = {'value': ['exact']}


class OrderFilter(FilterSet):
    status = ModelChoiceFilter(queryset=Status.objects.all())
    created = DateFromToRangeFilter()

    class Meta:
        model = Order
        fields = {
            'created_by': ['exact'],
            'id': ['exact'],
        }


class OrderItemResource(resources.ModelResource):
    class Meta:
        model = OrderItem

        fields = ['modified', 'name', 'status__label', 'image_url', 'item_url', 'currency__code',
                  'price', 'quantity', 'shipping', 'service_charge', 'tax_cost', 'weight',
                  'fragile', 'insurance', 'customer_note']
        export_order = ['modified', 'name', 'status__label', 'image_url', 'item_url',
                        'currency__code', 'price', 'quantity', 'shipping', 'service_charge',
                        'value', 'tax_cost', 'total', 'weight', 'fragile', 'insurance', 'customer_note']

        widgets = {
            'modified': {'format': ExportCSVDateFormat},
        }

    value = fields.Field(attribute='value', readonly=True)
    total = fields.Field(attribute='total', readonly=True)

    @staticmethod
    def dehydrate_weight(item):
        return '%s KG' % item.weight

    @staticmethod
    def dehydrate_fragile(item):
        return _('YES') if item.fragile else _('NO')

    @staticmethod
    def dehydrate_insurance(item):
        return _('YES') if item.insurance else _('NO')

    def get_export_headers(self):
        # headers = [force_text(field.column_name) for field in self.get_fields()]
        # return headers
        headers = []
        for field in self.get_fields():
            model_fields = self.Meta.model._meta.get_fields()
            header = next((x.verbose_name for x in model_fields if x.name == field.column_name), _(field.column_name))
            headers.append(header)
        logging.debug(headers)
        return headers


class XLSXRenderer(renderers.BaseRenderer):
    media_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    format = 'xlsx'
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data


class OrderDetailClientAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    """
    GET:
    """

    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderDetailSerializer

    def get_queryset(self):
        return Order.objects.filter(customer=self.request.user)

    @action(detail=True, methods=['GET', 'POST'], permission_classes=[permissions.IsAuthenticated],
            serializer_class=OrderDetailSerializer, )
    def make_prepaid_payment(self, request, pk):
        # ::type: Order
        order = self.get_object()
        if order.status.logic_step != LogicStep.pending:
            return Response({"error": _("Order status must be in pending state"), "code": 1}, status=400)

        balance = BalanceController.get_default_user_balance(self.request.user)
        if balance.currency != order.currency:
            return Response({"error": _("Currency of balance and order doesn't match")}, status=400)

        payment = CustomerPayment()
        payment.customer = payment.customer
        payment.order = order
        payment.type = PaymentType.objects.get_or_create(pk='prePaid', is_customerpayment=True,
                                                         defaults={'value': 'prePaid', 'label': 'Pre Paid',
                                                                   'is_customerpayment': 'True'})[0]

        # TODO: in_amount
        prepaid_amount = order.prepaid_percent * order.payment_left / 100

        if prepaid_amount <= 0 or prepaid_amount > order.payment_left:
            return Tool.error_response_restful("Unexpected prepaid ammount: %s" % prepaid_amount)

        try:
            PaymentController.atomic_transaction_for_payment(payment, balance, None, balance.currency, prepaid_amount,
                                                             balance.currency.exchange_rate,
                                                             detail="User make prepaid payment via api",
                                                             user=request.user)
        except Exception as e:
            logging.error("Unable to complete transaction: %s" % e)
            return Tool.error_response_restful("Unable to complete transaction")

        order.status = Status.objects.filter(logic_step=LogicStep.approved).first()
        order.save()

        return Tool.success_response_restful()

    @action(detail=True, methods=['GET', 'POST'], permission_classes=[permissions.IsAuthenticated],
            serializer_class=OrderDetailSerializer,
            renderer_classes=[XLSXRenderer], )
    def export_csv(self, request, pk):
        translation.activate("vi")
        # ::type: Order
        order = self.get_object()

        dataset = OrderItemResource().export(order.orderitem_set.all())
        header = ['created', 'order', 'status', 'customer', 'note', 'currency', '', '', '', '', 'total_item_cost',
                  'extra_charge', 'total_cost', 'tax_cost', 'grant_total', 'sum_payment_transaction', 'payment_left']
        value = [order.created.strftime(ExportCSVDateFormat), '#%s' % order.id, order.status, order.customer,
                 order.note, order.currency.code, '', '', '', '', order.total_item_cost, order.extra_charge,
                 order.total_cost, order.tax_cost, order.grant_total, order.sum_payment_transaction, order.payment_left]
        dataset.append([__(h) for h in header])
        dataset.append(value)
        return Response(dataset.xlsx)


class OrderItemDetailClientAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    """
    GET:
    """
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = OrderItemAdminSerializer
    queryset = OrderItem.objects.all()

    def get_serializer(self, *args, **kwargs):
        # partial update (one by one field update)
        kwargs['partial'] = True
        return super(OrderItemDetailClientAPIView, self).get_serializer(*args, **kwargs)

    @action(detail=True, methods=['GET'], serializer_class=ItemHistorySerializer)
    def history(self, request, pk):
        obj = self.get_object()
        history = obj.history.all()
        return Response(ItemHistorySerializer(history, many=True).data)


def handle_uploaded_file(f):
    wb = openpyxl.load_workbook(f)
    worksheet = wb["Sheet1"]
    print(worksheet)


class ExcelFileParser(FileUploadParser):
    media_type = ''


class FileUploadView(APIView):
    parser_classes = (FileUploadParser,)
    permission_classes = (permissions.IsAdminUser,)

    @staticmethod
    def put(self, request, form=None):
        file_obj = request.data['file']
        handle_uploaded_file(file_obj)
        return Response(status=204)
