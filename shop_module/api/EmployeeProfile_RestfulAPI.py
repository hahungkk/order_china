#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  EmployeeProfile_RestfulAPI.py
#  
#
#  Created by TVA on 10/23/15.
#  Copyright (c) 2015 order_china. All rights reserved.


from rest_framework import serializers, generics, permissions, viewsets

from ..models import EmployeeProfile


class EmployeeProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = EmployeeProfile
        fields = ('employeeProfile_url', 'full_name', 'phone_number', 'superior_employee', 'position',
                  'is_active')  # 'emergency_number','address'
        read_only_fields = ('employeeProfile_url', 'is_referrer')

    employeeProfile_url = serializers.HyperlinkedIdentityField(view_name='EmployeeProfile-detail', read_only=True)
    superior_employee = serializers.HyperlinkedRelatedField(view_name='EmployeeProfile-detail', read_only=True)
    position = serializers.StringRelatedField()


class EmployeeProfileDetailClientAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateAPIView):
    """A view that returns the count of active users in JSON.
    """

    serializer_class = EmployeeProfileSerializer

    permission_classes = (permissions.IsAuthenticated,)

    def get_queryset(self):
        return EmployeeProfile.objects.filter(user=self.request.user)
