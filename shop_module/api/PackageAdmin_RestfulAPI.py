#!/usr/bin/python
#  Created by Cherry
#  Copyright (c) 2015 order_china. All rights reserved.
from datetime import datetime

import pytz
from django.db import transaction
from django.utils import timezone
from rest_framework import serializers, generics, permissions, viewsets, status
from rest_framework.authentication import SessionAuthentication
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication
from rest_framework import exceptions
from shop_module.serializers.order_serializers import OrderItemAdminSerializer
from ..models import ShipmentPack, OrderItem, Package, User, Status

TZ = pytz.timezone('Asia/Ho_Chi_Minh')


class ExportSerializer(serializers.Serializer):
    def update(self, instance, validated_data):
        pass

    def create(self, validated_data):
        pass

    tracking_code = serializers.CharField()
    created = serializers.DateTimeField(read_only=True)
    modified = serializers.DateTimeField(read_only=True)
    note = serializers.CharField(read_only=True)


class ShipmentPackSerializer(serializers.ModelSerializer):
    class Meta:
        model = ShipmentPack
        fields = '__all__'


class ExportClientAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = ExportSerializer
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)

    def get_queryset(self):
        d = timezone.localtime(timezone.now()) - timezone.timedelta(days=15)
        return ShipmentPack.objects.filter(created__gte=d).order_by('-created')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        tracking_code = request.data['tracking_code']
        sp = ShipmentPack.objects.filter(tracking_code=tracking_code).order_by('-created')
        if sp.exists() and serializer.is_valid():
            items = OrderItem.objects.filter(shipment_pack=str(tracking_code), status='ShipToVN')
            with transaction.atomic():
                for item in items:
                    item.status = Status.objects.get(value='Completed')
                    item.exporter = request.user
                    item.exported_date = datetime.now()
                    item.save()
                return Response(OrderItemAdminSerializer(items, many=True).data,
                                status=status.HTTP_200_OK)
        else:
            return Response({"error": "Can not find this shipment pack !"},
                            status=status.HTTP_404_NOT_FOUND)


class PackageSerializer(serializers.ModelSerializer):
    created_by = serializers.SlugRelatedField(slug_field='username',
                                              queryset=User.objects.filter(is_staff=True))
    tracking = serializers.SerializerMethodField(read_only=True)

    class Meta:
        model = Package
        fields = ('id', 'label', 'version', 'created', 'modified', 'tracking', 'created_by', 'code', 'count_item', 'weight')

    @staticmethod
    def get_tracking(package):
        return ShipmentPackSerializer(package.shipment_set.all(), many=True).data

    def create(self, validated_data):
        instance = super(PackageSerializer, self).create(validated_data)
        instance.created_by = validated_data.get('created_by', None)
        instance.save()
        return instance


class PackageClientAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = PackageSerializer

    def get_queryset(self):
        return Package.objects.all().order_by('-created')


class PackageClientDetailAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    permission_classes = ()
    authentication_classes = ()
    serializer_class = PackageSerializer
    queryset = Package.objects.all()

    def get_serializer(self, *args, **kwargs):
        # partial update (one by one field update)
        kwargs['partial'] = True
        return super(PackageClientDetailAPIView, self).get_serializer(*args, **kwargs)


class ShipmentPackDetailAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated, )
    authentication_classes = (SessionAuthentication, JSONWebTokenAuthentication)
    serializer_class = ShipmentPackSerializer
    queryset = ShipmentPack.objects.all()

    def get_serializer(self, *args, **kwargs):
        # partial update (one by one field update)
        kwargs['partial'] = True
        return super(ShipmentPackDetailAPIView, self).get_serializer(*args, **kwargs)

    def perform_update(self, serializer):
        if serializer.data.get('package', None):
            if ShipmentPack.objects.filter(package_id=serializer.data.get('package')).exists():
                raise exceptions.PermissionDenied
        super(ShipmentPackDetailAPIView, self).perform_update(serializer)
