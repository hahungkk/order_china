# #!/usr/bin/python
# # -*- coding: utf-8 -*-
# #
# #  Category_RestfulAPI.py
# #
# #
# #  Created by TVA on 11/10/15.
# #  Copyright (c) 2015 order_china. All rights reserved.
#
# import django_filters
# from rest_framework import serializers, generics, viewsets, filters
# from rest_framework.decorators import detail_route
# from requests_browser.browser import parse_int
# from system_configure.controllers import Tool
# from ..models import Category
# from django_filters.rest_framework import DjangoFilterBackend
#
#
# class CategorySerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = ('code', 'label', 'parent', 'metadata')
#
#
# class CategoryListFilter(django_filters.FilterSet):
#     class Meta:
#         model = Category
#         fields = ['parent']
#
#     parent = django_filters.CharFilter(name="parent__code")
#
#
# class CategoryCollectionClientAPIView(viewsets.GenericViewSet, generics.ListAPIView):
#     """ A view that returns all currency data in JSON.
#     """
#
#     serializer_class = CategorySerializer
#
#     permission_classes = []
#
#     filter_backends = (DjangoFilterBackend, filters.SearchFilter)
#     filter_class = CategoryListFilter
#     search_fields = ('label', 'metadata')
#
#     def get_queryset(self):
#         return Category.objects.filter().order_by('order_index')
#
#
# class CategoryDetailSerializer(serializers.ModelSerializer):
#     class Meta:
#         model = Category
#         fields = ('code', 'label', 'metadata', 'children')
#         read_only_fields = ('code', 'label', 'metadata', 'children')
#
#     children = CategorySerializer(many=True, read_only=True)
#
#
# class CategoryDetail2LevelSerializer(CategoryDetailSerializer):
#     children = CategoryDetailSerializer(many=True, read_only=True)
#
#
# class CategoryDetail3LevelSerializer(CategoryDetailSerializer):
#     children = CategoryDetail2LevelSerializer(many=True, read_only=True)
#
#
# # TODO: there is a package to work arround recursive Serializer problem
#
# class CategoryDetailClientAPIView(viewsets.GenericViewSet, generics.RetrieveAPIView):
#     """
#     GET:
#     """
#
#     serializer_class = CategoryDetailSerializer
#
#     permission_classes = []
#
#     filter_backends = (DjangoFilterBackend, filters.SearchFilter)
#     filter_class = CategoryListFilter
#     search_fields = ('label', 'metadata')
#
#     def get_queryset(self):
#         return Category.objects.all()
#
#     lookup_field = 'code'  # default is pk, this is the field use to look for object
#
#     # TODO: use lookup_field instead do not use this cause it break permission
#     # def get_object(self):
#     # 	try:
#     # 		return self.get_queryset().get(code=self.kwargs['pk'])
#     # 	except Category.DoesNotExist:
#     # 		raise exceptions.NotFound
#
#     def get_serializer_class(self):
#         child_level = parse_int(self.request.GET.get('child_level', 1))
#         if child_level == 3:
#             return CategoryDetail3LevelSerializer
#         elif child_level == 2:
#             return CategoryDetail2LevelSerializer
#         else:
#             return self.serializer_class
#
#     @action(detail=True, methods=['get'], permission_classes=[])
#     def backTrace(self, request, pk=None):
#         try:
#             category = Category.objects.get(code=pk)
#         except Category.DoesNotExist:
#             return Tool.error_response_restful("Object not found", code=404)
#
#         slz = CategorySerializer(data=category.get_ancestors(ascending=False, include_self=True), many=True)
#         slz.is_valid()
#         return Tool.success_response_restful(slz.data)
#
#     @action(detail=True, methods=['get'], permission_classes=[])
#     def getDescendants(self, request, pk=None):
#         try:
#             category = Category.objects.get(code=pk)
#         except Category.DoesNotExist:
#             return Tool.error_response_restful("Object not found", code=404)
#
#         slz = CategorySerializer(data=category.get_descendants(include_self=True), many=True)
#         slz.is_valid()
#         return Tool.success_response_restful(slz.data)
