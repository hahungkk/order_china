import decimal
import uuid
from munch import Munch
from rest_framework import generics, exceptions, viewsets, status, permissions
from rest_framework.response import Response
from shop_module.controllers.OrderController import build_item_manual
from .Serializer import CartItemSerializer, FullCartItemSerializer
from system_configure.controllers import SystemConfigureController
from ..controllers import OrderController
from ..models import CartItem, Currency


class CartItemClientAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    serializer_class = CartItemSerializer
    # permission_classes = [permissions.IsAuthenticated]

    def get_serializer_class(self):
        return FullCartItemSerializer

    def get_queryset(self):
        if self.request.user.is_authenticated:
            return CartItem.objects.filter(customer=self.request.user.username)

    def perform_create(self, serializer):
        print(self.request.user)
        if not serializer.validated_data.get('http_referer'):
            serializer.validated_data['http_referer'] = self.request.META.get('HTTP_REFERER')
        if self.request.user.is_authenticated:
            if CartItem.objects.filter(
                    customer=self.request.user.username).count() >\
                    SystemConfigureController.get_configure('maximumNumberOfCartItem', 30):
                raise exceptions.NotAcceptable(detail="You have exceed maximum number of cart item!")
            serializer.save(customer=self.request.user)
        else:
            if len([key for key in self.request.session.keys() if
                    'cartItem_' in key]) > SystemConfigureController.get_configure('maximumNumberOfCartItem', 30):
                raise exceptions.NotAcceptable(detail="You have exceed maximum number of cart item!")
            cart_item_id = 'cartItem_' + str(uuid.uuid4().node)
            print(self.request.session.keys())
            self.request.session[cart_item_id] = serializer.data

    def list(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            OrderController.save_cartitem_in_session(self.request)
            queryset = self.filter_queryset(self.get_queryset())
            page = self.paginate_queryset(queryset)

            if page is not None:
                queryset_data = page
            else:
                queryset_data = queryset

            total_cost = 0
            currency_dict = {}
            for item in queryset_data:
                currency = currency_dict.get(item.currency, Currency.objects.get(pk=item.currency))
                total_cost += currency.convert(item.price * item.quantity + item.shipping)

            customer = self.request.user  # current user is owner of the cart item
            for item in queryset_data:
                item.service_charge, \
                    item.insurance_charge, \
                    item.fast_order_charge, \
                    item.bargain_charge = OrderController.calculate_service_charge2(
                        total_item_cost=total_cost,
                        price=item.price,
                        quantity=item.quantity,
                        insurance=item.insurance,
                        fast_order=item.fast_order,
                        bargain=item.bargain,
                        customer=customer)

            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)
            serializer = self.get_serializer(queryset, many=True)
            return Response(serializer.data)

        else:
            data_list = []
            total_cost = 0
            currency_dict = {}
            for key in self.request.session.keys():
                if 'cartItem_' in key:
                    data = Munch({'id': key[9:]})
                    data.update(self.request.session[key])
                    data_list.append(data)
                    # calculate total cost of this cart, get currency from database
                    currency = currency_dict.get(data.currency, Currency.objects.get(pk=data.currency))

                    try:
                        data.price = decimal.Decimal(data.get('price', 0))
                        data.quantity = int(data.get('quantity', 0))
                    except ValueError:
                        data.price = 0
                        data.quantity = 0
                    try:
                        data.shipping = decimal.Decimal(data.get('shipping', 0))
                    except ValueError:
                        data.shipping = 0

                    total_cost += currency.convert(data.price * data.quantity + data.shipping)

            # user is not authenticated so consider customer = None
            for data in data_list:
                data['service_charge'], data['insurance_charge'],\
                    data['fast_order_charge'], data['bargain_charge'] = OrderController.calculate_service_charge2(
                    total_cost, data['price'],
                    data['quantity'], data['insurance'], False, False)

            page = self.paginate_queryset(data_list)
            if page is not None:
                serializer = self.get_serializer(page, many=True)
                return self.get_paginated_response(serializer.data)
            else:
                serializer = self.get_serializer(data=data_list, many=True)
                serializer.is_valid(raise_exception=True)
                return Response(serializer.data)


class CartItemManualtAPIView(CartItemClientAPIView):
    def perform_create(self, serializer):
        """
        :type serializer: CartItemSerializer
        """
        if self.request.user.is_authenticated:
            if CartItem.objects.filter(
                    customer=self.request.user.username).count() >\
                    SystemConfigureController.get_configure('maximumNumberOfCartItem', 30):
                raise exceptions.NotAcceptable(detail="You have exceed maximum number of cart item!")
            serializer.save(customer=self.request.user)
            # send_to_chanel(self.request.user.username, CART_ITEM_ADDED_NOTIFY)
        else:
            raise exceptions.AuthenticationFailed

    def create(self, request, *args, **kwargs):
        data = build_item_manual(Munch(request.data))
        serializer = self.get_serializer(data=data)
        serializer.is_valid(raise_exception=True)
        self.perform_create(serializer)
        headers = self.get_success_headers(serializer.data)
        return Response(serializer.data, status=status.HTTP_201_CREATED, headers=headers)


class CartItemDetailClientAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    """
    GET:
    """
    # permission_classes = (permissions.IsAuthenticated, )
    serializer_class = CartItemSerializer

    def get_queryset(self):
        return CartItem.objects.filter(customer=self.request.user)

    def retrieve(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super(CartItemDetailClientAPIView, self).retrieve(request, *args, **kwargs)
        else:
            session_data = self.request.session.get('cartItem_' + kwargs['pk'])
            if not session_data:
                raise exceptions.NotFound
            data = {'id': kwargs['pk']}
            data.update(session_data)
            return Response(data)

    def destroy(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return super(CartItemDetailClientAPIView, self).destroy(request, *args, **kwargs)
        else:
            data = self.request.session.pop('cartItem_' + kwargs['pk'])
            if not data:
                raise exceptions.NotFound
            return Response(status=status.HTTP_204_NO_CONTENT)

    def update(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            quantity = request.data['quantity']
            if quantity == 0:
                instance = self.get_object()
                instance.delete()
                return Response({"status": "Deleted"})
            return super(CartItemDetailClientAPIView, self).update(request, *args, **kwargs)
        else:
            session_data = self.request.session.pop('cartItem_' + kwargs['pk'])
            if not session_data:
                raise exceptions.NotFound
            data = {'id': kwargs['pk']}
            data.update(session_data)
            partial = kwargs.pop('partial', False)
            serializer = self.get_serializer(data=request.data)
            serializer.is_valid(raise_exception=True)

            data.update(serializer.data)
            cart_item_id = 'cartItem_' + kwargs['pk']
            self.request.session[cart_item_id] = data
            return Response(data)
