import pytz
from django.http import HttpResponse
from rest_framework import serializers, generics, permissions, viewsets, status, renderers
from rest_framework.authentication import SessionAuthentication
from rest_framework.decorators import action
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from shop_module.serializers.order_serializers import OrderItemAdminSerializer
from shop_module.task import update_orderitem_status_from_checker
from ..models import ShipmentPack, TrackingChecker, VietNamTracking, Order, Status, OrderItem
import decimal
from django.utils import timezone
from import_export import resources, fields

TZ = pytz.timezone('Asia/Ho_Chi_Minh')


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 1000
    page_size_query_param = 'page_size'
    max_page_size = 10000


class TrackingCheckerSerializer(serializers.ModelSerializer):
    latest_version = serializers.SerializerMethodField()
    package = serializers.SerializerMethodField()

    @staticmethod
    def get_latest_version(tc):
        today = timezone.localtime(timezone.now()).date()
        return TrackingChecker.objects.filter(tracking_code=tc.tracking_code, created__gte=today).count()

    @staticmethod
    def get_package(tc):
        return tc.tracking_code.package.label if tc.tracking_code.package else None

    class Meta:
        fields = ('id', 'tracking_code', 'package', 'created', 'version', 'latest_version', 'checked')
        model = TrackingChecker
        read_only_fields = ('version',)


class TrackingCheckerClientAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = TrackingCheckerSerializer
    pagination_class = LargeResultsSetPagination
    filter_fields = ('checked',)

    def get_queryset(self):
        d = timezone.localtime(timezone.now()) - timezone.timedelta(days=15)
        return TrackingChecker.objects.filter(created__gte=d).order_by('-created')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        tracking_code = request.data['tracking_code']
        sp = ShipmentPack.objects.filter(tracking_code=tracking_code).order_by('-created')
        if sp.exists():
            update_orderitem_status_from_checker(tracking_code, 'ShipToCN')
            if serializer.is_valid():
                tc = TrackingChecker.objects.filter(tracking_code=tracking_code).order_by('-created')
                if tc.exists() and tc.first().created.date() == timezone.localtime(timezone.now()).date():
                    serializer.save(version=tc.first().version + 1)
                else:
                    serializer.save(version=1)
                return Response({"info": f"{tracking_code} has been Checked"},
                                status=status.HTTP_200_OK)
        else:
            shipment_pack = ShipmentPack.objects.create(tracking_code=tracking_code,
                                                        note="created from checking")
            if serializer.is_valid():
                serializer.save(tracking_code=shipment_pack, version=1)
                headers = self.get_success_headers(serializer.data)
                return Response({"info": f"{tracking_code} Created"},
                                status=status.HTTP_201_CREATED,
                                headers=headers)


class TrackingCheckerDeleteAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = TrackingCheckerSerializer
    queryset = TrackingChecker.objects.all()
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)

    def perform_destroy(self, instance):
        instance = TrackingChecker.objects.filter(tracking_code=instance.tracking_code).last()
        instance.delete()


class VietTrackingSerializer(serializers.ModelSerializer):
    latest_version = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_latest_version(tc):
        today = timezone.localtime(timezone.now()).date()
        return VietNamTracking.objects.filter(tracking_code=tc.tracking_code,
                                              created__gte=today).count()

    class Meta:
        model = VietNamTracking
        fields = ('id', 'tracking_code', 'created', 'weight',
                  'width', 'height', 'length', 'version', 'latest_version')
        read_only_fields = ('version',)


ExportCSVDateFormat = '%d/%m/%Y'


class VietTrackingResource(resources.ModelResource):
    order = fields.Field(column_name='order', readonly=True)
    service_charge = fields.Field(column_name='service_charge', readonly=True)
    customer = fields.Field(column_name='customer', readonly=True)

    class Meta:
        model = VietNamTracking

        fields = ['created', 'tracking_code', 'weight', 'width', 'height', 'length']

        export_order = ['created', 'tracking_code', 'order', 'customer', 'width', 'height',
                        'length', 'service_charge', 'weight']

        widgets = {
            'created': {'format': ExportCSVDateFormat},
        }

    def get_queryset(self):
        query_set = super(VietTrackingResource, self).get_queryset()
        today = timezone.localtime(timezone.now()).today()
        return query_set.filter(created_gte=today)

    @staticmethod
    def dehydrate_order(item):
        order = Order.objects.filter(orderitem__shipment_pack=str(item.tracking_code)).first()
        if order:
            return order.id
        return None

    @staticmethod
    def dehydrate_customer(item):
        order = Order.objects.filter(orderitem__shipment_pack=str(item.tracking_code)).first()
        if order:
            return order.customer
        return None

    @staticmethod
    def get_service_charge():
        return 0


class XLSXRenderer(renderers.BaseRenderer):
    media_type = 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet'
    format = 'xlsx'
    charset = None
    render_style = 'binary'

    def render(self, data, media_type=None, renderer_context=None):
        return data


class VietTrackingClientAPIView(viewsets.GenericViewSet, generics.ListCreateAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = VietTrackingSerializer
    pagination_class = LargeResultsSetPagination

    def get_queryset(self):
        today = timezone.localtime(timezone.now())
        d = today - timezone.timedelta(days=15)
        return VietNamTracking.objects.filter(created__gte=d).order_by('-created')

    def create(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)

        tracking_code = request.data['tracking_code']
        weight = request.data['weight']

        sp = ShipmentPack.objects.filter(tracking_code=tracking_code).order_by('-created')
        if sp.exists():
            if serializer.is_valid():
                sta = Status.objects.get(value='ShipToVN')
                orderitems = OrderItem.objects.filter(shipment_pack=tracking_code).update(status=sta)
                for item in orderitems:
                    item.importer = request.user
                    item.imported_date = timezone.localtime(timezone.now())
                    item.save()
                # tc = VietNamTracking.objects.filter(tracking_code=tracking_code).order_by('-created')
                # if tc.exists() and tc.first().created.date() == timezone.localtime(timezone.now()).date():
                #     track = serializer.save(version=tc.first().version + 1)
                # else:
                #     track = serializer.save(version=1)

                # save weight
                # if track.weight != 0 or track.weight != 0.0:
                ShipmentPack.objects.filter(tracking_code=tracking_code).update(weight=decimal.Decimal(weight))
                return Response({"info": f"{tracking_code} has been Checked"},
                                status=status.HTTP_200_OK)
        else:
            return Response({"info": f'{tracking_code} does not exists'},
                            status=status.HTTP_404_NOT_FOUND)

    @action(detail=False, methods=['GET'],
            permission_classes=[permissions.IsAuthenticated],
            renderer_classes=[XLSXRenderer])
    def export(self, request):
        queryset = self.get_queryset()
        dataset = VietTrackingResource().export(queryset)
        response = HttpResponse(dataset.xlsx, content_type='application/xlsx')
        response['Content-Disposition'] = 'attachment; filename=%s.xlsx' % timezone.localtime(timezone.now()).date()
        return response


class TrackingSerialzer(serializers.ModelSerializer):
    class Meta:
        model = ShipmentPack
        fields = ('note', 'width', 'height', 'length', 'weight')


class TrackingClientAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    """
    GET:
    """
    permission_classes = (permissions.IsAuthenticated,)
    authentication_classes = (JSONWebTokenAuthentication, SessionAuthentication)
    serializer_class = TrackingSerialzer

    def get_queryset(self):
        today = timezone.localtime(timezone.now())
        d = today - timezone.timedelta(days=30)
        return ShipmentPack.objects.all().order_by('-created')

    def update(self, request, *args, **kwargs):
        partial = kwargs.pop('partial', False)
        instance = self.get_object()
        serializer = self.get_serializer(instance, data=request.data, partial=partial)
        serializer.is_valid(raise_exception=True)
        self.perform_update(serializer)

        # update order items
        sta = Status.objects.get(value='ShipToVN')
        orderitems = OrderItem.objects.filter(shipment_pack=str(instance.tracking_code)).exclude(status=sta)
        # with transaction.atomic():
        #     for item in orderitems:
        #         item.importer = request.user
        #         item.status = sta
        #         item.imported_date = timezone.localtime(timezone.now())
        #         item.save()
        #     order = orderitems[0].order
        #     order_item_set = order.orderitem_set.exclude(status__value='Failed')
        #     if len(order_item_set.values('status').distinct()) < 2:
        #         order.status = sta
        #         order.save()
        #     else:
        #         print(order_item_set.values('status').distinct())

        if getattr(instance, '_prefetched_objects_cache', None):
            # If 'prefetch_related' has been applied to a queryset, we need to
            # forcibly invalidate the prefetch cache on the instance.
            instance._prefetched_objects_cache = {}
        data = OrderItemAdminSerializer(orderitems, many=True)
        return Response(data.data, status=status.HTTP_200_OK)


class VietTrackingDeleteAPIView(viewsets.GenericViewSet, generics.RetrieveUpdateDestroyAPIView):
    permission_classes = (permissions.IsAuthenticated,)
    serializer_class = VietTrackingSerializer
    queryset = VietNamTracking.objects.all()

    def perform_destroy(self, instance):
        instance = VietNamTracking.objects.filter(tracking_code=instance.tracking_code).last()
        instance.delete()
