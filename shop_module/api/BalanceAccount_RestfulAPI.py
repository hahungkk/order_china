#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  BalanceAccount_RestfulAPI.py
#  
#
#  Created by TVA on 5/22/16.
#  Copyright (c) 2016 order_china. All rights reserved.
#
import django_filters
from rest_framework import serializers, generics, permissions, viewsets, filters
from ..models import BalanceAccount
from django_filters.rest_framework import DjangoFilterBackend


class BalanceAccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = BalanceAccount

        fields = ['id', 'name', 'type', 'currency', 'amount', 'account_holder', 'metadata', 'info',]
        read_only_fields = fields

    account_holder = serializers.StringRelatedField()


class BalanceAccountListFilter(django_filters.FilterSet):
    class Meta:
        model = BalanceAccount
        fields = ['type', 'currency']


class BalanceAccountAPIView(viewsets.GenericViewSet, generics.ListAPIView):
    """ A view that returns data in JSON.
    """

    serializer_class = BalanceAccountSerializer

    permission_classes = [permissions.IsAuthenticated]

    filter_backends = (DjangoFilterBackend, filters.SearchFilter, filters.OrderingFilter)
    filter_class = BalanceAccountListFilter
    search_fields = ['name', ]
    ordering_fields = ['created', ]

    def get_queryset(self):
        return BalanceAccount.objects.filter(account_holder=self.request.user)

    # @action(detail=False, methods=['GET', 'POST'], permission_classes=[permissions.IsAuthenticated],
    # 			serializer_class=BalanceAccountSerializer, url_path='custom-list-action')
    # def custom_list_action(self, request, *args, **kwargs):
    # 	queryset = self.filter_queryset(self.get_queryset())
    # 	page = self.paginate_queryset(queryset)
    # 	if page is not None:
    # 		queryset = page
    # 	pass;  # TODO
