from order_china.routers import GroupedRouter
from shop_module.api.Order_Admin_RestfulAPI import OrderCollectionAdminAPI, OrderDetailAdminAPI, \
    OrderItemCollectionAdminAPI, OrderItemExportAdminAPI, StatusAPIView
from shop_module.api.PackageAdmin_RestfulAPI import ExportClientAPIView, PackageClientAPIView, \
    PackageClientDetailAPIView, ShipmentPackDetailAPIView
from .ShipmentPackage_RestfulAPI import (TrackingCheckerClientAPIView,
                                         TrackingCheckerDeleteAPIView,
                                         VietTrackingClientAPIView,
                                         VietTrackingDeleteAPIView, TrackingClientAPIView)

from .CartItem_RestfulAPI import CartItemClientAPIView, CartItemDetailClientAPIView, CartItemManualtAPIView
from .Order_RestfulAPI import (OrderClientAPIView,
                               OrderDetailClientAPIView,
                               OrderItemDetailClientAPIView)
from .CustomerProfile_RestfulAPI import CustomerProfileDetailClientAPIView
from .EmployeeProfile_RestfulAPI import EmployeeProfileDetailClientAPIView
from .Currency_RestfulAPI import CurrencyClientAPIView, CurrencyUpdateClientAPIView
from .Payment_RestfulAPI import CustomerPaymentAPIView, CustomerPaymentAdminAPIView, TransactionAPIView
from .BalanceAccount_RestfulAPI import BalanceAccountAPIView

router = GroupedRouter('shop_module')
router.register(r'cart', CartItemClientAPIView, basename='Cart')
router.register(r'cart-manual', CartItemManualtAPIView, basename='Cart')
router.register(r'cart', CartItemDetailClientAPIView, basename='Cart')

router.register(r'order', OrderClientAPIView, basename='Order')
router.register(r'order', OrderDetailClientAPIView, basename='Order')

router.register(r'order-admin', OrderCollectionAdminAPI, basename='OrderAdmin')
router.register(r'order-admin', OrderDetailAdminAPI, basename='OrderDetailAdmin')

router.register(r'items', OrderItemDetailClientAPIView, basename='OrderItemDetail')
router.register(r'items', OrderItemCollectionAdminAPI, basename='OrderItem')
router.register(r'exitems', OrderItemExportAdminAPI, basename='OrderItemExport')

# China tracking router
router.register(r'smp', TrackingCheckerClientAPIView, basename='ShipmentPack')
router.register(r'smp', TrackingCheckerDeleteAPIView, basename='ShipmentPack')

# vietnam tracking router
router.register(r'tracking', VietTrackingClientAPIView, basename='Tracking')
router.register(r'tracking', VietTrackingDeleteAPIView, basename='Tracking')


router.register(r'customer', CustomerProfileDetailClientAPIView, basename='CustomerProfile')
router.register(r'employee', EmployeeProfileDetailClientAPIView, basename='EmployeeProfile')

router.register(r'currency', CurrencyClientAPIView, basename='Currency')
router.register(r'currency', CurrencyUpdateClientAPIView, basename='Currency Update')

router.register(r'export', ExportClientAPIView, basename='Export')
router.register(r'import', TrackingClientAPIView, basename='Import')

router.register(r'package', PackageClientAPIView, basename='Package')
router.register(r'package', PackageClientDetailAPIView, basename='Package Detail')

router.register(r'shipment', ShipmentPackDetailAPIView, basename='Shipment Detail')

router.register(r'payment', CustomerPaymentAPIView, basename='Payment')
router.register(r'payment-admin', CustomerPaymentAdminAPIView, basename='Payment')
router.register(r'transaction', TransactionAPIView, basename='Transaction')
router.register(r'balance', BalanceAccountAPIView, basename='BalanceAccount')
router.register(r'status', StatusAPIView, basename='Status')

urlpatterns = router.urls
