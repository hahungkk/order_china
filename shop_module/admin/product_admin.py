# #!/usr/bin/python
# # -*- coding: utf-8 -*-
# #
# #  product_admin.py
# #
# #
# #  Created by TVA on 11/4/15.
# #  Copyright (c) 2015 order_china. All rights reserved.
#
# from django.contrib import admin
# # Register your models here.
# from ..models import Product, ProductOption, Category, Vendor
# from django.utils.translation import ugettext_lazy as _
# from mptt.admin import DraggableMPTTAdmin
# from django.utils.html import format_html
# from mptt.exceptions import InvalidMove
# from suit.admin import SortableModelAdmin
#
# admin.site.register(Vendor)
# admin.site.register(ProductOption)
#
#
# class SortableModelAdminForMPTT(SortableModelAdmin):
#     def __init__(self, *args, **kwargs):
#         super().__init__(*args, **kwargs)
#         # self.ordering = (self.sortable,)
#         if self.list_display and self.sortable not in self.list_display:
#             self.list_display = list(self.list_display) + [self.sortable]
#
#         self.list_editable = self.list_editable or []
#         if self.sortable not in self.list_editable:
#             self.list_editable = list(self.list_editable) + [self.sortable]
#
#         self.exclude = self.exclude or []
#         if self.sortable not in self.exclude:
#             self.exclude = list(self.exclude) + [self.sortable]
#
#     def get_changelist(self, request, **kwargs):
#         from django.contrib.admin.views.main import ChangeList
#         return ChangeList
#
#
# @admin.register(Category)
# class CategoryAdmin(DraggableMPTTAdmin, SortableModelAdminForMPTT):  #
#     class Media:
#         css = {
#             'all': (
#                 '/static/shop_admin_mock/test_mock.css',  # project static folder
#             )
#         }
#         js = (
#             '/static/shop_admin_mock/test_mock.js',  # project static folder
#
#         )
#
#     mptt_level_indent = 15
#     mptt_dash_indent = 3
#
#     search_fields = ['label']
#     list_display = ['tree_actions', 'dash_indent', 'label']  # , 'parent' 'order_index'
#     list_display_links = ['dash_indent']  # 'indented_title',
#     # list_filter = ()
#     list_editable = ['label']  # , 'parent'
#     readonly_fields = []  # 'order_index'
#
#     actions = ['mass_change_selected']
#
#     sortable = 'order_index'
#
#     def dash_indent(self, instance):
#         space_indent = instance._mpttfield('level') * self.mptt_level_indent
#         dashed_indent = instance._mpttfield('level') * self.mptt_dash_indent
#         return format_html(
#             '<div style="text-indent:0px">{} {}</div>',
#             '-' * dashed_indent,
#             instance,
#         )
#     dash_indent.short_description = _('dash indent')
#
#     def save_model(self, request, obj, form, change):
#         """
#
#         :param request:
#         :param obj:
#         :type obj: Category
#         :param form:
#         :param change:
#         :return:
#         """
#         try:
#             super(CategoryAdmin, self).save_model(request, obj, form, change)
#         except InvalidMove:
#             Category.objects.rebuild()
#             try:
#                 super(CategoryAdmin, self).save_model(request, obj, form, change)
#             except InvalidMove as e:
#                 pass
#             # save category parrent to insert after create to avoid bug
#             # if not obj.pk and obj.parent:
#             # 	# parent = obj.parent
#             # 	# obj.parent = None
#             # 	# obj.save();
#             # 	#insert to category parent
#             # 	# setattr(obj, Category._mptt_meta.tree_id_attr, 0);
#             # 	# obj.refresh_from_db()
#             # 	# Category.objects.rebuild();
#             # 	try:
#             # 		obj.move_to(obj.parent) #, position='last-child'
#             # 	except InvalidMove as e:
#             # 		pass;
#             # else:
#             # 	raise e;
#             # obj.parent = parent
#             # obj.save();
#
#         Category.objects.rebuild()
#
#
# @admin.register(Product)
# class ProductAdmin(admin.ModelAdmin):
#     class Media:
#         css = {
#             'all': (
#                 '/static/shop_admin_mock/test_mock.css',  # project static folder
#             )
#         }
#         js = (
#             '/static/shop_admin_mock/test_mock.js',  # project static folder
#
#         )
#
#     search_fields = ['name', ]
#     list_display = ['__str__', 'sku', 'image_tag', 'currency', 'price', 'inventory_quantity', 'is_hidden']
#     list_filter = ['price', 'shopping_domain', 'is_hidden', 'categories', 'options_selected']
#     list_editable = ['inventory_quantity', 'is_hidden']
#
#     readonly_fields = ['image_tag', 'html_cache_path']
#     prepopulated_fields = {'slug': ['name', 'sku']}
#     fieldsets = [
#         (None, {
#             # 'classes': ('suit-tab', 'suit-tab-general',),
#             'fields': ['name', 'slug', 'detail_url', 'sku',
#                        ('image_url', 'image_tag'),
#                        'short_description',
#                        'categories', 'options_selected',
#                        'currency',
#                        ('price', 'shipping'),
#                        ('inventory_quantity', 'weight')
#                        ]
#         }),
#         (_('Detail'), {
#             'classes': ('collapse',),
#             'fields': ['shopping_domain', 'vendor', 'options_metadata', 'html_cache_path']
#         }),
#     ]
