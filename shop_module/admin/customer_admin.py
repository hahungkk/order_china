#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  customer_admin.py
#
#
#  Created by TVA on 3/28/16.
#  Copyright (c) 2016 ordercn. All rights reserved.

# Register your models here.
from django.contrib import admin
from django.http import HttpResponse
from import_export import resources
from ..models import CustomerProfile, CartItem
from django.utils.translation import ugettext_lazy as _
from django.forms import modelformset_factory
from django import forms


def export_excel(modeladmin, request, queryset):
    data = CustomUserResource().export(CustomUserResource().get_queryset())
    response = HttpResponse(data.xlsx, content_type="application/octet-stream")
    response['Content-Disposition'] = 'attachment; filename=%s' % 'User.xlsx'
    return response


class CustomerProfileForm(forms.ModelForm):

    def __init__(self, *args, **kwargs):
        super(CustomerProfileForm, self).__init__(*args, **kwargs)
        referrer_queryset = CustomerProfile.objects.all().exclude(user=self.instance.user)
        self.fields['referrer'].queryset = referrer_queryset

    class Meta:
        model = CustomerProfile
        fields = ['vip_status', 'referrer', 'city', 'district', 'street',
                  'receive_sms_notify', 'receive_email_notify', 'gender', 'about']


@admin.register(CustomerProfile)
class CustomerProfileAdmin(admin.ModelAdmin):
    class Media:
        def __init__(self):
            pass

        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder

        )

    search_fields = ['user__username', 'user__profile__full_name', 'user__profile__phone_number',
                     'vip_status']
    list_display = ['__str__', 'full_name', 'phone_number', 'referrer', 'supporter_employee',
                    'facebook', 'vip_status']
    list_filter = ['supporter_employee', 'is_referrer']
    list_editable = ['supporter_employee']

    actions = [export_excel]

    form = CustomerProfileForm

    def get_changelist_formset(self, request, **kwargs):
        if request.user.is_superuser:
            list_editable = ['supporter_employee']
        else:
            list_editable = []
        return modelformset_factory(CustomerProfile, fields=list_editable)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = ['full_name', 'phone_number', 'address', 'facebook', 'is_referrer']
        if not request.user.is_superuser:
            readonly_fields += ['supporter_employee', 'vip_status']
        return readonly_fields

    fieldsets = [
        (None, {
            'classes': ('suit-tab', 'suit-tab-general',),
            'fields': ['full_name', 'vip_status', 'facebook', 'referrer', 'is_referrer',
                       'supporter_employee', 'phone_number', 'address', 'city', 'district', 'street',
                       'receive_sms_notify', 'receive_email_notify', 'gender', 'about']
        }),
    ]

    suit_form_tabs = (('general', _('General')), ('supporter', _('Supporter')))

    def save_model(self, request, obj, form, change):
        super(CustomerProfileAdmin, self).save_model(request, obj, form, change)


@admin.register(CartItem)
class CartItemAdmin(admin.ModelAdmin):
    search_fields = ['name', ]
    list_display = ['__str__', 'price', 'quantity']


class CustomUserResource(resources.ModelResource):
    class Meta:
        model = CustomerProfile
        fields = ['user__username', 'user__email', 'user__profile__full_name',
                  'user__profile__phone_number', 'vip_status']
        export_order = ['user__username', 'user__email', 'user__profile__full_name',
                        'user__profile__phone_number', 'vip_status']

    def get_queryset(self):
        return CustomerProfile.objects.all()
