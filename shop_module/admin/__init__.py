#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  __init__.py
#
#
#  Created by TVA on 3/28/16.
#  Copyright (c) 2016 ordercn. All rights reserved.
#

from . import (customer_admin,
               logistic_admin,
               order_admin,
               payment_admin,
               product_admin,
               shop_admin,
               staff_admin)
