#!/usr/bin/python
#  Created by Cherry
# limit status choice form for special permission
from dal import autocomplete
from django import forms
from django.db.models import Q
from shop_module.enums import *
from shop_module.models import Status, Order, OrderItem, ShipmentPack


class LimitStatusOrderOnlyChoiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LimitStatusOrderOnlyChoiceForm, self).__init__(*args, **kwargs)
        if self.instance.id and self.instance.status:
            q = Q(is_orderstatus=True) | Q(value=self.instance.status.value)
            self.fields['status'].queryset = Status.objects.filter(q)

    class Meta:
        model = Order
        fields = '__all__'


class LimitStatusOrderItemOnlyChoiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(LimitStatusOrderItemOnlyChoiceForm, self).__init__(*args, **kwargs)
        if self.instance.id and self.instance.status:
            q = Q(is_orderstatus=True) | Q(value=self.instance.status.value)
            self.fields['status'].queryset = Status.objects.filter(q)

    class Meta:
        model = OrderItem
        fields = '__all__'


class PrepaidStatusOnlyChoiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(PrepaidStatusOnlyChoiceForm, self).__init__(*args, **kwargs)
        if self.instance.id and self.instance.status:
            q = Q(value=PREPAID_STATUS) | Q(value=self.instance.status.value)
            self.fields['status'].queryset = Status.objects.filter(q)

    class Meta:
        model = Order
        fields = '__all__'


class CheckerStatusChoiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(CheckerStatusChoiceForm, self).__init__(*args, **kwargs)
        if self.instance.id and self.instance.status:
            q2 = Q(value=SHIP_TO_VN_STATUS) | Q(value=COMPLETED_STATUS) | Q(value=self.instance.status.value)
            self.fields['status'].queryset = Status.objects.filter(q2)

    class Meta:
        model = Order
        fields = '__all__'


class OrdererStatusChoiceForm(forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(OrdererStatusChoiceForm, self).__init__(*args, **kwargs)
        if self.instance.id and self.instance.status:
            self.fields['status'].queryset = Status.objects.filter(value=self.instance.status.value)

    class Meta:
        model = Order
        fields = '__all__'
