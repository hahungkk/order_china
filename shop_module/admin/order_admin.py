import logging
from typing import List

from audit_log.middleware import disable_audit_log, enable_audit_log
from concurrency.admin import ConcurrencyActionMixin, ConcurrencyListEditableMixin
from date_range_filter import DateRangeFilter
from django.conf import settings
from django.contrib import admin
from django.urls import reverse
from django.db import transaction
from django.db.models import Sum
from django.forms import modelformset_factory
from django.utils import html
from django.utils.translation import ugettext_lazy as _
from django_object_actions import DjangoObjectActions
from import_export.admin import ExportActionModelAdmin

from shop_module.admin_table import TrackingTable, OrderResource, SumByCurrencyTable, SumByCurrencyTableChecker
from .forms import LimitStatusOrderOnlyChoiceForm, LimitStatusOrderItemOnlyChoiceForm
from ..enums import *
from ..models import Order, OrderItem, CustomerPayment, Currency, ShipmentPack
from ..utils.AuditLogStaffOnlyListFilter import CreatedByStaffListFilter, ModifiedByStaffListFilter


@admin.register(OrderItem)
class OrderItemAdmin(admin.ModelAdmin, ConcurrencyActionMixin, ConcurrencyListEditableMixin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/order_item.css',
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
            '/static/shop_admin_mock/orderitem_admin_mock.js',
        )

    search_fields = ['id', 'order_code', 'shipment_pack', 'order__id']

    list_display = ('id', 'order_id_tag', 'image_tag', 'shipment_pack', 'quantity',
                    'options_selected', 'status', 'customer', 'weight', 'shipvn',
                    'count_order_quantity')

    list_filter = ['status', 'company']
    list_editable = ['status', 'shipment_pack', 'weight']

    readonly_fields = ('image_tag', 'image_inline_tag', 'name_tag', 'quantity', 'price_tag', 'shipping',
                       'customer_note', 'options_selected', 'status_table', 'customer', 'shipvn')
    fields = ['name_tag', 'order_employee',
              'options_selected',
              ('image_inline_tag', 'status'),
              ('shipment_pack', 'order_code', 'company'),
              'status_table',
              ('weight', 'fragile', 'insurance', 'fast_order', 'bargain'),
              ('currency', 'exchange_rate'),
              ('service_charge', 'insurance_charge', 'fast_order_charge', 'bargain_charge', 'tax_cost'),
              ('customer_note', 'note'),
              'quantity', 'order_quantity', 'paid_quantity', 'price_tag', 'order_price',
              'paid_price', 'shipping', 'order_shipping', 'paid_shipping',
              ]

    form = LimitStatusOrderItemOnlyChoiceForm

    def get_queryset(self, request):
        qs = OrderItem.objects.all().select_related('status', 'currency')
        return qs

    def get_changelist_form(self, request, **kwargs):
        return LimitStatusOrderItemOnlyChoiceForm

    def get_list_display_links(self, request, list_display):
        return ['id']  # don't display change link

    def order_id_tag(self, orderitem):
        return "#%d" % orderitem.order.id

    order_id_tag.short_description = _("Order ID")

    def name_tag(self, orderitem):
        text = "(%d) [%s]: %s" % (orderitem.id, orderitem.shopping_domain, orderitem.name)
        if len(text) > 100:
            text = text[:100] + '...,'
        return text

    name_tag.short_description = _("Name")

    def status_table(self, obj):
        if obj.shipment_pack is not None:
            data = ShipmentPack.objects.filter(tracking_code=obj.shipment_pack).first()
            if isinstance(data, ShipmentPack):
                if data.status_set.first():
                    return data.status_set.first().context
                return None
            return None
        return None

    status_table.short_description = _("Tracking Status")

    def count_order_quantity(self, obj):
        if obj.shipment_pack is not None:
            count = OrderItem.objects.filter(shipment_pack=obj.shipment_pack) \
                .exclude(status__value='Failed') \
                .aggregate(count=Sum('quantity'))
            return count['count']
        return 0

    count_order_quantity.short_description = _("Count By Tracking Code")

    @staticmethod
    def suit_row_attributes(obj, request):
        """

        :param obj:
        :type obj: Order
        :param request:
        :return:
        """
        css_class = {
            LogicStep.approved: 'info',
            LogicStep.processing: 'warning',
            LogicStep.completed: 'success',
            LogicStep.failed: 'error',
        }.get(obj.status.logic_step)
        if css_class:
            return {'class': css_class}

    def customer(self, obj):
        return obj.order.customer

    customer.short_description = _("Customer")

    @staticmethod
    def suit_cell_attributes(obj, column):
        if column == 'list_status_table':
            return {'class': 'wide'}

    def log_change(self, request, obj, message):
        pass


class OrderItemInline(admin.StackedInline, ConcurrencyActionMixin, ConcurrencyListEditableMixin):
    model = OrderItem
    show_change_link = False
    can_delete = False
    min_num = 1
    extra = 0
    ordering = ('id',)

    readonly_fields = ('image_inline_tag', 'item_url_tag', 'customer_note', 'options_selected',
                       'total_tag', 'total_order_currency_tag', 'option_selected_tag',
                       'status_table', 'shipvn')

    suit_classes = 'suit-tab suit-tab-OrderItem'

    fieldsets = [
        (None, {
            'classes': (),
            'fields': [
                'image_inline_tag',
                'option_selected_tag',
                ('quantity', 'price', 'shipping', 'shipvn'),
                ('status', 'shipment_pack', 'order_code'),
                'status_table',
                'customer_note',
                'note',
                ('weight', 'fragile', 'insurance', 'fast_order', 'bargain'),
                ('currency', 'exchange_rate'),
                ('order_quantity', 'order_price', 'order_shipping',),
                ('paid_quantity', 'paid_price', 'paid_shipping'),
                ('service_charge', 'insurance_charge', 'fast_order_charge', 'bargain_charge', 'tax_cost'),
                ('total_order_currency_tag', 'total_tag')
            ]
        }),
        (_('Extra info'), {
            'classes': ('collapse',),
            'fields': [
                'name',
                ('item_url_tag', 'image_url'),
                ('order_item_url', 'vendor'),
            ]
        }),
    ]

    form = LimitStatusOrderItemOnlyChoiceForm

    def status_table(self, obj):
        if obj.shipment_pack is not None:
            data = ShipmentPack.objects.filter(tracking_code=obj.shipment_pack).first()
            if isinstance(data, ShipmentPack):
                if data.status_set.all():
                    return TrackingTable(data.status_set.all()).as_html(request=None)
                return None
            return None
        return None

    status_table.short_description = _("Tracking Status")

    def get_fieldsets(self, request, obj=None):
        fieldsets = self.fieldsets
        if request.user.groups.filter(name=ITEM_CHECKER_PERM).exists():
            fieldsets = [
                (None, {
                    'classes': (),
                    'fields': [
                        'image_inline_tag',
                        'option_selected_tag',
                        ('quantity', 'price', 'shipping', 'shipvn'),
                        ('status', 'shipment_pack', 'order_code'),
                        'customer_note',
                        'note',
                        ('weight', 'fragile', 'insurance', 'fast_order', 'bargain'),
                        ('currency', 'exchange_rate'),
                        ('paid_quantity', 'paid_price', 'paid_shipping'),
                        ('tax_cost', 'insurance_charge'),
                        ('total_order_currency_tag', 'total_tag')
                    ]
                }),
                (_('Extra info'), {
                    'classes': ('collapse',),
                    'fields': [
                        'name',
                        ('item_url_tag', 'image_url'),
                        ('order_item_url', 'vendor'),
                    ]
                }),
            ]
        return fieldsets

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = self.readonly_fields
        if request.user.groups.filter(name=ITEM_CHECKER_PERM).exists():
            readonly_fields = ('image_inline_tag', 'item_url_tag', 'customer_note', 'options_selected',
                               'total_tag', 'total_order_currency_tag', 'option_selected_tag',
                               'quantity', 'price', 'shipping', 'weight', 'fragile', 'insurance',
                               'currency', 'exchange_rate', 'paid_quantity', 'paid_price', 'paid_shipping',
                               'tax_cost', 'shipvn')
            return readonly_fields
        if request.user.groups.filter(name=ORDERER_PERM).exists():
            readonly_fields += ('service_charge',)
        return readonly_fields


class CustomerPaymentInline(admin.TabularInline):
    model = CustomerPayment
    fk_name = "order"
    show_change_link = True
    can_delete = False
    min_num = 1
    extra = 0

    fields = ['type', '__str__', 'note', 'created', 'created_by']
    readonly_fields = ['__str__', 'created', 'created_by']

    suit_classes = 'suit-tab suit-tab-CustomerPayment'

    def has_add_permission(self, request, obj=None):
        return False


@admin.register(Order)
class OrderAdmin(ExportActionModelAdmin, DjangoObjectActions):
    class Media:
        def __init__(self):
            pass

        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
                '/static/shop_admin_mock/order_admin_mock.css',
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
            '/static/shop_admin_mock/order_admin_mock.js',
        )

    search_fields = ['id', 'customer__username', 'customer__email', 'receiver_name', 'receiver_phone']
    list_display = ['__str__', 'customer', 'count_item', 'order_cost', 'total_cost_tag',
                    'sum_payment_transaction_tag', 'sum_item_service_charge_orig_tag', 'created', 'created_by',
                    'latest_transaction_date', 'ordered_date', 'shiptovn_date']
    list_filter = ('status', ('created', DateRangeFilter), CreatedByStaffListFilter, ModifiedByStaffListFilter)
    list_editable = ['created_by']

    inlines = [
        CustomerPaymentInline,
        OrderItemInline,
    ]

    form = LimitStatusOrderOnlyChoiceForm
    resource_class = OrderResource
    suit_form_tabs = (('general', _('General')), ('OrderItem', _('Order Item')), ('CustomerPayment', _('Payment')),
                      ('ShipmentPackageInline', _('Shipment')))

    def get_queryset(self, request):
        qs = super(OrderAdmin, self).get_queryset(request)
        # optimize queryset
        qs = qs.select_related('status', 'currency')
        return qs

    # get_list_editable
    def get_changelist_formset(self, request, **kwargs):
        if request.user.is_superuser or request.user.groups.filter(name=ACCOUNTANT_CE_PERM).exists():
            list_editable = ['created_by']
        else:
            list_editable = []
        return modelformset_factory(Order, fields=list_editable)

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = [
            'created', 'modified', 'id', 'count_item_tag', 'count_payment',
            'customer_admin_link_tag', 'user_facebook_tag', 'phone_tag', 'sum_fast_order_charge_tag',
            'sum_item_cost_tag', 'sum_item_shipping_tag', 'sum_item_service_charge_tag', 'total_cost_tag',
            'total_item_cost_tag', 'grant_total_tag', 'sum_cost_by_weight_tag', 'redude_cost_tag',
            'sum_payment_transaction_tag', 'payment_left_tag', 'display_sum_by_currency', 'customer_id',
            'display_sum_by_currency_checker', 'insurance_charge', 'ship_to_vn', 'order_weight_by_track',
            'sum_bargain_charge_tag'
        ]

        if obj:  # readonly when edit
            readonly_fields += ['customer', 'customer_note']
        return readonly_fields

    def get_fieldsets(self, request, obj=None):
        tab1_fields = [
            'ship_to_vn',
            'customer', 'phone_tag', 'user_facebook_tag',
            'created', 'modified', 'grant_total_tag', 'note'
        ]
        tab1_fields_collapse = [
            'delivery_method', 'delivery_address', 'city', 'district', 'street', 'receiver_name', 'receiver_phone',
        ]
        tab2_fields = [
            'status',
            ('customer_note', 'note'),
            'count_item_tag',
            'sum_item_cost_tag', 'sum_item_shipping_tag',
            'insurance_charge',
            'sum_fast_order_charge_tag',
            'sum_bargain_charge_tag',
            'sum_item_service_charge_tag', 'total_item_cost_tag',
            'display_sum_by_currency'
        ]

        tab3_fields = [('currency', 'exchange_rate'), 'extra_charge',
                       'prepaid_percent', 'total_item_cost_tag',
                       'total_cost_tag', 'tax_cost', 'sum_cost_by_weight_tag',
                       'redude_cost_tag', 'grant_total_tag',
                       'sum_payment_transaction_tag', 'payment_left_tag']

        tab4_fields = ['count_item_tag', ('order_weight', 'cost_per_weight'),
                       'order_weight_by_track', 'sum_cost_by_weight_tag']

        fieldsets = [
            (None, {
                'classes': ('suit-tab', 'suit-tab-general',),
                'fields': tab1_fields
            }),
            (_('Delivery info'), {
                'classes': ('suit-tab', 'suit-tab-general', 'collapse',),
                'fields': tab1_fields_collapse
            }),

            (_('Order Info'), {
                'classes': ('suit-tab', 'suit-tab-OrderItem'),
                'fields': tab2_fields
            }),

            (_('Payment'), {
                'classes': ('suit-tab', 'suit-tab-CustomerPayment'),
                'fields': tab3_fields
            }),

            (_('Shipment'), {
                'classes': ('suit-tab', 'suit-tab-ShipmentPackageInline'),
                'fields': tab4_fields
            }),
        ]

        # Check permission
        if request.user.groups.filter(name=ITEM_CHECKER_PERM).exists():
            tab2_fields = [
                'status',
                ('customer_note', 'note'),
                'count_item_tag',
                'sum_item_cost_tag', 'sum_item_shipping_tag', 'sum_item_service_charge_tag', 'total_item_cost_tag',
                'display_sum_by_currency_checker'
            ]
            fieldsets = [
                (None, {
                    'classes': ('suit-tab', 'suit-tab-general',),
                    'fields': tab1_fields
                }),
                (_('Delivery info'), {
                    'classes': ('suit-tab', 'suit-tab-general', 'collapse',),
                    'fields': tab1_fields_collapse
                }),

                (_('Order Info'), {
                    'classes': ('suit-tab', 'suit-tab-OrderItem'),
                    'fields': tab2_fields
                })
            ]

        if obj:  # readonly when edit but still display link to this customer instead of just name
            tab1_fields[tab1_fields.index('customer')] = 'customer_admin_link_tag'
            tab3_fields.insert(0, 'id')
            tab3_fields.insert(0, 'customer_id')
            tab3_fields.insert(0, 'customer_admin_link_tag')
        return fieldsets

    def customer_admin_link_tag(self, obj):
        change_url = reverse('admin:shop_module_customerprofile_change',
                             args=(obj.customer.customerProfile.id,))
        return html.format_html('<a href="{0}" target="blank">{1} ({2})</a>',
                                change_url,
                                str(obj.customer),
                                obj.customer.customerProfile.full_name
                                if obj.customer.customerProfile.full_name else '')

    customer_admin_link_tag.short_description = _('Customer')

    def display_sum_by_currency(self, obj):
        table_data = []
        for row in obj.sum_by_currency:
            try:
                currency = Currency.objects.get(pk=row['currency'])
            except Currency.DoesNotExist:
                table_data.append(row)
                continue
            row_data = {}
            for key, value in list(row.items()):
                if key == 'currency':
                    row_data[key] = value
                else:
                    row_data[key] = currency.display(value)
            table_data.append(row_data)
        table = SumByCurrencyTable(table_data)
        return table.as_html(request=None)

    display_sum_by_currency.short_description = _('Sum by currency')

    def display_sum_by_currency_checker(self, obj):
        table_data = []

        for row in obj.sum_by_currency:
            try:
                currency = Currency.objects.get(pk=row['currency'])
            except Currency.DoesNotExist:
                table_data.append(row)
                continue
            row_data = {}
            for key, value in list(row.items()):
                if key == 'sum_order_cost':
                    continue
                if key == 'currency':
                    row_data[key] = value
                else:
                    row_data[key] = currency.display(value)
            table_data.append(row_data)

        table = SumByCurrencyTableChecker(table_data)
        return table.as_html(request=None)

    display_sum_by_currency_checker.short_description = _('Sum by currency checker')

    @staticmethod
    def suit_row_attributes(obj, request):
        """

        :param obj:
        :type obj: Order
        :param request:
        :return:
        """
        if not request:
            print('no request')
        css_class = {
            LogicStep.approved: 'info',
            LogicStep.processing: 'warning',
            LogicStep.completed: 'success',
            LogicStep.failed: 'error',
            # LogicStep.not_completed: 'danger',
        }.get(obj.status.logic_step)
        if obj.status.logic_step == LogicStep.completed and obj.payment_left > \
                getattr(settings, 'MARGIN_MISSING_IN_CUSTOMER_PAYMENT', 10 ** 4):
            css_class = 'success-wrong'
            return {'class': css_class}
        if obj.status.value in ['WaitingForPrePaid', 'PrePaid'] and obj.orderitem_set.filter(fast_order=True).exists():
            if css_class:
                css_class += ' fast-order'
            else:
                css_class = 'fast-order'
        if obj.payment_left < 0 and css_class:
            css_class += ' change'
        if css_class:
            return {'class': css_class}

    def save_model(self, request, obj, form, change):
        """
        :param request:
        :param obj:
        :type obj: Order
        :param form:
        :param change:
        :return:
        """
        check_set = {'id', 'created_by', 'modified_by'}
        # order_item_set = obj.orderitem_set.all()
        # if len(order_item_set.values('status').distinct()) < 2:
        #     print(obj.status)
        #     obj.status = order_item_set.first().status
        field_name_set = set(form.fields.keys())
        if field_name_set.issubset(check_set):
            disable_audit_log(obj)
            # import ipdb;ipdb.set_trace(context=10)
            obj.save()
            enable_audit_log(obj)
        else:
            obj.save()

    def save_formset(self, request, form, formset, change):
        """
        Given an inline formset save it to the database.
        """
        instances = formset.save()
        try:
            if instances:
                order = instances[0].order
                orderitem = order.orderitem_set.all()
                mod = list(set([item.status for item in orderitem]))
                if len(mod) == 1:
                    order.status = mod[0]
                    with transaction.atomic():
                        order.save()
        except Exception as e:
            logging.debug(e)

    def log_change(self, request, obj, message):
        """ Log that an object has been successfully changed.
        The default implementation creates an admin LogEntry object.
        This is an override to add more context information
        :param request:
        :param obj:
        :type obj: Order
        :param message:
        :return:
        """
        pass

    actions = ['createShipmentPackage']
