#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  staff_admin.py
#  
#
#  Created by TVA on 3/28/16.
#  Copyright (c) 2016 ordercn. All rights reserved.
#

from django.contrib import admin
# Register your models here.
from ..models import EmployeeProfile, EmployeePosition
from django.utils.translation import ugettext_lazy as _
from suit.admin import SortableModelAdmin


@admin.register(EmployeeProfile)
class EmployeeProfileAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder

        )

    search_fields = ['user__username', ]
    list_display = ['__str__', 'full_name', 'position', 'is_active']
    list_filter = [
        'is_active',
        ('position', admin.RelatedOnlyFieldListFilter),
    ]
    list_editable = ['position']
    readonly_fields = ['full_name', 'phone_number', 'address']
# actions = ['']


@admin.register(EmployeePosition)
class EmployeePositionAdmin(SortableModelAdmin):
    search_fields = ['title', ]
    list_display = ('__str__',)

    # list_filter = ()
    # list_editable = []
    # actions = ['']

    sortable = 'order_index'
