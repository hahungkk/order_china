#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  shop_admin.py
#
#
#  Created by TVA on 3/28/16.
#  Copyright (c) 2016 ordercn. All rights reserved.
#
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
# Register your models here.
from ..models import Currency, Status


@admin.register(Currency)
class CurrencyAdmin(admin.ModelAdmin):
    list_display = ('__str__', 'code', 'exchange_rate')
    list_editable = ['exchange_rate']
    sortable = 'order_index'


@admin.register(Status)
class StatusAdmin(admin.ModelAdmin):
    # search_fields = ['title',]
    list_display = ('__str__', 'logic_step', 'default', 'is_orderstatus', 'is_shipmentstatus')
    list_filter = ('logic_step', 'is_orderstatus', 'is_shipmentstatus')
    list_editable = ['logic_step', 'default', 'is_orderstatus', 'is_shipmentstatus']

    # readonly_fields = ['value']
    # actions = ['']

    def get_readonly_fields(self, request, obj=None):
        readonly_fields = []
        if obj:  # readonly when edit
            readonly_fields += ['value']
        return readonly_fields

    sortable = 'order_index'
