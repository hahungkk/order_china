#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  widgets.py
#  
#
#  Created by TVA on 11/23/15.
#  Copyright (c) 2015 order_china. All rights reserved.
from ..models import User, Order
from django_select2.forms import ModelSelect2Widget


class PKFilterMixin:
    def pk_filter(self, term, queryset=None):
        try:
            pk_as_int = int(term)
        except ValueError:
            pass
        else:
            if not queryset: queryset = self.get_queryset()
            return queryset.filter(pk=pk_as_int).first()
        return None


class SelectOrderSearchWidget(ModelSelect2Widget, PKFilterMixin):
    search_fields = ['customer__username__icontains', 'customer__email__icontains', 'receiver_name__icontains',
                     'receiver_phone__icontains']
    model = Order

    def filter_queryset(self, term, queryset=None, **dependent_fields):
        result = self.pk_filter(term, queryset)
        queryset = super(self.__class__, self).filter_queryset(term, queryset, **dependent_fields)
        if result:
            return [result] + list(queryset)

        return queryset


class SelectUserSearchWidget(ModelSelect2Widget, PKFilterMixin):
    search_fields = ['username__icontains', 'email__icontains', 'first_name__icontains', 'last_name']
    model = User

    def filter_queryset(self, term, queryset=None, **dependent_fields):
        result = self.pk_filter(term, queryset)
        if result: return result

        return super(self.__class__, self).filter_queryset(term, queryset, **dependent_fields)
