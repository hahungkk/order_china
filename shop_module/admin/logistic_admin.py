#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  logistic_admin.py
#
#
#  Created by TVA on 10/27/15.
#  Copyright (c) 2015 order_china. All rights reserved.

import django_tables2 as tables
from date_range_filter import DateRangeFilter
from django.contrib import admin
from django.utils.translation import ugettext_lazy as _
from ..models import ShipmentPack


class StatusTable(tables.Table):
    time = tables.Column(orderable=False, verbose_name=_('Time'))
    context = tables.Column(orderable=False, verbose_name=_('Status'))

    class Meta:
        attrs = {'class': 'table table-bordered table-hover'}


@admin.register(ShipmentPack)
class ShipmentPackAdmin(admin.ModelAdmin):
    list_display = ('tracking_code', 'note', 'weight', 'status_view', 'created')
    list_editable = ['weight']
    search_fields = ('tracking_code', )
    list_filter = [('created', DateRangeFilter)]
    ordering = ('-created', )

    def status_view(self, obj):
        data = [obj.status_set.first()]
        table = StatusTable(data)
        return table.as_html(request=None)
    status_view.short_description = _("Status")


# class ShipmentItemAssetListForTaxResource(resources.ModelResource):
#     class Meta:
#         model = ShipmentItem
#         fields = ('id', 'shipment_package__departed', 'order_item__name', 'arrived_quantity', 'quantity')
#         export_order = ('id', 'shipment_package__departed',
#                         'order_item__name', 'arrived_quantity', 'quantity', 'item_price')
#
#     item_price = fields.Field(attribute='order_item__order_price_tag')
#
#
# @admin.register(ShipmentItem)
# class ShipmentItemAdmin(ExportActionModelAdmin, ConcurrencyActionMixin, ConcurrencyListEditableMixin):
#     class Media:
#         css = {
#             'all': (
#                 '/static/shop_admin_mock/test_mock.css',  # project static folder
#             )
#         }
#         js = (
#             '/static/shop_admin_mock/test_mock.js',  # project static folder
#
#         )
#
#     search_fields = ['shipment_package__tracking_code', 'shipment_package__shipment_logistics__tracking_code',
#                      'order_item__name', 'order_item__note']
#     list_filter = ['shipment_package__status', 'shipment_package__inventory_employee',
#                    'shipment_package__shipment_service', 'shipment_package__departed', 'shipment_package__arrived',
#                    'shipment_package__delivered']
#     list_display = ['order_item__name', 'order_item__image_tag', 'arrived_quantity', 'quantity',
#                     'order_item__order_price_tag',
#                     'shipment_package']
#     # 'order_item__order_quantity','order_item__paid_quantity',
#     # 'order_item__paid_shipping','order_item__status','order_item__note']
#     list_editable = []
#     readonly_fields = ['shipment_package', 'order_item__image_tag', 'order_item__quantity',
#                        'order_item__order_quantity', 'order_item__paid_quantity', 'order_item__order_price_tag']
#     raw_id_fields = ['order_item']
#
#     resource_class = ShipmentItemAssetListForTaxResource
#
#     def order_item__name(self, obj):
#         return obj.order_item.name
#
#     order_item__name.short_description = _("Item Name")
#
#     def order_item__quantity(self, obj):
#         return obj.order_item.quantity
#
#     order_item__quantity.short_description = _("Item Quantity")
#
#     def order_item__order_quantity(self, obj):
#         return obj.order_item.order_quantity
#
#     order_item__order_quantity.short_description = _("Order Quantity")
#
#     def order_item__paid_quantity(self, obj):
#         return obj.order_item.paid_quantity
#
#     order_item__paid_quantity.short_description = _("Paid Quantity")
#
#     def order_item__image_tag(self, obj):
#         return obj.order_item.image_tag()
#
#     order_item__image_tag.short_description = _("Item Image")
#
#     def order_item__order_price_tag(self, obj):
#         return obj.order_item.order_price_tag()
#
#     order_item__order_price_tag.short_description = _("Item Price")
#
#
# class ShipmentItemInline(admin.TabularInline):
#     model = ShipmentItem
#     show_change_link = True
#     min_num = 1
#     extra = 0
#     can_delete = False
#
#     fields = ['order_item', 'order_item__image_tag', 'quantity', 'arrived_quantity',
#               'order_item__order_quantity']
#     # ,'order_item__currency', 'order_item__paid_quantity',
#     # 'order_item__paid_price','order_item__paid_shipping','order_item__status','order_item__note']
#     readonly_fields = ['arrived_quantity', 'order_item__image_tag', 'order_item__quantity',
#                        'order_item__order_quantity', 'order_item__paid_quantity']
#     raw_id_fields = ['order_item']
#
#     @staticmethod
#     def order_item__quantity(obj):
#         return obj.order_item.quantity
#
#     @staticmethod
#     def order_item__order_quantity(obj):
#         return obj.order_item.order_quantity
#
#     @staticmethod
#     def order_item__paid_quantity(obj):
#         return obj.order_item.paid_quantity
#
#     @staticmethod
#     def order_item__image_tag(obj):
#         return obj.order_item.image_tag()
#
#     suit_classes = 'suit-tab suit-tab-ShipmentItem'
#
#
# class ShipmentPackageModelForm(forms.ModelForm):
#     class Meta:
#         model = ShipmentPackage
#         exclude = []
#         widgets = {
#             'inventory_employee': SelectUserSearchWidget,
#             'order': SelectOrderSearchWidget,
#             'customer': SelectUserSearchWidget,
#         }
#
#
# @admin.register(ShipmentPackage)
# class ShipmentPackageAdmin(DjangoObjectActions, admin.ModelAdmin, ConcurrencyActionMixin, ConcurrencyListEditableMixin):
#     class Media:
#         css = {
#             'all': (
#                 '/static/shop_admin_mock/test_mock.css',  # project static folder
#             )
#         }
#         js = (
#             '/static/shop_admin_mock/test_mock.js',  # project static folder
#             '/static/shop_admin_mock/logistic_admin_mock.js',
#         )
#
#     search_fields = ['tracking_code', 'order__receiver_name', 'note']
#     list_display = ['__str__', 'weight', 'status', 'inventory_employee', 'departed', 'arrived', 'delivered']
#     list_filter = ['status', 'inventory_employee', 'shipment_service', 'departed', 'arrived', 'delivered']
#     list_editable = ['weight', 'status']
#
#     # modify search result
#     def get_search_results(self, request, queryset, search_term):
#         queryset, use_distinct = super(self.__class__, self).get_search_results(request, queryset, search_term)
#         if len(search_term) > 1 and search_term[0] == '#':
#             try:
#                 search_term_as_int = int(search_term[1:])
#             except ValueError:
#                 pass
#             else:
#                 queryset |= self.model.objects.filter(order_id=search_term_as_int)
#
#         return queryset, use_distinct
#
#     # readonly_fields = []
#     raw_id_fields = ['customer_payment', 'shipment_logistics', 'vendor_payment']  # 'order',
#
#     form = ShipmentPackageModelForm
#
#     inlines = [
#         ShipmentItemInline,
#     ]
#
#     fieldsets = [
#         (None, {
#             'classes': ('suit-tab', 'suit-tab-general',),
#             'fields': [
#                 'departed', 'arrived', 'delivered',
#                 ('status', 'tracking_code'),
#                 ('shipment_service', 'shipment_method'),
#                 ('location', 'destination'),
#                 'inventory_employee',
#                 'order',
#                 'note',
#                 'customer',
#                 'customer_payment',
#
#             ]
#         }),
#         (_('Shipment Items'), {
#             'classes': ('suit-tab', 'suit-tab-ShipmentItem'),
#             'fields': [('currency', 'exchange_rate'),
#                        'weight', 'shipping_cost',
#                        'vendor_payment',
#                        'shipment_logistics']
#         }),
#     ]
#
#     suit_form_tabs = (('general', _('General')), ('ShipmentItem', _('Shipment Items')))
#
#     objectactions = ['applyOrderStatusForAllOrderItem']
#     actions = ['createShipmentLogisticsContainer']
#
#     # =========== Action ===========
#     def createShipmentLogisticsContainer(self, request, queryset):
#         package = LogisticController.createShipmentLogisticsForPackages(queryset)
#         change_url = urlresolvers.reverse('admin:shop_module_shipmentlogistics_change', args=(package.id,))
#         self.message_user(request,
#                           html.format_html(_('{} Package are successfully added to container: <a href="{}">{}</a>'),
#                                            len(queryset), change_url, str(package)), extra_tags=['safe'])
#
#     # return HttpResponseRedirect(change_url);
#
#     createShipmentLogisticsContainer.short_description = _("Create shipment logistics container")
#
#     @takes_instance_or_queryset
#     def applyOrderStatusForAllOrderItem(self, request, queryset):
#         count = 0
#         shipmentPackage = None
#         for shipmentPackage in queryset:
#             for shipmentItem in ShipmentItem.objects.filter(shipment_package_id=shipmentPackage.id):
#                 shipmentItem.order_item.status = shipmentPackage.status
#                 try:
#                     shipmentItem.order_item.save()
#                 except Exception as e:
#                     logging.debug("ShipmentItem: %s, save error:%s" % (shipmentItem, e))
#                 else:
#                     count += 1
#
#         if len(queryset) == 1 and shipmentPackage:
#             self.message_user(request, html.format_html(_('{} OrderItem are successfully change status to {}'), count,
#                                                         str(shipmentPackage.status)))
#         else:
#             self.message_user(request,
#                               html.format_html(_('{} OrderItem are successfully sync status with its ShipmentPackage'),
#                                                count))
#
#     applyOrderStatusForAllOrderItem.short_description = _("Apply status for all order item")
#     applyOrderStatusForAllOrderItem.label = _("Apply status")
#     applyOrderStatusForAllOrderItem.attrs = {
#         'class': 'change-action',
#     }
#
#
# class ShipmentPackageInline(admin.TabularInline):
#     model = ShipmentPackage
#     show_change_link = True
#     min_num = 1
#     extra = 0
#     fields = ['tracking_code', 'weight', 'shipment_service', 'note']
#     readonly_fields = ['shipment_service']
#
#     suit_classes = 'suit-tab suit-tab-ShipmentPackage'
#
#     def has_add_permission(self, request):
#         return False
#
#
# @admin.register(ShipmentLogistics)
# class ShipmentLogisticsAdmin(DjangoObjectActions, admin.ModelAdmin, ConcurrencyActionMixin,
#                              ConcurrencyListEditableMixin):
#     class Media:
#         css = {
#             'all': (
#                 '/static/shop_admin_mock/test_mock.css',  # project static folder
#             )
#         }
#         js = (
#             '/static/shop_admin_mock/test_mock.js',  # project static folder
#         )
#
#     search_fields = ['tracking_code', 'note']
#     list_display = ['__str__', 'weight', 'status', 'inventory_employee', 'departed', 'arrived', 'delivered']
#     list_filter = ['status', 'inventory_employee', 'shipment_service', 'departed', 'arrived', 'delivered']
#     list_editable = ['weight', 'status']
#     # actions = ['']
#     # readonly_fields = []
#     raw_id_fields = ['logistics_payment']
#     inlines = [
#         ShipmentPackageInline,
#     ]
#
#     fieldsets = [
#         (None, {
#             'classes': ('suit-tab', 'suit-tab-general',),
#             'fields': [
#                 'departed', 'arrived', 'delivered',
#                 ('status', 'tracking_code'),
#                 ('shipment_service', 'shipment_method'),
#                 ('location', 'destination'),
#                 'inventory_employee', 'note',
#             ]
#         }),
#         (_('Shipment Package'), {
#             'classes': ('suit-tab', 'suit-tab-ShipmentPackage'),
#             'fields': [('currency', 'exchange_rate'), 'weight', 'shipping_cost', 'tax_cost', 'logistics_payment']
#         }),
#     ]
#
#     suit_form_tabs = (('general', _('General')), ('ShipmentPackage', _('Shipment Package')))
