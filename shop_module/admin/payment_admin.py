from import_export.formats import base_formats
from django.utils import timezone
from datetime import timedelta
from user_module.controllers.EmailController import send_email_notify_order_prepaid
from . import widgets
from django import forms
from django.contrib import admin
from django.db import IntegrityError
from django.utils.translation import ugettext_lazy as _
from import_export import resources
from import_export.admin import ExportActionModelAdmin
from suit.admin import SortableModelAdmin
from dal import autocomplete
from ..controllers import PaymentController
from ..models import (PaymentType, BalanceAccount, Transaction,
                      CustomerPayment, LogisticsPayment, VendorPayment, GenericPayment,
                      Currency, User, Status, OrderItem, Order,
                      XRATE_MAX_DIGITS, XRATE_DECIMAL_PLACES, MONEY_MAX_DIGITS, MONEY_DECIMAL_PLACES
                      )


@admin.register(PaymentType)
class PaymentTypeAdmin(SortableModelAdmin):
    search_fields = ['label', ]
    list_display = ('__str__', 'is_customerpayment', 'is_vendorpayment', 'is_logisticspayment')

    # list_filter = ()
    list_editable = ['is_customerpayment', 'is_vendorpayment', 'is_logisticspayment']
    # actions = ['']

    sortable = 'order_index'


@admin.register(BalanceAccount)
class BalanceAccountAdmin(admin.ModelAdmin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
        )

    search_fields = ['account_holder__username']
    list_display = ['__str__', 'amount_tag', 'type', 'account_holder']
    list_filter = ['type']
    list_editable = []

    # actions = ['']
    # readonly_fields = []

    def get_readonly_fields(self, request, obj=None):
        if obj:  # readonly when edit
            return ['currency', 'amount']
        return []


class TransactionResource(resources.ModelResource):
    class Meta:
        model = Transaction
        fields = ('id', 'created', 'from_balance__name', 'to_balance__name', 'currency', 'exchange_rate',
                  'amount', 'cashier__username')


@admin.register(Transaction)
class TransactionAdmin(ExportActionModelAdmin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
        )

    search_fields = []
    list_display = ['__str__', 'created', 'from_balance_history', 'to_balance_history', 'cashier']
    list_filter = ['created', 'from_balance', 'to_balance', 'cashier']
    list_editable = []
    # actions = ['']
    # import export
    resource_class = TransactionResource

    def get_readonly_fields(self, request, obj=None):
        if obj:  # readonly when edit
            return ['from_balance', 'to_balance', 'currency', 'exchange_rate', 'amount']
        return []

    def save_model(self, request, obj, form, change):
        """ Override admin save model action

        :param request:
        :param obj:
        :type obj: Transaction
        :param form:
        :type form:
        :param change:
        :return:
        """
        if obj.pk is None:  # add new Payment
            try:
                PaymentController.atomic_transaction_for_payment(None, obj.from_balance, obj.to_balance,
                                                                 obj.currency, obj.amount, obj.exchange_rate,
                                                                 obj.detail,
                                                                 obj.cashier if obj.cashier else request.user
                                                                 )
            except IntegrityError as e:
                self.message_user(request, "Unable to create transaction: %s" % e)
                return
        else:
            obj.save()


class AddTransactionForm(forms.Form):
    from_balance = forms.ModelChoiceField(BalanceAccount.objects.filter(account_holder__is_staff=True),
                                          required=False)
    to_balance = forms.ModelChoiceField(BalanceAccount.objects.filter(account_holder__is_staff=True),
                                        required=False)
    currency = forms.ModelChoiceField(Currency.objects.all())
    exchange_rate = forms.DecimalField(initial=0, min_value=0, max_digits=XRATE_MAX_DIGITS,
                                       decimal_places=XRATE_DECIMAL_PLACES, required=False)
    amount = forms.DecimalField(initial=0, min_value=0, max_digits=MONEY_MAX_DIGITS,
                                decimal_places=MONEY_DECIMAL_PLACES, required=True)

    detail = forms.CharField(widget=forms.Textarea(attrs={'cols': '40', 'rows': '1'}), required=False)


class AddAndChangeStatusForm(AddTransactionForm):
    status = forms.ModelChoiceField(Status.objects.all(), required=True, label=_("Change Status to"))


class AddCustomerPaymentForm(AddTransactionForm, forms.ModelForm):
    def __init__(self, *args, **kwargs):
        super(AddCustomerPaymentForm, self).__init__(*args, **kwargs)
        self.fields['order'].queryset = Order.objects.all().order_by('-id')

    class Meta:
        fields = ['order', 'type', 'note'] + ['from_balance', 'to_balance', 'currency',
                                              'exchange_rate', 'amount', 'detail']


class AddVendorPaymentForm(AddTransactionForm, forms.ModelForm):
    class Meta:
        # model = CustomerPayment #dont need if use for admin
        fields = ['vendor', 'type', 'note'] + ['from_balance', 'to_balance', 'currency', 'exchange_rate',
                  'amount', 'detail']


class AddLogisticsPaymentForm(AddTransactionForm, forms.ModelForm):
    class Meta:
        # model = CustomerPayment #dont need if use for admin
        fields = ['shipment_service', 'type', 'note'] + ['from_balance', 'to_balance', 'currency',
                  'exchange_rate', 'amount', 'detail']


class AddGenericPaymentForm(AddTransactionForm, forms.ModelForm):
    class Meta:
        fields = ['content_type', 'object_id', 'type', 'note'] + ['from_balance', 'to_balance', 'currency',
                                                                  'exchange_rate', 'amount', 'detail']


class AddCustomerPaymentFormAndChangeStatus(AddAndChangeStatusForm, forms.ModelForm):

    class Meta:
        fields = ['order', 'type', 'status', 'note'] + \
                 ['from_balance', 'to_balance', 'currency', 'exchange_rate', 'amount', 'detail']

    def __init__(self, *args, **kwargs):
        super(AddCustomerPaymentFormAndChangeStatus, self).__init__(*args, **kwargs)
        # last_month = timezone.now() - timedelta(days=30)
        self.fields['order'] = forms.ModelChoiceField(
            # queryset=Order.objects.filter(created__gte=last_month).order_by('-id'),
            queryset=Order.objects.all().order_by('-id'),
            widget=autocomplete.ModelSelect2(url='order-complete')
        )
        self.fields['currency'].initial = 'VND'
        self.fields['exchange_rate'].initial = 1

    # def save(self, commit=True):
    #     super(AddCustomerPaymentFormAndChangeStatus, self).save()
    #     print(self.instance.order)
    #     print(self.instance.status)


class CustomerPaymentResource(resources.ModelResource):
    class Meta:
        model = CustomerPayment
        fields = ('id', 'created', 'order', 'order__customer__username', 'type__label', 'note',
                  'transaction__currency', 'transaction__exchange_rate', 'transaction__amount',
                  'transaction__cashier__username')


class VendorPaymentResource(resources.ModelResource):
    class Meta:
        model = VendorPayment
        fields = (
            'id', 'created', 'vendor__shopping_domain', 'vendor__name', 'type__label', 'note',
            'transaction__currency', 'transaction__exchange_rate', 'transaction__amount',
            'transaction__cashier__username')


class LogisticsPaymentResource(resources.ModelResource):
    class Meta:
        model = LogisticsPayment
        fields = ('id', 'created', 'shipment_service__name', 'type__label', 'note', 'transaction__currency',
                  'transaction__exchange_rate', 'transaction__amount', 'transaction__cashier__username')


class GenericPaymentResource(resources.ModelResource):
    class Meta:
        model = GenericPayment
        fields = ('id', 'created', 'content_type', 'content_object', 'type__label', 'note',
                  'transaction__currency', 'transaction__exchange_rate', 'transaction__amount',
                  'transaction__cashier__username')


@admin.register(CustomerPayment)
class CustomerPaymentAdmin(ExportActionModelAdmin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
        )

    search_fields = ['order__customer__username']
    list_display = ['__str__', 'type', 'customer_tag', 'order']
    list_filter = ['type']
    list_editable = []
    # actions = ['']
    readonly_fields = ['transaction']
    # import export
    resource_class = CustomerPaymentResource

    formats = [base_formats.XLS]

    form = AddCustomerPaymentFormAndChangeStatus

    def save_model(self, request, obj, form, change):
        """ Override admin save model action

        :param request:
        :param obj:
        :type obj: CustomerPayment
        :param form:
        :type form: CustomerPaymentForm
        :param change:
        :return:
        """
        if obj.pk is None:  # add new Payment
            try:
                PaymentController.atomic_transaction_for_payment(obj,
                                                                 form.cleaned_data['from_balance'],
                                                                 form.cleaned_data['to_balance'],
                                                                 form.cleaned_data['currency'],
                                                                 form.cleaned_data['amount'],
                                                                 form.cleaned_data['exchange_rate'],
                                                                 form.cleaned_data['detail'],
                                                                 request.user,
                                                                 )

            except IntegrityError as e:
                self.message_user(request, "Unable to create transaction: %s" % e)
                return
            status = form.cleaned_data['status']
            prepaid = form.cleaned_data['amount']
            currency: Currency = form.cleaned_data['currency']

            if status:
                Order.objects.filter(pk=obj.order_id).update(status=status)
                if status.is_orderstatus and status.value == "PrePaid":
                    send_email_notify_order_prepaid.delay(obj.order_id, prepaid, code=currency.code)

            orderitem_list = request.GET.getlist('orderitem_id')
            if orderitem_list:
                count_updated = OrderItem.objects.filter(pk__in=orderitem_list).update(
                    status=form.cleaned_data['status'])
                print(count_updated)
                # for orderitem in OrderItem.objects.filter(pk__in=orderitemList):
                # 	orderitem.status = form.cleaned_data['status']
                # 	orderitem.save()

        else:
            obj.save()

    @staticmethod
    def suit_row_attributes(payment, request):
        if payment.transaction.from_balance_history and not payment.transaction.to_balance_history:
            return {'class': 'error'}


@admin.register(VendorPayment)
class VendorPaymentAdmin(ExportActionModelAdmin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
        )

    search_fields = []
    list_display = ['__str__', 'type', 'vendor']
    list_filter = ['type']
    list_editable = []
    # actions = ['']
    readonly_fields = ['transaction']
    # import export
    resource_class = VendorPaymentResource

    def get_form(self, request, obj=None, **kwargs):
        """ Control which form will be rendered to user

        :param request:
        :param obj:
        :type obj:
        :param kwargs:
        :return:
        """
        # if request.user.is_superuser:
        if obj is None:
            # change the add new action to use this form
            kwargs['form'] = AddVendorPaymentForm

        return super(self.__class__, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        """ Override admin save model action

        :param request:
        :param obj:
        :type obj:
        :param form:
        :type form:
        :param change:
        :return:
        """
        if obj.pk is None:  # add new Payment
            try:
                PaymentController.atomic_transaction_for_payment(obj, form.cleaned_data['from_balance'],
                                                                 form.cleaned_data['to_balance'],
                                                                 form.cleaned_data['currency'],
                                                                 form.cleaned_data['amount'],
                                                                 form.cleaned_data['exchange_rate'],
                                                                 form.cleaned_data['detail'], request.user
                                                                 )

            except IntegrityError as e:
                self.message_user(request, "Unable to create transaction: %s" % e)
                return
        else:
            obj.save()


@admin.register(LogisticsPayment)
class LogisticsPaymentAdmin(ExportActionModelAdmin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
        )

    search_fields = []
    list_display = ['__str__', 'type', 'shipment_service']
    list_filter = ['type']
    list_editable = []
    # actions = ['']
    readonly_fields = ['transaction']

    # import export
    resource_class = LogisticsPaymentResource

    def get_form(self, request, obj=None, **kwargs):
        """ Control which form will be rendered to user

        :param request:
        :param obj:
        :type obj: CustomerPayment
        :param kwargs:
        :return:
        """
        # if request.user.is_superuser:
        if obj is None:
            # change the add CustomerPayment action to use this form
            kwargs['form'] = AddLogisticsPaymentForm

        return super(self.__class__, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        """ Override admin save model action

        :param request:
        :param obj:
        :type obj: CustomerPayment
        :param form:
        :type form: CustomerPaymentForm
        :param change:
        :return:
        """
        if obj.pk is None:  # add new Payment
            try:
                PaymentController.atomic_transaction_for_payment(obj, form.cleaned_data['from_balance'],
                                                                 form.cleaned_data['to_balance'],
                                                                 form.cleaned_data['currency'],
                                                                 form.cleaned_data['amount'],
                                                                 form.cleaned_data['exchange_rate'],
                                                                 form.cleaned_data['detail'], request.user
                                                                 )

            except IntegrityError as e:
                self.message_user(request, "Unable to create transaction: %s" % e)
                return
        else:
            obj.save()


class EditGenericPaymentForm(forms.ModelForm):
    class Meta:
        fields = ['content_type', 'object_id', 'content_object', 'type', 'note']

    object_id = forms.ModelChoiceField(BalanceAccount.objects.all(), required=False)

    def __init__(self, *args, **kwargs):
        super(EditGenericPaymentForm, self).__init__(*args, **kwargs)
        if self.instance.id and self.instance.content_type:
            if self.instance.content_type.model.lower() == 'user':
                self.fields['object_id'] = forms.ModelChoiceField(User.objects.all(), required=True,
                                                                  widget=widgets.SelectUserSearchWidget)
            # self.fields['object_id'].widget = widgets.SelectUserSearchWidget()
            elif self.instance.content_type.model.lower() == 'order':
                self.fields['object_id'] = forms.ModelChoiceField(Order.objects.all(), required=True,
                                                                  widget=widgets.SelectOrderSearchWidget)

    def clean(self):
        if self.instance.id and self.instance.content_type:
            if self.instance.content_type.model.lower() in ['user', 'order']:
                self.cleaned_data['object_id'] = self.cleaned_data['object_id'].id
        return super(EditGenericPaymentForm, self).clean()


@admin.register(GenericPayment)
class GenericPaymentAdmin(ExportActionModelAdmin):
    class Media:
        css = {
            'all': (
                '/static/shop_admin_mock/test_mock.css',  # project static folder
            )
        }
        js = (
            '/static/shop_admin_mock/test_mock.js',  # project static folder
        )

    search_fields = []
    list_display = ['__str__', 'type', 'content_type', 'content_object']
    list_filter = ['type', 'content_type']
    list_editable = []
    # actions = ['']
    readonly_fields = ['transaction', 'content_object']

    # import export
    resource_class = GenericPaymentResource

    def get_form(self, request, obj=None, **kwargs):
        """ Control which form will be rendered to user

        :param request:
        :param obj:
        :type obj: CustomerPayment
        :param kwargs:
        :return:
        """
        # if request.user.is_superuser:
        if obj is None:
            # change the add action to use this form
            kwargs['form'] = AddGenericPaymentForm
        else:
            kwargs['form'] = EditGenericPaymentForm

        return super(self.__class__, self).get_form(request, obj, **kwargs)

    def save_model(self, request, obj, form, change):
        """ Override admin save model action

        :param request:
        :param obj:
        :type obj: CustomerPayment
        :param form:
        :type form: CustomerPaymentForm
        :param change:
        :return:
        """
        if obj.pk is None:  # add new Payment
            try:
                PaymentController.atomic_transaction_for_payment(obj, form.cleaned_data['from_balance'],
                                                                 form.cleaned_data['to_balance'],
                                                                 form.cleaned_data['currency'],
                                                                 form.cleaned_data['amount'],
                                                                 form.cleaned_data['exchange_rate'],
                                                                 form.cleaned_data['detail'], request.user
                                                                 )

            except IntegrityError as e:
                self.message_user(request, "Unable to create transaction: %s" % e)
                return
        else:
            obj.save()
