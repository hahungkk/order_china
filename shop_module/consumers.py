# This example uses WebSocket consumer, which is synchronous, and so
# needs the async channel layer functions to be converted.
import json

from channels.generic.websocket import AsyncWebsocketConsumer


class ChatConsumer(AsyncWebsocketConsumer):

    async def connect(self):
        # Join room group
        if not self.scope['user']:
            await self.disconnect(1000)
        else:
            await self.channel_layer.group_add(
                'default_group',
                self.channel_name
            )
            await self.accept()

    async def disconnect(self, close_code):
        # Leave room group
        await self.channel_layer.group_discard(
            'default_group',
            self.channel_name
        )

    async def group_message(self, message):
        await self.send(json.dumps(message))
