#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  AuditLogStaffOnlyListFilter.py
#
#
#  Created by TVA on 4/14/16.
#  Copyright (c) 2016 order_china. All rights reserved.
#
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.contrib.auth.models import User


class CreatedByStaffListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Created by staff')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'created_by'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return [(user.id, user.username) for user in User.objects.filter(is_staff=True)]

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            return queryset.filter(created_by_id=self.value())
        else:
            return queryset


class ModifiedByStaffListFilter(admin.SimpleListFilter):
    # Human-readable title which will be displayed in the
    # right admin sidebar just above the filter options.
    title = _('Modified by staff')

    # Parameter for the filter that will be used in the URL query.
    parameter_name = 'modified_by'

    def lookups(self, request, model_admin):
        """
        Returns a list of tuples. The first element in each
        tuple is the coded value for the option that will
        appear in the URL query. The second element is the
        human-readable name for the option that will appear
        in the right sidebar.
        """
        return [(user.id, user.username) for user in User.objects.filter(is_staff=True)]

    def queryset(self, request, queryset):
        """
        Returns the filtered queryset based on the value
        provided in the query string and retrievable via
        `self.value()`.
        """
        if self.value():
            return queryset.filter(modified_by_id=self.value())
        else:
            return queryset
