from .models import Order, ShipmentPack
from dal import autocomplete


class OrderAutoCompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        if not self.request.user.is_authenticated:
            return Order.objects.none()
        qs = Order.objects.all().order_by('-id')
        if self.q:
            qs = qs.filter(id=self.q)
        return qs


class ShipmentPackAutoCompleteView(autocomplete.Select2QuerySetView):
    def get_queryset(self):
        qs = ShipmentPack.objects.all()
        if self.q:
            qs = qs.filter(tracking_code=self.q)
        return qs

