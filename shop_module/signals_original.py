#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  signals.py
#  
#
#  Created by TVA
#
# import re
#
# from django.db.models.signals import pre_save
# from django.dispatch import receiver
# from .models import CartItem
#
#
# # TODO: presave cart item
# @receiver(pre_save, sender=CartItem)
# def pre_save_cart_item(sender, instance=CartItem(), **kwargs):
#     if kwargs['raw']:
#         return  # disable this handler during fixture loading
#     instance.options_selected = re.sub(r'\s+', ' ', instance.options_selected)
#     instance.options_selected = re.sub(r'\n|\\t', '', instance.options_selected)
#     instance.options_metadata = re.sub(r'\s+', ' ', instance.options_metadata)
#     instance.options_metadata = re.sub(r'\n|\\t', '', instance.options_metadata)


# @receiver(pre_save, sender=OrderItem)
# def pre_save_order_item(sender, instance=OrderItem(), **kwargs):
#     if kwargs['raw']:
#         return  # disable this handler during fixture loading
#     if instance.product_id:
#         # auto fill form if there is product and no value in field
#         if not instance.shopping_domain:
#             instance.shopping_domain = instance.product.shopping_domain
#         if not instance.vendor:
#             instance.vendor = instance.product.vendor.name
#         if not instance.name:
#             instance.name = instance.product.name
#         if not instance.image_url:
#             instance.image_url = instance.product.image_url
#         if not instance.item_url:
#             instance.item_url = instance.product.detail_url
#         if not instance.currency:
#             instance.currency = instance.product.currency
#         if instance.price is None:
#             instance.price = instance.product.price
#         if instance.shipping is None:
#             instance.shipping = instance.product.shipping
#         if instance.weight is None:
#             instance.weight = instance.product.weight
#
#     if instance.exchange_rate == 0 and instance.currency:
#         instance.exchange_rate = instance.currency.exchange_rate


# @receiver(post_save, sender=OrderItem)
# def postSaveOrderItem(sender, instance=OrderItem(), created=False, **kwargs):
#     if kwargs['raw']: return  # disable this handler during fixture loading

# if not created:
# 	#automaticaly change order.status of this orderItem if all orderItem of that order have the same status
# 	if instance.order.status_id!=instance.status_id:
# 		for orderItem in instance.order.orderitem_set.all():
# 			if orderItem.status_id!=instance.status_id:
# 				break
# 		else:
# 			instance.order.status_id = instance.status_id
# 			instance.order.save()
