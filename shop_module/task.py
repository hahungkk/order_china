import logging
from datetime import datetime, timedelta
from celery import shared_task
from dateutil import parser
from shop_module.controllers.OrderController import calculate_service_charge2
from shop_module.controllers.TrackingController import get_status
from shop_module.models import (Order, Status, Currency, ShipmentStatus,
                                BalanceAccount, CustomerProfile, OrderItem)
from system_configure.models import SystemConfig
from django.utils import timezone
from shop_module.enums import BalanceType, WAITING_PREPAID_STATUS
import decimal


@shared_task
def update_order_when_exchange_rate_changed():
    currency = Currency.objects.get(code='CNY')
    orders = Order.objects.filter(status__value='WaitingForPrePaid')
    for order in orders:
        order.orderitem_set.all().update(exchange_rate=currency.exchange_rate)
    return orders.count()


# @shared_task
# def reupdate_order_status_change(order_id: int):
#     order = Order.objects.get(pk=order_id)
#     order_item_set = order.orderitem_set.all()
#     if len(order_item_set.values('status').distinct()) < 2:
#         order.status = order_item_set.first().status
#         order.save()


@shared_task
def change_expired_order_status():
    status = Status.objects.get(value='Failed')
    try:
        date_config, created = SystemConfig.objects.get_or_create(
            key='expired_order',
            defaults={'key': '21'})
        orders = Order.objects.filter(
            status__value='WaitingForPrePaid',
            created__lte=datetime.now() - timedelta(days=int(date_config.value)))
        orders.update(status=status)
        logging.info(f"{orders.count()}")
        return orders.count()
    except Exception as e:
        logging.error(e)
        return e


#
# @shared_task()
# def get_shipment_status(code):
#     statuses = get_status(code)
#     translator = Translator()
#     if statuses is not None:
#         for status in statuses.data:
#             logging.info(status)
#             ShipmentStatus.objects.update_or_create(
#                 shipment_pack_id=code,
#                 time=parser.parse(status.time).replace(tzinfo=timezone.now().tzinfo),
#                 context=translator.translate(text=status.context, src='zh-cn', dest='vi').text,
#                 location=status.location
#             )


# @shared_task
# def udpate_shipmentpack_daily() -> bool:
#     orderitems = OrderItem.objects.filter(shipment_pack__isnull=False,
#                                           status__value__in=['Ordered', 'ShipToCN'])
#     list_id = [item.shipment_pack for item in orderitems]
#     for di in list_id:
#         get_shipment_status(di)
#     return True


@shared_task
def update_orderitem_status_from_checker(tracking_code, status):
    status = Status.objects.get(value=status)
    orderitems = OrderItem.objects.filter(shipment_pack=tracking_code).update(status=status)
    return orderitems


@shared_task
def add_referrer_credit(custommer_id, total_amount):
    customer = CustomerProfile.objects.get(user_id=custommer_id)
    try:
        if customer.referrer:
            balance = BalanceAccount.objects.get(account_holder=customer.referrer.user,
                                                 type=BalanceType.user_credit)
            earn = decimal.Decimal(total_amount) * decimal.Decimal(1 / 100)
            logging.info(earn)
            balance.amount += earn
            balance.save()
            return True
        return False
    except Exception as e:
        logging.error(e)
        return False


#
# @shared_task
# def change_order_status(order_id, status_id):
#     order = Order.objects.get(pk=order_id)
#     try:
#         order.status_id = status_id
#         order.save()
#         return True
#     except Exception as e:
#         logging.error(e)
#         return False


@shared_task
def cleanup_old_history():
    today = datetime.now()
    latest_month = today - timedelta(days=60)
    deleted_order = Order.history.filter(created__lte=latest_month).delete()
    return deleted_order[0]


@shared_task
def update_service_charge_change():
    list_orders = Order.objects.filter(status__value=WAITING_PREPAID_STATUS)
    currency = Currency.objects.first()

    for order in list_orders:
        total_cost = 0
        for order_item in order.orderitem_set.all():
            total_cost += currency.convert(order_item.price * order_item.quantity + order_item.shipping)
            order_item.service_charge, order_item.insurance_charge, order_item.fast_order_charge, order_item.bargain_charge = \
                calculate_service_charge2(
                    total_item_cost=total_cost,
                    price=order_item.price,
                    quantity=order_item.quantity,
                    insurance=order_item.insurance,
                    fast_order=order_item.fast_order,
                    bargain=order_item.bargain,
                    customer=order.customer
                )
            order_item.save()
