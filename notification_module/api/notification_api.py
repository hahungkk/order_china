#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  notification_api.py
#
#
#  Created by vinhnt on 16/08/16.
#  Copyright (c) 2016 weorder. All rights reserved.
#
import logging
from django.contrib.contenttypes.models import ContentType
from rest_framework.decorators import action
from rest_framework import serializers, generics, permissions, viewsets, filters
from notifications.models import Notification
from system_configure.controllers import Tool
from django.shortcuts import get_object_or_404
from django_filters.rest_framework import DjangoFilterBackend

logger = logging.getLogger(__name__)


class NotificationsAdminSerializer(serializers.ModelSerializer):
    class Meta:
        model = Notification
        fields = ('recipient', 'description', 'target_object_id', 'timestamp', 'unread', 'level', 'id',
                  'actor_object_id', 'actor_content_type')
        depth = 0

    actor_content_type = serializers.SlugRelatedField(slug_field='model', queryset=ContentType.objects.all())


class NotificationsAPIView(viewsets.GenericViewSet, generics.ListAPIView):
    serializer_class = NotificationsAdminSerializer
    permission_classes = (permissions.IsAdminUser, permissions.IsAuthenticated)
    filter_backends = (DjangoFilterBackend, filters.SearchFilter,)
    filter_fields = ['unread', ]

    def get_queryset(self):
        return Notification.objects.filter(recipient=self.request.user)

    @action(detail=False, methods=['PUT'], permission_classes=[permissions.IsAuthenticated, permissions.IsAdminUser], )
    def mark_all_as_read(self, request, *args, **kwargs):
        # mark all as read
        if request.method == 'PUT':
            request.user.notifications.mark_all_as_read()

        queryset = Notification.objects.filter(recipient=self.request.user)
        serializer = NotificationsAdminSerializer(queryset, context={'request': request}, many=True)

        return Tool.success_response_restful(serializer.data)


class NotificationsUpdateAPIView(viewsets.GenericViewSet, generics.RetrieveAPIView):
    permission_classes = (permissions.IsAdminUser, permissions.IsAuthenticated)

    def get_queryset(self):
        return Notification.objects.filter(recipient=self.request.user)

    @action(detail=True, methods=['PATCH'], permission_classes=[permissions.IsAuthenticated, permissions.IsAdminUser],
            serializer_class=NotificationsAdminSerializer)
    def mark_as_read(self, request, pk):
        notification = get_object_or_404(Notification, recipient=request.user, id=pk)
        # mark as read
        if request.method == 'PATCH':
            notification.mark_as_read()

        serializer = NotificationsAdminSerializer(notification)
        return Tool.success_response_restful(serializer.data)
