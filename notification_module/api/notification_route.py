import logging
from order_china.routers import GroupedRouter
from .notification_api import NotificationsAPIView, NotificationsUpdateAPIView

logger = logging.getLogger(__name__)

router = GroupedRouter('notification_module')
router.register(r'notification', NotificationsAPIView, basename='Notification')
router.register(r'notification', NotificationsUpdateAPIView, basename='Notification')

urlpatterns = router.urls
