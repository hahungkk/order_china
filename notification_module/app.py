#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  app.py
#  
#
#  Created by vinht 16/08/2016

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MyAppConfig(AppConfig):
    name = 'notification_module'
    verbose_name = _("notification_module")
