#count_order_all_time
SELECT count(id)
FROM order_china.shop_module_order;

#sum_order_charge_rate_exchanged
SELECT
  count(id),
  SUM(extra_charge * exchange_rate),
  SUM(tax_cost * exchange_rate)
FROM order_china.shop_module_order
WHERE (created BETWEEN %(from_date)s AND %(to_date)s);

#sum_orderitem_cost_by_currency
SELECT
  currency_id,
  count(id),
  SUM(order_quantity * order_price),
  SUM(order_shipping)
FROM order_china.shop_module_orderitem
WHERE (created BETWEEN %(from_date)s AND %(to_date)s)
GROUP BY currency_id;

#sum_orderitem_charge_rate_exchanged
SELECT
  count(id),
  SUM(quantity * price * exchange_rate),
  SUM(shipping * exchange_rate),
  SUM(service_charge * exchange_rate)
FROM order_china.shop_module_orderitem
WHERE (created BETWEEN %(from_date)s AND %(to_date)s);