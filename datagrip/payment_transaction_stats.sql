#count_transaction_all_time
SELECT count(id)
FROM order_china.shop_module_transaction;

#sum_transaction_amount_group_by_day_and_to_balance_id
SELECT
  DATE(created),
  to_balance_id,
  count(id),
  SUM(amount)
FROM order_china.shop_module_transaction
WHERE (created BETWEEN %(from_date)s AND %(to_date)s)
#created BETWEEN '2015-01-01 00:00:00' and '2017-01-01 00:00:00'
GROUP BY DATE(created), to_balance_id;

#sum_customerPayment_amount_rate_exchanged_group_by_day
SELECT
  DATE(customerPayment.created)                       AS created,
  SUM(transaction.amount * transaction.exchange_rate) AS amount_exchanged
FROM shop_module_customerpayment customerPayment
  LEFT JOIN shop_module_transaction transaction ON transaction.id = customerPayment.transaction_id
WHERE (customerPayment.created BETWEEN %(from_date)s AND %(to_date)s)
GROUP BY DATE(customerPayment.created);

#sum_customerPayment_amount_group_by_day_and_currency_id
SELECT
  DATE(customerPayment.created) AS created,
  transaction.currency_id,
  SUM(transaction.amount)
FROM shop_module_customerpayment customerPayment
  LEFT JOIN shop_module_transaction transaction ON transaction.id = customerPayment.transaction_id
WHERE (customerPayment.created BETWEEN %(from_date)s AND %(to_date)s)
GROUP BY DATE(customerPayment.created), transaction.currency_id;