SELECT
  `shop_module_order`.`id`,
  `shop_module_order`.`version`,
  `shop_module_order`.`created`,
  `shop_module_order`.`modified`,
  `shop_module_order`.`created_by_id`,
  `shop_module_order`.`modified_by_id`,
  `shop_module_order`.`status_id`,
  `shop_module_order`.`customer_id`,
  `shop_module_order`.`customer_note`,
  `shop_module_order`.`note`,
  `shop_module_order`.`delivery_method`,
  `shop_module_order`.`delivery_address`,
  `shop_module_order`.`city`,
  `shop_module_order`.`district`,
  `shop_module_order`.`street`,
  `shop_module_order`.`receiver_name`,
  `shop_module_order`.`receiver_phone`,
  `shop_module_order`.`prepaid_percent`,
  `shop_module_order`.`currency_id`,
  `shop_module_order`.`exchange_rate`,
  `shop_module_order`.`extra_charge`,
  `shop_module_order`.`tax_cost`,
  # RawQuerySet does not support fetch related
  #   `shop_module_status`.`created`,
  #   `shop_module_status`.`modified`,
  #   `shop_module_status`.`value`,
  #   `shop_module_status`.`label`,
  #   `shop_module_status`.`default`,
  #   `shop_module_status`.`logic_step` as ,
  #   `shop_module_status`.`is_orderstatus`,
  #   `shop_module_status`.`is_shipmentstatus`,
  #   `shop_module_status`.`order_index`,
  #   `shop_module_currency`.`created`,
  #   `shop_module_currency`.`modified`,
  #   `shop_module_currency`.`code`,
  #   `shop_module_currency`.`exchange_rate`,
  #   `shop_module_currency`.`label`,
  #   `shop_module_currency`.`order_index`,
  # count and sum order item
  SUM(((`shop_module_orderitem`.`price` * `shop_module_orderitem`.`quantity`) *
       `shop_module_orderitem`.`exchange_rate`))                                                   AS `_sum_item_cost`,
  SUM(`shop_module_orderitem`.`weight`)                                                            AS `_sum_item_weight`,
  SUM((`shop_module_orderitem`.`tax_cost` * `shop_module_orderitem`.`exchange_rate`))              AS `_sum_item_tax_cost`,
  SUM((`shop_module_orderitem`.`shipping` * `shop_module_orderitem`.`exchange_rate`))              AS `_sum_item_shipping`,
  SUM((`shop_module_orderitem`.`service_charge` * `shop_module_orderitem`.`exchange_rate`))        AS `_sum_item_service_charge`,
  COUNT(DISTINCT `shop_module_orderitem`.`id`)                                                     AS `_count_item`,
  # count and sum customer payment
  COUNT(DISTINCT `shop_module_customerpayment`.`id`)                                               AS `_count_payment`,
  IFNULL(SUM((`shop_module_transaction`.`amount` * `shop_module_transaction`.`exchange_rate`)), 0) AS `_sum_payment_transaction`
FROM `shop_module_order`
  INNER JOIN `shop_module_orderitem` ON (`shop_module_order`.id = shop_module_orderitem.order_id)
  INNER JOIN `shop_module_status` ON (`shop_module_orderitem`.`status_id` = `shop_module_status`.`value`)
  LEFT JOIN `shop_module_customerpayment` ON (`shop_module_order`.id = shop_module_customerpayment.order_id)
  LEFT JOIN `shop_module_transaction` ON (`shop_module_customerpayment`.`transaction_id` = `shop_module_transaction`.`id`)

WHERE NOT (`shop_module_status`.`logic_step` = 4)
GROUP BY `shop_module_order`.`id`
ORDER BY `shop_module_order`.`id` DESC
LIMIT 30;