from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from .enums import *
import datetime


YEAR_CHOICES = []
for r in range(1950, (datetime.datetime.now().year+1)):
    YEAR_CHOICES.append((r, r))


class UserProfile(models.Model):
    class Meta:
        verbose_name = _("User Profile")
        verbose_name_plural = _("User Profiles")

    user = models.OneToOneField(User, verbose_name=_("User"), related_name='profile',
                                editable=False, on_delete=models.CASCADE)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    full_name = models.CharField(verbose_name=_("full_name"), blank=True, max_length=255, db_index=True)
    dob = models.DateField(verbose_name=_("date of birth"), blank=True, null=True)
    dob_year = models.IntegerField(verbose_name=_("year of birth"), blank=True, null=True, choices=YEAR_CHOICES)
    avatar = models.TextField(verbose_name=_("avatar"), blank=True, default='')
    email = models.EmailField(verbose_name=_("email"), max_length=255, db_index=True, null=True, unique=True)
    address = models.TextField(verbose_name=_("address"), blank=True)
    phone_number = models.CharField(verbose_name=_("phone_number"), blank=True, null=True, max_length=255,
                                    db_index=True)
    user_facebook = models.CharField(max_length=255, null=True, blank=True, verbose_name=_("user_facebook"))
    facebook_uid = models.CharField(max_length=255, null=True, blank=True, verbose_name=_("facebook_uid"))

    account_status = models.PositiveSmallIntegerField(verbose_name=_("account_status"),
                                                      choices=AccountStatus.ChoiceList(),
                                                      default=AccountStatus.normal,
                                                      db_index=True)
    signup_ip = models.GenericIPAddressField(verbose_name=_("signup_ip"), blank=True, null=True, max_length=255,
                                             editable=False, db_index=True)

    def __str__(self):
        return str(self.user)

    def save(self, force_insert=False, force_update=False, using=None,
             update_fields=None):
        if self.signup_ip:
            self.signup_ip = self.signup_ip.strip()
        super(UserProfile, self).save(force_insert=False,
                                      force_update=False,
                                      using=None,
                                      update_fields=None)
