#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  admin.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#
from __future__ import unicode_literals

from django.contrib import admin
from django.contrib.admin.models import LogEntry
from django.contrib.auth import login
from django.contrib.auth.admin import UserAdmin, messages
from django.contrib.auth.models import User
from django.http import HttpResponseRedirect
from django.utils.translation import ugettext_lazy as _
from django_object_actions import DjangoObjectActions, takes_instance_or_queryset
from .controllers import UserController
from .models import UserProfile


@admin.register(LogEntry)
class CustomLogEntryAdmin(admin.ModelAdmin):
    search_fields = ['user__username', 'object_id', 'change_message', 'object_repr']
    list_display = ('action_time', 'user', 'short_represent',)
    list_filter = ('content_type', 'action_flag', 'action_time')
    list_display_links = ('short_represent',)

    def short_represent(self, instance):
        """
        :param instance:
        :type instance: LogEntry
        :return:
        """
        return instance.__str__()[:200]

    short_represent.short_description = _('short represent')


class UserInline(admin.StackedInline):
    model = User
    can_delete = False
    verbose_name_plural = _('User')


@admin.register(UserProfile)
class UserProfileAdmin(admin.ModelAdmin):
    # list view
    search_fields = ['user__username', 'full_name']
    list_display = ('__str__', 'email', 'signup_ip',)


# Define an inline admin descriptor for model
# which acts a bit like a singleton
class UserProfileInline(admin.StackedInline):
    model = UserController.UserProfileClass()
    can_delete = False
    verbose_name_plural = _('Profile')
    fk_name = 'user'


# Define a new User admin
class CustomUserAdmin(DjangoObjectActions, UserAdmin):
    inlines = [UserProfileInline]

    list_display = ('id', 'username', 'email', 'profile__full_name', 'is_staff', 'list_groups')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups__name')
    search_fields = ('username', 'email', 'profile__full_name', 'first_name', 'last_name')

    @staticmethod
    def list_groups(obj):
        return ','.join(list(obj.groups.all().values_list('name', flat=True)))

    def has_delete_permission(self, request, obj=None):
        if request.user.is_superuser:
            return True
        return False

    def get_actions(self, request):
        # Disable delete user for normal staff
        actions = super(CustomUserAdmin, self).get_actions(request)
        if not request.user.is_superuser:
            del actions['delete_selected']
        return actions

    @staticmethod
    def profile__full_name(obj):
        return obj.profile.full_name

    def get_inline_instances(self, request, obj=None):
        if not obj:  # do not create inline model along with user
            return []
        return [InlineClass(self.model, self.admin_site) for InlineClass in self.inlines]

    def get_fieldsets(self, request, obj=None):
        fieldsets = (
            (None, {'fields': ('username', 'password')}),
            (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
            (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                           'groups', 'user_permissions')}),
            (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        )

        staff_fieldsets = (
            (None, {'fields': ('username', 'password')}),
            (_('Personal info'), {'fields': ('first_name', 'last_name', 'email')}),
            (_('Permissions'), {'fields': ('is_active',)}),
            (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
        )

        add_fieldsets = (
            (None, {
                'classes': ('wide',),
                'fields': ('username', 'password1', 'password2'),
            }),
        )

        if not obj:
            return add_fieldsets
        if not request.user.is_superuser:
            return staff_fieldsets
        return fieldsets

    actions = ['login_using_this_user']
    change_actions = ['login_using_this_user']

    @takes_instance_or_queryset
    def login_using_this_user(self, request, queryset):
        user = queryset.first()
        if user.is_superuser or (request.user.is_superuser is False and user.is_staff):
            self.message_user(request, _("You don't have permission to login using this user!"), level=messages.ERROR)
            return
        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)
        return HttpResponseRedirect('/')

    login_using_this_user.short_description = _("Login this user")
    login_using_this_user.label = _("Login this user")


# Re-register UserAdmin
admin.site.unregister(User)
admin.site.register(User, CustomUserAdmin)
