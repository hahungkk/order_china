#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  signals.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#
import random
import string
from .controllers import EmailController
from django.db.models.signals import post_save, pre_save
from django.dispatch import receiver
from .enums import *
from .models import User, UserProfile
from system_configure.controllers.Tool import *
from system_configure.models import SystemConfig
from system_configure.signals import post_verify_code

print(u"Connecting %s success" % __name__)


def create_profile_for_user(user):
    new_profile = UserProfile()
    new_profile.user = user
    new_profile.id = user.id
    full_name = user.get_full_name()
    new_profile.full_name = full_name if full_name else user.username
    new_profile.account_status = AccountStatus.emailNotActivated
    # new_profile.email = user.email
    new_profile.save()


@receiver(pre_save, sender=User)
def presave_user(sender, instance=User(), **kwargs):
    if kwargs.get('raw'):
        return  # disable this handler during fixture loading
    if instance.id:  # user during login, last_login changes
        try:
            if instance.profile:
                pass
        except UserProfile.DoesNotExist:
            create_profile_for_user(instance)


@receiver(post_save, sender=User)
def post_save_user(sender, instance=User(), created=False, **kwargs):
    if kwargs.get('raw'):
        return  # disable this handler during fixture loading
    if created:
        # automaticaly create an UserProfile when a new User is saved
        create_profile_for_user(instance)


# activate email and reset password via email
@receiver(post_verify_code, sender=SystemConfig)
def postVerifyCode(sender, **kwargs):
    if kwargs.get('raw'): return  # disable this handler during fixture loading
    code = kwargs['code']
    data = kwargs['data']

    if data.get('password_reset', False):
        # reset password
        try:
            user = User.objects.get(id=data.user_id);
        except User.DoesNotExist:
            logging.error(u"user_id=%s does not exist" % (data.user_id))
            return

        new_password = ''.join(random.choice(string.ascii_uppercase + string.digits) for _ in range(6))
        user.set_password(new_password)
        user.save()

        EmailController.send_new_password_notify_mail(user.email, new_password)
        return
    elif data.get('activate_account', False):
        user_id, email = data.user_id, data.email
        try:
            user = User.objects.get(id=user_id)
        except User.DoesNotExist:
            logging.error(u"user_id=%s does not exist" % (user_id))
            return
        try:
            user.profile.account_status = AccountStatus.normal
            user.profile.email = email
            user.profile.save()
        except Exception as e:
            logging.error(u"Unable to save profile of user_id=%s, error=%s" % (user_id, e))
            return
        user.is_active = True
        user.email = email
        user.save()
        EmailController.send_welcome_mail(email)
        return

    else:
        logging.warning(u"Unidentified code=%s" % (code))
