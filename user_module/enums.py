#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  enums.py
#  
#
#  Created by TVA on 4/21/15.
#  Copyright (c) 2015 vietcomfund. All rights reserved.
#

from system_configure.enums import EnumBase


class AccountStatus(EnumBase):
    normal = 0
    emailNotActivated = 1
    banned = 2
    temporary = 3
