#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  EmailController.py
import datetime
import pytz
from django.urls import reverse
from django.conf import settings
from shop_module.models import Order, Currency, decimal
from system_configure.controllers.Tool import *
from system_configure.controllers import SystemConfigureController
from django.core.mail import send_mail, EmailMessage
from celery import shared_task

from utilities.VietnameseDateTimeFormat import convert_to_vietnamese_datetime


def send_welcome_mail(to_address):
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS
    header = SystemConfigureController.get_html('welcomeMailHeader',
                                                default=u"Welcome to %s" % settings.PROJECT_NAME_IN_EMAIL)

    html_body = SystemConfigureController.get_html('welcomeMailBody', default=u"Your account has been activated!")

    try:
        send_mail(header, html_body, sender_address, [to_address], html_message=html_body)
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s" % to_address)
    return True


def send_account_activation_mail(request, to_address, user_id):
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS
    header = SystemConfigureController.get_html(
        'accountActivationMaillHeader',
        default=u"Activation email from %s" % settings.PROJECT_NAME_IN_EMAIL)

    code = SystemConfigureController.generate_temporary_jwt(user_id=user_id,
                                                            email=to_address,
                                                            activate_account=True)

    success_url = '/activate-success/'
    failed_url = '/activate-failed/'
    activate_link = request.build_absolute_uri(
        reverse('verifyCode') +
        '?sucess=%s&failed=%s&code=%s' % (success_url, failed_url, code))

    html_body = SystemConfigureController.get_html(
        'accountActivationMaillBody',
        default=u'Please follow this link to activate your account: <a href="{{activate_link}}">{{activate_link}}</a>',
        activate_link=activate_link)

    try:
        send_mail(header, html_body, sender_address, [to_address], html_message=html_body)
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s" % to_address)
    return True


def send_reset_password_mail(request, to_address, user_id):
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS
    header = SystemConfigureController.get_html('resetPasswordMailHeader', default=u"Reset Account Password")

    code = SystemConfigureController.generate_temporary_jwt(user_id=user_id, password_reset=True)
    reset_link = request.build_absolute_uri(reverse('verifyCode') + '?code=%s' % code)

    html_body = SystemConfigureController.get_html(
        'resetPasswordMailBody',
        default=u'Please follow this link to reset your account password: <a href="{{link}}">{{link}}</a>',
        link=reset_link)

    try:
        send_mail(header, html_body, sender_address, [to_address], html_message=html_body)
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s" % to_address)
    return True


def send_new_password_notify_mail(to_address, new_password):
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS
    header = SystemConfigureController.get_html('notifyNewPasswordMailHeader',
                                                default=u"Your Account Password Has Been Reset")
    html_body = SystemConfigureController.get_html(
        'notifyNewPasswordMailBody',
        default=u'Your new account password: {{password}}',
        password=new_password)

    try:
        send_mail(header, html_body, sender_address, [to_address], html_message=html_body)
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s" % to_address)
    return True


def send_make_contact_mail(to_address, user_display_name, user_email_address, subject, message):
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS

    email = EmailMessage(subject, message, "%s <%s>" % (user_display_name, sender_address),
                         [to_address], cc=[user_email_address], headers={'Reply-To': user_email_address})
    try:
        email.send()
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s" % to_address)
    return True


@shared_task
def send_email_notify_order_status_changed(to_address, status, time_modify):
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS
    header = SystemConfigureController.get_html('notify_status_changed_header',
                                                default=u"Thay đổi trạng thái đơn hàng")
    html_body = u'Đơn hàng của bạn đã được chuyển trạng thái thành %s vào lúc %s' % (status, time_modify)
    try:
        send_mail(header, html_body, sender_address, [to_address], html_message=html_body)
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s" % to_address)
    return True


@shared_task
def send_email_notify_ordered(to_address, full_name, order_id, created, order_cost, prepaid):
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS
    header = SystemConfigureController.get_html('notify_ordered_header_test',
                                                default=u"order_china.vn Xin thông báo tình trạng đơn hàng")
    html_body = SystemConfigureController.get_html('notify_created_order',
                                                   default=u"Đơn Hàng #{{order_id}} của Bạn Đã Được Đặt",
                                                   fullname=full_name,
                                                   order_id=order_id,
                                                   created=created,
                                                   order_cost=order_cost,
                                                   prepaid=prepaid)
    try:
        send_mail(header, html_body, sender_address, [to_address], html_message=html_body)
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s" % to_address)
    return True


@shared_task
def send_email_notify_order_prepaid(order_id, prepaid, code):
    order = Order.objects.get(pk=order_id)
    username = order.customer.username
    to_address = order.customer.email

    currency = Currency.objects.get(code=code)
    tz = pytz.timezone('Asia/Ho_Chi_Minh')
    time_modify = convert_to_vietnamese_datetime(datetime.datetime.now(tz))
    sender_address = settings.PROJECT_MAIN_EMAIL_ADDRESS
    list_item = []
    header = "order_china xin thông báo tình trạng đơn hàng %s" % order_id
    html_body = SystemConfigureController.get_html('prepaid_order_email',
                                                   default=u"Đơn Hàng #{{order_id}} của Bạn Đã đặt cọc thành công",
                                                   order_id=order_id,
                                                   list_items=list_item,
                                                   username=username,
                                                   prepaid=currency.display(decimal.Decimal(prepaid)),
                                                   date=time_modify)
    try:
        send_mail(header, html_body, sender_address, [to_address], html_message=html_body)
    except Exception as e:
        logging.error("send email from %s to %s error=%s" % (sender_address, to_address, e))
        return False
    else:
        logging.info("send email success to: %s " % to_address)
    return True
