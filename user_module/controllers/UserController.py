#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  UserController
#  
#
#  Created by TVA on 4/21/15.
#  Copyright (c) 2015 vietcomfund. All rights reserved.
#

import importlib
from django.conf import settings;
from ..enums import *
from ..models import UserProfile as _DefaultProfile


def getExtraProfileSerializerList():
    serializerList = []
    USER_MODULE_CONFIG = getattr(settings, 'USER_MODULE_CONFIG', {})
    EXTRA_PROFILE_SERIALIZER_LIST = USER_MODULE_CONFIG.get('EXTRA_PROFILE_SERIALIZER_LIST', [])
    for classPath in EXTRA_PROFILE_SERIALIZER_LIST:
        module_name, class_name = classPath.rsplit(".", 1)
        SerializerClass = getattr(importlib.import_module(module_name), class_name)
        serializerList.append(SerializerClass)
    # serializerList.append(__import__(classNameList[-1],globals(), locals(), classNameList[:-1], -1));
    return serializerList


def UserProfileClass():
    USER_MODULE_CONFIG = getattr(settings, 'USER_MODULE_CONFIG', {})
    USER_PROFILE_CUSTOM_MODEL = USER_MODULE_CONFIG.get('USER_PROFILE_CUSTOM_MODEL')
    if USER_PROFILE_CUSTOM_MODEL:
        module_name, class_name = USER_PROFILE_CUSTOM_MODEL.rsplit(".", 1)
        CustomerUserProfileClass = getattr(importlib.import_module(module_name), class_name)
        return CustomerUserProfileClass
    else:
        return _DefaultProfile


def createProfileForUser(user):
    newProfile = UserProfileClass()()
    newProfile.user = user
    newProfile.id = user.id
    full_name = user.get_full_name()
    newProfile.full_name = full_name if full_name else user.username
    newProfile.account_status = AccountStatus.emailNotActivated
    # newProfile.email = user.email
    newProfile.save()
