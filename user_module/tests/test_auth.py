#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  tests.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#
import json
import re

from django.core import mail
from django.urls import reverse
from django.test import TestCase
from rest_framework import status
from rest_framework.test import APIClient

from system_configure.tests.test_sys_config import urlopen
from user_module.enums import *
from user_module.models import User


# noinspection PyUnusedLocal
class TestAuthRestfulAPI(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User(username='test_user', email='test_user@gmail.com')
        self.user.set_password('123456')
        self.user.save()

    def test_login(self, show_test=True):
        if show_test:
            print(u"\n test_login:")
        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-login'),
                       {'username': self.user.username, 'password': '123456'})
        data = json.loads(html)

    # self.assertEqual(data['success'],True);

    def test_logout(self, show_test=True):
        if show_test:
            print(u"\n test_logout:")
        self.test_login(False)
        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-logout'), {})
        data = json.loads(html)

    # self.assertEqual(data['success'],True);

    def test_signup_and_activate_account(self, show_test=True):
        if show_test:
            print(u"\n test_signup:")
        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-signup'),
                       {'username': 'test_user_2', 'password': '123456', 'email': 'test_user_2@gmail.com'})

        try:
            html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-login'),
                           {'username': 'test_user_2', 'password': '123456'})
        except Exception as e:
            status_code, message = e
            self.assertEqual(status_code, status.HTTP_403_FORBIDDEN)

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the subject of the first message is correct.
        self.assertIn('Activation email', mail.outbox[0].subject)

        m = re.search(r'href="http://testserver(/.+)"', mail.outbox[0].body)
        self.assertNotEqual(m, None)
        url = m.group(1)  # activate link

        # test: another user use the same email to signup before first user activated
        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-signup'),
                       {'username': 'test_user_3', 'password': '123456', 'email': 'test_user_2@gmail.com'})
        # result is ok because, email has not been taken yet
        self.assertEqual(len(mail.outbox), 2)

        # activate email
        try:
            html = urlopen(self.client, url)
        except Exception as e:
            status_code, message = e
            self.assertEqual(status_code, status.HTTP_302_FOUND)

        self.assertEqual(len(mail.outbox), 3)
        self.assertIn('Welcome', mail.outbox[2].subject)

        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-login'),
                       {'username': 'test_user_2', 'password': '123456'})
        data = json.loads(html)
        # self.assertEqual(data['success'],True);

        try:  # test: another user signup using the same email after first user activated email
            html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-signup'),
                           {'username': 'test_user_4', 'password': '123456', 'email': 'test_user_2@gmail.com'})
        except Exception as e:
            status_code, message = e
            self.assertEqual(status_code, status.HTTP_406_NOT_ACCEPTABLE)

    def test_resetPasswordViaEmail(self, show_test=True):
        if show_test:
            print(u"\n test_resetPasswordViaEmail:")
        self.test_logout(False)
        url = reverse('UserRestfulAPI:AuthAPI-resetPasswordViaEmail')

        html = urlopen(self.client, url, {'email': self.user.email})

        # Test that one message has been sent.
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the subject of the first message is correct.
        self.assertIn("Reset Account Password", mail.outbox[0].subject)
        m = re.search(r'href="(http://.+)"', mail.outbox[0].body)
        self.assertNotEqual(m, None)
        reset_link = m.group(1)

        self.assertEqual(len(mail.outbox), 1)

        try:
            html = urlopen(self.client, reset_link)
        except Exception as e:
            self.assertEqual(e, 302)

        self.assertEqual(len(mail.outbox), 2)
        self.assertIn("Password Has Been Reset", mail.outbox[1].subject)
        # print mail.outbox[1].body;
        m = re.search(r'password: (\w+)', mail.outbox[1].body)
        self.assertNotEqual(m, None)
        new_password = m.group(1)

        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-login'),
                       {'username': self.user.username, 'password': new_password})
        data = json.loads(html)

    # self.assertEqual(data['success'],True);

    def test_resendActivateEmail(self, show_test=True):
        if show_test:
            print(u"\n test_resendActivateEmail:")
        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-signup'),
                       {'username': 'test_user_1', 'password': '123456', 'email': 'test_user_2@gmail.com'})
        self.assertEqual(len(mail.outbox), 1)
        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-resendActivateEmail'),
                       {'username': 'test_user_1', 'password': '123456', 'email': 'test_user_1@gmail.com'})
        self.assertEqual(len(mail.outbox), 2)

        # Verify that the subject of the first message is correct.
        self.assertIn('Activation email', mail.outbox[1].subject)

        m = re.search(r'href="http://testserver(/.+)"', mail.outbox[1].body)
        self.assertNotEqual(m, None)
        url = m.group(1)  # activate link

        # activate email
        try:
            html = urlopen(self.client, url)
        except Exception as e:
            status_code, message = e
            self.assertEqual(status_code, status.HTTP_302_FOUND)

        self.assertEqual(len(mail.outbox), 3)

        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-login'),
                       {'username': 'test_user_1', 'password': '123456'})
        data = json.loads(html)

        html = urlopen(self.client, reverse('UserRestfulAPI:MyProfile-show'))
        data = json.loads(html)

        self.assertEqual(data['username'], 'test_user_1')
        self.assertEqual(data['email'], 'test_user_1@gmail.com')

    def test_changePassword(self, show_test=True):
        if show_test:
            print(u"\n test_changePassword:")
        self.client.login(username=self.user.username, password='123456')
        try:
            html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-changePassword'), method='PUT', data={
                'current_password': '123123',
                'new_password': '123654'
            })
        except Exception as e:
            self.assertEqual(e, status.HTTP_403_FORBIDDEN)

        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-changePassword'), method='PUT', data={
            'current_password': '123456',
            'new_password': '123654'
        })

        self.assertEqual(self.client.login(username=self.user.username, password='123456'), False)
        self.assertEqual(self.client.login(username=self.user.username, password='123654'), True)


# noinspection PyUnusedLocal
class TestUserRestfulAPI(TestCase):

    def setUp(self):
        self.client = APIClient()
        self.user = User(username='test_user', email='test_user@gmail.com')
        self.user.set_password('123456')
        self.user.save()
        self.user.profile.email = self.user.email
        self.user.profile.account_status = AccountStatus.normal
        self.user.profile.save()

        self.client.login(username=self.user.username, password='123456')

    def test_getProfile(self, show_test=True):
        if show_test:
            print(u"\n test_getProfile:")

        html = urlopen(self.client, reverse('UserRestfulAPI:MyProfile-show'))
        data = json.loads(html)

        self.assertEqual(data['username'], self.user.username)

    def test_editProfile(self, show_test=True):
        if show_test:
            print(u"\n test_editProfile:")

        html = urlopen(self.client, reverse('UserRestfulAPI:MyProfile-edit'), method='PATCH', data={
            'full_name': 'Mr. Smith'
        })
        data = json.loads(html)

        self.assertEqual(data['full_name'], 'Mr. Smith')

    def test_changeEmailUsingResendActivateEmail(self, show_test=True):
        if show_test:
            print(u"\n test_changeEmailUsingResendActivateEmail:")

        html = urlopen(self.client, reverse('UserRestfulAPI:AuthAPI-resendActivateEmail'),
                       {'username': self.user.username, 'password': '123456', 'email': 'test_user_1@gmail.com'})
        self.assertEqual(len(mail.outbox), 1)

        # Verify that the subject of the first message is correct.
        self.assertIn('Activation email', mail.outbox[0].subject)

        m = re.search(r'href="http://testserver(/.+)"', mail.outbox[0].body)
        self.assertNotEqual(m, None)
        url = m.group(1)  # activate link

        # activate email
        try:
            html = urlopen(self.client, url)
        except Exception as e:
            status_code, message = e
            self.assertEqual(status_code, status.HTTP_302_FOUND)

        self.assertEqual(len(mail.outbox), 2)

        # recheck email
        html = urlopen(self.client, reverse('UserRestfulAPI:MyProfile-show'))
        data = json.loads(html)
        self.assertEqual(data['email'], 'test_user_1@gmail.com')
