#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  RestfulRoute.py

from .Auth_RestfulAPI import AuthAPI
from .User_RestfulAPI import CurrentUserProfileView, StaffUserListAPI, UserListAPI
from system_configure.controllers.Tool import FullRouter

router = FullRouter('user')
router.register(r'auth', AuthAPI, basename='AuthAPI')
router.register(r'liststaff', StaffUserListAPI, basename='List Staff')
router.register(r'myProfile', CurrentUserProfileView, basename='MyProfile')
router.register(r'', UserListAPI, basename='List User')
urlpatterns = router.urls
