#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  User_RestfulAPI
#
#
#  Created by TVA on 4/2/15.
#  Copyright (c) 2015 storagon. All rights reserved.
from rest_framework import serializers, permissions, viewsets
from rest_framework.decorators import action
from rest_framework.response import Response

from shop_module.enums import BalanceType
from shop_module.models import CustomerProfile, BalanceAccount
from ..controllers import UserController
from ..models import User


class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ('id', 'username', 'date_joined', 'last_login',
                  'first_name', 'last_name', 'email',
                  'is_active', 'is_staff', 'is_superuser', 'groups', 'user_permissions')


class StaffUserSerializer(serializers.ModelSerializer):
    groups = serializers.SerializerMethodField()

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'groups', 'is_superuser')

    @staticmethod
    def get_groups(user):
        return list(user.groups.values_list('name', flat=True))


class UserProfileSerializer(serializers.ModelSerializer):
    class Meta:
        model = UserController.UserProfileClass()
        fields = ('account_status', 'full_name', 'email', 'address', 'user_facebook',
                  'phone_number', 'dob_year', 'signup_ip', 'vip_status', 'earn')
        read_only_fields = ('account_status', 'signup_ip', 'email',)

    # full_name=serializers.CharField(min_length=1,required=False)
    # email=serializers.CharField(min_length=1,required=False)
    # address=serializers.CharField(min_length=1,required=False)
    vip_status = serializers.SerializerMethodField(read_only=True)
    earn = serializers.SerializerMethodField(read_only=True)

    @staticmethod
    def get_vip_status(user):
        customer = CustomerProfile.objects.get(user=user.user)
        return customer.vip_status

    @staticmethod
    def get_earn(user):
        earn = BalanceAccount.objects.filter(account_holder=user.user, type=BalanceType.user_credit)
        if earn.exists():
            return earn[0].amount
        return 0


class UserAdminSerializer(serializers.ModelSerializer):
    profile = UserProfileSerializer()

    class Meta:
        model = User
        fields = ('id', 'username', 'date_joined',
                  'first_name', 'last_name', 'email',
                  'is_active', 'is_staff', 'is_superuser', 'groups', 'profile')


class CurrentUserProfileView(viewsets.GenericViewSet):
    """A view that returns the count of active users in JSON.
    """

    serializer_class = UserProfileSerializer
    permission_classes = (permissions.IsAuthenticated,)

    @action(detail=False, methods=['get'], permission_classes=[permissions.IsAuthenticated])
    def show(self, request, *args, **kwargs):
        slz_list = [UserSerializer(instance=request.user)]

        for serializer in [UserProfileSerializer] + UserController.getExtraProfileSerializerList():
            try:
                profile = serializer.Meta.model.objects.get(user=request.user)
            except Exception as e:
                print(e)
                pass
            else:
                slz_list.append(serializer(instance=profile, context={'request': request}))

        merged_data = {}
        for slz in slz_list:
            merged_data.update(slz.data)
        return Response(merged_data)

    @action(detail=False, methods=['patch'], permission_classes=[permissions.IsAuthenticated])
    def edit(self, request, *args, **kwargs):
        serializer = UserProfileSerializer(instance=request.user.profile,
                                           data=request.data,
                                           partial=True)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=200)
        else:
            return Response(serializer.errors, status=400)


class StaffUserListAPI(viewsets.ReadOnlyModelViewSet):
    serializer_class = StaffUserSerializer
    queryset = User.objects.filter(is_staff=True, is_active=True).order_by('id')


class UserListAPI(viewsets.ModelViewSet):
    serializer_class = UserAdminSerializer
    queryset = User.objects.all().order_by('id')
