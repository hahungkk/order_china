import re
from django.contrib.auth import authenticate, login, logout
from munch import Munch
from rest_framework import serializers, permissions, viewsets
from rest_framework import status
from rest_framework.decorators import action
from shop_module.signals.customer_signals import create_customer_profile
from system_configure.controllers.Tool import *
from ..controllers import EmailController
from ..enums import *
from ..models import User, UserProfile


class BlankForm(serializers.Serializer):
    pass


class LoginForm(serializers.Serializer):
    username = serializers.CharField(min_length=1, max_length=30)
    password = serializers.CharField(min_length=1, max_length=32)


class SignUpForm(LoginForm):
    email = serializers.EmailField(min_length=5, max_length=256)
    referer = serializers.CharField(required=False)

    @staticmethod
    def validate_referer(value):
        """
        Check that referer exist in database.
        """
        if not User.objects.filter(username=value).exists():
            raise serializers.ValidationError("This referer does not exist !")
        return value


class ResetPasswordForm(serializers.Serializer):
    email = serializers.EmailField(min_length=5, max_length=256)


class ResendActivateEmailForm(LoginForm):
    email = serializers.EmailField(min_length=5, max_length=256)


class ChangePasswordForm(serializers.Serializer):
    def create(self, validated_data):
        super(ChangePasswordForm, self).create(validated_data)

    def update(self, instance, validated_data):
        super(ChangePasswordForm, self).update(instance, validated_data)

    current_password = serializers.CharField(min_length=1)
    new_password = serializers.CharField(min_length=3)


class AuthAPI(viewsets.GenericViewSet):
    throttle_scope = 'serious_api'

    @action(detail=False, methods=['post'], serializer_class=LoginForm)
    def login(self, request, *args, **kwargs):
        form_post = LoginForm(data=request.data)
        if not form_post.is_valid():
            return error_response_restful(form_post.errors, code=status.HTTP_400_BAD_REQUEST)
        data = Munch(form_post.data)

        user = authenticate(username=data.username, password=data.password)

        if user is None:
            return error_response_restful(u"Username or password is not correct")
        if user.is_active is False:
            return error_response_restful(u"You need to activate account in order to login",
                                          code=status.HTTP_403_FORBIDDEN)

        user.backend = 'django.contrib.auth.backends.ModelBackend'
        login(request, user)

        return success_response_restful({
            'username': user.username,
            'id': user.id,
            'email': user.email,
            'is_active': user.is_active,
            'is_staff': user.is_staff,
            'last_login': user.last_login,
            'first_name': user.first_name,
            'last_name': user.last_name
        })

    @action(detail=False, methods=['post'], serializer_class=BlankForm)
    def logout(self, request, *args, **kwargs):  # TODO: fix bug cookie CSRF token failed
        logout(request)
        return success_response_restful()

    @action(detail=False, methods=['post'], serializer_class=SignUpForm)
    def signup(self, request, *args, **kwargs):
        form_post = SignUpForm(data=request.data)
        if not form_post.is_valid():
            return error_response_restful(form_post.errors, code=status.HTTP_400_BAD_REQUEST)
        data = Munch(form_post.data)
        if not re.match(r'^\w[a-zA-Z0-9._]{5,29}$', data.username):
            return error_response_restful({'username': [u"Username must be 6-30 char and not contain invalid char."]},
                                          code=status.HTTP_400_BAD_REQUEST)

        # todo: uncomment this, check captcha
        # result=checkRecaptcha(request);
        # if result is not True:
        # 	return result; # errorResponse

        # save user
        if User.objects.filter(username=data.username).exists():
            return error_response_restful({'username': [u"This username is already taken by another user"]})
        if UserProfile.objects.filter(email=data.email).exists():
            return error_response_restful({'email': [u"This email is already taken by another user"]})
        user = User(username=data.username, email=data.email)
        user.set_password(data.password)
        # if SystemConfigureController.getConfigure('requireUserToActivateAccountInOrderToLogin', False):

        try:
            user.is_active = True
            user.save()
        except Exception as e:
            if user.pk:
                user.delete()
            logging.error(u"Unable to create new user, error=%s" % e)
            return error_response_restful(u"Unable to create account, error=%s" % e)

        if request.META['REMOTE_ADDR']:
            user.profile.signup_ip = request.META['REMOTE_ADDR']

        user.profile.account_status = AccountStatus.normal
        user.profile.email = data.email
        user.profile.save()

        # login user
        # user.backend = 'django.contrib.auth.backends.ModelBackend'
        # login(request, user)
        referer = getattr(data, 'referer', None)
        create_customer_profile(user, referer=referer)

        return success_response_restful()

    @action(detail=False,methods=['post'], serializer_class=ResetPasswordForm)
    def reset_password_via_email(self, request, *args, **kwargs):
        if request.user and request.user.is_authenticated:
            return error_response_restful(u"You must logout before reset password.")
        else:
            form_post = ResetPasswordForm(data=request.data)
            if not form_post.is_valid():
                return error_response_restful(form_post.errors, code=status.HTTP_400_BAD_REQUEST)
            data = Munch(form_post.data)

            try:
                user = User.objects.get(email=data.email)
            except User.DoesNotExist:
                return error_response_restful(u"User email is not exist!")

            EmailController.send_reset_password_mail(request, data.email, user.id)

            return success_response_restful()

    @action(detail=False,methods=['post'], serializer_class=ResendActivateEmailForm)
    def resend_activate_email(self, request, *args, **kwargs):
        form_post = ResendActivateEmailForm(data=request.data)
        if not form_post.is_valid():
            return error_response_restful(form_post.errors, code=status.HTTP_400_BAD_REQUEST)
        data = Munch(form_post.data)

        user = authenticate(username=data.username, password=data.password)

        if user is None:
            return error_response_restful(u"Username or password is not correct")

        if UserProfile.objects.filter(email=data.email).exists():
            return error_response_restful({'email': [u"This email is already taken by another user"]})

        # if user.is_active:
        # 	return errorResponseRestful(u"Your account is already activated",code=status.HTTP_403_FORBIDDEN)

        EmailController.send_account_activation_mail(request, data.email, user.id)
        return success_response_restful()

    @action(detail=False,methods=['put'], permission_classes=[permissions.IsAuthenticated], serializer_class=ChangePasswordForm)
    def change_password(self, request, *args, **kwargs):
        form_post = ChangePasswordForm(data=request.data)
        if not form_post.is_valid():
            return error_response_restful(form_post.errors, code=status.HTTP_400_BAD_REQUEST)
        # ::type: ChangePasswordForm
        data = Munch(form_post.data)
        if not request.user.check_password(data.current_password):
            return error_response_restful(u"Current password is invalid", code=status.HTTP_403_FORBIDDEN)

        request.user.set_password(data.new_password)
        request.user.save()
        return success_response_restful()
