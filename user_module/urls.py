#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  urls.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#

from django.conf.urls import include
from django.urls import path

from .api import User_RestfulAPI_route
urlpatterns =[
    path('api/', include(User_RestfulAPI_route)),
]
