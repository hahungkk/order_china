#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  app.py
#  
#
#  Created by TVA on 4/22/15.
#  Copyright (c) 2015 vietcomfund. All rights reserved.
#

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MyAppConfig(AppConfig):
    name = 'user_module'
    verbose_name = _("User Module")

    def ready(self):
        from . import signals
