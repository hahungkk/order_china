SELECT CAST(DATE_FORMAT(CONVERT_TZ(`shop_module_order`.`created`, 'UTC', 'Asia/Bangkok'), '%Y-%m-%d 00:00:00') AS DATETIME)
  AS `created_date`, COUNT(`shop_module_order`.`id`)
  AS `count` FROM `shop_module_order`
WHERE `shop_module_order`.`created` >= '2017-11-22 10:26:25.442428'
GROUP BY CAST(DATE_FORMAT(CONVERT_TZ(`shop_module_order`.`created`, 'UTC', 'Asia/Bangkok'), '%Y-%m-%d 00:00:00') AS DATETIME) ORDER BY NULL;


SELECT
  DATE(customerPayment.created) AS created,
  transaction.currency_id,
  SUM(transaction.amount)
FROM shop_module_customerpayment customerPayment
  LEFT JOIN shop_module_transaction transaction ON transaction.id = customerPayment.transaction_id
WHERE (customerPayment.created BETWEEN %(from_date)s AND %(to_date)s)
GROUP BY DATE(customerPayment.created), transaction.currency_id;