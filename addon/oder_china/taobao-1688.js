function start() {
    const e = window.location.href;
    if (!(e.match(/item.taobao/) || e.match(/detail.tmall/) || e.match(/detail.1688/) || e.match(/tmall.com\/item\//) || e.match(/ebay.com\/itm\//) || e.match(/taobao.com\/item\//) || e.match(/amazon.com[\/A-Za-z\d-]*\/dp/) || e.match(/amazon.com\/gp/) || e.match(/zara.com/) || e.match(/hm.com/))) {
        const tool = new CommonTool;
        t = tool.htmlFormAddCart();
        $("body").append(t);
    } else {
        let t = new factory(cart_url, add_to_cart_url);
        t.init();
    }
    $(document).on("change", "._select_category", function () {
        const e = $(this).val();
        $("._select_category").val(e);
        common_tool.setCategorySelected(e);
        const t = $("._input_category");
        if (e === "-1") {
            t.show();
            t.focus()
        } else {
            t.hide()
        }
    });
    $(document).on("keyup", "._brand_item", function () {
        const e = $(this).val();
        $("._brand_item").val(e)
    });
    $(document).on("keyup", "._comment_item", function () {
        const e = $(this).val();
        $("._comment_item").val(e)
    });
    $(document).on("keyup", "._input_category", function () {
        const e = $(this).val();
        $("._input_category").val(e)
    });
    $(document).on("click", "._addToCart", function () {
        let e = window.location.href;
        if (!(e.match(/item.taobao/) || e.match(/detail.tmall/) || e.match(/detail.yao.95095/) || e.match(/detail.1688/) || e.match(/tmall.com\/item\//) || e.match(/ebay.com\/itm\//) || e.match(/taobao.com\/item\//) || e.match(/amazon.com[\/A-Za-z\d-]*\/dp/) || e.match(/amazon.com\/gp/) || e.match(/zara.com/) || e.match(/hm.com/))) {
            alert(page_not_true);
        } else {
            let e = new factory(cart_url, add_to_cart_url);
            common_tool.addDisabledButtonCart();
            if (origin_site.match(/1688.com/)) {
                e.add_to_cart()
            } else {
                addon_tool.AddToCart()
            }
        }
    });
    $(document).on("click", "._is_translate", function () {
        const e = common_tool.setIsTranslateToCookie();
        if (e) {
            window.location.reload()
        }
    });
    $("._close_tool").click(function () {
        $(".frm-tool").hide();
        $("li#li_sd_price").fadeIn()
    });
    $("._minimize_tool").click(function () {
        $(".frm-tool").fadeIn();
        $("li#li_sd_price").hide()
    });
    $("#txt-category").change(function () {
        const e = $(this).val();
        if (parseInt(e) === -1) {
            $(".category-other").show();
            $(".category-other input").focus()
        } else {
            $(".category-other").hide()
        }
    });

    return true;
}

let exchange_rate = 3590;

$.ajax({
    url: exchange_url,
    type: 'GET',
    contentType: "application/json",
    success: function (response) {
        for (let i in response['results']) {
            if (response['results'][i]['code'] === 'CNY') {
                exchange_rate = parseFloat(response['results'][i]['exchange_rate']);
                console.log("Exchange rate of CNY=" + exchange_rate);
            }
        }
    },
    error: function () {
        console.log('Không thể lấy tỷ giá, vui lòng liên hệ với bộ phận kỹ thuật Alo68.vn');
    }
});

const generateUUID = function () {
    let d = new Date().getTime();
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
        const r = (d + Math.random() * 16) % 16 | 0;
        d = Math.floor(d / 16);
        return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
    });
};
let CommonTool = function () {
    this.addDisabledButtonCart = function () {
        $("._addToCart").attr("disabled", "disabled")
    };
    this.removeDisabledButtonCart = function () {
        $("._addToCart").removeAttr("disabled")
    };
    this.getDivCategory = function () {
        return '<div class="_div_category"><img src="' + seudo_host + 'assets/images/add_on/note_brand.png" style="width:100%">' + '<p>Chọn danh mục <span style="color: red">*</span>: </p>' + '<select class="_select_category" style="width: 100%;height: 26px;">' + "</select>" + '<input placeholder="Tự nhập danh mục" style="width: 100%;height: 26px;margin-top: 10px;display: none" class="_input_category">' + '<p style="margin-top: 6px;height: 26px;margin-bottom: -6px;">Thương Hiệu: </p>' + '<input type="text" style="width: 100%;" placeholder="Nhập thương hiệu của sản phẩm" class="_brand_item">' + "</div>"
    };
    this.getDivCategoryMini = function () {
        // return '<li class="pos-relative">' + '<label class="lbl hidden" for="txt-category">Chọn danh mục <span class="font-red">*</span>: </label>' + '<select id="txt-category" class="fl form-select _select_category">' + "</select>" + '<span class="font-red">*</span>' + '<div class="category-other hidden"><div class="arrow_box"><label class="" for="txt3">Danh mục khác: </label>' + '<input type="text" class="form-input _input_category" placeholder="Tự nhập danh mục"  /></div>        </div>    ' + "</li>" + "<li>" + '<label class="lbl hidden" for="txt1">Thương hiệu: </label>' + '<input type="text" class="fl form-input _brand_item" placeholder="Thương hiệu của sản phẩm..." />    ' + "</li>"
        return '<li class="pos-relative categoryMini">' + '<label class="lbl hidden" for="txt-category">Chọn danh mục <span class="font-red">*</span>: </label>' + '<select id="txt-category" class="fl form-select _select_category">' + '</select>' + '<span class="font-red">*</span>' + '<div class="category-other hidden"><div class="arrow_box"><label class="" for="txt3">Danh mục khác: </label>' + '<input type="text" class="form-input _input_category" placeholder="Tự nhập danh mục"  /></div>        </div>    ' + '</li>' + '<li class="brandMini">' + '<label class="lbl hidden" for="txt1">Thương hiệu: </label>' + '<input type="text" class="fl form-input _brand_item" placeholder="Thương hiệu của sản phẩm..." />    ' + '</li>'
    };
    this.loadOptionCategory = function () {
        $.get(catalog_scalar_url, function (e) {
            // let t = '<option value="0">Chọn danh mục</option>';
            let t = '<option value="1">Chọn danh mục</option>';
            const n = common_tool.getCategorySelected();
            for (let r = 0; r < e.length; r++) {
                const i = e[r];
                t += '<option value="' + i.id + '"';
                if (parseInt(n) === parseInt(i.id)) {
                    t += ' selected="selected"'
                }
                t += ">";
                for (let s = 0; s < i.level; s++) {
                    if (parseInt(i.level) > 1) {
                        t += "&#8212;"
                    }
                }
                t += i.name + "</option>"
            }
            console.log(e);
            t += '<option value="-1">Khác</option>';
            $("._select_category").html(t)
        })
    };
    this.htmlFormAddCart = function () {
        let e = "";
        if (typeof services_name === "undefined") {
            e = ""
        }
        const t = this.getDivCategory();
        const n = this.getDivCategoryMini();
        let r = '<div class="frm-tool"><div class="frm-tool-ct">' + '<a href="javascript:" class="_close_tool"><img width="10px" src="' + seudo_host + '/assets/images/add_on/icon-extend.png" /></a>' + '<div class="content_tool"><span class="frm-title">Công cụ đặt hàng</span>' + "<ul>" + n + "<li>" + '<label class="lbl hidden"  for="txt2">Chú thích: </label>        ' + '<textarea class="fl form-textarea _comment_item" placeholder="Chú thích cho sản phẩm..."></textarea>    ' + '</li></ul><a href="javascript:" class="fl btn-default _addToCart" title="Đặt hàng"></a>' + '<a href="https://order_china.vn/cart" target="_blank" class="fl link-cart" title="Vào giỏ hàng">Vào giỏ hàng</a><label class="fl translate">    ' + '<input type="checkbox" checked="checked">Dịch sang tiếng việt</label></div>' + "</div></div>" + '<li class="div-block-price-book" id="li_sd_price" style="display: none;">' + '<a href="javascript:" title="" class="_minimize_tool">' + '<img width="10px" src="' + seudo_host + 'assets/images/add_on/icon-minimize.png" />' + "</a>" + '<h1 style="font-size: 22px;/* color: red; */color: rgb(232, 3, 3);' + 'text-align: center;">Công Công Cụ Đặt Hàng ' + e + "</h1>" + '<div class="seu-note-book" id="_box_input_exception" style="color: #111;">' + '<div class="note-imgv2"></div>' + '<div class="note-item"><span>Giá:</span><p><input type="text" id="_price" placeholder="Giá"></p></div>' + '<div class="note-item"><span>Thuộc tính:</span><p><textarea style="width: 100%" rows="3" id="_properties" ' + 'placeholder="Nhập màu sắc, kích thước VD:Màu đen; Size 41" name="_properties"></textarea></p></div>' + '<div class="note-item"><span>Số lượng:</span><p><input type="text" id="_quantity" placeholder="Số lượng"></p></div>' + "</div>" + t + '<div class="note-text"><p>Chú thích: </p>' + '<textarea cols="60" class="_comment_item" placeholder="Chú thích cho sản phẩm" ' + 'name="_comment_item"></textarea>' + '</div><div class="xbTipBlock add-book"><div class="add-button" id="block_button_sd">' + '<button class="_addToCart" type="button" style="border: none;cursor: pointer;"></button>' + '<a href="' + cart_url + '" class="cart" target="_blank" style="float: left; margin-left: 20px;margin-top: 10px; color: rgb(0, 114, 188);">Vào giỏ hàng</a>    ' + '<div class="note-img"></div></div></div>';
        // let r = '<div class="frm-tool"><div class="frm-tool-ct">' + '<a href="javascript:" class="_close_tool"><img width="10px" src="' + seudo_host + '/assets/images/add_on/icon-extend.png" /></a>' + '<div class="content_tool"><span class="frm-title">Công cụ đặt hàng</span>' + "<ul>" + "<li>" + '<label class="lbl hidden"  for="txt2">Chú thích: </label>        ' + '<textarea class="fl form-textarea _comment_item" placeholder="Chú thích cho sản phẩm..."></textarea>    ' + '</li></ul><a href="javascript:" class="fl btn-default _addToCart" title="Đặt hàng"></a>' + '<a href="javascript:" class="fl link-cart" title="Vào giỏ hàng">Vào giỏ hàng</a><label class="fl translate">    ' + '<input type="checkbox" checked="checked">Dịch sang tiếng việt</label></div>' + "</div></div>" + '<li class="div-block-price-book" id="li_sd_price" style="display: none;">' + '<a href="javascript:" title="" class="_minimize_tool">' + '<img width="10px" src="' + seudo_host + 'assets/images/add_on/icon-minimize.png" />' + "</a>" + '<h1 style="font-size: 22px;/* color: red; */color: rgb(232, 3, 3);' + 'text-align: center;">Công Công Cụ Đặt Hàng ' + e + "</h1>" + '<div class="seu-note-book" id="_box_input_exception" style="color: #111;">' + '<div class="note-imgv2"></div>' + '<div class="note-item"><span>Giá:</span><p><input type="text" id="_price" placeholder="Giá"></p></div>' + '<div class="note-item"><span>Thuộc tính:</span><p><textarea style="width: 100%" rows="3" id="_properties" ' + 'placeholder="Nhập màu sắc, kích thước VD:Màu đen; Size 41" name="_properties"></textarea></p></div>' + '<div class="note-item"><span>Số lượng:</span><p><input type="text" id="_quantity" placeholder="Số lượng"></p></div>' + "</div>" + '<div class="note-text"><p>Chú thích: </p>' + '<textarea cols="60" class="_comment_item" placeholder="Chú thích cho sản phẩm" ' + 'name="_comment_item"></textarea>' + '</div><div class="xbTipBlock add-book"><div class="add-button" id="block_button_sd">' + '<button class="_addToCart" type="button" style="border: none;cursor: pointer;"></button>' + '<a href="' + cart_url + '" class="cart" target="_blank" style="float: left; margin-left: 20px;margin-top: 10px; color: rgb(0, 114, 188);">Vào giỏ hàng</a>    ' + '<div class="note-img"></div></div></div>';
        const i = this.getCookie("is_translate");
        let s = 1;
        if (typeof translate_first === "undefined") {
            s = 1
        }
        let o = "2.0.0";
        if (typeof version_tool === "undefined") {
            o = "2.0.0"
        } else {
            o = version_tool
        }
        if (parseInt(i) === 1 || i === "" && s === 1) {
            r += '<label><input type="checkbox" checked="checked" name="is_translate" class="_is_translate" />Dịch sang tiếng Việt</label>'
        } else if (i === "" && s === 0) {
            r += '<label><input type="checkbox" name="is_translate" class="_is_translate" />Dịch sang tiếng Việt</label>'
        } else {
            r += '<label><input type="checkbox" name="is_translate" class="_is_translate" />Dịch sang tiếng Việt</label>'
        }
        r += '<span style="float: right;"><b>Phiên bản ' + o + "</b></span></li>";
        return r
    };
    this.getOriginSite = function () {
        let e = window.location.href;
        e = e.replace(/^https?:\/\//, "");
        const t = e.split("/");
        return t[0]
    };
    this.getHomeLand = function () {
        const e = window.location.href;
        if (e.match(/taobao/)) {
            return "TAOBAO"
        }
        if (e.match(/tmall/)) {
            return "TMALL"
        }
        if (e.match(/95095/)) {
            return "TMALL"
        }
        if (e.match(/1688|alibaba/)) {
            return "1688"
        }
        if (e.match(/ebay/)) {
            return "EBAY"
        }
        if (e.match(/amazon/)) {
            return "AMAZON"
        }
        if (e.match(/zara/)) {
            return "ZARA"
        }
        if (e.match(/hm/)) {
            return "HM"
        }
        return null
    };
    this.currency_format = function (e, t) {
        if (!$.isNumeric(e)) {
            return e
        }
        if (t === null || typeof t === "undefined" || t === false) {
            const n = 10;
            e = Math.ceil(e / n) * n
        }
        e = e.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
        return e
    };
    this.getExchangeRate = function () {
        return exchange_rate
    };
    this.trackError = function (e, t, n) {
        const r = "link=" + e + "&error=" + t + "&tool=bookmarklet";
        $.ajax({
            url: n,
            type: "POST",
            data: r,
            success: function (e) {
            }
        })
    };
    this.hasClass = function (e, t) {
        return e.className.indexOf(t) > -1
    };
    this.resizeImage = function (e) {
        return e.replace(/[0-9]{2,3}[x][0-9]{2,3}/g, "150x150")
    };
    this.getParamsUrl = function (e, t) {
        let n = "";
        if (t) {
            n = t
        } else {
            n = window.location.href
        }
        if (n === "") return null;
        const r = (new RegExp("[\\?&]" + e + "=([^&#]*)")).exec(n);
        if (r === null) return null;
        return r[1] || 0
    };
    this.processPrice = function (e, t) {
        if (e == null || parseFloat(e) === 0) return 0;
        let n = 0;
        if (e.constructor === Array) {
            n = String(e[0]).replace(",", ".").match(/[0-9]*[\.]?[0-9]+/g)
        } else {
            n = String(e).replace(",", ".").match(/[0-9]*[\.]?[0-9]+/g)
        }
        if (isNaN(n) || parseFloat(e) === 0) {
            return 0
        }
        let r = "";
        let i = 0;
        if (e.constructor === Array && e.length > 1) {
            const s = this.currency_format(parseFloat(e[0]) * this.getExchangeRate());
            const o = e.length - 1;
            const u = this.currency_format(parseFloat(e[o]) * this.getExchangeRate());
            if (parseFloat(e[o]) > 0) {
                r = s + " ~ " + u
            } else {
                r = s
            }
        } else {
            i = parseFloat(e);
            r = this.currency_format(i * this.getExchangeRate())
        }
        const a = document.createElement("li");
        let f = null;
        let l = null;
        let c = null;
        if (t === "TAOBAO") {
            a.setAttribute("style", "color: blue ! important; padding: 30px 0px; font-family: arial;");
            f = '<span class="tb-property-type" style="color: blue; font-weight: bold; font-size: 25px;">Giá</span>     ' + '<strong id="price_vnd" class="" style="font-size: 25px;">' + '<em class=""> ' + r + ' </em><em class=""> VNĐ</em></strong>';
            a.innerHTML = f;
            l = document.getElementById("J_PromoPrice");
            if (l != null) {
                l.parentNode.insertBefore(a, l.nextSibling)
            } else {
                c = document.getElementById("J_StrPriceModBox");
                if (c != null) {
                    c.parentNode.insertBefore(a, c.nextSibling)
                }
            }
        } else if (t === "TMALL") {
            a.setAttribute("style", "font-weight: bold; padding: 10px 0px;");
            a.setAttribute("class", "tm-promo-price tm-promo-cur");
            f = '<strong id="price_vnd" class="" style="font-size: 30px;">' + '<span class="tm-price">Giá</span>' + '<em class="tm-price" style="font-size: 30px; margin-left: 10px;"> ' + r + "  VNĐ </em></strong>";
            a.innerHTML = f;
            l = document.getElementById("J_PromoPrice");
            if (l != null) {
                l.parentNode.insertBefore(a, l.nextSibling)
            } else {
                c = document.getElementById("J_StrPriceModBox");
                if (c != null) {
                    c.parentNode.insertBefore(a, c.nextSibling)
                }
            }
        }
        return parseFloat(n)
    };
    this.sendAjaxToCart = function (e, k) {
        const n = new CommonTool;
        //reparse data
        // if (options_selected != undefined && options_selected != null && Object.keys(options_selected).length > 0) {
        //     options = options_selected;
        // } else {
        options = {};
        if (k.options !== undefined) {
            const opt = k.options.split(';');
            for (let i in opt) {
                if (opt[i].length > 0) options[k.site + '_' + i] = opt[i]
            }
        }
        // }


        price = Number(k.price);
        price_origin = Number(k.price_origin);
        price_promotion = Number(k.price_promotion);
        if (price_promotion > 0) price = price_promotion;
        if (price <= 0) price = price_origin;
        console.log(price);
        vendor = k.seller_name; // strip HTML
        postData = {
            "shopping_domain": document.domain,
            "vendor": vendor,
            "detail_url": k.link_origin,
            "name": k.name,
            "sku": k.site.toLowerCase() + '_' + k.sku,
            "image_url": decodeURIComponent(k.pic_url),
            "short_description": k.title_origin,
            "price": price,
            "shipping": k.shipping,
            "weight": Math.round(k.weight * 100) / 100,
            "quantity": k.quantity,
            "options_selected": JSON.stringify(options),
            "options_metadata": "",
            "category_list": "",
            "fragile": false,
            "insurance": false,
            "note": k.comment,
            "html": "",
            "currency": "CNY",
            "http_referer": http_referer
        };

        $.ajax({
            url: e,
            type: "POST",
            dataType: "json",
            data: JSON.stringify(postData),
            contentType: "application/json; charset=UTF-8",
            //xhrFields: {
            //	withCredentials: true
            //},
            beforeSend: function () {
                $('#box-confirm-nh-site').remove();
            },
            success: function (data, textStatus, jqXHR) {
                n.removeDisabledButtonCart();
                msg = "Thêm vào giỏ hàng thành công"
                alert(msg)
                //$("body").append()
                //if (e.html) {
                //	$("body").append(e.html)
                //} else {
                //	$("body").append(e)
                //}
            },
            error: function (jqXHR, textStatus, errorThrown) {
                n.removeDisabledButtonCart();
                console.log(jqXHR.responseJSON.detail);
                let msg = "Có lỗi xảy ra (cần đăng nhập trước khi đặt hàng):" + textStatus;
                if (jqXHR.responseJSON.detail === "You have exceed maximum number of cart item!") {
                    msg = full_cart_err;
                }
                alert(msg);
                //$("body").append(msg)
            }
        });

        return true
    };
    this.loadJsFile = function (e) {
        const t = document.createElement("script");
        t.setAttribute("src", e + "?t=" + Math.random());
        document.body.appendChild(t);
        return true
    };
    this.key_translate_lib = function (e) {
        const t = [];
        t["颜色"] = "Màu";
        t["尺码"] = "Kích cỡ";
        t["尺寸"] = "Kích cỡ";
        t["价格"] = "Giá";
        t["促销"] = "Khuyến mại";
        t["配送"] = "Vận Chuyển";
        t["数量"] = "Số Lượng";
        t["销量"] = "Chính sách";
        t["评价"] = "Đánh Giá";
        t["颜色分类"] = "Màu sắc";
        t["促销价"] = "Giá";
        t["套餐类型"] = "Loại";
        t["单价（元）"] = "Giá (NDT)";
        t["库存量"] = "Tồn kho";
        t["采购量"] = "SL mua";
        let n = e;
        if (t[e]) {
            n = t[e]
        }
        return n
    };
    this.stripTags = function (e) {
        if (typeof e == "object") {
            return e.replaceWith(e.html().replace(/<\/?[^>]+>/gi, ""))
        }
        return false
    };
    this.setCategorySelected = function (e) {
        this.setCookie("category_selected", e, 100);
        return true
    };
    this.getCategorySelected = function () {
        return this.getCookie("category_selected")
    };
    this.setIsTranslateToCookie = function () {
        if ($("#_is_translate").prop("checked")) {
            this.setCookie("is_translate", 1, 100)
        } else {
            this.setCookie("is_translate", 0, 100)
        }
        return true
    };
    this.translate_title = function (e, t, n) {
        return;
        const r = this.getCookie("is_translate");
        if (parseInt(r) === 1 || r === "") {
            $.ajax({
                url: translate_url,
                type: "post",
                contentType: "application/x-www-form-urlencoded",
                xhrFields: {
                    withCredentials: true
                },
                data: {
                    text: e,
                    type: t
                },
                success: function (e) {
                    const t = $.parseJSON(e);
                    n.set_translate({
                        title: t["data_translate"]
                    })
                }
            });
            return true
        }
        return false
    };
    this.translate = function (e, t) {
        const n = this.getCookie("is_translate");
        if (parseInt(n) === 1 || n === "") {
            if (t === "properties") {
                this.translateStorage(e)
            }
        }
    };
    this.translateStorage = function (e) {
        try {
            let t = e.text();
            const n = t;
            const r = keyword;
            if (r != null) {
                const i = r.resource;
                for (let s = 0; s < i.length; s++) {
                    const o = i[s];
                    try {
                        if (t.match(o.k_c, "g")) {
                            t = t.replace(o.k_c, o.k_v + " ")
                        }
                    } catch (u) {
                        try {
                            if (t.match(o.keyword_china, "g")) {
                                t = t.replace(o.keyword_china, o.keyword_vi + " ")
                            }
                        } catch (u) {
                        }
                    }
                }
                e.text(t);
                e.attr("data-text", n)
            }
        } catch (u) {
            console.log("error")
        }
    };
    this.ajaxTranslate = function (e, t) {
        const n = e.text();
        $.ajax({
            url: translate_url,
            type: "POST",
            contentType: "application/x-www-form-urlencoded",
            xhrFields: {
                withCredentials: true
            },
            data: {
                text: n,
                type: t
            },
            success: function (t) {
                const n = $.parseJSON(t);
                if (n["data_translate"] && n["data_translate"] != null) {
                    e.attr("data-text", e.text());
                    e.text(n["data_translate"])
                }
            }
        })
    };
    this.getKeywordSearch = function () {
        $.ajax({
            url: translate_keyword_url,
            type: "POST",
            contentType: "application/x-www-form-urlencoded",
            xhrFields: {
                withCredentials: true
            },
            data: {
                text: "text",
                type: "type"
            },
            success: function (e) {
                const t = JSON.stringify(e);
                localStorage.setItem("keyword_search", t)
            }
        });
        return true
    };
    this.showInputEx = function (e) {
        $(".frm-tool").hide();
        $("li#li_sd_price").fadeIn();
        const t = $("#_box_input_exception");
        t.show();
        t.attr("data-is-show", 1);
        const n = $("#_price");
        const r = new factory(cart_url, add_to_cart_url);
        const i = r.getPriceInput();
        const s = r.getPropertiesInput();
        const o = r.getQuantityInput();
        if (o === "" && s === "" && i === "") {
            alert("Chúng tôi không thể lấy được thông tin của sản phẩm." + "Bạn vui lòng điền thông tin để chúng tôi mua hàng cho bạn");
            n.focus();
            try {
                if (e !== "alibaba") {
                    if (parseFloat(r.getPromotionPrice()) > 0) {
                        n.val(r.getPromotionPrice())
                    } else {
                        n.attr("placeholder", "Nhập tiền tệ - Trung Quốc")
                    }
                    $("#_properties").val(r.getPropertiesOrigin());
                    $("#_quantity").val(r.getQuantity())
                }
            } catch (u) {
                console.log(u)
            }
        }
        return true
    };
    this.setCookie = function (e, t, n) {
        const r = new Date;
        r.setTime(r.getTime() + n * 24 * 60 * 60 * 1e3);
        const i = "expires=" + r.toGMTString();
        document.cookie = e + "=" + t + "; " + i;
        return true
    };
    this.getCookie = function (e) {
        const t = e + "=";
        const n = document.cookie.split(";");
        for (let r = 0; r < n.length; r++) {
            let i = n[r];
            while (i.charAt(0) === " ") i = i.substring(1);
            if (i.indexOf(t) !== -1) {
                return i.substring(t.length, i.length)
            }
        }
        return ""
    }
    // convert a number of money from one currency to usd
    this.convertToUsd = function (to_currency, orgin_price) {
        return us_exchange_rate[to_currency] * orgin_price;
    }

    // get only number from price
    this.getMoneyNumber = function (price) {
        const i = price.match(/[0-9,\.]+/g);
        price = i[0].replace(",", "");
        return price;
    }
};
let factory = function (e, t) {
    const n = new CommonTool;
    const r = n.getOriginSite();
    let i;
    if (r.match(/1688.com/)) {
        i = new alibaba(e, t)
    } else if (r.match(/taobao.com/)) {
        i = new taobao(e)
    } else if (r.match(/tmall.com/)) {
        i = new tmall(e)
    } else if (r.match(/95095.com/)) {
        i = new tmall(e)
    } else if (r.match(/ebay.com/)) {
        i = new ebay(e)
    } else if (r.match(/amazon.com/)) {
        i = new amazon(e)
    } else if (r.match(/zara.com/)) {
        i = new zara(e)
    } else if (r.match(/hm.com/)) {
        i = new hm(e)
    } else {
        console.log("this website does not support !")
    }
    return i
};
const AddonTool = function () {
    this.common_tool = new CommonTool;
    this.AddToCart = function () {
        let e = 0;
        const t = new factory(cart_url, add_to_cart_url);
        const n = $("#_box_input_exception").attr("data-is-show");
        let r = "",
            i = "",
            s = "",
            o = "",
            u = "";
        const a = $("._select_category");
        const f = a.val();
        const l = $("._select_category option:selected").text();
        const c = $("._brand_item").val();
        const h = t.checkSelectFull();
        if (!h) {
            alert("Yêu cầu chọn đầy đủ thuộc tính của SP");
            this.common_tool.removeDisabledButtonCart();
            return false
        }
        try {
            r = t.getOriginPrice();
            i = t.getPromotionPrice();
            // alert(r);
            // alert(i);
            if ($.isArray(r)) {
                r = r[0]
            }
            if ($.isArray(i)) {
                i = i[0]
            }
            s = t.getProperties();
            o = t.getPropertiesOrigin();
            u = t.getQuantity()
        } catch (p) {
            e = 1;
            r = i = t.getPriceInput();
            o = s = t.getPropertiesInput();
            u = t.getQuantityInput()
        }
        const d = t.getImgLink();
        const v = t.getSellerId();
        const m = t.getSellerName();
        const shop_seller = t.getShopSeller();
        let g = t.getWangwang();
        if (g === "") {
            g = m
        }
        const y = t.getTitleOrigin();
        const b = t.getTitleTranslate();
        const w = t.getCommentInput();
        const E = window.location.href;
        const S = t.getItemID();
        const x = t.getDataValue();
        let T = t.getOuterId(x);
        if ($.isArray(T)) {
            T = T[0]
        }
        const N = this.common_tool.getHomeLand();
        let currency_type = '';
        if (N === 'EBAY' || N === 'ZARA') {
            currency_type = t.getCurrencyType();
        }
        let C = t.getStock();
        if (!$.isNumeric(C) || parseInt(C) <= 0) {
            C = 99
        }

        // alert(e);
        // alert(n);
        // alert(i);
        // alert(r);

        if (e && parseFloat(n) !== 1 || parseFloat(n) !== 1 && parseInt(i) <= 0 && parseInt(r) <= 0) {
            //alert('vao day');
            this.common_tool.showInputEx();
            this.common_tool.removeDisabledButtonCart();
            return false
        }
        if ((r === "" && i === "" || u === "") && parseFloat(n) === 1) {
            alert("Yêu cầu bổ sung thông tin.");

            $("#_price").focus();
            this.common_tool.removeDisabledButtonCart();
            return false
        }
        if (!$.isNumeric(i) && parseFloat(n) === 1) {
            alert("Yêu cầu nhập giá của sản phẩm (không chứa chữ cái)");
            $("#_price").focus();
            this.common_tool.removeDisabledButtonCart();
            return false
        }
        const domestic_shipping_price = t.getShippingPrice(); // gia chuyen hang den kho

        if (domestic_shipping_price === '-1') {
            alert(shipping_err_cant_ship);
            this.common_tool.removeDisabledButtonCart();
            return false
        }
        const uuid = generateUUID();
        const k = {
            title_origin: $.trim(y),
            title_translated: $.trim(b),
            price_origin: r,
            price_promotion: i,
            shipping: domestic_shipping_price,
            property_translated: s,
            property: o,
            data_value: x,
            image_model: d,
            image_origin: d,
            shop_id: v,
            shop_name: m,
            shop_seller: shop_seller,
            wangwang: g,
            quantity: u,
            stock: C,
            site: N,
            comment: w,
            item_id: S,
            link_origin: E,
            // outer_id: T,
            outer_id: uuid,
            error: e,
            weight: 0,
            step: 1,
            brand: c,
            category_name: l,
            category_id: f,
            tool: "Addon",
            currency_type: currency_type,

            name: $.trim(y),
            sku: uuid,
            price: r,
            seller_id: v,
            seller_name: shop_seller,
            pic_url: d,
            options: o,
            link: E,
            http_referer: http_referer
        };
        // alert('123');
        const homeland = this.common_tool.getHomeLand();
        if (homeland === 'AMAZON' || homeland === 'EBAY')
            this.common_tool.sendAjaxToCart(add_to_us_cart_url, k)
        else this.common_tool.sendAjaxToCart(add_to_cart_url, k)
    }
};
let taobao = function (e) {
    this.source = "taobao";
    this.common_tool = new CommonTool;
    this.init = function () {
        const e = $("#detail");
        e.css("border", "1px solid red");
        e.css("font-size", "11px");
        $(".tb-rmb").remove();
        this.parse()
    };
    this.parse = function () {
        const e = this.common_tool;
        $(".tb-property-type").each(function (e, t) {
            const n = $(this).text();
            $(this).text(common_tool.key_translate_lib(n))
        });
        const t = e.htmlFormAddCart();
        $("body").append(t);

        let n = this.getPromotionPrice("TAOBAO");
        const r = '<p style="font-size: 16px;margin-top: 15px;">' + "Tỉ giá: " + e.currency_format(e.getExchangeRate(), true) + " VNĐ / 1 CNY</p>";
        let i = $("#J_StrPriceModBox");
        if (i == null || i === "" || typeof i === "object" && i.length === 0) {
            i = $(".tm-promo-price")
        }
        if (i == null || i === "" || typeof i === "object" && i.length === 0) {
            i = $(".tb-detail-hd")
        }
        if (i == null || i === "" || typeof i === "object" && i.length === 0) {
            i = $("#J_PromoPrice")
        }
        if (i != null && i !== "") {
            i.append(r)
        }
        const s = this.getTitleOrigin();
        e.setIsTranslateToCookie();
        e.translate_title(s, "title", this);
        this.translateProperties();
        return false
    };
    this.translateProperties = function () {
        const e = this.common_tool;
        let t = $(".J_TSaleProp li span");
        if (t == null || t.length === 0) {
            t = $(".J_SKU a span")
        }
        t.each(function () {
            e.translate($(this), "properties")
        })
    };
    this.getPriceInput = function () {
        return $("#_price").val()
    };
    this.getPropertiesInput = function () {
        return $("#_properties").val()
    };
    this.getQuantityInput = function () {
        return $("#_quantity").val()
    };
    this.getCommentInput = function () {
        return $("._comment_item").val()
    };
    this.set_translate = function (e) {
        const t = this.getDomTitle();
        if (t != null && e.title !== "") {
            t.setAttribute("data-text", t.textContent);
            t.textContent = e.title
        }
    };
    this.getPromotionPrice = function (e) {
        try {
            let t = null;
            let n = document.getElementById("J_StrPrice");
            if (n == null) {
                n = document.getElementById("J_priceStd")
            }
            if (n == null) {
                n = document.getElementById("J_StrPriceModBox")
            }
            if (n == null) {
                n = document.getElementById("J_PromoPrice")
            }
            let r = document.getElementById("J_PromoPrice");
            let i = 0;
            if (r == null) {
                r = n
            }
            if (r != null) {
                try {
                    if (r.getElementsByClassName("tm-price").length > 0) {
                        t = r.getElementsByClassName("tm-price");
                        if (t != null && t !== "" && t !== "undefined") {
                            i = t[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                        }
                    } else if (r.getElementsByClassName("tb-rmb-num").length > 0) {
                        t = r.getElementsByClassName("tb-rmb-num");
                        if (t != null && t !== "" && t !== "undefined") {
                            i = t[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                        }
                    }
                } catch (s) {
                    i = 0
                }
            }
            return this.common_tool.processPrice(i, e)
        } catch (o) {
            throw Error(o.message + " Line:" + o.lineNumber + " function getPromotionPrice")
        }
    };
    this.getStock = function () {
        try {
            let e = document.getElementById("J_EmStock");
            let t = 99;
            if (e == null || e === "undefined") {
                e = document.getElementById("J_SpanStock")
            }
            if (e != null && e !== "undefined") {
                t = e.textContent;
                t = parseInt(t.replace(/[^\d.]/g, ""))
            }
        } catch (n) {
            t = 99
        }
        return t
    };
    this.getOriginPrice = function () {
        try {
            let e = document.getElementById("J_StrPrice");
            if (e == null) {
                e = document.getElementById("J_priceStd")
            }
            if (e == null) {
                e = document.getElementById("J_StrPriceModBox")
            }
            if (e == null) {
                e = document.getElementById("J_PromoPrice")
            }
            if (window.location.href.indexOf("taobao") > -1) {
                let t = 0;
                const n = e.getElementsByClassName("tb-rmb-num");
                if (n != null && typeof n === "object" && n.length > 0) {
                    try {
                        t = e.getElementsByClassName("tb-rmb-num")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                    } catch (r) {
                        t = e.getElementsByClassName("tm-price")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                    }
                }
            } else {
                try {
                    t = e.getElementsByClassName("tm-price")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                } catch (r) {
                    t = e.getElementsByClassName("tb-rmb-num")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                }
            }
            return this.common_tool.processPrice(t)
        } catch (i) {
            throw Error(i.message + " Can't get origin price function getOriginPrice")
        }
    };
    this.getOuterId = function (e) {
        try {
            const t = document.getElementsByTagName("script");
            let n = "";
            let r = null;
            if (t.length > 0) {
                for (let i = 0; i < t.length; i++) {
                    if (t[i].innerHTML.match(/Hub\.config\.set/)) {
                        try {
                            detailJsStart();
                            n = Hub.config.get("sku").valItemInfo.skuMap[";" + e + ";"].skuId
                        } catch (s) {
                            r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
                            n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
                        }
                    } else if (t[i].innerHTML.match(/TShop\.Setup/)) {
                        r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
                        n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
                    }
                }
            }
            return n
        } catch (o) {
            return ""
        }
    };
    this.getTitleTranslate = function () {
        try {
            const e = this.getDomTitle();
            let t = e.textContent;
            if (t === "") {
                t = e.getAttribute("data-text")
            }
            return t
        } catch (n) {
            return ""
        }
    };
    this.getTitleOrigin = function () {
        try {
            const e = this.getDomTitle();
            let t = e.getAttribute("data-text");
            if (t === "" || typeof t == "undefined" || t == null) {
                t = e.textContent
            }
            return t
        } catch (n) {
            return ""
        }
    };
    this.getDomTitle = function () {
        try {
            let e = null;
            if (document.getElementsByClassName("tb-main-title").length > 0) {
                e = document.getElementsByClassName("tb-main-title")[0]
            }
            if (e == null && document.getElementsByClassName("tb-detail-hd").length > 0) {
                const t = document.getElementsByClassName("tb-detail-hd")[0];
                if (t.getElementsByTagName("h3").length > 0 && t != null) {
                    e = t.getElementsByTagName("h3")[0]
                } else {
                    e = t.getElementsByTagName("h1")[0]
                }
            }
            if (e.textContent === "" && document.getElementsByClassName("tb-tit").length > 0) {
                e = document.getElementsByClassName("tb-tit")[0]
            }
            if (e.textContent === "") {
                e = document.querySelectorAll("h3.tb-item-title");
                if (e != null) {
                    e = e[0]
                } else {
                    e = document.getElementsByClassName("tb-item-title");
                    if (e.length > 0) {
                        e = e[0]
                    }
                }
            }
            return e
        } catch (n) {
            return null
        }
    };
    this.getShopSeller = function () {
        try {
            let e = "";
            if (document.getElementsByClassName("tb-shop-seller").length > 0) {
                e = document.getElementsByClassName("tb-shop-seller")[0].getElementsByTagName("a")[0].innerText;
            }
            return e;
        } catch (i) {
            return "";
        }
    };
    this.getSellerName = function () {
        try {
            let e = "";
            if (document.getElementsByClassName("tb-seller-name").length > 0) {
                e = document.getElementsByClassName("tb-seller-name")[0].textContent;
                if (e === "" || e == null) {
                    const t = document.getElementsByClassName("shop-card");
                    const n = t.length > 0 ? t[0].getElementsByClassName("ww-light") : "";
                    e = n.length > 0 ? n[0].getAttribute("data-nick") : "";
                    if (e === "") {
                        if (document.getElementsByClassName("base-info").length > 0) {
                            for (let r = 0; r < document.getElementsByClassName("base-info").length; r++) {
                                if (document.getElementsByClassName("base-info")[r].getElementsByClassName("seller").length > 0) {
                                    if (document.getElementsByClassName("base-info")[r].getElementsByClassName("seller")[0].getElementsByClassName("J_WangWang").length > 0) {
                                        e = document.getElementsByClassName("base-info")[r].getElementsByClassName("seller")[0].getElementsByClassName("J_WangWang")[0].getAttribute("data-nick");
                                        break
                                    }
                                    if (document.getElementsByClassName("base-info")[r].getElementsByClassName("seller")[0].getElementsByClassName("ww-light").length > 0) {
                                        e = document.getElementsByClassName("base-info")[r].getElementsByClassName("seller")[0].getElementsByClassName("ww-light")[0].getAttribute("data-nick");
                                        break
                                    }
                                }
                            }
                        }
                    }
                }
            } else if ($("#J_tab_shopDetail").length > 0) {
                e = $("#J_tab_shopDetail").find("span").first().attr("data-nick")
            }
            e = e.trim();
            return e
        } catch (i) {
            return ""
        }
    };
    this.getSellerId = function () {
        try {
            let e = "";
            let t = "";
            let n = "";
            let r = document.querySelectorAll(".tb-shop-name");
            if (r.length > 0) {
                n = r[0].getElementsByTagName("a")[0]
            } else {
                r = document.querySelectorAll("div.shop-card");
                if (r.length > 0) {
                    n = r[0].getElementsByTagName("p")[0].getElementsByTagName("a")[0]
                }
            }
            if (!(typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0)) {
                r = document.getElementsByClassName("hd-shop-name");
                if (typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0) {
                    n = r[0].getElementsByTagName("a")[0]
                }
            }
            if (!(typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0)) {
                r = document.querySelectorAll("span.shop-name");
                if (r.length > 0 && r != null) {
                    n = r[0].getElementsByTagName("a")[0]
                } else {
                    r = document.querySelectorAll("div.shop-card");
                    if (r.length > 0 && r != null) {
                        n = r[0].getElementsByTagName("p")[0].getElementsByTagName("a")[0]
                    } else {
                        r = document.querySelectorAll("a.slogo-shopname");
                        if (r.length > 0 && r != null) {
                            n = r[0]
                        } else {
                            r = document.querySelectorAll("div#side-shop-info");
                            if (typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0) {
                                r = r[0].getElementsByClassName("shop-intro")[0];
                                n = r.getElementsByTagName("a");
                                n = n[0]
                            }
                        }
                    }
                }
            }
            if (n != null && n !== "") {
                t = n.getAttribute("href");
                e = t.split(".")[0];
                e = e.split("http://")[1]
            }
            return e
        } catch (i) {
            return ""
        }
    };
    this.getProperties = function () {
        try {
            let e = document.getElementsByClassName("J_TSaleProp");
            let t = "";
            if (!(typeof e != "object" && e !== "" && e != null || typeof e === "object" && e.length > 0)) {
                e = document.querySelectorAll("ul.tb-cleafix")
            }
            if (e.length > 0 && e != null) {
                for (let n = 0; n < e.length; n++) {
                    const r = e[n].getElementsByClassName("tb-selected")[0];
                    if (r != null) {
                        let i = r.getElementsByTagName("span")[0].getAttribute("data-text");
                        if (i === "" || i == null || typeof i == "undefined") {
                            i = r.getElementsByTagName("span")[0].textContent
                        }
                        t += i + ";"
                    }
                }
            }
            return t
        } catch (s) {
            throw Error(s.message + " Can't get origin price function getPropertiesOrigin")
        }
    };
    this.getPropertiesOrigin = function () {
        try {
            let e = document.getElementsByClassName("J_TSaleProp");
            let t = "";
            if (!(typeof e !== "object" && e !== "" && e != null || typeof e === "object" && e.length > 0)) {
                e = document.querySelectorAll("ul.tb-cleafix")
            }
            if (e.length > 0) {
                for (let n = 0; n < e.length; n++) {
                    const r = e[n].getElementsByClassName("tb-selected")[0];
                    if (r != null) {
                        let i = r.getElementsByTagName("span")[0].getAttribute("data-text");
                        if (i === "" || i == null || typeof i == "undefined") {
                            i = r.getElementsByTagName("span")[0].textContent
                        }
                        t += i + ";"
                    }
                }
            }
            return t
        } catch (s) {
            throw Error(s.message + " Can't get origin price function getPropertiesOrigin")
        }
    };
    this.getDataValue = function () {
        try {
            const e = document.getElementsByClassName("J_TSaleProp");
            let t = "";
            if (e.length > 0) {
                for (let n = 0; n < e.length; n++) {
                    const r = e[n].getElementsByClassName("tb-selected")[0];
                    t += ";" + r.getAttribute("data-value")
                }
            }
            if (t.charAt(0) === ";") {
                t = t.substring(1, t.length)
            }
            return t
        } catch (i) {
            return ""
        }
    };
    this.getWangwang = function () {
        try {
            let e = document.querySelectorAll("span.seller");
            if (e != null && e !== "" && e !== "undefined" && e.length > 0) {
                const t = document.getElementsByClassName("slogo-extraicon");
                if (t != null && t !== "" && t !== "undefined" && t.length > 0) {
                    e = t[0].getElementsByClassName("ww-light")
                }
            }
            if (e == null || e === "" || e.length === 0) {
                e = document.querySelectorAll("div.hd-shop-desc span.ww-light")
            }
            let n = "";
            if (e.length > 0) {
                const r = e[0].getElementsByTagName("span");
                if (r != null && r !== "" && r.length === 0) {
                    n = decodeURIComponent(r[0].getAttribute("data-nick"))
                } else {
                    n = decodeURIComponent(e[0].getAttribute("data-nick"))
                }
            }
        } catch (i) {
            n = ""
        }
        return n
    };
    this.checkSelectFull = function () {
        let e = document.getElementsByClassName("J_TSaleProp");

        if (!(typeof e != "object" && e !== "" && e != null || typeof e === "object" && e.length > 0)) {
            e = document.querySelectorAll("ul.tb-cleafix");

        }
        let t = true;

        if (e.length > 0) {
            let n = 0;
            for (let r = 0; r < e.length; r++) {
                const i = e[r].getElementsByClassName("tb-selected");
                if (i != null && i !== "undefined") n += i.length
            }
            if (n < e.length) {
                t = false
            }
        }
        return t
    };
    this.getQuantity = function () {
        try {
            let e = "";
            const t = document.getElementById("J_IptAmount");
            if (t) {
                e = t.value
            } else e = "";
            if (e === "") {
                try {
                    e = document.getElementsByClassName("mui-amount-input")[0].value
                } catch (n) {
                    console.log(n)
                }
            }
            return e
        } catch (r) {
            throw Error(r.message + " Can't get origin price function getQuantity")
        }
    };
    this.getImgLink = function () {
        try {
            let e = "";
            try {
                let t = document.getElementById("J_ImgBooth");
                if (t != null) {
                    e = t.getAttribute("src");
                    e = this.common_tool.resizeImage(e);
                    return encodeURIComponent(e)
                }
                t = document.getElementById("J_ThumbView");
                if (t != null && t !== "") {
                    e = t.getAttribute("src");
                    e = this.common_tool.resizeImage(e);
                    return encodeURIComponent(e)
                }
                if (document.getElementById("J_ImgBooth").tagName === "IMG") {
                    const n = document.getElementById("J_UlThumb");
                    try {
                        if (n != null) {
                            e = n.getElementsByTagName("img")[0].src
                        } else {
                            e = document.getElementById("J_ImgBooth").src
                        }
                    } catch (r) {
                        console.log(r)
                    }
                } else {
                    const i = document.getElementById("J_UlThumb");
                    if (i != null) {
                        e = i.getElementsByTagName("li")[0].style.backgroundImage.replace(/^url\(["']?/, "").replace(/["']?\)$/, "")
                    } else {
                        e = document.getElementById("J_ImgBooth").style.backgroundImage.replace(/^url\(["']?/, "").replace(/["']?\)$/, "")
                    }
                }
            } catch (r) {
                console.log("Image not found!" + r)
            }
            e = this.common_tool.resizeImage(e);
            return encodeURIComponent(e)
        } catch (s) {
            return ""
        }
    };
    this.getItemID = function () {
        try {
            const e = window.location.href;
            let t = this.common_tool.getParamsUrl("id", e);
            let n = document.getElementsByName("item_id");
            if (t <= 0 || !$.isNumeric(t)) {
                if (n.length > 0) {
                    n = n[0];
                    t = n.value
                } else t = 0;
                if (t === 0 || t == null || t === "") {
                    n = document.getElementsByName("item_id_num");
                    if (n.length > 0) {
                        n = n[0];
                        t = n.value
                    } else t = 0
                }
            }
            if (parseInt(t) <= 0 || !$.isNumeric(t)) {
                t = e.split(".htm")[0];
                t = t.split("item/")[1]
            }
            return t
        } catch (r) {
            return 0
        }
    }
    this.getShippingPrice = function () {
        let china_shipping_price_taobao;

        const e = window.location.href;

        if (e.match(/item.taobao/)) {
            let itemIdLink = document.getElementById("detail-recommend-viewed").getAttribute("data-itemid");
            let sellerIdLink = document.getElementById("detail-recommend-bought").getAttribute("data-sellerid");
        } else if (e.match(/world.taobao/)) {
            const thumbDiv = document.getElementById("J_ThumbContent").getElementsByTagName("a")[0].getAttribute("href");
            let itemIdLink = thumbDiv.match(/itemId=(\d*)/)[1];
            let sellerIdLink = thumbDiv.match(/shopId=(\d*)/)[1];
        }


        const url = 'https://detailskip.taobao.com/json/postageFee.htm?itemId=' + itemIdLink + '&ap=true&sellerId=' + sellerIdLink + '&areaId=451481';
        // Response handlers.
        let china_shipping_price;
        $.ajax({
            url: url,
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            complete: function (data) {

                const text = data.responseText;
                const text_ex = text.match("<span>(.*?)</span>")[0];
                if (text_ex.indexOf("免运费") >= 0) {
                    china_shipping_price = 0;
                } else {
                    china_shipping_price = text_ex.match(/\d+/)[0];
                }
            }
        });
        return china_shipping_price;
    };
};
let tmall = function (e) {
    this.source = "tmall";
    this.common_tool = new CommonTool;
    this.init = function () {
        $("#detail").css("border", "2px solid orange");
        this.parse()
    };
    this.parse = function () {
        const e = this.common_tool;
        $(".tb-metatit").each(function () {
            const t = $(this).text();
            $(this).text(e.key_translate_lib(t))
        });
        let t = e.htmlFormAddCart();
        $("body").append(t);
        this.common_tool.loadOptionCategory();
        $("#J_ButtonWaitWrap").hide();
        const n = '<p style="font-size: 16px;margin-top: 15px;">Tỉ giá: ' + e.currency_format(e.getExchangeRate(), true) + " VNĐ / 1 CNY</p>";
        this.getPromotionPrice("TMALL");
        const r = $(".tb-detail-hd");
        if (r != null && r !== "") {
            r.append(n)
        } else {
            $(".tb-btn-buy").html(n)
        }
        const i = this.getTitleOrigin();
        e.setIsTranslateToCookie();
        e.translate_title(i, "title", this);
        this.translateProperties();
        return false
    };
    this.set_translate = function (e) {
        const t = this.getDomTitle();
        if (t != null && e.title !== "") {
            t.setAttribute("data-text", t.textContent);
            t.textContent = e.title
        }
    };
    this.translateProperties = function () {
        const e = this.common_tool;
        $(".J_TSaleProp li span").each(function () {
            e.translate($(this), "properties")
        })
    };
    this.getPriceInput = function () {
        return $("#_price").val()
    };
    this.getPropertiesInput = function () {
        return $("#_properties").val()
    };
    this.getQuantityInput = function () {
        return $("#_quantity").val()
    };
    this.getCommentInput = function () {
        return $("._comment_item").val()
    };
    this.checkSelectFull = function () {
        let e = document.getElementsByClassName("J_TSaleProp");
        if (!(typeof e != "object" && e !== "" && e != null || typeof e === "object" && e.length > 0)) {
            e = document.querySelectorAll("ul.tb-cleafix")
        }
        let t = true;
        if (e.length > 0) {
            let n = 0;
            for (let r = 0; r < e.length; r++) {
                const i = e[r].getElementsByClassName("tb-selected");
                if (i != null && i !== "undefined") n += i.length
            }
            if (n < e.length) {
                t = false
            }
        }
        return t
    };
    this.getPromotionPrice = function (e) {
        try {
            let t = null;
            let n = document.getElementById("J_StrPrice");
            if (n == null) {
                n = document.getElementById("J_priceStd")
            }
            if (n == null) {
                n = document.getElementById("J_StrPriceModBox")
            }
            if (n == null) {
                n = document.getElementById("J_PromoPrice")
            }
            let r = document.getElementById("J_PromoPrice");
            let i = 0;
            if (r == null) {
                r = n
            }
            if (r != null) {
                try {
                    if (r.getElementsByClassName("tm-price").length > 0) {
                        t = r.getElementsByClassName("tm-price");
                        if (t != null && t !== "" && t !== "undefined") {
                            i = t[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                        }
                    } else if (r.getElementsByClassName("tb-rmb-num").length > 0) {
                        t = r.getElementsByClassName("tb-rmb-num");
                        if (t != null && t !== "" && t !== "undefined") {
                            i = t[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                        }
                    }
                } catch (s) {
                    i = 0
                }
            }
            return this.common_tool.processPrice(i, e)
        } catch (o) {
            throw Error(o.message + " Line:" + o.lineNumber + " function getPromotionPrice")
        }
    };
    this.getOriginPrice = function () {
        try {
            let e = document.getElementById("J_StrPrice");
            if (e == null) {
                e = document.getElementById("J_priceStd")
            }
            if (e == null) {
                e = document.getElementById("J_StrPriceModBox")
            }
            if (e == null) {
                e = document.getElementById("J_PromoPrice")
            }
            if (window.location.href.indexOf("taobao") > -1) {
                let t = 0;
                const n = e.getElementsByClassName("tb-rmb-num");
                if (n != null && typeof n === "object" && n.length > 0) {
                    try {
                        t = e.getElementsByClassName("tb-rmb-num")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                    } catch (r) {
                        t = e.getElementsByClassName("tm-price")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                    }
                }
            } else {
                try {
                    t = e.getElementsByClassName("tm-price")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                } catch (r) {
                    t = e.getElementsByClassName("tb-rmb-num")[0].textContent.match(/[0-9]*[\.,]?[0-9]+/g)
                }
            }
            return this.common_tool.processPrice(t)
        } catch (i) {
            throw Error(i.message + " Can't get origin price function getOriginPrice")
        }
    };
    this.getQuantity = function () {
        try {
            let e = "";
            const t = document.getElementById("J_IptAmount");
            if (t) {
                e = t.value
            } else e = "";
            if (e === "") {
                e = document.getElementsByClassName("mui-amount-input")[0].value
            }
            return e
        } catch (n) {
            throw Error(n.message + " Can't get origin price function getQuantity")
        }
    };
    this.getOuterId = function (e) {
        try {
            const t = document.getElementsByTagName("script");
            let n = "";
            let r = null;
            if (t.length > 0) {
                for (let i = 0; i < t.length; i++) {
                    if (t[i].innerHTML.match(/Hub\.config\.set/)) {
                        try {
                            detailJsStart();
                            n = Hub.config.get("sku").valItemInfo.skuMap[";" + e + ";"].skuId
                        } catch (s) {
                            r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
                            n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
                        }
                    } else if (t[i].innerHTML.match(/TShop\.Setup/)) {
                        r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
                        n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
                    }
                }
            }
            return n
        } catch (o) {
            return ""
        }
    };
    this.getTitleTranslate = function () {
        try {
            const e = this.getDomTitle();
            let t = e.textContent;
            if (t === "") {
                t = e.getAttribute("data-text")
            }
            return t
        } catch (n) {
            return ""
        }
    };
    this.getTitleOrigin = function () {
        try {
            const e = this.getDomTitle();
            let t = e.getAttribute("data-text");
            if (t === "" || typeof t == "undefined" || t == null) {
                t = e.textContent
            }
            return t
        } catch (n) {
            return ""
        }
    };
    this.getDomTitle = function () {
        let e = null;
        if (document.getElementsByClassName("tb-main-title").length > 0) {
            e = document.getElementsByClassName("tb-main-title")[0]
        }
        if (e == null && document.getElementsByClassName("tb-detail-hd").length > 0) {
            const t = document.getElementsByClassName("tb-detail-hd")[0];
            if (t.getElementsByTagName("h3").length > 0 && t != null) {
                e = t.getElementsByTagName("h3")[0]
            } else {
                e = t.getElementsByTagName("h1")[0]
            }
        }
        if (e.textContent === "" && document.getElementsByClassName("tb-tit").length > 0) {
            e = document.getElementsByClassName("tb-tit")[0]
        }
        if (e.textContent === "") {
            e = document.querySelectorAll("h3.tb-item-title");
            if (e != null) {
                e = e[0]
            } else {
                e = document.getElementsByClassName("tb-item-title");
                if (e.length > 0) {
                    e = e[0]
                }
            }
        }
        return e
    };
    this.getShopSeller = function () {
        try {
            let e = "";
            if (document.getElementsByClassName("slogo").length > 0) {
                if (document.getElementsByClassName("slogo-shopname").length > 0) {
                    e = document.getElementsByClassName("slogo-shopname")[0].innerHTML
                } else if (document.getElementsByClassName("flagship-icon").length > 0) {
                    e = document.getElementsByClassName("slogo")[0].getElementsByTagName("span")[1].getAttribute("data-tnick")
                } else {
                    e = document.getElementsByClassName("slogo")[0].getElementsByTagName("span")[0].getAttribute("data-tnick")
                }
            } else {
                if (document.getElementsByClassName("bts-extend").length > 0) {
                    try {
                        e = document.getElementsByClassName("bts-extend")[0].getElementsByTagName("li")[1].getElementsByTagName("span")[0].getAttribute("data-tnick")
                    } catch (t) {
                        console.log("Seller name not found!" + t)
                    }
                } else {
                    seller_dict = document.getElementsByName("seller_nickname")[0];
                    e = seller_dict.value
                }
            }
            e = e.trim()

        } catch (n) {
            e = ""
        }
        return e
    };
    this.getSellerName = function () {
        try {
            let e = "";
            if (document.getElementsByClassName("slogo").length > 0) {
                if (document.getElementsByClassName("slogo-shopname").length > 0) {
                    e = document.getElementsByClassName("slogo-shopname")[0].innerHTML
                } else if (document.getElementsByClassName("flagship-icon").length > 0) {
                    e = document.getElementsByClassName("slogo")[0].getElementsByTagName("span")[1].getAttribute("data-tnick")
                } else {
                    e = document.getElementsByClassName("slogo")[0].getElementsByTagName("span")[0].getAttribute("data-tnick")
                }
            } else {
                if (document.getElementsByClassName("bts-extend").length > 0) {
                    try {
                        e = document.getElementsByClassName("bts-extend")[0].getElementsByTagName("li")[1].getElementsByTagName("span")[0].getAttribute("data-tnick")
                    } catch (t) {
                        console.log("Seller name not found!" + t)
                    }
                } else {
                    seller_dict = document.getElementsByName("seller_nickname")[0];
                    e = seller_dict.value
                }
            }
            e = e.trim()

        } catch (n) {
            e = ""
        }
        return e
    };
    this.getStock = function () {
        try {
            let e = document.getElementById("J_EmStock");
            let t = 99;
            if (e == null || e === "undefined") {
                e = document.getElementById("J_SpanStock")
            }
            if (e != null && e !== "undefined") {
                t = e.textContent;
                t = parseInt(t.replace(/[^\d.]/g, ""))
            }
        } catch (n) {
            t = 99
        }
        return t
    };
    this.getSellerId = function () {
        try {
            let e = "";
            let t = "";
            let n = "";
            let r = document.querySelectorAll(".tb-shop-name");
            if (r.length > 0) {
                n = r[0].getElementsByTagName("a")[0]
            } else {
                r = document.querySelectorAll("div.shop-card");
                if (r.length > 0) {
                    n = r[0].getElementsByTagName("p")[0].getElementsByTagName("a")[0]
                }
            }
            if (!(typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0)) {
                r = document.getElementsByClassName("hd-shop-name");
                if (typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0) {
                    n = r[0].getElementsByTagName("a")[0]
                }
            }
            if (!(typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0)) {
                r = document.querySelectorAll("span.shop-name");
                if (r.length > 0 && r != null) {
                    n = r[0].getElementsByTagName("a")[0]
                } else {
                    r = document.querySelectorAll("div.shop-card");
                    if (r.length > 0 && r != null) {
                        n = r[0].getElementsByTagName("p")[0].getElementsByTagName("a")[0]
                    } else {
                        r = document.querySelectorAll("a.slogo-shopname");
                        if (r.length > 0 && r != null) {
                            n = r[0]
                        } else {
                            r = document.querySelectorAll("div#side-shop-info");
                            if (typeof r !== "object" && r !== "" && r != null || typeof r === "object" && r.length > 0) {
                                r = r[0].getElementsByClassName("shop-intro")[0];
                                n = r.getElementsByTagName("a");
                                n = n[0]
                            }
                        }
                    }
                }
            }
            if (n != null && n !== "") {
                t = n.getAttribute("href");
                e = t.split(".")[0];
                e = e.split("http://")[1]
            }
        } catch (i) {
            e = ""
        }
        return e
    };
    this.getProperties = function () {
        let e = document.getElementsByClassName("J_TSaleProp");
        let t = "";
        if (!(typeof e != "object" && e !== "" && e != null || typeof e === "object" && e.length > 0)) {
            e = document.querySelectorAll("ul.tb-cleafix")
        }
        if (e.length > 0 && e != null) {
            for (let n = 0; n < e.length; n++) {
                const r = e[n].getElementsByClassName("tb-selected")[0];
                if (r != null) {
                    let i = r.getElementsByTagName("span")[0].getAttribute("data-text");
                    if (i === "" || i == null || typeof i == "undefined") {
                        i = r.getElementsByTagName("span")[0].textContent
                    }
                    t += i + ";"
                }
            }
        }
        return t
    };
    this.getPropertiesOrigin = function () {
        let e = document.getElementsByClassName("J_TSaleProp");
        let t = "";
        if (!(typeof e !== "object" && e !== "" && e != null || typeof e === "object" && e.length > 0)) {
            e = document.querySelectorAll("ul.tb-cleafix")
        }
        if (e.length > 0) {
            for (let n = 0; n < e.length; n++) {
                const r = e[n].getElementsByClassName("tb-selected")[0];
                if (r != null) {
                    let i = r.getElementsByTagName("span")[0].getAttribute("data-text");
                    if (i === "" || i == null || typeof i == "undefined") {
                        i = r.getElementsByTagName("span")[0].textContent
                    }
                    t += i + ";"
                }
            }
        }
        return t
    };
    this.getDataValue = function () {
        try {
            const e = document.getElementsByClassName("J_TSaleProp");
            let t = "";
            if (e.length > 0) {
                for (let n = 0; n < e.length; n++) {
                    const r = e[n].getElementsByClassName("tb-selected")[0];
                    t += ";" + r.getAttribute("data-value")
                }
            }
            if (t.charAt(0) === ";") {
                t = t.substring(1, t.length)
            }
            return t
        } catch (i) {
            return ""
        }
    };
    this.getWangwang = function () {
        try {
            let e = document.querySelectorAll("span.seller");
            if (e != null && e !== "" && e !== "undefined" && e.length > 0) {
                const t = document.getElementsByClassName("slogo-extraicon");
                if (t != null && t !== "" && t !== "undefined" && t.length > 0) {
                    e = t[0].getElementsByClassName("ww-light")
                }
            }
            if (e == null || e === "" || e.length === 0) {
                e = document.querySelectorAll("div.hd-shop-desc span.ww-light")
            }
            let n = "";
            if (e.length > 0) {
                const r = e[0].getElementsByTagName("span");
                if (r != null && r !== "" && r.length === 0) {
                    n = decodeURIComponent(r[0].getAttribute("data-nick"))
                } else {
                    n = decodeURIComponent(e[0].getAttribute("data-nick"))
                }
            }
        } catch (i) {
            console.log(i);
            n = ""
        }
        return n
    };
    this.getImgLink = function () {
        let e = "";
        try {
            let t = document.getElementById("J_ImgBooth");
            if (t != null) {
                e = t.getAttribute("src");
                e = this.common_tool.resizeImage(e);
                return encodeURIComponent(e)
            }
            t = document.getElementById("J_ThumbView");
            if (t != null && t !== "") {
                e = t.getAttribute("src");
                e = this.common_tool.resizeImage(e);
                return encodeURIComponent(e)
            }
            if (document.getElementById("J_ImgBooth").tagName === "IMG") {
                const n = document.getElementById("J_UlThumb");
                try {
                    if (n != null) {
                        e = n.getElementsByTagName("img")[0].src
                    } else {
                        e = document.getElementById("J_ImgBooth").src
                    }
                } catch (r) {
                    console.log(r)
                }
            } else {
                const i = document.getElementById("J_UlThumb");
                if (i != null) {
                    e = i.getElementsByTagName("li")[0].style.backgroundImage.replace(/^url\(["']?/, "").replace(/["']?\)$/, "")
                } else {
                    e = document.getElementById("J_ImgBooth").style.backgroundImage.replace(/^url\(["']?/, "").replace(/["']?\)$/, "")
                }
            }
        } catch (r) {
            e = ""
        }
        e = this.common_tool.resizeImage(e);
        return encodeURIComponent(e)
    };
    this.getItemID = function () {
        try {
            const e = window.location.href;
            let t = this.common_tool.getParamsUrl("id", e);
            let n = document.getElementsByName("item_id");
            if (t <= 0 || !$.isNumeric(t)) {
                if (n.length > 0) {
                    n = n[0];
                    t = n.value
                } else t = 0;
                if (t === 0 || t == null || t === "") {
                    n = document.getElementsByName("item_id_num");
                    if (n.length > 0) {
                        n = n[0];
                        t = n.value
                    } else t = 0
                }
            }
            if (parseInt(t) <= 0 || !$.isNumeric(t)) {
                t = e.split(".htm")[0];
                t = t.split("item/")[1]
            }
            return t
        } catch (r) {
            return ""
        }
    }
    this.getShippingPrice = function () {
        const areaId = "&areaId=440100";
        const scripts = document.getElementsByTagName('script');
        if (scripts.length > 0) {
            for (let script = 0; script < scripts.length; script++) {
                if (scripts[script].innerHTML.match('LocationApi":"(.*?)"')) {
                    let url = scripts[script].innerHTML.match('LocationApi":"(.*?)"')[1];
                    url = url + areaId;
                }
            }
        }
        let china_shipping_price;
        $.ajax({
            url: url,
            type: "GET",
            contentType: 'application/json; charset=utf-8',
            dataType: 'json',
            async: false,
            complete: function (data) {
                const response = data.responseText;

                // if (response.match('"default":(.*?),"') > 0) {
                //     china_shipping_price = response.match('"postage":"(.*?)"')[0].match(/\d+/)[0];                       
                // } else {
                //     china_shipping_price = response.match('"money":(.*?),"')[0].match(/\d+/)[0];
                // }
                //console.log(china_shipping_price)
                try {
                    china_shipping_price = response.match('"postage":"(.*?)"')[0].match(/\d+/)[0];
                } catch (err) {
                    china_shipping_price = 0;
                }
            }
        });
        return china_shipping_price;
    };
};
let alibaba = function (e, t) {
    this.source = "alibaba";
    this.common_tool = new CommonTool;
    this.init = function () {
        $("#J_DetailInside").css("border", "solid 1px blue;");
        this.parse()
    };
    this.parse = function () {
        const e = this.common_tool;
        $(".content-wrapper table thead th").each(function () {
            const t = $.trim($(this).text());
            $(this).text(e.key_translate_lib(t))
        });
        let t = $(".prop-single");
        const n = $.trim(t.text());
        t.text(e.key_translate_lib(n));
        const r = $("div.content-wrapper-spec");
        r.css("height", "300px");
        const i = r.find(".summary");
        const s = r.find(".content");
        const o = r.find(".unit-detail-order-action");
        i.css("height", "100% !important");
        s.css("height", "100%");
        o.css("width", "230px");
        const u = this.getPrice(1);
        const a = $(".table-wrap");
        const f = e.htmlFormAddCart();
        $("body").append(f);

        const l = $("#mod-detail-bd");
        if (l != null) {
            l.css("border", "2px solid red")
        }
        const c = '<div style="font-size: 24px;color: #c00;height: 100px;padding: 20px">' + "<p>Tỉ giá : " + this.common_tool.getExchangeRate() + " VNĐ / 1 CNY</p>" + '<span style="font-weight:normal">Giá tạm tính: ' + e.currency_format(u * this.common_tool.getExchangeRate()) + " VNĐ</span></div>";
        if (a != null && typeof a === "object" && a.length > 0) {
            a.append(c)
        } else {
            try {
                const h = $(".obj-leading");
                if (h != null && h.length > 0) {
                    h.before(c)
                } else {
                    $(".obj-sku").before(c)
                }
            } catch (p) {
            }
        }
        const d = $(".mod-detail-hd h1");
        d.attr("data-origin-title", d.text());
        this.common_tool.translate_title(d.text(), "title", this);
        e.setIsTranslateToCookie();
        return false
    };
    this.set_translate = function (e) {
        const t = $(".mod-detail-hd h1");
        t.html(e["title"]);
        return true
    };
    this.getPriceInput = function () {
        return $("#_price").val()
    };
    this.getPropertiesInput = function () {
        return $("#_properties").val()
    };
    this.getQuantityInput = function () {
        return $("#_quantity").val()
    };
    this.getCommentInput = function () {
        return $("._comment_item").val()
    };
    this.add_to_cart = function () {
        let e = null;
        // try {
        const n = this.get_item_data();
        const r = document.getElementsByClassName("content-wrapper");
        let i = null;
        let s = true;
        if (r.length > 0) {
            i = r[0].getElementsByClassName("content");
            if (i.length > 0) {
                const o = i[0].getElementsByClassName("leading");
                if (!(o != null && typeof o === "object" && o.length > 0)) {
                    s = false
                }
            }
        } else {
            const u = document.getElementsByClassName("list-leading");
            if (u.length > 0) {
                const a = u[0].getElementsByClassName("selected");
                if (a.length > 0) {
                    s = true
                }
            }
        }
        if (s === false) {
            alert("Bạn chưa chọn màu sắc");
            this.common_tool.removeDisabledButtonCart();
            return
        }
        if (n.length === 0) {
            alert("Bạn chưa chọn số lượng sản phẩm");
            this.common_tool.removeDisabledButtonCart();
            return
        }

        let total_quatity = 0;
        for (let f in n) {
            if (!$.isNumeric(f)) {
                continue
            }
            if (n[f][0] === 0) {
                alert("Bạn chưa chọn số lượng sản phẩm");
                this.common_tool.removeDisabledButtonCart();
                return
            }
            total_quatity = total_quatity + parseInt(n[f][0])
        }

        for (let f in n) {
            if (!$.isNumeric(f)) {
                continue
            }
            if (n[f][0] === 0) {
                alert("Bạn chưa chọn số lượng sản phẩm");
                this.common_tool.removeDisabledButtonCart();
                return
            }
            e = this.getDataSend(n[f], total_quatity);
            if (e != null && typeof e != "undefined" && e !== "") {
                this.common_tool.sendAjaxToCart(t, e)
            }
        }
        return true
        // } catch (l) {
        // 	console.log(l)
        //     e = this.getDataSend([]);
        //     if (e != null) {
        //         this.common_tool.sendAjaxToCart(t, e)
        //     }
        //     return false
        // }
    };
    this.getDataSend = function (e, total_quatity) {
        console.log(e);
        try {
            const t = $("._select_category");
            const n = t.val();
            const r = $("._select_category option:selected").text();
            const i = $("._brand_item").val();

            let s = 0;
            const o = this.getItemId();
            const u = this.getItemTitle();
            const a = this.getItemImage();
            const f = this.getItemLink();
            const l = this.getShopId();
            const c = this.getShopName();
            const memberId = this.getMemberId();
            const h = this.getPriceTable();
            const p = this.getStep();
            const d = this.getRequireMin();
            let v = this.getStock();
            const uuid = generateUUID();
            const templateId = this.getTemplateId();
            try {
                if (v <= 0) {
                    v = e[1]
                }
            } catch (m) {
                v = 9999
            }
            let g = this.getWangwang();
            if (g === "") {
                g = c
            }
            const y = this.getWeight();
            try {
                console.log(e[0])
                let b = this.getPrice(total_quatity.toString());
                if (parseFloat(b) <= 0) {
                    const w = document.getElementsByClassName("content-wrapper");
                    if (w.length > 0) {
                        if (w[0].getElementsByClassName("content").length > 0) {
                            b = e[4]
                        }
                    }
                }
                if (!$.isNumeric(b) || parseFloat(b) <= 0) {
                    s = 1;
                    b = this.getPriceInput()
                }
            } catch (m) {
                s = 1;
                b = this.getPriceInput()
            }
            try {
                let E = e[3] + ";" + e[2];
                if (E === "undefined;undefined" || E === "undefined") {
                    s = 1;
                    E = this.getPropertiesInput()
                }
            } catch (m) {
                s = 1;
                E = this.getPropertiesInput()
            }
            try {
                let S = e[0];
                if (!$.isNumeric(S) || parseInt(S) <= 0) {
                    s = 1;
                    S = this.getQuantityInput()
                }
            } catch (m) {
                s = 1;
                S = this.getQuantityInput()
            }
            console.log("S:", S);
            console.log("b: ", b);
            const x = $(".unit-detail-order-action textarea").val();
            if (!$.isNumeric(S) || parseInt(S) <= 0 || !$.isNumeric(b) || parseFloat(b) <= 0) {
                const T = $("#_box_input_exception").attr("data-is-show");
                console.log(T, "QQ");
                if (parseFloat(T) !== 1) {
                    try {
                        alert("Chúng tôi không thể lấy được thông tin của sản phẩm, " + "Bạn vui lòng điền thông tin để chúng tôi mua hàng cho bạn");
                        $(".frm-tool").hide();
                        $("li#li_sd_price").fadeIn();
                        const N = $("#_price");
                        N.focus();
                        if (parseFloat(b) > 0) {
                            N.val(b)
                        } else {
                            N.attr("placeholder", "Nhập tiền nhân dân tệ")
                        }
                        $("#_properties").val(E);
                        $("#_quantity").val(S)
                    } catch (m) {
                    }
                    this.common_tool.showInputEx("alibaba")
                }
                this.common_tool.removeDisabledButtonCart();
                return null
            }
            if (memberId === 0) {
                let url_get_shipping_price = 'https://laputa.1688.com/offer/ajax/CalculateFreight.do?callback=1688_china_shipping_price&amount=' + S + '&weight=' + y + '&price=' + b + '&templateId=' + templateId + '&memberId=' + l + '&volume=0&offerId=' + o + '&countryCode=1001&provinceCode=2614&cityCode=2615'
            } else {
                let url_get_shipping_price = 'https://laputa.1688.com/offer/ajax/CalculateFreight.do?callback=1688_china_shipping_price&amount=' + S + '&weight=' + y + '&price=' + b + '&templateId=' + templateId + '&memberId=' + memberId + '&volume=0&offerId=' + o + '&countryCode=1001&provinceCode=2614&cityCode=2615'
            }

            let china_shipping_price = function () {
                $.ajax({
                    url: url_get_shipping_price,
                    type: "GET",
                    contentType: 'application/json; charset=utf-8',
                    dataType: 'json',
                    async: false,
                    complete: function (data) {
                        const response = data.responseText;
                        try {
                            china_shipping_price = response.match('cost":"(.*?)"')[0].match(/\d+/)[0];
                        } catch (e) {
                            china_shipping_price = 0;
                        }
                    }
                });
                return china_shipping_price;
            };

            return {
                title_origin: $.trim(u),
                title_translated: $.trim(u),
                price_origin: b,
                price_promotion: b,
                price_table: h,
                shipping: china_shipping_price,
                property_translated: E,
                property: E,
                data_value: "",
                image_model: a,
                image_origin: a,
                shop_id: l,
                memberId: memberId,
                shop_name: c,
                wangwang: g,
                quantity: S,
                require_min: d,
                stock: v,
                site: "1688",
                comment: x,
                item_id: o,
                link_origin: f,
                // outer_id: "",
                outer_id: uuid,
                templateId: templateId,
                weight: y,
                error: s,
                step: p,
                brand: i,
                category_name: r,
                category_id: n,
                tool: "Addon",

                name: $.trim(u),
                sku: uuid,
                price: b,
                seller_id: c,
                seller_name: c,
                pic_url: a,
                options: E,
                link: f
            }
        } catch (C) {
            throw Error(C + "Error function getDataSend()")
        }
    };
    this.get_item_data = function () {
        const e = [];
        let t = [];
        let n = 0;
        let r = null;
        try {
            const i = document.getElementsByClassName("content-wrapper");
            let s = null;
            let o = null;
            if (i.length > 0) {
                s = i[0].getElementsByClassName("content")
            }
            if (s != null) {
                s = s[0];
                t = s.getElementsByClassName("amount-input");
                if (t.length > 0) {
                    n = 0;
                    o = i[0].getElementsByClassName("leading");
                    if (o.length > 0) {
                        o = o[0].getElementsByClassName("selected")[0].getAttribute("title").replace(/\n+/, "").replace(/\s+/, "");
                        for (let u in t) {
                            if (isNaN(t[u].value) || t[u].value === 0) {
                                continue
                            }
                            r = t[u].parentNode.parentNode.parentNode.parentNode;
                            e[n] = new Array;
                            e[n][0] = t[u].value;
                            e[n][1] = r.getElementsByClassName("count")[0].getElementsByTagName("span")[0].textContent.replace(/\s+/, "");
                            e[n][2] = o === "" ? "" : r.getElementsByClassName("name")[0].getElementsByTagName("span")[0].textContent.replace(/\s+/, "").replace(/\n+/, "");
                            e[n][3] = o === "" ? r.getElementsByClassName("name")[0].getElementsByTagName("span")[0].textContent.replace(/\s+/, "").replace(/\n+/, "") : o;
                            e[n][4] = r.getElementsByClassName("price")[0].getElementsByTagName("em")[0].textContent.replace(/\s+/, "");
                            n++
                        }
                    } else {
                        for (let u in t) {
                            if (isNaN(t[u].value) || t[u].value === 0) {
                                continue
                            }
                            r = t[u].parentNode.parentNode.parentNode.parentNode;
                            e[n] = new Array;
                            e[n][0] = t[u].value;
                            e[n][1] = r.getElementsByClassName("count")[0].getElementsByTagName("span")[0].textContent.replace(/\s+/, "");
                            e[n][2] = "";
                            const a = r.getElementsByClassName("name")[0].getElementsByTagName("span");
                            const f = r.getElementsByClassName("name")[0].getElementsByClassName("image");
                            e[n][3] = f.length > 0 ? f[0].getAttribute("title") : a[0].textContent.replace(/\s+/, "").replace(/\n+/, "");
                            e[n][4] = r.getElementsByClassName("price")[0].getElementsByTagName("em")[0].textContent.replace(/\s+/, "");
                            n++
                        }
                    }
                }
            } else {
                const l = document.getElementsByClassName("obj-sku");
                const c = document.getElementsByClassName("obj-amount");
                if (l != null && typeof l === "object" && l.length > 0) {
                    t = l[0].getElementsByClassName("amount-input")
                } else if (c != null && typeof c === "object" && c.length > 0) {
                    t = c[0].getElementsByClassName("amount-input")
                }
                if (t.length > 0) {
                    n = 0;
                    o = document.getElementsByClassName("obj-leading");
                    if (o.length > 0) {
                        o = o[0].querySelectorAll("a.selected");
                        if (o != null) {
                            o = o[0].getAttribute("title").replace(/\n+/, "").replace(/\s+/, "")
                        }
                        for (let u in t) {
                            if (isNaN(t[u].value) || t[u].value === 0) {
                                continue
                            }
                            r = t[u].parentNode.parentNode.parentNode.parentNode;
                            e[n] = this.getProperties(r, t[u], o);
                            n++
                        }
                    } else {
                        for (let u in t) {
                            if (isNaN(t[u].value) || t[u].value === 0) {
                                continue
                            }
                            r = t[u].parentNode.parentNode.parentNode.parentNode;
                            e[n] = this.getProperties(r, t[u], "");
                            n++
                        }
                    }
                }
            }
            return e
        } catch (h) {
            throw Error(h + "Error function get_item_data()")
        }
    };
    this.getProperties = function (e, t, n) {
        try {
            const r = null;
            let i = null;
            let s = null;
            let o = null;
            const u = [];
            let a = null;
            u[0] = t.value;
            i = e.getElementsByClassName("count");
            if (i != null && typeof i === "object" && i.length > 0) {
                u[1] = i[0].getElementsByTagName("span")[0].textContent.replace(/\s+/, "")
            } else {
                u[1] = 9999
            }
            s = e.getElementsByClassName("name");
            if (s != null && typeof s === "object" && s.length > 0 && n !== "") {
                a = s[0].getElementsByTagName("span")[0];
                if (this.common_tool.hasClass(a, "image")) {
                    u[2] = a.getAttribute("title").replace(/\s+/, "").replace(/\n+/, "")
                } else {
                    u[2] = a.textContent.replace(/\s+/, "").replace(/\n+/, "")
                }
            } else {
                u[2] = ""
            }
            if (s != null && typeof s === "object" && s.length > 0 && n === "") {
                a = s[0].getElementsByTagName("span")[0];
                if (this.common_tool.hasClass(a, "image")) {
                    u[3] = a.getAttribute("title").replace(/\s+/, "").replace(/\n+/, "")
                } else {
                    u[3] = a.textContent.replace(/\s+/, "").replace(/\n+/, "")
                }
            } else {
                u[3] = n
            }
            o = e.getElementsByClassName("price");
            if (o != null && typeof o === "object" && o.length > 0) {
                u[4] = o[0].getElementsByTagName("em")[0].textContent.replace(/\s+/, "")
            } else {
                u[4] = 0
            }
            return u
        } catch (f) {
            throw Error(f + "Error function getProperties()")
        }
    };
    this.getComment = function () {
        let e = document.getElementById("_comment_sd");
        if (e != null) {
            e = e.value
        } else {
            e = ""
        }
        return e
    };
    this.getPriceTable = function () {
        const e = [];
        let t = null;
        let n = [];
        let r = null;
        let i = null;
        let s = 0;
        try {
            r = document.getElementById("mod-detail-price");
            if (r != null) {
                const o = r.getElementsByClassName("unit-detail-price-amount");
                if (o != null && o.length > 0) {
                    i = o[0].getElementsByTagName("tr");
                    if (i.length > 0) {
                        for (s = 0; s < i.length; s++) {
                            n = i[s];
                            t = JSON.parse(n.getAttribute("data-range"));
                            e.push(t)
                        }
                    }
                } else {
                    i = r.querySelectorAll("tr.price td");
                    if (i != null && i.length > 0) {
                        for (let u = 0; u < i.length; u++) {
                            try {
                                n = i[u];
                                const a = n.getAttribute("data-range");
                                if (a !== "") {
                                    t = JSON.parse(a);
                                    e.push(t)
                                }
                            } catch (f) {
                            }
                        }
                    }
                }
            } else {
                const l = {};
                const c = document.getElementsByClassName("offerdetail_common_beginAmount");
                if (c.length > 0) {
                    l.begin = c[0].getElementsByTagName("p")[0].textContent;
                    l.begin = l.begin.match(/[0-9]+/)[0];
                    r = document.getElementsByClassName("unit-detail-price-display")[0].textContent.split("-");
                    const h = {};
                    for (s = 0; s < r.length; s++) {
                        h[s] = r[s].match(/[0-9]*[\.]?[0-9]+/g).join("")
                    }
                    l.price = h;
                    l.end = ""
                }
                e.push(l)
            }
        } catch (p) {
            throw Error(f + "Error function getPriceTable()")
        }
        return JSON.stringify(e)
    };
    this.getRequireMin = function () {
        let e = 1;
        try {
            e = iDetailConfig.beginAmount
        } catch (t) {
            try {
                const n = $(".unit-detail-freight-cost");
                if (n != null) {
                    let r = n.attr("data-unit-config");
                    r = $.parseJSON(r);
                    e = r.beginAmount
                }
            } catch (i) {
                e = 1
            }
        }
        return e
    };
    this.getStep = function () {
        try {
            let e = 1;
            const t = document.getElementsByClassName("mod-detail-purchasing-multiple");
            const n = document.getElementsByClassName("mod-detail-purchasing-single");
            const r = document.getElementsByClassName("mod-detail-purchasing-quotation");
            let i = null;
            if (t.length > 0 && t != null) {
                i = JSON.parse(t[0].getAttribute("data-mod-config"));
                e = i.wsRuleNum
            } else if (n.length > 0 && n != null) {
                i = JSON.parse(n[0].getAttribute("data-mod-config"));
                e = i.wsRuleNum
            } else if (r.length > 0 && r != null) {
                e = 0
            } else {
                e = 1
            }
            if (e === "" || e == null) {
                e = 1
            }
            return e
        } catch (s) {
            throw Error(s + "Error function getStep()")
        }
    };
    this.getMinAmount = function () {
        const e = 1;
        const t = document.getElementById("mod-detail-price");
        if (t == null) {
            return e
        }
        const n = t.getElementsByTagName("span");
        if (n == null) {
            return e
        }
        const r = n[0].textContent;
        if (r.indexOf("-") !== -1) {
            return r.split("-")[0]
        }
        if (r.indexOf("<") !== -1) {
            return e
        }
        if (r.indexOf(">") !== -1) {
            return r.split(">")[0]
        }
        if (r.indexOf("?") !== -1) {
            return r.split("?")[1]
        }
        return e
    };
    this.getPrice = function (e) {
        try {
            let t = 0;
            e = parseInt(e);
            const n = this.getPriceTable();
            t = this.getPriceByPriceTable(n, e);
            if (parseFloat(t) > 0) {
                return this.common_tool.processPrice(t)
            }
            let r = document.getElementsByClassName("mod-detail-price-sku");
            if (r != null && r !== "" && r !== "undefined") {
                r = r[0]
            }
            if (r != null && r !== "" && r !== "undefined") {
                const i = r.getElementsByTagName("span")[2].textContent;
                const s = r.getElementsByTagName("span")[3].textContent;
                t = i + s;
                return this.common_tool.processPrice(t)
            }
            const o = document.getElementById("mod-detail-price");
            if (o == null) {
                return this.common_tool.processPrice(t)
            }
            const u = $("#mod-detail-price table");
            if (u != null && u !== "" && u !== "undefined") {
                const a = u.find('tr[class="price"] td');
                if (t === 0) {
                    $(".table-sku tr .amount-input").each(function () {
                        const e = $(this).val();
                        if (e > 0) {
                            const n = $(this).parent().parent().parent().parent();
                            const r = $.trim(n.find("td.name").text());
                            t = n.find("td.price .value").text()
                        }
                    })
                }
                if (t === 0) {
                    const f = $(".price-original-sku").find("span:nth-child(5)");
                    const l = f.html();
                    if (l && l != null && l !== "undefined") {
                        t = f.text()
                    } else {
                        t = $(".price-original-sku").find("span:nth-child(2)").text()
                    }
                }
                if (t === 0) {
                    const c = $(".unit-detail-price-amount").find("tr");
                    const h = $(".amount-input").first();
                    let e = h.val();
                    for (let p = 0; p < c.length; p++) {
                        let d = $(c[p]).attr("data-range");
                        d = $.parseJSON(d);
                        if (e < d.begin) {
                            d.begin = 1
                        }
                        if (d.end === 0 || d.end === "") {
                            d.end = 5e3
                        }
                        if (e >= d.begin && e <= d.end) {
                            t = d.price;
                            break
                        }
                    }
                }
                return this.common_tool.processPrice(t)
            }
            const v = o.getElementsByTagName("span");
            if (v == null || v === "") {
                return this.common_tool.processPrice(t)
            } else {
                let m = "";
                for (let g = 0; g < v.length; g++) {
                    const y = v[g].textContent;
                    if (y.indexOf("-") !== -1 || y.indexOf("?") !== -1) {
                        if (y.indexOf("-") !== -1) {
                            m = y.split("-");
                            t = v[g + 1].textContent + "" + v[g + 2].textContent;
                            if (e >= m[0] && e <= m[1]) {
                                break
                            }
                        }
                        if (y.indexOf("?") !== -1) {
                            t = v[g + 1].textContent + "" + v[g + 2].textContent
                        }
                    }
                }
            }
            return this.common_tool.processPrice(t)
        } catch (b) {
            throw Error(b + "Error function getPrice()")
        }
    };
    this.getPriceByPriceTable = function (e, t) {
        let n = 0;
        try {
            e = JSON.parse(e);
            if (typeof e === "object") {
                for (let r in e) {
                    if (e[r] != null) {
                        const i = e[r].begin;
                        const s = e[r].end;
                        if (i <= t && t <= s || i <= t && (parseInt(s) === 0 || s == null || s === "") || t <= i) {
                            n = e[r].price;
                            break
                        } else {
                            n = e[r].price
                        }
                    }
                }
            }
        } catch (o) {
            n = 0
        }
        return n
    };
    this.getShopName = function () {
        let e = "";
        try {
            let s_name = document.querySelector("a[data-tracelog='wp_widget_supplierinfo_compname']").innerHTML;
            if (s_name.length > 0) {
                e = s_name
            } else {
                s_name = document.querySelector("a[onclick=\"return aliclick(this,'?tracelog=wp_infowidget_membername')\"]").innerHTML
                if (s_name.length > 0) {
                    e = s_name
                }
            }
        } catch (n) {
            console.log("Khong lay duoc thong tin nguoi ban!")
        }
        return e
    };
    this.getShopId = function () {
        let e = "";
        try {
            const t = t;
            e = t.acookieAdditional.member_id
        } catch (n) {
            try {
                e = iDetailConfig.feedbackUid
            } catch (r) {
                e = ""
            }
        }
        if (e === "") {
            let i = document.getElementsByClassName("tplogo");
            if (i != null && i.length > 0) {
                let s = i[0].getAttribute("href");
                e = s.split(".")[0];
                e = e.split("http://")[1]
            } else {
                const o = document.querySelectorAll("div.logo");
                if (o != null && o.length > 0) {
                    i = o[0].getElementsByTagName("a");
                    if (i != null && i.length > 0) {
                        s = i[0].getAttribute("href");
                        e = s.split(".")[0];
                        e = e.split("http://")[1]
                    }
                }
            }
        }
        return e
    };
    this.getItemId = function () {
        try {
            let e = 0;
            try {
                e = iDetailConfig.offerid
            } catch (t) {
                const n = window.location.href;
                const r = n.split(".html")[0];
                e = r.split("offer/")[1]
            }
            return e
        } catch (i) {
            return 0
        }
    };
    this.getMemberId = function () {
        try {
            let memberId = 0;
            try {
                memberId = document.querySelector("#complaintOb").getAttribute('value')
                console.log(memberId);
            } catch (t) {
                console.log('Khong the lay thong tin memberid');
            }
            return memberId
        } catch (i) {
            return 0
        }
    };
    this.getStock = function () {
        let e = 0;
        try {
            e = iDetailData.sku.canBookCount
        } catch (t) {
            e = 0
        }
        return e
    };
    this.getItemTitle = function () {
        try {
            let e = document.getElementsByName("offerTitle");
            let t = "";
            if (e.length > 0) {
                e = e[0];
                t = e.value
            } else {
                if (document.getElementById("mod-detail-hd") != null) {
                    t = document.getElementById("mod-detail-hd").getElementsByTagName("h1")[0].innerHTML
                } else {
                    t = ""
                }
            }
            return t
        } catch (n) {
            return ""
        }
    };
    this.getItemImage = function () {
        try {
            const e = document.getElementsByClassName("mod-detail-gallery");
            let t = "";
            if (e != null) {
                const n = e[0].getElementsByTagName("img");
                if (n.length > 1) {
                    t = n[1].getAttribute("src")
                } else {
                    t = n[0].getAttribute("src")
                }
            }
            t = this.common_tool.resizeImage(t);
            return t
        } catch (r) {
            return ""
        }
    };
    this.getItemLink = function () {
        return window.location.href
    };
    this.getVNDPrice = function (e) {
        const t = this.common_tool.getExchangeRate();
        let n = e * t;
        n = this.common_tool.currency_format(n);
        return n
    };
    this.getWangwang = function () {
        let e = "";
        try {
            e = eService.contactList[0].name
        } catch (t) {
            e = ""
        }
        return e
    };
    this.getWeight = function () {
        let e = 0;
        try {
            const t = document.getElementsByClassName("unit-detail-freight-cost");
            if (t.length > 0) {
                const n = JSON.parse(t[0].getAttribute("data-unit-config"));
                e = !isNaN(n.unitWeight) ? n.unitWeight : 0
            }
        } catch (r) {
            e = 0
        }
        return parseFloat(e)
    };
    this.getTemplateId = function () {
        let templateId = 0;
        try {
            const t = document.getElementsByClassName("unit-detail-freight-cost");
            if (t.length > 0) {
                const n = JSON.parse(t[0].getAttribute("data-unit-config"));
                templateId = !isNaN(n.freightTemplateId) ? n.freightTemplateId : 0
            }
        } catch (r) {
            templateId = 0
        }
        return parseInt(templateId)
    };
    setTimeout(alibaba, 5000);

    return true
};
let common_tool = new CommonTool;
let origin_site = common_tool.getOriginSite();
let addon_tool = new AddonTool;
common_tool.getExchangeRate();
// setTimeout(function() {
// 	start();
// }, 2e3)
setTimeout(function () {
    start();
}, 3000)