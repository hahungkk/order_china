function start() {
	var e = window.location.href;
	if (!(e.match(/item.taobao/) || e.match(/detail.tmall/) || e.match(/detail.1688/) || e.match(/tmall.com\/item\//) || e.match(/ebay.com\/itm\//) || e.match(/taobao.com\/item\//) || e.match(/amazon.com[\/A-Za-z\d-]*\/dp/) || e.match(/amazon.com\/gp/) || e.match(/zara.com/) || e.match(/hm.com/))) {
		var tool = new CommonTool;
		t = tool.htmlFormAddCart();
		$("body").append(t);
	}
	else{
		var t = new factory(cart_url, add_to_cart_url);
		t.init();
	}
	$(document).on("change", "._select_category", function() {
		var e = $(this).val();
		$("._select_category").val(e);
		common_tool.setCategorySelected(e);
		var t = $("._input_category");
		if (e === "-1") {
			t.show();
			t.focus()
		} else {
			t.hide()
		}
	});
	$(document).on("keyup", "._brand_item", function() {
		var e = $(this).val();
		$("._brand_item").val(e)
	});
	$(document).on("keyup", "._comment_item", function() {
		var e = $(this).val();
		$("._comment_item").val(e)
	});
	$(document).on("keyup", "._input_category", function() {
		var e = $(this).val();
		$("._input_category").val(e)
	});
	$(document).on("click", "._addToCart", function() {
		var e = window.location.href;
		if (!(e.match(/item.taobao/) || e.match(/detail.tmall/) || e.match(/detail.1688/) || e.match(/tmall.com\/item\//) || e.match(/ebay.com\/itm\//) || e.match(/taobao.com\/item\//) || e.match(/amazon.com[\/A-Za-z\d-]*\/dp/) || e.match(/amazon.com\/gp/) || e.match(/zara.com/) || e.match(/hm.com/))) {
			alert(page_not_true);
		}
		else{
			var e = new factory(cart_url, add_to_cart_url);
			common_tool.addDisabledButtonCart();
			if (origin_site.match(/1688.com/)) {
				e.add_to_cart()
			} 
			else {
				addon_tool.AddToCart()
			}
		}
	});
	$(document).on("click", "._is_translate", function() {
		var e = common_tool.setIsTranslateToCookie();
		if (e) {
			window.location.reload()
		}
	});
	$("._close_tool").click(function() {
		$(".frm-tool").hide();
		$("li#li_sd_price").fadeIn()
	});
	$("._minimize_tool").click(function() {
		$(".frm-tool").fadeIn();
		$("li#li_sd_price").hide()
	});
	$("#txt-category").change(function() {
		var e = $(this).val();
		if (parseInt(e) == -1) {
			$(".category-other").show();
			$(".category-other input").focus()
		} else {
			$(".category-other").hide()
		}
	});

	return true;
}

var exchange_rate = 3590;

$.ajax({
	url: exchange_url,
	type: 'GET',
	success: function(response)
	{
		exchange_rate = response;
	},
	error: function()
	{
		console.log('Không thể lấy tỷ giá, vui lòng liên hệ với bộ phận kỹ thuật MạiDzô');
	}
});

var generateUUID = function()
{
	var d = new Date().getTime();
    var uuid = 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
        var r = (d + Math.random()*16)%16 | 0;
        d = Math.floor(d/16);
        return (c=='x' ? r : (r&0x3|0x8)).toString(16);
    });
    return uuid;
};

var CommonTool = function() {
	this.addDisabledButtonCart = function() {
		$("._addToCart").attr("disabled", "disabled")
	};
	this.removeDisabledButtonCart = function() {
		$("._addToCart").removeAttr("disabled")
	};
	this.getDivCategory = function() {
		return '<div class="_div_category"><img src="' + seudo_host + 'assets/images/add_on/note_brand.png" style="width:100%">' + '<p>Chọn danh mục <span style="color: red">*</span>: </p>' + '<select class="_select_category" style="width: 100%;height: 26px;">' + "</select>" + '<input placeholder="Tự nhập danh mục" style="width: 100%;height: 26px;margin-top: 10px;display: none" class="_input_category">' + '<p style="margin-top: 6px;height: 26px;margin-bottom: -6px;">Thương Hiệu: </p>' + '<input type="text" style="width: 100%;" placeholder="Nhập thương hiệu của sản phẩm" class="_brand_item">' + "</div>"
	};
	this.getDivCategoryMini = function() {
		// return '<li class="pos-relative">' + '<label class="lbl hidden" for="txt-category">Chọn danh mục <span class="font-red">*</span>: </label>' + '<select id="txt-category" class="fl form-select _select_category">' + "</select>" + '<span class="font-red">*</span>' + '<div class="category-other hidden"><div class="arrow_box"><label class="" for="txt3">Danh mục khác: </label>' + '<input type="text" class="form-input _input_category" placeholder="Tự nhập danh mục"  /></div>        </div>    ' + "</li>" + "<li>" + '<label class="lbl hidden" for="txt1">Thương hiệu: </label>' + '<input type="text" class="fl form-input _brand_item" placeholder="Thương hiệu của sản phẩm..." />    ' + "</li>"
		return '<li class="pos-relative categoryMini">' + '<label class="lbl hidden" for="txt-category">Chọn danh mục <span class="font-red">*</span>: </label>' + '<select id="txt-category" class="fl form-select _select_category">' + '</select>' + '<span class="font-red">*</span>' + '<div class="category-other hidden"><div class="arrow_box"><label class="" for="txt3">Danh mục khác: </label>' + '<input type="text" class="form-input _input_category" placeholder="Tự nhập danh mục"  /></div>        </div>    ' + '</li>' + '<li class="brandMini">' + '<label class="lbl hidden" for="txt1">Thương hiệu: </label>' + '<input type="text" class="fl form-input _brand_item" placeholder="Thương hiệu của sản phẩm..." />    ' + '</li>'
	};
	this.loadOptionCategory = function () {
        $.get(catalog_scalar_url, function (e) {
            // var t = '<option value="0">Chọn danh mục</option>';
            var t = '<option value="1">Chọn danh mục</option>';
            var n = common_tool.getCategorySelected();
            for (var r = 0; r < e.length; r++) {
                var i = e[r];
                t += '<option value="' + i.id + '"';
                if (parseInt(n) === parseInt(i.id)) {
                    t += ' selected="selected"'
                }
                t += ">";
                for (var s = 0; s < i.level; s++) {
                    if (parseInt(i.level) > 1) {
                        t += "&#8212;"
                    }
                }
                t += i.name + "</option>"
            }
            t += '<option value="-1">Khác</option>';
            $("._select_category").html(t)
        })
    };
	this.htmlFormAddCart = function() {
		var e = "";
		if (typeof services_name === "undefined") {
			e = ""
		}
		var t = this.getDivCategory();
		var n = this.getDivCategoryMini();
		var r = '<div class="frm-tool"><div class="frm-tool-ct">' + '<a href="javascript:" class="_close_tool"><img width="10px" src="' + seudo_host + '/assets/images/add_on/icon-extend.png" /></a>' + '<div class="content_tool"><span class="frm-title">Công cụ đặt hàng</span>' + "<ul>" + n + "<li>" + '<label class="lbl hidden"  for="txt2">Chú thích: </label>        ' + '<textarea class="fl form-textarea _comment_item" placeholder="Chú thích cho sản phẩm..."></textarea>    ' + '</li></ul><a href="javascript:" class="fl btn-default _addToCart" title="Đặt hàng"></a>' + '<a href="https://order_china.vn/cart" target="_blank" class="fl link-cart" title="Vào giỏ hàng">Vào giỏ hàng</a><label class="fl translate">    ' + '<input type="checkbox" checked="checked">Dịch sang tiếng việt</label></div>' + "</div></div>" + '<li class="div-block-price-book" id="li_sd_price" style="display: none;">' + '<a href="javascript:" title="" class="_minimize_tool">' + '<img width="10px" src="' + seudo_host + 'assets/images/add_on/icon-minimize.png" />' + "</a>" + '<h1 style="font-size: 22px;/* color: red; */color: rgb(232, 3, 3);' + 'text-align: center;">Công Công Cụ Đặt Hàng ' + e + "</h1>" + '<div class="seu-note-book" id="_box_input_exception" style="color: #111;">' + '<div class="note-imgv2"></div>' + '<div class="note-item"><span>Giá:</span><p><input type="text" id="_price" placeholder="Giá"></p></div>' + '<div class="note-item"><span>Thuộc tính:</span><p><textarea style="width: 100%" rows="3" id="_properties" ' + 'placeholder="Nhập màu sắc, kích thước VD:Màu đen; Size 41" name="_properties"></textarea></p></div>' + '<div class="note-item"><span>Số lượng:</span><p><input type="text" id="_quantity" placeholder="Số lượng"></p></div>' + "</div>" + t + '<div class="note-text"><p>Chú thích: </p>' + '<textarea cols="60" class="_comment_item" placeholder="Chú thích cho sản phẩm" ' + 'name="_comment_item"></textarea>' + '</div><div class="xbTipBlock add-book"><div class="add-button" id="block_button_sd">' + '<button class="_addToCart" type="button" style="border: none;cursor: pointer;"></button>' + '<a href="' + cart_url + '" class="cart" target="_blank" style="float: left; margin-left: 20px;margin-top: 10px; color: rgb(0, 114, 188);">Vào giỏ hàng</a>    ' + '<div class="note-img"></div></div></div>';
		// var r = '<div class="frm-tool"><div class="frm-tool-ct">' + '<a href="javascript:" class="_close_tool"><img width="10px" src="' + seudo_host + '/assets/images/add_on/icon-extend.png" /></a>' + '<div class="content_tool"><span class="frm-title">Công cụ đặt hàng</span>' + "<ul>" + "<li>" + '<label class="lbl hidden"  for="txt2">Chú thích: </label>        ' + '<textarea class="fl form-textarea _comment_item" placeholder="Chú thích cho sản phẩm..."></textarea>    ' + '</li></ul><a href="javascript:" class="fl btn-default _addToCart" title="Đặt hàng"></a>' + '<a href="javascript:" class="fl link-cart" title="Vào giỏ hàng">Vào giỏ hàng</a><label class="fl translate">    ' + '<input type="checkbox" checked="checked">Dịch sang tiếng việt</label></div>' + "</div></div>" + '<li class="div-block-price-book" id="li_sd_price" style="display: none;">' + '<a href="javascript:" title="" class="_minimize_tool">' + '<img width="10px" src="' + seudo_host + 'assets/images/add_on/icon-minimize.png" />' + "</a>" + '<h1 style="font-size: 22px;/* color: red; */color: rgb(232, 3, 3);' + 'text-align: center;">Công Công Cụ Đặt Hàng ' + e + "</h1>" + '<div class="seu-note-book" id="_box_input_exception" style="color: #111;">' + '<div class="note-imgv2"></div>' + '<div class="note-item"><span>Giá:</span><p><input type="text" id="_price" placeholder="Giá"></p></div>' + '<div class="note-item"><span>Thuộc tính:</span><p><textarea style="width: 100%" rows="3" id="_properties" ' + 'placeholder="Nhập màu sắc, kích thước VD:Màu đen; Size 41" name="_properties"></textarea></p></div>' + '<div class="note-item"><span>Số lượng:</span><p><input type="text" id="_quantity" placeholder="Số lượng"></p></div>' + "</div>" + '<div class="note-text"><p>Chú thích: </p>' + '<textarea cols="60" class="_comment_item" placeholder="Chú thích cho sản phẩm" ' + 'name="_comment_item"></textarea>' + '</div><div class="xbTipBlock add-book"><div class="add-button" id="block_button_sd">' + '<button class="_addToCart" type="button" style="border: none;cursor: pointer;"></button>' + '<a href="' + cart_url + '" class="cart" target="_blank" style="float: left; margin-left: 20px;margin-top: 10px; color: rgb(0, 114, 188);">Vào giỏ hàng</a>    ' + '<div class="note-img"></div></div></div>';
		var i = this.getCookie("is_translate");
		var s = 1;
		if (typeof translate_first === "undefined") {
			s = 1
		}
		var o = "2.0.0";
		if (typeof version_tool === "undefined") {
			o = "2.0.0"
		} else {
			o = version_tool
		}
		if (parseInt(i) == 1 || i === "" && s == 1) {
			r += '<label><input type="checkbox" checked="checked" name="is_translate" class="_is_translate" />Dịch sang tiếng Việt</label>'
		} else if (i === "" && s == 0) {
			r += '<label><input type="checkbox" name="is_translate" class="_is_translate" />Dịch sang tiếng Việt</label>'
		} else {
			r += '<label><input type="checkbox" name="is_translate" class="_is_translate" />Dịch sang tiếng Việt</label>'
		}
		r += '<span style="float: right;"><b>Phiên bản ' + o + "</b></span></li>";
		return r
	};
	this.getOriginSite = function() {
		var e = window.location.href;
		e = e.replace("http://", "");
		var t = e.split("/");
		return t[0]
	};
	this.getHomeLand = function() {
		var e = window.location.href;
		if (e.match(/taobao/)) {
			return "TAOBAO"
		}
		if (e.match(/tmall/)) {
			return "TMALL"
		}
		if (e.match(/1688|alibaba/)) {
			return "1688"
		}
		if (e.match(/ebay/)) {
			return "EBAY"
		}	
		if (e.match(/amazon/)) {
			return "AMAZON"
		}
		if (e.match(/zara/)) {
			return "ZARA"
		}
		if (e.match(/hm/)) {
			return "HM"
		}	
		return null
	};
	this.currency_format = function(e, t) {
		if (!$.isNumeric(e)) {
			return e
		}
		if (t === null || typeof t === "undefined" || t == false) {
			var n = 10;
			e = Math.ceil(e / n) * n
		}
		e = e.toString().replace(/(\d)(?=(\d\d\d)+(?!\d))/g, "$1.");
		return e
	};
	this.getExchangeRate = function() {
		return exchange_rate
	};
	this.trackError = function(e, t, n) {
		var r = "link=" + e + "&error=" + t + "&tool=bookmarklet";
		$.ajax({
			url: n,
			type: "POST",
			data: r,
			success: function(e) {}
		})
	};
	this.hasClass = function(e, t) {
		return e.className.indexOf(t) > -1
	};
	this.resizeImage = function(e) {
		return e.replace(/[0-9]{2,3}[x][0-9]{2,3}/g, "150x150")
	};
	this.getParamsUrl = function(e, t) {
		var n = "";
		if (t) {
			n = t
		} else {
			n = window.location.href
		}
		if (n == "") return null;
		var r = (new RegExp("[\\?&]" + e + "=([^&#]*)")).exec(n);
		if (r === null) return null;
		return r[1] || 0
	};
	this.processPrice = function(e, t) {
		if (e == null || parseFloat(e) == 0) return 0;
		var n = 0;
		if (e.constructor === Array) {
			n = String(e[0]).replace(",", ".").match(/[0-9]*[\.]?[0-9]+/g)
		} else {
			n = String(e).replace(",", ".").match(/[0-9]*[\.]?[0-9]+/g)
		}
		if (isNaN(n) || parseFloat(e) == 0) {
			return 0
		}
		var r = "";
		var i = 0;
		if (e.constructor === Array && e.length > 1) {
			var s = this.currency_format(parseFloat(e[0]) * this.getExchangeRate());
			var o = e.length - 1;
			var u = this.currency_format(parseFloat(e[o]) * this.getExchangeRate());
			if (parseFloat(e[o]) > 0) {
				r = s + " ~ " + u
			} else {
				r = s
			}
		} else {
			i = parseFloat(e);
			r = this.currency_format(i * this.getExchangeRate())
		}
		var a = document.createElement("li");
		var f = null;
		var l = null;
		var c = null;
		if (t == "TAOBAO") {
			a.setAttribute("style", "color: blue ! important; padding: 30px 0px; font-family: arial;");
			f = '<span class="tb-property-type" style="color: blue; font-weight: bold; font-size: 25px;">Giá</span>     ' + '<strong id="price_vnd" class="" style="font-size: 25px;">' + '<em class=""> ' + r + ' </em><em class=""> VNĐ</em></strong>';
			a.innerHTML = f;
			l = document.getElementById("J_PromoPrice");
			if (l != null) {
				l.parentNode.insertBefore(a, l.nextSibling)
			} else {
				c = document.getElementById("J_StrPriceModBox");
				if (c != null) {
					c.parentNode.insertBefore(a, c.nextSibling)
				}
			}
		} else if (t == "TMALL") {
			a.setAttribute("style", "font-weight: bold; padding: 10px 0px;");
			a.setAttribute("class", "tm-promo-price tm-promo-cur");
			f = '<strong id="price_vnd" class="" style="font-size: 30px;">' + '<span class="tm-price">Giá</span>' + '<em class="tm-price" style="font-size: 30px; margin-left: 10px;"> ' + r + "  VNĐ </em></strong>";
			a.innerHTML = f;
			l = document.getElementById("J_PromoPrice");
			if (l != null) {
				l.parentNode.insertBefore(a, l.nextSibling)
			} else {
				c = document.getElementById("J_StrPriceModBox");
				if (c != null) {
					c.parentNode.insertBefore(a, c.nextSibling)
				}
			}
		}
		return parseFloat(n)
	};
	this.sendAjaxToCart = function(e, k) {
		var n = new CommonTool;
		//reparse data
        console.log(k);

        options = {}
        if (k.property != undefined) {
            var opt = k.property.split(';')
            for (var i in opt) {
                if (opt[i].length > 0)options[k.site + '_' + i] = opt[i]
            }
        }

        price = Number(k.price_origin);
        price_promotion = Number(k.price_promotion);
        if (price_promotion > 0)price = Math.min(price, price_promotion);

        postData = {
            "shopping_domain": document.domain,
            "vendor": k.seller_name,
            "detail_url": k.link_origin,
            "name": k.name,
            "sku": k.site.toLowerCase() + '_' + k.sku,
            "image_url": decodeURIComponent(k.pic_url),
            "short_description": k.title_origin,
            "price": price,
            "shipping": k.shipping,
            "weight": k.weight,
            "quantity": k.quantity,
            "options_selected": JSON.stringify(options),
            "options_metadata": "",
            "category_list": "",
            "fragile": false,
            "insurance": false,
            "note": k.comment,
            "html": "",
            "currency": k.currency_type
        };

		$.ajax({
            url: e,
            type: "POST",
            dataType: "json",
            data: JSON.stringify(postData),
            contentType: "application/json; charset=UTF-8",
            //xhrFields: {
            //	withCredentials: true
            //},
            beforeSend: function () {
                $('#box-confirm-nh-site').remove();
            },
            success: function (data, textStatus, jqXHR) {
                n.removeDisabledButtonCart();
                msg = "Thêm vào giỏ hàng thành công"
                alert(msg)
                //$("body").append()
                //if (e.html) {
                //	$("body").append(e.html)
                //} else {
                //	$("body").append(e)
                //}
            },
            error: function (jqXHR, textStatus, errorThrown) {
                n.removeDisabledButtonCart();
                msg = "Có lỗi xảy ra (cần đăng nhập trước khi đặt hàng):" + textStatus
                alert(msg)
                //$("body").append(msg)
                console.log(jqXHR);
            }
        });

		return true
	};
	this.loadJsFile = function(e) {
		var t = document.createElement("script");
		t.setAttribute("src", e + "?t=" + Math.random());
		document.body.appendChild(t);
		return true
	};
	this.key_translate_lib = function(e) {
		var t = [];
		t["颜色"] = "Màu";
		t["尺码"] = "Kích cỡ";
		t["尺寸"] = "Kích cỡ";
		t["价格"] = "Giá";
		t["促销"] = "Khuyến mại";
		t["配送"] = "Vận Chuyển";
		t["数量"] = "Số Lượng";
		t["销量"] = "Chính sách";
		t["评价"] = "Đánh Giá";
		t["颜色分类"] = "Màu sắc";
		t["促销价"] = "Giá";
		t["套餐类型"] = "Loại";
		t["单价（元）"] = "Giá (NDT)";
		t["库存量"] = "Tồn kho";
		t["采购量"] = "SL mua";
		var n = e;
		if (t[e]) {
			n = t[e]
		}
		return n
	};
	this.stripTags = function(e) {
		if (typeof e == "object") {
			return e.replaceWith(e.html().replace(/<\/?[^>]+>/gi, ""))
		}
		return false
	};
	this.setCategorySelected = function(e) {
		this.setCookie("category_selected", e, 100);
		return true
	};
	this.getCategorySelected = function() {
		return this.getCookie("category_selected")
	};
	this.setIsTranslateToCookie = function() {
		if ($("#_is_translate").prop("checked")) {
			this.setCookie("is_translate", 1, 100)
		} else {
			this.setCookie("is_translate", 0, 100)
		}
		return true
	};
	this.translate_title = function(e, t, n) {
		return;
		var r = this.getCookie("is_translate");
		if (parseInt(r) == 1 || r == "") {
			$.ajax({
				url: translate_url,
				type: "post",
				contentType: "application/x-www-form-urlencoded",
				xhrFields: {
					withCredentials: true
				},
				data: {
					text: e,
					type: t
				},
				success: function(e) {
					var t = $.parseJSON(e);
					n.set_translate({
						title: t["data_translate"]
					})
				}
			});
			return true
		}
		return false
	};
	this.translate = function(e, t) {
		var n = this.getCookie("is_translate");
		if (parseInt(n) == 1 || n == "") {
			if (t == "properties") {
				this.translateStorage(e)
			}
		}
	};
	this.translateStorage = function(e) {
		try {
			var t = e.text();
			var n = t;
			var r = keyword;
			if (r != null) {
				var i = r.resource;
				for (var s = 0; s < i.length; s++) {
					var o = i[s];
					try {
						if (t.match(o.k_c, "g")) {
							t = t.replace(o.k_c, o.k_v + " ")
						}
					} catch (u) {
						try {
							if (t.match(o.keyword_china, "g")) {
								t = t.replace(o.keyword_china, o.keyword_vi + " ")
							}
						} catch (u) {}
					}
				}
				e.text(t);
				e.attr("data-text", n)
			}
		} catch (u) {
			console.log("error")
		}
	};
	this.ajaxTranslate = function(e, t) {
		var n = e.text();
		$.ajax({
			url: translate_url,
			type: "POST",
			contentType: "application/x-www-form-urlencoded",
			xhrFields: {
				withCredentials: true
			},
			data: {
				text: n,
				type: t
			},
			success: function(t) {
				var n = $.parseJSON(t);
				if (n["data_translate"] && n["data_translate"] != null) {
					e.attr("data-text", e.text());
					e.text(n["data_translate"])
				}
			}
		})
	};
	this.getKeywordSearch = function() {
		$.ajax({
			url: translate_keyword_url,
			type: "POST",
			contentType: "application/x-www-form-urlencoded",
			xhrFields: {
				withCredentials: true
			},
			data: {
				text: "text",
				type: "type"
			},
			success: function(e) {
				var t = JSON.stringify(e);
				localStorage.setItem("keyword_search", t)
			}
		});
		return true
	};
	this.showInputEx = function(e) {
		$(".frm-tool").hide();
		$("li#li_sd_price").fadeIn();
		var t = $("#_box_input_exception");
		t.show();
		t.attr("data-is-show", 1);
		var n = $("#_price");
		var r = new factory(cart_url, add_to_cart_url);
		var i = r.getPriceInput();
		var s = r.getPropertiesInput();
		var o = r.getQuantityInput();
		if (o == "" && s == "" && i == "") {
			alert("Chúng tôi không thể lấy được thông tin của sản phẩm." + "Bạn vui lòng điền thông tin để chúng tôi mua hàng cho bạn");
			n.focus();
			try {
				if (e != "alibaba") {
					if (parseFloat(r.getPromotionPrice()) > 0) {
						n.val(r.getPromotionPrice())
					} else {
						n.attr("placeholder", "Nhập tiền tệ - Trung Quốc")
					}
					$("#_properties").val(r.getPropertiesOrigin());
					$("#_quantity").val(r.getQuantity())
				}
			} catch (u) {
				console.log(u)
			}
		}
		return true
	};
	this.setCookie = function(e, t, n) {
		var r = new Date;
		r.setTime(r.getTime() + n * 24 * 60 * 60 * 1e3);
		var i = "expires=" + r.toGMTString();
		document.cookie = e + "=" + t + "; " + i;
		return true
	};
	this.getCookie = function(e) {
		var t = e + "=";
		var n = document.cookie.split(";");
		for (var r = 0; r < n.length; r++) {
			var i = n[r];
			while (i.charAt(0) == " ") i = i.substring(1);
			if (i.indexOf(t) != -1) {
				return i.substring(t.length, i.length)
			}
		}
		return ""
	}
	// convert a number of money from one currency to usd
	this.convertToUsd = function( to_currency, orgin_price){
		return us_exchange_rate[to_currency]*orgin_price;
	}

	// get only number from price
	this.getMoneyNumber = function(price){
		var i = price.match(/[0-9,\.]+/g);
		price = i[0].replace(",", "");
		return price;
	}
};
var factory = function(e, t) {
	var n = new CommonTool;
	var r = n.getOriginSite();
	var i;
	if (r.match(/1688.com/)) {
		i = new alibaba(e, t)
	} else if (r.match(/taobao.com/)) {
		i = new taobao(e)
	} else if (r.match(/tmall.com/)) {
		i = new tmall(e)
	} else if (r.match(/ebay.com/)) {
		i = new ebay(e)		
	} else if (r.match(/amazon.com/)) {
		i = new amazon(e)		
	} else if (r.match(/zara.com/)) {
		i = new zara(e)		
	} else if (r.match(/hm.com/)) {
		i = new hm(e)		
	} 
	else {
		console.log("this website does not support !")
	}
	return i
};
var AddonTool = function() {
	this.common_tool = new CommonTool;
	this.AddToCart = function() {
		var e = 0;
		var t = new factory(cart_url, add_to_cart_url);
		var n = $("#_box_input_exception").attr("data-is-show");
		var r = "",
			i = "",
			s = "",
			o = "",
			u = "";
		var a = $("._select_category");
		var f = a.val();
		var l = $("._select_category option:selected").text();
		var c = $("._brand_item").val();
		while (l.match(/-/i)) {
			l = l.replace(/-/i, "")
		}
		if (f === "-1") {
			l = $("._input_category").val()
		}
		if (f === "0" || l == "") {
			alert("Yêu cầu chọn danh mục cho sản phẩm");
			this.common_tool.removeDisabledButtonCart();
			return false
		}
		var h = t.checkSelectFull();
		if (!h) {
			alert("Yêu cầu chọn đầy đủ thuộc tính của SP");
			this.common_tool.removeDisabledButtonCart();
			return false
		}
		try {
			r = t.getOriginPrice();
			i = t.getPromotionPrice();
			// alert(r);
			// alert(i);
			if ($.isArray(r)) {
				r = r[0]
			}
			if ($.isArray(i)) {
				i = i[0]
			}
			s = t.getProperties();
			o = t.getPropertiesOrigin();
			u = t.getQuantity()
		} catch (p) {
			e = 1;
			r = i = t.getPriceInput();
			o = s = t.getPropertiesInput();
			u = t.getQuantityInput()
		}
		var d = t.getImgLink();
		var v = t.getSellerId();
		var m = t.getSellerName();
		var g = t.getWangwang();
		if (g == "") {
			g = m
		}
		var y = t.getTitleOrigin();
		var b = t.getTitleTranslate();
		var w = t.getCommentInput();
		var E = window.location.href;
		var S = t.getItemID();
		var x = t.getDataValue();
		var T = t.getOuterId(x);
		if ($.isArray(T)) {
			T = T[0]
		}
		var N = this.common_tool.getHomeLand();
		var	currency_type = t.getCurrencyType();
		
		var C = t.getStock();
		if (!$.isNumeric(C) || parseInt(C) <= 0) {
			C = 99
		}

		// alert(e);
		// alert(n);
		// alert(i);
		// alert(r);

		if (e && parseFloat(n) != 1 || parseFloat(n) != 1 && parseInt(i) <= 0 && parseInt(r) <= 0) {
			//alert('vao day');
			this.common_tool.showInputEx();
			this.common_tool.removeDisabledButtonCart();
			return false
		}
		if ((r == "" && i == "" || u == "") && parseFloat(n) == 1) {
			alert("Yêu cầu bổ sung thông tin.");
			
			$("#_price").focus();
			this.common_tool.removeDisabledButtonCart();
			return false
		}
		if (!$.isNumeric(i) && parseFloat(n) == 1) {
			alert("Yêu cầu nhập giá của sản phẩm (không chứa chữ cái)");
			$("#_price").focus();
			this.common_tool.removeDisabledButtonCart();
			return false
		}
		var domestic_shipping_price = t.getShippingPrice(); // gia chuyen hang den kho

		if(domestic_shipping_price == '-1'){
			alert(shipping_err_cant_ship);
			this.common_tool.removeDisabledButtonCart();
			return false
		}
		var uuid = generateUUID();
		if(t.error.length == 0){
			var k = {
				title_origin: $.trim(y),
				title_translated: $.trim(b),
				price_origin: r,
				price_promotion: i,
				shipping:domestic_shipping_price,
				property_translated: s,
				property: o,
				data_value: x,
				image_model: d,
				image_origin: d,
				shop_id: v,
				shop_name: m,
				wangwang: g,
				quantity: u,
				stock: C,
				site: N,
				comment: w,
				item_id: S,
				link_origin: E,
				// outer_id: T,
				outer_id: uuid,
				error: e,
				weight: 0,
				step: 1,
				brand: c,
				category_name: l,
				category_id: f,
				tool: "Addon",
				currency_type: currency_type
			};
			console.log(k);
			this.common_tool.sendAjaxToCart(add_to_us_cart_url, k);
		}
		else{
			for (var index = 0; index < t.error.length; index++) {
			    alert(t.error[index]);
			}
		}
	}
};

var zara = function(e) {
	this.source = "zara";
	this.common_tool = new CommonTool;
	this.error = [];
	this.init = function() {
		var e = $(".right");
		e.css("padding", "5px");
		e.css("border", "2px solid orange");
		e.css("font-size", "11px");
		this.parse()
	};
	this.parse = function() {
		var e = this.common_tool;
		$(".tb-property-type").each(function(e, t) {
			var n = $(this).text();
			$(this).text(common_tool.key_translate_lib(n))
		});
		var t = e.htmlFormAddCart();
		$("body").append(t);
        this.common_tool.loadOptionCategory();
		return false
	};

	this.getCurrencyType = function(){
		var price = 0;
		if($("span.price").length){
		 	price = $("span.price").text();
		 	var i = price.match(/[A-Z]+/g);
			currency_type = i[0];
			return currency_type;
		}
		else return false;
	}
	this.translateProperties = function() {
		var e = this.common_tool;
		var t = $(".J_TSaleProp li span");
		if (t == null || t.length == 0) {
			t = $(".J_SKU a span")
		}
		t.each(function() {
			e.translate($(this), "properties")
		})
	};
	this.getPriceInput = function() {
		return $("#_price").val()
	};
	this.getPropertiesInput = function() {
		return $("#_properties").val()
	};
	this.getQuantityInput = function() {
		if($(".qtyInput").length)
			return $(".qtyInput").val();
		else if($("#_quantity").length) return $("#_quantity").val()
		return false;
	};
	this.getCommentInput = function() {
		if($("._comment_item").length)
			return $("._comment_item").val()
		return false;
	};
	this.set_translate = function(e) {
		var t = this.getDomTitle();
		if (t != null && e.title != "") {
			t.setAttribute("data-text", t.textContent);
			t.textContent = e.title
		}
	};
	this.getPromotionPrice = function(e) {
		var price_block = 0;
		if($("span.price").length){
		 	price_block = $("span.price").text();
		 	var i = price_block.match(/\d+\.?\d*/g);
			var price = i[0];
			return price;
		}
		else return false;
	};
	this.getStock = function() {
		try {
			var e = document.getElementById("J_EmStock");
			var t = 99;
			if (e == null || e == "undefined") {
				e = document.getElementById("J_SpanStock")
			}
			if (e != null && e != "undefined") {
				t = e.textContent;
				t = parseInt(t.replace(/[^\d.]/g, ""))
			}
		} catch (n) {
			t = 99
		}
		return t
	};
	this.getOriginPrice = function() {
		var price_block = 0;
		if($("span.price").length){
		 	price_block = $("span.price").text();
		 	var i = price_block.match(/\d+\.?\d*/g);
			var price = i[0];
			return price;
		}
		else return false;

	};
	this.getOuterId = function(e) {
		try {
			var t = document.getElementsByTagName("script");
			var n = "";
			var r = null;
			if (t.length > 0) {
				for (var i = 0; i < t.length; i++) {
					if (t[i].innerHTML.match(/Hub\.config\.set/)) {
						try {
							detailJsStart();
							n = Hub.config.get("sku").valItemInfo.skuMap[";" + e + ";"].skuId
						} catch (s) {
							r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
							n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
						}
					} else if (t[i].innerHTML.match(/TShop\.Setup/)) {
						r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
						n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
					}
				}
			}
			return n
		} catch (o) {
			return ""
		}
	};
	this.getTitleTranslate = function() {
		var title = $("header > h1").text();
		return title.trim();
	};
	this.getTitleOrigin = function() {
		var title = $("header > h1").text();
		return title.trim();
	};
	this.getDomTitle = function() {
		try {
			var e = null;
			if (document.getElementsByClassName("tb-main-title").length > 0) {
				e = document.getElementsByClassName("tb-main-title")[0]
			}
			if (e == null && document.getElementsByClassName("tb-detail-hd").length > 0) {
				var t = document.getElementsByClassName("tb-detail-hd")[0];
				if (t.getElementsByTagName("h3").length > 0 && t != null) {
					e = t.getElementsByTagName("h3")[0]
				} else {
					e = t.getElementsByTagName("h1")[0]
				}
			}
			if (e.textContent == "" && document.getElementsByClassName("tb-tit").length > 0) {
				e = document.getElementsByClassName("tb-tit")[0]
			}
			if (e.textContent == "") {
				e = document.querySelectorAll("h3.tb-item-title");
				if (e != null) {
					e = e[0]
				} else {
					e = document.getElementsByClassName("tb-item-title");
					if (e.length > 0) {
						e = e[0]
					}
				}
			}
			return e
		} catch (n) {
			return null
		}
	};
	this.getSellerName = function() {
		return 'zara';
	};
	this.getSellerId = function() {
		return 'zara';
	};
	this.getProperties = function() {
		var result = '';
		if($('.colors').length > 0)
		   	result += $('label.selected').find('div.imgCont').attr('title');
		if($('table.contracted').length > 0){
			result += ';';
			var size_option = $('table.contracted tr.selected td.size-name').text();
			result += size_option.trim();
		}
		return result;
	};
	this.getPropertiesOrigin = function() {
		return this.getProperties();
	};
	this.getDataValue = function() {
		return 0;
	};
	this.getWangwang = function() {
		try {
			var e = document.querySelectorAll("span.seller");
			if (e != null && e != "" && e != "undefined" && e.length > 0) {
				var t = document.getElementsByClassName("slogo-extraicon");
				if (t != null && t != "" && t != "undefined" && t.length > 0) {
					e = t[0].getElementsByClassName("ww-light")
				}
			}
			if (e == null || e == "" || e.length == 0) {
				e = document.querySelectorAll("div.hd-shop-desc span.ww-light")
			}
			var n = "";
			if (e.length > 0) {
				var r = e[0].getElementsByTagName("span");
				if (r != null && r != "" && r.length == 0) {
					n = decodeURIComponent(r[0].getAttribute("data-nick"))
				} else {
					n = decodeURIComponent(e[0].getAttribute("data-nick"))
				}
			}
		} catch (i) {
			n = ""
		}
		return n
	};
	this.checkSelectFull = function() {
		var result = true;
		if($('.size-select').length > 0)
		{
		    if($('.size-select').find('table.contracted').length == 0)
		    	result = false;
		}
		return result;
	};
	this.getQuantity = function() {
		if($(".qtyInput").length)
			return $(".qtyInput").val();
		else return "1";
	};
	this.getImgLink = function() {
		var img_src = $(".imageZoom.full > img").attr("src");
		return img_src;
	};
	this.getItemID = function() {
  		return 'zara';
	}
	this.getShippingPrice = function () {
		return '4.95';
    };
};

var hm = function(e) {
	this.source = "hm";
	this.error = [];
	this.common_tool = new CommonTool;

	this.init = function() {
		var e = $("#product");
		e.css("padding", "5px");
		e.css("border", "2px solid orange");
		e.css("font-size", "11px");
		this.parse()
	};
	this.parse = function() {
		var e = this.common_tool;
		$(".tb-property-type").each(function(e, t) {
			var n = $(this).text();
			$(this).text(common_tool.key_translate_lib(n))
		});
		var t = e.htmlFormAddCart();
		$("body").append(t);
        this.common_tool.loadOptionCategory();
		return false
	};

	this.getCurrencyType = function(){
		var price = 0;
		if($(".price").length){
		 	price = $(".price").text();
		 	var i = price.match(/[^\d]/g);
			currency_type = i[0];
			if(currency_type == '$')
				currency_type = 'USD';
			return currency_type;
		}
		else return false;
	}
	this.translateProperties = function() {
		var e = this.common_tool;
		var t = $(".J_TSaleProp li span");
		if (t == null || t.length == 0) {
			t = $(".J_SKU a span")
		}
		t.each(function() {
			e.translate($(this), "properties")
		})
	};
	this.getPriceInput = function() {
		return $("#_price").val()
	};
	this.getPropertiesInput = function() {
		return $("#_properties").val()
	};
	this.getQuantityInput = function() {
		if($(".qtyInput").length)
			return $(".qtyInput").val();
		else if($("#_quantity").length) return $("#_quantity").val()
		return false;
	};
	this.getCommentInput = function() {
		if($("._comment_item").length)
			return $("._comment_item").val()
		return false;
	};
	this.set_translate = function(e) {
		var t = this.getDomTitle();
		if (t != null && e.title != "") {
			t.setAttribute("data-text", t.textContent);
			t.textContent = e.title
		}
	};
	this.getPromotionPrice = function(e) {
		var price_block = 0;
		if($(".price").length){
		 	price_block = $(".price").text();
		 	var i = price_block.match(/\d+\.?\d*/g);
			var price = i[0];
			return price;
		}
		else return false;
	};
	this.getStock = function() {
		try {
			var e = document.getElementById("J_EmStock");
			var t = 99;
			if (e == null || e == "undefined") {
				e = document.getElementById("J_SpanStock")
			}
			if (e != null && e != "undefined") {
				t = e.textContent;
				t = parseInt(t.replace(/[^\d.]/g, ""))
			}
		} catch (n) {
			t = 99
		}
		return t
	};
	this.getOriginPrice = function() {
		var price_block = 0;
		if($(".price").length){
		 	price_block = $(".price").text();
		 	var i = price_block.match(/\d+\.?\d*/g);
			var price = i[0];
			return price;
		}
		else return false;

	};
	this.getOuterId = function(e) {
		try {
			var t = document.getElementsByTagName("script");
			var n = "";
			var r = null;
			if (t.length > 0) {
				for (var i = 0; i < t.length; i++) {
					if (t[i].innerHTML.match(/Hub\.config\.set/)) {
						try {
							detailJsStart();
							n = Hub.config.get("sku").valItemInfo.skuMap[";" + e + ";"].skuId
						} catch (s) {
							r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
							n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
						}
					} else if (t[i].innerHTML.match(/TShop\.Setup/)) {
						r = t[i].innerHTML.replace(/\s/g, "").substr(t[i].innerHTML.replace(/\s/g, "").indexOf(e), 60);
						n = r.substr(r.indexOf("skuId") + 8, 15).match(/[0-9]+/)
					}
				}
			}
			return n
		} catch (o) {
			return ""
		}
	};
	this.getTitleTranslate = function() {
		var title = $("h1").clone().children().remove().end().text();
		return title.trim();
	};
	this.getTitleOrigin = function() {
		var title = $("h1").clone().children().remove().end().text();
		return title.trim();
	};
	this.getDomTitle = function() {
		try {
			var e = null;
			if (document.getElementsByClassName("tb-main-title").length > 0) {
				e = document.getElementsByClassName("tb-main-title")[0]
			}
			if (e == null && document.getElementsByClassName("tb-detail-hd").length > 0) {
				var t = document.getElementsByClassName("tb-detail-hd")[0];
				if (t.getElementsByTagName("h3").length > 0 && t != null) {
					e = t.getElementsByTagName("h3")[0]
				} else {
					e = t.getElementsByTagName("h1")[0]
				}
			}
			if (e.textContent == "" && document.getElementsByClassName("tb-tit").length > 0) {
				e = document.getElementsByClassName("tb-tit")[0]
			}
			if (e.textContent == "") {
				e = document.querySelectorAll("h3.tb-item-title");
				if (e != null) {
					e = e[0]
				} else {
					e = document.getElementsByClassName("tb-item-title");
					if (e.length > 0) {
						e = e[0]
					}
				}
			}
			return e
		} catch (n) {
			return null
		}
	};
	this.getSellerName = function() {
		return 'hm';
	};
	this.getSellerId = function() {
		return 'hm';
	};
	this.getProperties = function() {
		var result = '';
		var self = this;
		if($('ul.options').length > 0){
		   	$("ul.options").each(function(){
		   		if($(this).find('li.act').length !=0 && $(this).find('li.act').hasClass('soldOut')){
		   			if(self.error.indexOf('Mẫu bạn chọn đã hết hàng, bạn hãy chọn mẫu khác') == -1)
		   				self.error.push("Mẫu bạn chọn đã hết hàng, bạn hãy chọn mẫu khác");
		   		}
		        else if($(this).find('li.act').length !=0){
		        	
		        	result += $(this).find('li.act').text();
		        	result += ';';
		        }
		    });
		}
		return result;
	};
	this.getPropertiesOrigin = function() {
		return this.getProperties();
	};
	this.getDataValue = function() {
		return 0;
	};
	this.getWangwang = function() {
		try {
			var e = document.querySelectorAll("span.seller");
			if (e != null && e != "" && e != "undefined" && e.length > 0) {
				var t = document.getElementsByClassName("slogo-extraicon");
				if (t != null && t != "" && t != "undefined" && t.length > 0) {
					e = t[0].getElementsByClassName("ww-light")
				}
			}
			if (e == null || e == "" || e.length == 0) {
				e = document.querySelectorAll("div.hd-shop-desc span.ww-light")
			}
			var n = "";
			if (e.length > 0) {
				var r = e[0].getElementsByTagName("span");
				if (r != null && r != "" && r.length == 0) {
					n = decodeURIComponent(r[0].getAttribute("data-nick"))
				} else {
					n = decodeURIComponent(e[0].getAttribute("data-nick"))
				}
			}
		} catch (i) {
			n = ""
		}
		return n
	};
	this.checkSelectFull = function() {
		var result = true;
		$('ul.options').each(function(){
			if($(this).find('li.act').length == 0) result = false;
		});
		console.log(result);
		return result;
	};
	this.getQuantity = function() {
		if($(".qtyInput").length)
			return $(".qtyInput").val();
		else return "1";
	};
	this.getImgLink = function() {
		var img_src = $("#product-image").attr("src");
		return img_src;
	};
	this.getItemID = function() {
  		return 'hm';
	}
	this.getShippingPrice = function () {
		return '5.95';
    };
};

var common_tool = new CommonTool;
var origin_site = common_tool.getOriginSite();
var addon_tool = new AddonTool;
common_tool.getExchangeRate();
setTimeout(function() {
	start();
}, 2e3)