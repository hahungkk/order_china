const service_host = "http://localhost:8888/";
const seudo_host = "https://seudo.vn/";
const track_url = seudo_host + 'goodies_util/track_error';
const add_to_cart_url = service_host + 'api/shop_module/cart/';
const add_to_us_cart_url = service_host + 'api/shop_module/cart/';
const cart_url = service_host + '#/cart';
const translate_keyword_url = seudo_host + "goodies_util/translate_pro";
const translate_url = seudo_host + 'goodies_util/translate';
const exchange_url = service_host + "api/shop_module/currency/";
const catalog_scalar_url = ''; //http://logistic.seudo.vn/api/catalog/scalar';
const version_tool = "1.0.1";
const postal_code = '95128'; // postal_code of store
const http_referer = service_host;
// var ebay_app_id = 'DoTrungD-9cde-4182-8578-5dff24d12aa0';

//exchange rate compare to USD
const us_exchange_rate = {EUR: "1.5", US: "1", "$": "1", GBP: "1.5"};
// some style for err
const err = 'style=\'color:red;font-weight:bold;\'';

// some err code
const shipping_err_cant_ship = 'Không thể chuyển hàng về kho của Alo68. Hãy liên lạc với bộ phận bán hàng của Alo68.vn để biết thêm chi tiết';
const full_cart_err = 'Giỏ hàng đầy. Vui lòng ko đặt quá 50 sản phẩm.';
const get_shipping_cost_err = 'Không thể lấy thông tin phí vận chuyển';
const page_not_true = 'Hãy vào trang chi tiết sản phẩm để đặt hàng';
