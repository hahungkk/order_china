from rest_framework import serializers
from django.contrib.auth.models import User


class UserSerializer(serializers.ModelSerializer):
    list_group = serializers.SerializerMethodField()

    @staticmethod
    def get_list_group(user):
        return user.groups.values_list('name', flat=True)

    class Meta:
        model = User
        fields = ('id', 'username', 'email', 'is_staff', 'list_group', 'is_superuser')


def jwt_response_payload_handler(token, user=None, request=None):
    return {
        'token': token,
        'user': UserSerializer(user, context={'request': request}).data
    }
