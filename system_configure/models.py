#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  models.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#

from django.db import models
from django.contrib.auth.models import User
from django.utils.translation import ugettext_lazy as _
from .enums import *


# Config
class SystemConfig(models.Model):
    class Meta:
        verbose_name = _("System Config")
        verbose_name_plural = _("System Configs")

    key = models.CharField(max_length=255, primary_key=True, unique=True)
    value = models.TextField(blank=True)
    description = models.CharField(blank=True, max_length=511, default='')
    mode = models.PositiveSmallIntegerField(choices=ConfigMode.ChoiceList(), default=ConfigMode.private, db_index=True)

    def __str__(self):
        if not self.description:
            return self.pk
        return '%s (%s)' % (self.pk, self.description)


class TemplateCategory(models.Model):
    key = models.CharField(max_length=255, primary_key=True, unique=True)
    label = models.CharField(max_length=255)
    mode = models.PositiveSmallIntegerField(choices=ConfigMode.ChoiceList(), default=ConfigMode.private, db_index=True)

    def __str__(self):
        return self.key


class TemplateHTML(models.Model):
    class Meta:
        verbose_name = _("Template HTML")
        verbose_name_plural = _("Templates HTML")

    key = models.CharField(max_length=255, primary_key=True, unique=True)
    body = models.TextField(blank=True)
    description = models.CharField(blank=True, max_length=511, default='')
    mode = models.PositiveSmallIntegerField(choices=ConfigMode.ChoiceList(), default=ConfigMode.private, db_index=True)
    category = models.ForeignKey('TemplateCategory', null=True, related_name='templates', on_delete=models.CASCADE)

    def __str__(self):
        if not self.description:
            return self.pk
        return '%s (%s)' % (self.pk, self.description)
