#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  urls.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#

from django.conf.urls import include
from django.urls import path

from .api import SystemConfig_RestfulAPI_route
from system_configure.views import verifyCode

urlpatterns = [
    path('i18n/', include('django.conf.urls.i18n')),
    path('api/', include(SystemConfig_RestfulAPI_route)),
    path('verifyCode/', verifyCode, name='verifyCode'),
]
