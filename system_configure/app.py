#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  app.py
#
#
#  Created by TVA

from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MyAppConfig(AppConfig):
    name = 'system_configure'
    verbose_name = _("System Configure")

    def ready(self):
        from . import signals
