import json
import logging
import re
import socket
import sys
import tempfile
import traceback
from functools import wraps
from typing import *

from django import shortcuts
from django.conf import settings
from django.conf.urls import include, url
from django.core.cache import cache
from django.core.exceptions import PermissionDenied as Http403
from django.core.exceptions import SuspiciousOperation as Http400
from django.core.files.storage import FileSystemStorage
from django.core.mail import send_mail
from django.http import HttpResponse, HttpResponseServerError, Http404
from django.urls import NoReverseMatch, reverse
from django.utils.deprecation import MiddlewareMixin
from munch import *
from rest_framework import exceptions
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework.routers import DefaultRouter
from rest_framework.views import APIView

logFilePath = getattr(settings, 'LOG_FILE_PATH', 'logging_local.log')

logging.basicConfig(filename=logFilePath,
                    filemode='a',
                    format='%(asctime)s,%(msecs)d | %(name)s | %(levelname)s | %(filename)s->%(funcName)s: %(message)s',
                    datefmt='%Y-%m-%d %H:%M:%S',
                    level=logging.DEBUG)

__all__ = ['logging', 'cache', 'shortcuts', 'Http403', 'Http400', 'Http404', 'custom_400',
           'reverse_base', 'BadRequest', 'ServerError',
           'get_media_storage', 'get_temp_storage', 'DisableCSRF', 'ipsh',
           'get_object_or_none', 'get_object_or_404', 'error_response',
           'success_response', 'parse_data_to_munch',
           'success_response_restful', 'error_response_restful', 'FullRouter']


def execute_sql_file(cursor, file_path, params=None, run_sql_pattern=None, map_result_to_dict=False,
                     include_description=False):
    with open(file_path) as f:
        sql_file_content = f.read()
        sql_commands = [s for s in sql_file_content.split('\n\n') if s and 'SELECT' in s.upper()]

    result_list = []
    result_dict = {}
    if not isinstance(params, list):
        params = [params]  # convert params to list when it is non list obj
    for i in range(len(sql_commands)):
        sql_cmd = sql_commands[i]
        if run_sql_pattern and re.search(run_sql_pattern, sql_cmd, re.I) is None:
            continue
        param = params[i] if i < len(params) else params[-1]
        cursor.execute(sql_cmd, param)
        raw_result = []
        if include_description:
            raw_result.append([d[0] for d in cursor.description])
        raw_result.extend(cursor.fetchall())

        if map_result_to_dict:
            m = re.search(r'^#(\w+)', sql_cmd.strip())
            if m:
                result_dict[m.group(1)] = raw_result
            else:
                result_dict['SELECT_%s' % i] = raw_result
        else:
            result_list.append(raw_result)

    if map_result_to_dict:
        return result_dict

    if len(sql_commands) == 1 and len(result_list) == 1:
        return result_list[0]
    return result_list


def disable_for_loaddata(signal_handler):
    """
    Decorator that turns off signal handlers when loading fixture data.
    """

    @wraps(signal_handler)
    def wrapper(*args, **kwargs):
        if kwargs['raw']:
            return
        signal_handler(*args, **kwargs)

    return wrapper


class LargeResultsSetPagination(PageNumberPagination):
    page_size = 100
    page_size_query_param = 'page_size'
    max_page_size = 1000


class StandardResultsSetPagination(PageNumberPagination):
    page_size_query_param = 'page_size'
    max_page_size = 100


class BadRequest(exceptions.APIException):
    status_code = 400
    default_detail = 'Bad Request'


class ServerError(exceptions.APIException):
    status_code = 500
    default_detail = 'Server Error'


def reverse_base(request, view_name, absolute=False, urlconf=None, args=None, kwargs=None, current_app=None):
    if not request.resolver_match.namespace:
        namespace = ''
    else:
        namespace = request.resolver_match.namespace + ':'
    _url = reverse(namespace + view_name, urlconf, args, kwargs, current_app)
    if absolute:
        _url = request.build_absolute_uri(_url)
    return _url


def custom_400():
    _, value, _ = sys.exc_info()
    return error_response(value.message, code=400)


def get_media_storage():
    return FileSystemStorage(location=settings.MEDIA_ROOT)


def get_temp_storage():
    return FileSystemStorage(location=tempfile.gettempdir(), file_permissions_mode=0o600)


class FullRouter(DefaultRouter):
    """
    The default router extends the SimpleRouter, but also adds in a default
    API root view, and adds format suffix patterns to the URLs.
    """
    include_root_view = True
    include_format_suffixes = True
    root_view_name = 'api-root'

    def __init__(self, root_view_name='api-root'):
        super(self.__class__, self).__init__()
        self.includeRouterList = []
        self.root_view_name = root_view_name

    def get_api_root_view(self, api_urls=None):
        """
        Return a view to use as the API root.
        """
        api_root_dict = {}
        for prefix, viewset, basename in self.registry:
            api_root_dict[prefix] = basename, viewset  # list_name.format(basename=basename)
        router_self = self

        class APIRoot(APIView):
            _ignore_model_permissions = True

            @staticmethod
            def get(request, *args, **kwargs):
                ret = []
                for r in router_self.includeRouterList:
                    try:
                        ret += [{
                            'name': r.root_view_name,
                            'url': reverse_base(request, r.root_view_name, absolute=True),
                        }]
                    except NoReverseMatch:
                        continue

                for root_prefix in api_root_dict:
                    base_name, view_set = api_root_dict[root_prefix]
                    for urlx, mapping, name, initkwargs in router_self.get_routes(view_set):
                        correct_name = name.format(basename=base_name)
                        reverse_kwagrs = {}
                        if 'lookup' in urlx:
                            reverse_kwagrs = {'pk': 1}
                        try:
                            ret += [{
                                'name': correct_name,
                                'methods': mapping,
                                'url': reverse_base(request, correct_name, absolute=True, kwargs=reverse_kwagrs),

                            }]
                        except NoReverseMatch:
                            continue

                return Response(ret)

        APIRoot.__name__ = self.root_view_name
        return APIRoot.as_view()

    def get_urls(self):
        urlpatterns = super(self.__class__, self).get_urls()
        for r in self.includeRouterList:
            urlpatterns.append(url('^' + r.root_view_name + '/', include(r.urls)))

        return urlpatterns

    def include(self, r):
        self.includeRouterList.append(r)


class DisableCSRF(MiddlewareMixin):
    @staticmethod
    def process_request(request):
        setattr(request, '_dont_enforce_csrf_checks', True)


# Wrap it in a function that gives me more context:
def ipsh():
    # First import the embed function
    from IPython.terminal.embed import InteractiveShellEmbed
    import inspect
    ipshell = InteractiveShellEmbed()  # config=cfg, banner1=banner_msg, exit_msg=exit_msg
    frame = inspect.currentframe().f_back
    msg = 'Stopped at {0.f_code.co_filename} at line {0.f_lineno}'.format(frame)
    # Go back one level!
    # This is needed because the call to ipshell is inside the function ipsh()
    ipshell(msg, stack_depth=2)


def get_object_or_none(model, *args, **kwargs):
    cache_time = 60
    if len(args) >= 1:
        cache_time = int(args[0])
    cache_key = model.__name__ + '?' + '&'.join(['%s=%s' % (k, v) for k, v in kwargs.items()])

    # try to get from cache first
    model_object = cache.get(cache_key, None)
    if model_object is None:  # cache miss
        try:
            model_object = model.objects.get(**kwargs)
        except model.DoesNotExist:
            return None
        except model.MultipleObjectsReturned:
            if len(args) >= 2:
                return None
            model_object = model.objects.filter(**kwargs).first()
        if cache_time > 0:
            cache.set(cache_key, model_object, cache_time)
            model_object._cacheKey = cache_key
        else:
            model_object._cacheKey = None

    return model_object


def get_object_or_404(model_object: Any, *args, **kwargs) -> object:
    try:
        obj = model_object.objects.get(*args, **kwargs)
    except model_object.DoesNotExist:
        raise Http404()
    return obj


def error_response(error, code=0, response=None):
    if response:
        HttpResponseServerError(json.dumps({"error": error, 'code': code, "response": str(response)}))
    return HttpResponseServerError(json.dumps({"error": error, 'code': code}))


def success_response(data=None, encode=True):
    if data is not None:
        if encode:
            return HttpResponse(json.dumps(data))
        else:
            return HttpResponse(data)
    return HttpResponse('success')


def success_response_restful(data=None):
    if data is not None:
        return Response(data)
    else:
        return Response({"success": True})


def error_response_restful(error, code=None):
    if code is not None:
        return Response({"success": False, "error": error}, status=code)
    else:
        return Response({"success": False, "error": error}, status=406)  # HTTP_406_NOT_ACCEPTABLE


def parse_data_to_munch(res_data):
    json_data = munchify(json.loads(res_data))
    return json_data


def send_error_email(fn_name, args, kwargs, host, formatted_exc):
    formatted_exc = formatted_exc.strip()
    contents = 'Task: {fn_name}\nArgs: {args}\nKwargs: {kwargs}\n' + 'Host: {host}\nError: {error}'
    body = contents.format(
        fn_name=fn_name,
        args=args,
        kwargs=kwargs,
        host=host,
        error=formatted_exc,
    )
    short_exc = formatted_exc.rsplit('\n')[-1]
    subject = '[celery-error] {host} {fn_name} {short_exc}'.format(
        host=host,
        fn_name=fn_name,
        short_exc=short_exc,
    )
    send_mail(subject=subject, from_email="order_china.vn@gmail.com", message=body,
              recipient_list=['hahungkk@gmail.com'], fail_silently=False)


def email_if_fails(fn):
    @wraps(fn)
    def decorated(*args, **kwargs):
        try:
            return fn(*args, **kwargs)
        except Exception as e:
            print(e)
            if not settings.DEBUG:
                try:
                    fn_name = fn.func_name
                except AttributeError:
                    fn_name = fn.__name__
                send_error_email(fn_name, args, kwargs, socket.gethostname(),
                                 traceback.format_exc())
            raise

    return decorated
