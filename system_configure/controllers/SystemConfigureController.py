#!/usr/bin/python
# -*- coding: utf-8 -*-
#
#  SystemConfigureController.py
#
#
#  Created by V.Anh Tran on 11/29/14.
#  Copyright (c) 2014 __MyCompanyName__. All rights reserved.
#

import random
import hashlib
import json
import yaml
from django.utils import timezone
from django.core.cache import cache
from django.conf import settings
import jwt
from munch import Munch
from django.template import Context, Template
from ..models import SystemConfig, TemplateHTML, ConfigMode


def get_configure(key, default='', json_format=False, yaml_format=False, munch=False):
    overide_encode = getattr(settings, 'SYSTEM_CONFIG_ENCODE_OVERIDE')
    if overide_encode is not None:
        json_format = overide_encode.get('JSON', json_format)
        yaml_format = overide_encode.get('YAML', yaml_format)
        munch = overide_encode.get('BUNCH', munch)

    data_default = default
    if json_format:
        data_default = json.dumps(default)
    elif yaml_format:
        data_default = yaml.dump(default, default_flow_style=False, default_style='')

    config, created = SystemConfig.objects.get_or_create(pk=key, defaults={'key': key, 'value': data_default})
    result = config.value

    if json_format:
        result = json.loads(config.value)
    elif yaml_format:
        result = yaml.safe_load(config.value)

    if munch and isinstance(result, dict):
        result = Munch.fromDict(result)

    return result


def get_public_config(key):
    value = SystemConfig.objects.filter(pk=key, mode=ConfigMode.public)
    if value.exists():
        return value[0]
    return None

# def setConfigure(key, value, JSON=False, YAML=False, updateDict=False):
#       config, created = SystemConfig.objects.get_or_create(pk=key, defaults={'key': key, 'value': value})
#
#       if not created:
#               if JSON:
#                       if updateDict and isinstance(value, dict):  # update value with current data first
#                               value.update(json.loads(config.value))
#                       # save new value
#                       config.value = json.dumps(value)
#               else:
#                       config.value = value
#               config.save()
#
#       if JSON:
#               return json.loads(config.value)
#       return config.value


def get_html(key, **kwargs):
    default = kwargs.pop('default')

    template_html, created = TemplateHTML.objects.get_or_create(pk=key, defaults={'key': key, 'body': default})

    # changed = False;
    # if len(kwargs)>0:
    #       for context_name in kwargs:
    #               context_insert = '{{%s}}'%context_name;
    #               if context_insert not in template_html.description:
    #                       template_html+=' '+context_insert;
    #                       changed=True;
    # if changed:
    #       template_html.save();

    context = Context(kwargs)
    return Template(template_html.body).render(context)


def set_html(key, body):
    template_html, created = TemplateHTML.objects.get_or_create(pk=key, defaults={'key': key, 'body': body})
    if not created:
        template_html.body = body
        template_html.save()
    return template_html.body


def generate_temporary_code(**kwargs):
    temp_code = hashlib.sha1('temporary_code_%s' % (random.randint(0, 10 ** 9))).hexdigest()[:10].upper()
    timeout = settings.TEMPORARY_CODE_EXPIRES
    cache.set(temp_code, kwargs, timeout)
    return temp_code


def verify_temporary_code(temp_code):
    data = cache.get(temp_code)
    cache.delete(temp_code)
    if not data:
        return None
    return Munch.fromDict(data)


def generate_temporary_jwt(**kwargs):
    # timezone.now().strftime(settings.STRING_FORMAT_TIME)
    expires = timezone.now() + timezone.timedelta(seconds=settings.TEMPORARY_CODE_EXPIRES)
    kwargs['expires'] = expires.strftime(settings.STRING_FORMAT_TIME)
    token = jwt.encode(kwargs, settings.SECRET_KEY, algorithm='HS256')
    return token


def verify_temporary_jwt(token):
    try:
        token_data = jwt.decode(token, settings.SECRET_KEY)
    except jwt.DecodeError as e:
        return None

    try:
        expires = timezone.datetime.strptime(token_data['expires'], settings.STRING_FORMAT_TIME)
    except Exception as e:
        print(e)
        return None

    if expires > timezone.datetime.now():
        return Munch.fromDict(token_data)

    return None
