#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  admin.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#

from ckeditor.widgets import CKEditorWidget
from ckeditor_uploader.widgets import CKEditorUploadingWidget
from django.contrib import admin
from django.forms import ModelForm, Widget, ModelChoiceField
from .models import SystemConfig, TemplateHTML, TemplateCategory


class SystemConfigForm(ModelForm):
    class Meta:
        fields = ['key', 'description', 'value', 'mode']
    # widgets = {
    # 	'value': RedactorWidget(editor_options={'lang': 'en'})
    # }


@admin.register(SystemConfig)
class SystemConfigAdmin(admin.ModelAdmin):
    search_fields = ['key', 'description']
    list_display = ('__str__', 'mode')

    form = SystemConfigForm


@admin.register(TemplateCategory)
class TemplateCategoryAdmin(admin.ModelAdmin):
    list_display = ('key', 'label')


class TemplateHTMLForm(ModelForm):
    category = ModelChoiceField(required=False, queryset=TemplateCategory.objects.all())

    class Meta:
        fields = ['key', 'description', 'body', 'category', 'mode']
        widgets = {
            'body': CKEditorUploadingWidget()
        }


@admin.register(TemplateHTML)
class TemplateHTMLAdmin(admin.ModelAdmin):
    search_fields = ['key', 'description']
    list_display = ('__str__', 'mode')

    form = TemplateHTMLForm
