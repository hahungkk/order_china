#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  views.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#

from django import shortcuts
from django.conf import settings;
from django.http import *
from .models import SystemConfig
from .signals import post_verify_code

from .controllers import SystemConfigureController


def verifyCode(request):
    if request.method == 'GET':
        code = request.GET.get('code');
        success = request.GET.get('success');
        failed = request.GET.get('failed');
        if not code: raise Http404();
        result = SystemConfigureController.verify_temporary_jwt(code);
        if result:
            post_verify_code.send(sender=SystemConfig, code=code, data=result);
            if success: return shortcuts.redirect(success);
            return shortcuts.redirect(settings.ACTIVATE_CODE_SUCCESS_REDIRECT);
        else:
            if failed: return shortcuts.redirect(failed);
            return HttpResponseBadRequest(u"Invalid code or code expired");
    else:
        raise Http404()
