#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  signals.py
#  
#
#  Created by TVA on 4/20/15.
#  Copyright (c) 2015 storagon. All rights reserved.
#
from django.db.models.signals import pre_save
from django.dispatch import Signal, receiver
from system_configure.models import SystemConfig
from shop_module.task import update_service_charge_change

post_verify_code = Signal(providing_args=['code', 'data'])


@receiver(pre_save, sender=SystemConfig)
def presave_order(sender, instance, **kwargs):
    if instance.key == 'khuyenmai' and instance.value == '0':
        update_service_charge_change.delay()
