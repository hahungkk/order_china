#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  Config_RestfulAPI
#  
#
#  Created by TVA on 6/7/15.
#  Copyright (c) 2015 vietcomfund. All rights reserved.
#

from rest_framework import serializers, generics, permissions, viewsets, filters
from rest_framework.authentication import SessionAuthentication
from rest_framework_jwt.authentication import JSONWebTokenAuthentication

from ..enums import *
from ..models import SystemConfig, TemplateHTML, TemplateCategory


class SystemConfigSerializer(serializers.ModelSerializer):
    class Meta:
        model = SystemConfig
        fields = ('key', 'description', 'detail_url')

    detail_url = serializers.HyperlinkedIdentityField(view_name='ConfigAPI-detail')


class SystemConfigDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = SystemConfig
        fields = '__all__'


class TemplateHTMLSerializer(serializers.ModelSerializer):
    class Meta:
        model = TemplateHTML
        fields = ('key', 'description', 'detail_url', 'body')

    detail_url = serializers.HyperlinkedIdentityField(view_name='TemplateAPI-detail')


class TemplateHTMLDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = TemplateHTML
        fields = '__all__'


class TemplateSerializer(serializers.ModelSerializer):

    class Meta:
        model = TemplateHTML
        fields = ('key', 'description', 'mode')


class TemplateCategorySerializer(serializers.ModelSerializer):
    templates = TemplateSerializer(many=True, read_only=True)

    class Meta:
        model = TemplateCategory
        fields = ('key', 'label', 'templates')


class SystemConfigureCollectionClientAPIView(viewsets.GenericViewSet,
                                             generics.ListAPIView,
                                             generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.AllowAny, ]
    authentication_classes = [SessionAuthentication, JSONWebTokenAuthentication]
    serializer_class = SystemConfigSerializer

    def get_serializer_class(self):
        if self.kwargs.get('pk'):
            return SystemConfigDetailSerializer
        return SystemConfigSerializer

    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.is_superuser:
            return SystemConfig.objects.all().order_by('key')
        return SystemConfig.objects.filter(mode=ConfigMode.public).order_by('key')


class TemplateHTMLCollectionClientAPIView(viewsets.GenericViewSet,
                                          generics.ListAPIView,
                                          generics.RetrieveUpdateDestroyAPIView):
    permission_classes = [permissions.AllowAny, ]
    serializer_class = TemplateHTMLSerializer

    def get_serializer_class(self):
        if self.kwargs.get('pk'):
            return TemplateHTMLDetailSerializer
        return TemplateHTMLSerializer

    def get_queryset(self):
        if self.request.user.is_staff or self.request.user.is_superuser:
            return TemplateHTML.objects.all().order_by('key')
        return TemplateHTML.objects.filter(mode=ConfigMode.public).order_by('key')


class TemplateCategoryClientAPIView(viewsets.GenericViewSet,
                                    generics.ListAPIView,
                                    generics.RetrieveUpdateDestroyAPIView):
    serializer_class = TemplateCategorySerializer
    queryset = TemplateCategory.objects.all().order_by('key')
