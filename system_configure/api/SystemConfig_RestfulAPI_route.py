#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  SystemConfig_RestfullAPI_route.py
#  
#
#  Created by TVA on 6/7/15.
#  Copyright (c) 2015 vietcomfund. All rights reserved.
#

from .Config_RestfulAPI import SystemConfigureCollectionClientAPIView,\
    TemplateHTMLCollectionClientAPIView, TemplateCategoryClientAPIView
from ..controllers.Tool import FullRouter

router = FullRouter('system_configure')
router.register(r'template', TemplateHTMLCollectionClientAPIView, basename='TemplateAPI')
router.register(r'category', TemplateCategoryClientAPIView, basename='TemplateCategoryAPI')
router.register(r'', SystemConfigureCollectionClientAPIView, basename='ConfigAPI')


urlpatterns = router.urls
