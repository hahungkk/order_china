from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class MyAppConfig(AppConfig):
    name = 'ibanking_synctransaction'
    verbose_name = _("ibanking_synctransaction")
