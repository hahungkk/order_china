from django.contrib import admin
from django.forms import ModelForm, PasswordInput
from .models import Transaction, BalanceAccount, BalanceHistory


@admin.register(Transaction)
class TransactionAdmin(admin.ModelAdmin):
    # list view
    search_fields = ['transaction_id', 'description']
    list_display = ['transaction_id', 'balance_account', 'created', 'in_amount', 'out_amount', 'description']
    list_filter = ['created', 'balance_account']
    list_editable = []
    ordering = ['-created']


class BalanceAccountForm(ModelForm):
    class Meta:
        model = BalanceAccount
        fields = ['name', 'bank', 'currency', 'current_balance', 'username', 'password', 'detail']
        widgets = {
            'password': PasswordInput(),
        }


@admin.register(BalanceAccount)
class BalanceAccountAdmin(admin.ModelAdmin):
    # list view
    search_fields = ['name', 'username']
    list_display = ['id', 'bank', 'name', 'currency', 'current_balance', 'modified']
    list_filter = ['bank', 'modified']
    list_editable = []
    # detail view
    form = BalanceAccountForm


@admin.register(BalanceHistory)
class BalanceHistoryAdmin(admin.ModelAdmin):
    # list view
    search_fields = []
    list_display = ['id', 'balance_account', 'balance_amount', 'created']
    list_filter = ['balance_account', 'created']
    list_editable = []
