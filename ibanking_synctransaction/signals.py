from django.db.models.signals import post_save
from django.dispatch import receiver
from system_configure.controllers import Tool
from .models import BalanceAccount, BalanceHistory


@receiver(post_save, sender=BalanceAccount)
@Tool.disable_for_loaddata
def post_save_balance_account(sender, instance=BalanceAccount(), **kwargs):
    if instance.id:  # editing
        blance_history = BalanceHistory()
        blance_history.balance_account = instance
        blance_history.balance_amount = instance.current_balance
        blance_history.save()
