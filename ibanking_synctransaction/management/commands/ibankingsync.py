# import datetime
# import getpass
# import logging
# import os
# import re
# import sys
# import kronos
# from munch import munchify
# from django.conf import settings
# from django.core.management.base import BaseCommand
# from django.utils import timezone
# from requests_browser.browser import parse_int
#
#
# # from manga_manager.mongo_models import Chapter
# @kronos.register('*/10 * * * *', args={'30': None})
# class Command(BaseCommand):
#     help = 'Sync transaction by login to ibanking'
#
#     def add_arguments(self, parser):
#         parser.add_argument('check_back_days', nargs='?', type=int, default=1,
#                             help='Number of day to check back from now')
#
#     # parser.add_argument('param1',nargs='+',type=int)
#
#     def handle(self, *args, **options):
#         check_back_days = options.get('check_back_days', 1)
#         from ...models import BalanceAccount, Transaction
#         from ...enums import Bank
#
#         IBANKING_SYNC = getattr(settings, 'IBANKING_SYNC', {})
#
#         for balanceAccount in BalanceAccount.objects.all():
#             cookieFolderPath = os.path.join(settings.BASE_DIR, 'cookie')
#             if not os.path.exists(cookieFolderPath):
#                 os.makedirs(cookieFolderPath)
#             ses = BrowserSession(
#                 cookie_path=os.path.join(cookieFolderPath, balanceAccount.name + '_' + balanceAccount.bank + '.txt'),
#                 dbc_username=IBANKING_SYNC.get('dbc_username'), dbc_password=IBANKING_SYNC.get('dbc_password')
#                 )
#
#             if balanceAccount.bank == Bank.VCB:
#                 if not balanceAccount.username or not balanceAccount.password:
#                     logging.warning("Skip check ibanking due to not setup username/password: %s" % balanceAccount)
#                     continue
#                 ibankingVCB = IbankingVCB(ses, balanceAccount.username, balanceAccount.password)
#                 if ibankingVCB.login():
#                     result = ibankingVCB.check_ibanking(check_back_days)
#
#                     transactionList = []
#                     for tr in result.pop('transactionList'):
#                         trans = Transaction(**tr)
#                         trans.created = timezone.make_aware(tr.created, timezone.get_current_timezone())
#                         trans.balance_account_id = balanceAccount.id
#                         if Transaction.objects.filter(transaction_id=trans.transaction_id).exists():
#                             continue  # due to transaction_id check.
#                         transactionList.append(trans)
#                     Transaction.objects.bulk_create(transactionList, batch_size=500)
#                     if result.current_balance != balanceAccount.current_balance:
#                         balanceAccount.current_balance = result.current_balance
#                         balanceAccount.detail = result.toJSON()
#                         balanceAccount.save()
#
#
# class IbankingVCB():
#     def __init__(self, browserSession, username, password=None):
#         self.browserSession = browserSession
#
#         self.root_url = 'https://www.vietcombank.com.vn'
#         self.date_format = '%d/%m/%Y'
#         self.username = username
#         self.password = password
#
#     def login(self):
#
#         res = self.browserSession.get(self.root_url + '/IBanking/')
#
#         self.account_aid = ''
#
#         m = re.search(r'ibanking2015/(\w+)/thongtintaikhoan/taikhoan/chitiettaikhoan\?aid=(\w+)', res.content)
#         if m:  # logged in
#             self.session_id = m.group(1)
#             self.account_aid = m.group(2)
#         else:
#             if self.password is None: self.password = getpass.getpass("Password:");
#
#             form = res.dom().select_one('form')
#             post_url = form.attrs['action']  # this is without domain root
#             m = re.search(r'/IBanking2015/(\w+)/Account/Login', post_url)
#             self.session_id = m.group(1)
#
#             captcha_guid = form.select('input[name=captcha-guid]')[0].attrs['value']
#             captcha_url = form.select('img#captchaImage')[0].attrs['src']
#
#             # login form post
#             captcha_id, captcha_result = self.browserSession.resolve_captcha(self.root_url + captcha_url)
#
#             res = self.browserSession.post(self.root_url + post_url, {
#                 'source': '',
#                 'username': self.username,
#                 'pass': self.password,
#                 'captcha': captcha_result,
#                 'captcha-guid': captcha_guid,
#             })
#             m = re.search(r'ibanking2015/(\w+)/thongtintaikhoan/taikhoan/chitiettaikhoan\?aid=(\w+)', res.content)
#             if re.search(re.escape(u'kh&#244;ng ch&#237;nh x&#225;c! Qu&#253; kh&#225;ch vui l&#242;ng'), res.content):
#                 wrong_captcha = True
#             else:
#                 wrong_captcha = False
#
#             if m:  # logged in
#                 self.session_id = m.group(1)
#                 self.account_aid = m.group(2)
#             else:  # probaly wrong password or captcha fail test
#                 if wrong_captcha:
#                     logging.warning(u"Wrong captcha")
#                     self.browserSession.report_captcha(captcha_id)
#                 else:
#                     logging.error(u"Invalid ibanking username/password")
#
#                 return False
#         return True
#
#     def check_ibanking(self, check_back_days=1):
#         res = self.browserSession.get(
#             'https://www.vietcombank.com.vn/ibanking2015/%s/thongtintaikhoan/taikhoan/chitiettaikhoan?aid=%s' % (
#                 self.session_id, self.account_aid))
#
#         form_post = self.browserSession.extract_form_post_data(res.dom(), css_selector='form#ChiTietTaiKhoan')
#
#         self.account_aid, self.acount_cc = form_post.TaiKhoanTrichNo.split('|')
#
#         form_post.update({
#             # 'TaiKhoanTrichNo'      : 'AC109593A4AAF70AF53F59C210D06021|7C5739E2C1299FD0200B5948CF952B58',
#             'MaLoaiTaiKhoanEncrypt': self.acount_cc,
#             # 'SoDuHienTai'          : '',
#             # 'LoaiTaiKhoan'         : '',
#             # 'LoaiTienTe'           : '',
#             'AID': self.account_aid.lower(),
#             'NgayBatDauText': '',
#             'NgayKetThucText': '',
#         })
#         res = self.browserSession.post(
#             'https://www.vietcombank.com.vn/IBanking2015/%s/ThongTinTaiKhoan/TaiKhoan/GetThongTinChiTiet' % self.session_id,
#             form_post)
#         bdata = munchify(res.json())
#
#         current_balance = bdata.DanhSachTaiKhoan[0].SoDuHienTai
#
#         today = datetime.date.today()
#         startdate = today - datetime.timedelta(days=check_back_days)
#
#         form_post.update({
#             'SoDuHienTai': current_balance,
#             'LoaiTaiKhoan': 'TK_D',
#             'LoaiTienTe': 'VND',
#             'NgayBatDauText': startdate.strftime(self.date_format),
#             'NgayKetThucText': today.strftime(self.date_format),
#         })
#
#         res = self.browserSession.post(
#             'https://www.vietcombank.com.vn/IBanking2015/%s/ThongTinTaiKhoan/TaiKhoan/ChiTietGiaoDich' % self.session_id,
#             form_post)
#         bdata = munchify(res.json())
#         self.last_ibanking_info = bdata
#         result = munchify({
#             'account_number': bdata.TaiKhoan,
#             'current_balance': current_balance,
#             'currency': bdata.LoaiTienTe,
#             'begin_balance': parse_int(bdata.SoDuDauKy),
#             'end_balance': parse_int(bdata.SoDuCuoiKy),
#             'diff_balance': parse_int(bdata.TongTien),
#         })
#         print("########Account Balance Info#####")
#         print(result)
#         result.transactionList = []
#         for bankTransaction in bdata.ChiTietGiaoDich:
#             print('##################################')
#             trans = munchify({
#                 'description': bankTransaction.MoTa,
#                 'created': datetime.datetime.strptime(bankTransaction.NgayGiaoDich, self.date_format),
#                 'transaction_id': bankTransaction.SoThamChieu,
#                 'in_amount': parse_int(bankTransaction.SoTienGhiCo),
#                 'out_amount': parse_int(bankTransaction.SoTienGhiNo),
#                 'detail': bankTransaction.toJSON(),
#             })
#             print(trans)
#             result.transactionList.append(trans)
#
#         return result
#
#
# def main(argv):
#     ses = BrowserSession('test_cookie.txt')
#     print("###%s###" % datetime.datetime.now())
#     ibankingVCB = IbankingVCB(ses, '4465808A06')
#     if ibankingVCB.login():
#         ibankingVCB.check_ibanking(30)
#
#     ses.close()
#
#     return 0
#
#
# if __name__ == '__main__':
#     sys.exit(main(sys.argv))
