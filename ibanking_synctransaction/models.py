from django.conf import settings
from django.core.validators import MinValueValidator
from django.db import models
from django.utils.translation import ugettext_lazy as _
from .enums import *

MONEY_MAX_DIGITS = 24
MONEY_DECIMAL_PLACES = 2
XRATE_MAX_DIGITS = 15
XRATE_DECIMAL_PLACES = getattr(settings, 'XRATE_DECIMAL_PLACES', 5)


class Transaction(models.Model):
    class Meta:
        verbose_name = _("Transaction")
        verbose_name_plural = _("Transaction")
        unique_together = ('transaction_id', 'balance_account')

    created = models.DateTimeField(verbose_name=_("created"))
    transaction_id = models.CharField(verbose_name=_("transaction_id"), max_length=255, db_index=True)
    balance_account = models.ForeignKey("BalanceAccount", verbose_name=_("BalanceAccount"), null=True,
                                        blank=True, on_delete=models.PROTECT)

    in_amount = models.DecimalField(verbose_name=_("in_amount"), default=0.0, max_digits=MONEY_MAX_DIGITS,
                                    decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)],
                                    db_index=True)
    out_amount = models.DecimalField(verbose_name=_("out_amount"), default=0.0, max_digits=MONEY_MAX_DIGITS,
                                     decimal_places=MONEY_DECIMAL_PLACES, validators=[MinValueValidator(0)],
                                     db_index=True)
    description = models.CharField(verbose_name=_("description"), max_length=512, blank=True)
    detail = models.TextField(verbose_name=_("detail"), blank=True)
    # store detail automatically get from bank system

    def __str__(self):
        return str("Trans %s : %s" % (self.pk, self.transaction_id))


class BalanceAccount(models.Model):
    class Meta:
        verbose_name = _("BalanceAccount")
        verbose_name_plural = _("BalanceAccount")
        unique_together = ('name', 'bank')

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)

    name = models.CharField(verbose_name=_("name"), max_length=255, db_index=True)
    bank = models.CharField(verbose_name=_("bank"), max_length=255, choices=Bank.ChoiceList(),
                            default=Bank.VCB, db_index=True)

    currency = models.CharField(verbose_name=_("currency"), max_length=256, blank=True)
    current_balance = models.DecimalField(verbose_name=_("current_balance"), default=0.0,
                                          max_digits=MONEY_MAX_DIGITS, decimal_places=MONEY_DECIMAL_PLACES,
                                          validators=[MinValueValidator(0)], db_index=True)
    detail = models.TextField(verbose_name=_("detail"), blank=True)
    # store detail automatically get from bank system

    username = models.CharField(verbose_name=_("username"), max_length=512)
    password = models.CharField(verbose_name=_("password"), max_length=512)

    def __str__(self):
        return str("%s %s: %s %s" % (self.bank, self.name, self.current_balance, self.currency))


class BalanceHistory(models.Model):
    class Meta:
        verbose_name = _("BalanceHistory")
        verbose_name_plural = _("BalanceHistory")

    created = models.DateTimeField(verbose_name=_("created"), auto_now_add=True)
    modified = models.DateTimeField(verbose_name=_("modified"), auto_now=True)
    balance_account = models.ForeignKey("BalanceAccount", verbose_name=_("BalanceAccount"),
                                        null=True, blank=True, on_delete=models.PROTECT)
    balance_amount = models.DecimalField(verbose_name=_("balance_amount"), max_digits=MONEY_MAX_DIGITS,
                                         decimal_places=MONEY_DECIMAL_PLACES,
                                         validators=[MinValueValidator(0)], db_index=True)
