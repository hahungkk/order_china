# coding: utf-8
# Created by Cherry at Fri 07 Jul 2017 08:49:24 AM +07

from babel.dates import format_datetime, format_date


def convert_to_vietnamese_date(date_input, forma=None):
    return format_date(date_input, format=forma if forma else 'long', locale='vi')


def convert_to_vietnamese_datetime(date_input, forma=None):
    return format_datetime(date_input, format=forma if forma else 'long', locale='vi')
