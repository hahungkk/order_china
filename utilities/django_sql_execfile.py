#!/usr/bin/python
# -*- coding: utf-8 -*-   
#
#  django_sql_execfile.py
#  
#
#  Created by TVA on 3/30/16.
#  Copyright (c) 2016 django-query-execfile. All rights reserved.
#
from django.db import connection
from django.conf import settings
import re
import os


def raw_queryfile(orm_class, file_path, params=None, translations=None, using=None):
    """ Generate RawQuerySet from sql command defined in a .sql file

    :param orm_class: models.Model
    :type orm_class: django.db.models.Model
    :param file_path:
    :param params:
    :type params: dict
    :param translations: field name mapping dict
    :type translations: dict
    :param using: database alias
    :return:
    :rtype: django.db.models.query.RawQuerySet
    """
    if not os.path.isabs(file_path):
        file_path = os.path.join(settings.BASE_DIR, file_path)

    with open(file_path) as f:
        sql_file_content = f.read().strip()

    return orm_class.objects.raw(sql_file_content, params=params, translations=translations, using=using)


def sql_execfile(file_path, cursor=None, using=None, params=None, run_sql_pattern=None, map_result_to_dict=False,
                 include_description=False, map_description_to_field=False):
    """ Execute sql command defined in a .sql file

    :param map_description_to_field:
    :param cursor: connection.cursor()
    :param using: database alias
    :param file_path: path of .sql file
    :param params: params value pass to sql command. Incase of .sql file contain multiple command, the order of params
    corresponding with order of sql commands
    :type params: list[dict]
    :param run_sql_pattern: only run those command that contain this regex pattern (using re.search). Should be ignore
    if .sql file contain only one command
    :param map_result_to_dict: return a dict and map result to dict by key = comment at the first line of command
    :param include_description: include column description in result list/dict
    :return: result list or dict(map_result_to_dict=True) of cursor.fetchall() when execute those command in that
    .sql file in the same order of commands (top-down).
    """
    close_cursor_when_done = False
    if cursor is None:
        if using is None:
            cursor = connection.cursor()
        else:
            cursor = connection[using].cursor()
        close_cursor_when_done = True

    if not os.path.isabs(file_path):
        file_path = os.path.join(settings.BASE_DIR, file_path)

    with open(file_path) as f:
        sql_file_content = f.read()
        sql_commands = [s for s in sql_file_content.split('\n\n') if s.strip()]

    result_list = []
    result_dict = {}
    if not isinstance(params, list):
        params = [params]  # convert params to list when it is non list obj

    success_command = 0
    try:
        for i in range(len(sql_commands)):
            sql_cmd = sql_commands[i]
            if run_sql_pattern and re.search(run_sql_pattern, sql_cmd, re.I) is None:
                continue
            # escape % in DATEFORMAT
            sql_cmd = re.sub(r'(%[^s(])', r'%\1', sql_cmd)
            param = params[i] if i < len(params) else params[-1]
            cursor.execute(sql_cmd, param)
            raw_result = []

            if include_description:
                desc_row = [d[0] for d in cursor.description]
                if map_description_to_field:
                    for row in cursor.fetchall():
                        row_dict = {}
                        for j in range(len(row)):
                            row_dict[desc_row[j]] = row[j]
                        raw_result.append(row_dict)
                else:
                    raw_result.append(desc_row)
                    raw_result.extend(cursor.fetchall())
            else:
                raw_result.extend(cursor.fetchall())

            if map_result_to_dict:
                m = re.search(r'^#(\w+)', sql_cmd.strip())
                if m:
                    result_dict[m.group(1)] = raw_result
                else:
                    result_dict['QUERY_%s' % i] = raw_result
            else:
                result_list.append(raw_result)
            success_command += 1
    except Exception as e:
        cursor.close()
        raise e
    else:
        if close_cursor_when_done:
            cursor.close()

    if map_result_to_dict:
        if success_command == 1 and len(result_dict) == 1:
            return result_dict[result_dict.keys()[0]]
        return result_dict

    if success_command == 1 and len(result_list) == 1:
        return result_list[0]
    return result_list
