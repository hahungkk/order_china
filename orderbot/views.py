import json
from pprint import pprint
from django.conf import settings
import requests
from django.http.response import HttpResponse
from munch import munchify
from shop_module.models import Order


def botview(request):
    if request.method == 'GET':
        if request.GET['hub.verify_token'] == settings.YOUR_SECRET_TOKEN:
            return HttpResponse(request.GET['hub.challenge'])
        else:
            return HttpResponse('Error, invalid token')
    else:
        try:
            body_unicode = request.body.decode('utf-8')
            body = json.loads(body_unicode)
            body = munchify(body)
            mess = body.entry[0].messaging[0].message
            sender = body.entry[0].messaging[0].sender.id
            post_facebook_message(sender, mess.text)
        except Exception as e:
            print(e)
        return HttpResponse('ahihi')


def post_facebook_message(fbid, recevied_message):
    post_message_url = 'https://graph.facebook.com/v3.2/me/messages?access_token=' + settings.PAGE_ACCESS_TOKEN
    response_msg = json.dumps({"recipient": {"id": fbid}, "message": {"text": recevied_message}})
    status = requests.post(post_message_url, headers={"Content-Type": "application/json"}, data=response_msg)
    pprint(status.json())

