from celery import shared_task

from orderbot.views import post_facebook_message
from shop_module.models import Order


@shared_task
def send_message_task(order):
    fbid = order.customer.profile.facebook_uid
    if fbid:
        message = f"""
            order_china xin thông báo. Đơn hàng {order.id} của bạn đã về Việt Nam.
        """
        try:
            post_facebook_message(fbid, message)
            return fbid
        except Exception as e:
            print(e)
            return str(e)
