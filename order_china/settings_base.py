import os
import sys
from django.utils.translation import ugettext_lazy as _
from django.conf.locale.vi import formats as vi_formats
import datetime

BASE_DIR = os.path.dirname(os.path.dirname(os.path.abspath(__file__)))
vi_formats.DATETIME_FORMAT = 'H:i d/m/Y'

SECRET_KEY = '5tvylz4rc50%tgf(_tkhe)!u6@modh)=zh88rehfd0o(9yus4='

DEBUG = True

ALLOWED_HOSTS = ['*']

INSTALLED_APPS = (
    'dal',
    'dal_select2',
    'suit',
    'date_range_filter',
    'suit_redactor',
    'ckeditor',  # better admin interface
    'ckeditor_uploader',
    'rest_framework',  # Restful API
    'crispy_forms',
    'notifications',
    'corsheaders',  # CORS support
    'mptt',  # tree travelsal
    'massadmin',  # mass edit object
    'import_export',  # import export CVS
    'simple_history',

    'django_filters',
    'drf_tweaks',
    'django_select2',  # auto complete select box
    'django_tables2',
    'django_celery_beat',
    'django_object_actions',  # action on object
    'django_extensions',

    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'user_module',
    'shop_module',
    'ibanking_synctransaction',
    'webapp',
    'system_configure',
)

MIDDLEWARE = (
    'system_configure.controllers.Tool.DisableCSRF',
    'corsheaders.middleware.CorsMiddleware',

    'django.middleware.locale.LocaleMiddleware',
    'django.middleware.common.CommonMiddleware',
    'django.contrib.sessions.middleware.SessionMiddleware',
    'django.contrib.auth.middleware.AuthenticationMiddleware',
    'django.contrib.messages.middleware.MessageMiddleware',

    'simple_history.middleware.HistoryRequestMiddleware',
    'audit_log.middleware.UserLoggingMiddleware',
)

ROOT_URLCONF = 'order_china.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [os.path.join(BASE_DIR, 'webapp/static/app')],
        'APP_DIRS': True,
        'OPTIONS': {
            'loaders': [
                'django.template.loaders.app_directories.Loader',
                'django.template.loaders.filesystem.Loader',
            ],
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.request',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'order_china.wsgi.application'

CACHES = {
    "default": {
        "BACKEND": "django_redis.cache.RedisCache",
        "LOCATION": "redis://redis:6379/11",
        "OPTIONS": {
            "CLIENT_CLASS": "django_redis.client.DefaultClient",
        }
    }
}

# Internationalization
# https://docs.djangoproject.com/en/1.8/topics/i18n/
LOCALE_PATHS = (
    os.path.join(BASE_DIR, 'locale'),
)

LANGUAGES = (
    ('vi', 'Vietnamese'),
    ('en', 'English'),
    ('zh-cn', 'Chinese'),
)

TIME_ZONE = 'Asia/Bangkok'

USE_I18N = True

USE_L10N = True

USE_TZ = True

STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'static')

CORS_ORIGIN_ALLOW_ALL = True

CORS_ALLOW_HEADERS = (
    'x-requested-with',
    'content-type',
    'accept',
    'origin',
    'authorization',
    'x-csrftoken',
    'range',
    'cookie',
    'signature_authorization',
)

MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'media')

# Email
# EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
# EMAIL_HOST = 'email-smtp.us-west-2.amazonaws.com'
# EMAIL_PORT = 587
# #EMAIL_USE_SSL = True
# EMAIL_USE_TLS = True
# EMAIL_HOST_USER = 'AKIAIZA34Q5LS2VWZXNA'
# EMAIL_HOST_PASSWORD = 'Aj1jXL9zOsu97heXJuet6WU5LwrxFNH1EUOlInF4jtBH'

LOG_FOLDER = os.path.join(BASE_DIR, 'log')
if not os.path.exists(LOG_FOLDER):
    os.makedirs(LOG_FOLDER)
LOG_FILE_PATH = os.path.join(LOG_FOLDER, 'order_china.log')

IS_RUNNING_UNIT_TEST = False
if 'test' in sys.argv:
    IS_RUNNING_UNIT_TEST = True

REST_FRAMEWORK = {
    'DEFAULT_AUTHENTICATION_CLASSES': (
        'rest_framework.authentication.SessionAuthentication',
    ),

    'DEFAULT_RENDERER_CLASSES': (
        'rest_framework.renderers.JSONRenderer',
        'rest_framework.renderers.BrowsableAPIRenderer'
    ),
    'DEFAULT_VERSIONING_CLASS': 'rest_framework.versioning.URLPathVersioning',
    'DEFAULT_PAGINATION_CLASS': 'system_configure.controllers.Tool.StandardResultsSetPagination',
    'PAGE_SIZE': 30,  # Default to 30
    'DEFAULT_FILTER_BACKENDS': ('django_filters.rest_framework.DjangoFilterBackend',)
}

CKEDITOR_UPLOAD_PATH = 'uploads/'
CKEDITOR_IMAGE_BACKEND = 'pillow'
# CKEDITOR_JQUERY_URL = '//ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js'
CKEDITOR_CONFIGS = {
    'default': {
        'toolbar': 'full',
        'height': 400,
        'width': 500,
    },

    'readOnly': {
        'toolbar': 'Basic',
        'height': 400,
        'width': 500,
        'readOnly': True
    },
}

DJANGORESIZED_DEFAULT_SIZE = [800, 600]
DJANGORESIZED_DEFAULT_QUALITY = 75
DJANGORESIZED_DEFAULT_KEEP_META = True

ADMIN_PREFIX_URL = '/adl'
# Django Suit configuration example
SUIT_CONFIG = {
    # header
    'ADMIN_NAME': 'Admin',
    'HEADER_DATE_FORMAT': 'l, j. F Y',
    'HEADER_TIME_FORMAT': 'H:i',

    # forms
    'SHOW_REQUIRED_ASTERISK': True,  # Default True
    'CONFIRM_UNSAVED_CHANGES': True,  # Default True

    # menu
    'SEARCH_URL': ADMIN_PREFIX_URL + '/shop_module/order/',

    # 'MENU_OPEN_FIRST_CHILD': True, # Default True
    # 'MENU_EXCLUDE': ('auth.group',),

    'MENU': ('sites',

             {'label': _("Account"), 'icon': 'icon-lock', 'models': ('auth.User', 'auth.Group', 'admin.logentry')},

             {'label': _("Customer & Employee"), 'icon': 'icon-user', 'models': ['user_module.UserProfile',
                                                                                 'shop_module.CustomerProfile',
                                                                                 'shop_module.EmployeeProfile',
                                                                                 'shop_module.EmployeePosition']
              },

             {'label': _("Order & Product"), 'icon': 'icon-shopping-cart', 'models': [
                 'shop_module.Order',
                 {'label': _('Order Item'), 'permissions': ('shop_module.add_orderitem',),
                  'url': ADMIN_PREFIX_URL + '/shop_module/orderitem/?o=4'},
                 # 'shop_module.OrderItem',
                 'shop_module.Product', 'shop_module.ProductOption', 'shop_module.Category']
              },

             {'label': _("Logistics"), 'icon': 'icon-plane', 'models':
                 [
                     'shop_module.ShipmentPack',
                 ]
              },

             {'label': _("Payments"), 'icon': 'icon-fire', 'models': [
                 'shop_module.CustomerPayment',
                 # 'shop_module.VendorPayment', 'shop_module.LogisticsPayment', 'shop_module.GenericPayment',
                 'shop_module.PaymentType', 'shop_module.Transaction', 'shop_module.BalanceAccount']
              },

             {'label': _("Others"), 'icon': 'icon-briefcase', 'models': ['shop_module.Vendor',
                                                                         'shop_module.Currency',
                                                                         'shop_module.Status',
                                                                         'manual_dictionary.TranslateEntry']
              },
             {'label': _('System Config'), 'icon': 'icon-cog', 'models':
                 [
                     'system_configure.SystemConfig',
                     'system_configure.TemplateHtml',
                     'system_configure.TemplateCategory'
                 ]
              },

             ),
    # misc
    'LIST_PER_PAGE': 30
}

STRING_FORMAT_TIME = "%Y-%m-%d %H:%M:%S.%f"

TEMPORARY_CODE_EXPIRES = 3 * 86400  # seconds = 3 days
ACTIVATE_CODE_SUCCESS_REDIRECT = '/'
SYSTEM_CONFIG_ENCODE_OVERIDE = {
    'JSON': False,
    'YAML': True,
    'BUNCH': True
}

PROXY_CACHE_TIME = 24 * 60 * 60  # 24 hours

HTML_CACHE_FOLDER = os.path.join(MEDIA_ROOT, 'html')
if not os.path.exists(HTML_CACHE_FOLDER):
    os.makedirs(HTML_CACHE_FOLDER)

USER_MODULE_CONFIG = {
    'EXTRA_PROFILE_SERIALIZER_LIST': [
        'shop_module.api.CustomerProfile_RestfulAPI.CustomerProfileSerializer',
        'shop_module.api.EmployeeProfile_RestfulAPI.EmployeeProfileSerializer'
    ],
    'USER_PROFILE_CUSTOM_MODEL': 'user_module.models.UserProfile'
}

ATTENDANCE_TRACK_ALLOW_ALL_IP = False
ATTENDANCE_TRACK_ALLOW_IP_LIST = ['127.0.0.1']  #
ATTENDANCE_TRACK_TIME_BETWEEN_SUBMIT = 1 * 3600  # 1 hours

IBANKING_SYNC = {
    'dbc_username': 'tranvietanh1991',
    'dbc_password': 'TVA04101991',
}

MARGIN_MISSING_IN_CUSTOMER_PAYMENT = 10 ** 3
MAIN_CURRENCY_CODE = 'VND'

DATA_UPLOAD_MAX_NUMBER_FIELDS = None

JWT_AUTH = {
    'JWT_VERIFY': True,
    'JWT_VERIFY_EXPIRATION': True,
    'JWT_EXPIRATION_DELTA': datetime.timedelta(days=1),
    'JWT_ALLOW_REFRESH': True,
    'JWT_REFRESH_EXPIRATION_DELTA': datetime.timedelta(days=7),
    'JWT_RESPONSE_PAYLOAD_HANDLER': 'jwt_handle.jwt_response_payload_handler'
}

CHANNEL_LAYERS = {
    "default": {
        "BACKEND": "channels_redis.core.RedisChannelLayer",
        "CONFIG": {
            "hosts": [("redis", 6379)],
        },
    },
}

ASGI_APPLICATION = "order_china.routing.application"
YOUR_SECRET_TOKEN = "84468b502769e9f9076f7407ce1d3ca2"
PAGE_ACCESS_TOKEN = "EAADX23q8phIBAPgybH1ux4uGWjlU7Nv7EOuyM7iQPDos1" \
                    "hRPeVKlJcMjjQaKiPV1Gldnqy9FvqCWjTUV1qFVLgyVgRouUvEgRckbFCCcO" \
                    "cvdfLuiSE71aaVihBQmbszh7KAGgiQugFnZCPtI4SGKyMwXpu03CHJkC9tRi9NJhgrWd0E6k "
PAGE_ID = '939464082762519'
APP_ID = '237337656993298'
FACEBOOK_BOT = False

SESSION_COOKIE_SAMESITE = None
CSRF_COOKIE_SAMESITE = None

# SENTRY_CELERY_IGNORE_EXPECTED = True
# RAVEN_CONFIG = {
#     'dsn': 'https://7261689c64a949d1bad08d4eb105f15a:33ac37087f084e6b8bb90e2e10842889@sentry.io/1191946',
#     # If you are using git, you can also automatically configure the
#     # release based on the git info.
#     # 'release': raven.fetch_git_sha(os.path.abspath(os.pardir)),
# }
