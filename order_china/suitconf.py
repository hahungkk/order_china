from suit.apps import DjangoSuitConfig
from suit.menu import ParentItem, ChildItem


class SuitConfig(DjangoSuitConfig):
    layout = 'horizontal'
    menu = (
        ParentItem('Account', children=[
            ChildItem(model='auth.User'),
            ChildItem(model='auth.Group'),
            ChildItem(model='admin.logentry'),
        ], icon='icon-lock'),

        ParentItem('Customer & Employee', children=[
            ChildItem(model='user_module.UserProfile'),
            ChildItem(model='shop_module.CustomerProfile'),
            ChildItem(model='shop_module.EmployeeProfile'),
            ChildItem(model='shop_module.EmployeePosition'),
        ], icon='fa fa-users'),

        # ParentItem(_('Order & Product'), children=[
        #     ChildItem(model='shop_module.Order'),
        #     ChildItem(model='shop_module.CustomerProfile'),
        #     ChildItem(model='shop_module.EmployeeProfile'),
        #     ChildItem(model='shop_module.EmployeePosition'),
        # ], icon='icon-user'),
        #
        # ParentItem(_("Order & Product"), children=[
        #     ChildItem(model='auth.user'),
        #     ChildItem('User groups', 'auth.group'),
        # ], icon='fa fa-users'),
        # ParentItem('Right Side Menu', children=[
        #     ChildItem('Password change', url='admin:password_change'),
        #     ChildItem('Open Google', url='http://google.com', target_blank=True),
        #
        # ], align_right=True, icon='fa fa-cog'),
    )

    def ready(self):
        super(SuitConfig, self).ready()
