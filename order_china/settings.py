from .settings_base import *

SECRET_KEY = '5tvylz4rc50%tgf(_tkhe)!u6@modh)=zh88rehfd0o(9yus4='

DEBUG = True
ADMINS = [('Hung Ha', 'hahungkk@gmail.com'), ]
MANAGERS = [('Hung Ha', 'hahungkk@gmail.com'), ]

ALLOWED_HOSTS = ['*']

ROOT_URLCONF = 'order_china.urls'

TEMPLATES = [
    {
        'BACKEND': 'django.template.backends.django.DjangoTemplates',
        'DIRS': [
            os.path.join(BASE_DIR, 'webapp/static/app/'),
            os.path.join(BASE_DIR, 'system_configure/templates/')
        ],
        'APP_DIRS': True,
        'OPTIONS': {
            'context_processors': [
                'django.contrib.auth.context_processors.auth',
                'django.template.context_processors.request',
                'django.template.context_processors.debug',
                'django.template.context_processors.i18n',
                'django.template.context_processors.media',
                'django.template.context_processors.static',
                'django.template.context_processors.tz',
                'django.contrib.messages.context_processors.messages',
            ],
        },
    },
]

WSGI_APPLICATION = 'order_china.wsgi.application'
IMPORT_EXPORT_USE_TRANSACTIONS = True

DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql',
        'NAME': 'order_china',
        'USER': 'order_china',
        'PASSWORD': 'Y9KnVjUXkMhxH2F6',
        'HOST': 'postgresql',
        'PORT': 5432,
    },
}


CACHE_COUNT_TIMEOUT = 60  # seconds, not too long.
CACHE_INVALIDATE_ON_CREATE = 'whole-model'
TIME_ZONE = 'Asia/Bangkok'

PROJECT_NAME_IN_EMAIL = "order_china.vn"
PROJECT_MAIN_EMAIL_ADDRESS = 'order_china.vn <order_china.vn@gmail.com>'

EMAIL_USE_TLS = True
EMAIL_HOST = 'smtp.gmail.com'
EMAIL_HOST_USER = 'order_china.vn@gmail.com'
EMAIL_HOST_PASSWORD = 'hanoi123'
EMAIL_PORT = 587

SERVER_EMAIL = 'tranvietanh1991@gmail.com'
EMAIL_SUBJECT_PREFIX = ['order_china.vn']

USE_I18N = True

USE_L10N = True

USE_TZ = True

# My setting
XRATE_DECIMAL_PLACES = 0
MARGIN_MISSING_IN_CUSTOMER_PAYMENT = 50 * 10 ** 3  # allow mising 50K
MAIN_CURRENCY_CODE = 'VND'
ATTENDANCE_TRACK_ALLOW_ALL_IP = True
ATTENDANCE_TRACK_ALLOW_IP_LIST = ['127.0.0.1', '118.70.183.108', ]  #
ATTENDANCE_TRACK_TIME_BETWEEN_SUBMIT = 1 * 3600  #

CELERY_RESULT_BACKEND = "redis"
CELERY_REDIS_HOST = "redis"
CELERY_REDIS_PORT = 6379
CELERY_REDIS_DB = 0

CELERY_ENABLE_UTC = True
CELERY_TIMEZONE = 'Asia/Bangkok'
CELERY_BROKER_URL = 'redis://redis:6379/0'
CELERY_BEAT_SCHEDULER = 'django_celery_beat.schedulers:DatabaseScheduler'

if DEBUG:
    EMAIL_BACKEND = 'django.core.mail.backends.locmem.EmailBackend'
else:
    EMAIL_BACKEND = 'django.core.mail.backends.smtp.EmailBackend'
