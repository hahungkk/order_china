from django.conf.urls import include
from django.conf.urls.static import static
from django.contrib import admin
from django.urls import path
from django.views.generic.base import RedirectView

from orderbot.views import botview
from shop_module.api.Order_RestfulAPI import FileUploadView
from .routers import GroupedRouter
from system_configure.api import SystemConfig_RestfulAPI_route
from user_module.api import User_RestfulAPI_route
from shop_module.api import ShopModule_RestfulAPI_route
from notification_module.api import notification_route
from shop_module.views import OrderAutoCompleteView
from rest_framework_jwt.views import obtain_jwt_token, refresh_jwt_token, verify_jwt_token
from django.conf import settings

restfulRouter = GroupedRouter('api')
restfulRouter.include(SystemConfig_RestfulAPI_route.router)
restfulRouter.include(User_RestfulAPI_route.router)
restfulRouter.include(ShopModule_RestfulAPI_route.router)
restfulRouter.include(notification_route.router)


urlpatterns = [
    path('admin/', admin.site.urls),
    path('bot/', botview, name='botview'),
    path('upload/', FileUploadView.as_view(), name='upload'),
    path('ckeditor/', include('ckeditor_uploader.urls')),
    path('select2/', include('django_select2.urls')),
    path('inbox/notifications/', include('notifications.urls', namespace='notifications')),
    path('sys/', include('system_configure.urls')),
    path('user/', include('user_module.urls')),
    path('api/', include(restfulRouter.urls)),
    path('favicon.ico', RedirectView.as_view(url='/static/favicon.ico', permanent=True)),
    path('order-complete/', OrderAutoCompleteView.as_view(), name='order-complete'),
    path('api-token-auth/', obtain_jwt_token),
    path('api-token-refresh/', refresh_jwt_token),
    path('api-token-verify/', verify_jwt_token),
    path('', include('webapp.urls')),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
