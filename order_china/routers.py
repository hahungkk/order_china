from __future__ import unicode_literals
from rest_framework.routers import DefaultRouter
from rest_framework.views import APIView
from rest_framework.response import Response
from django.conf.urls import include, url
from django.urls import NoReverseMatch

from system_configure.controllers.Tool import reverse_base


class GroupedRouter(DefaultRouter):
    """
    The GroupedRouter router extends the DefaultRouter, add feature to include another
    GroupedRouter, NestedRouter or DefaultRounter to show in api root view
    """
    include_root_view = True
    include_format_suffixes = True
    root_view_name = 'api-root'

    def __init__(self, root_view_name='api-root'):
        super(self.__class__, self).__init__()
        self.includeRouterList = []
        self.root_view_name = root_view_name

    def get_api_root_view(self, api_urls=None):
        """
        Return a view to use as the API root.
        """
        api_root_dict = {}
        list_name = self.routes[0].name
        for prefix, viewset, basename in self.registry:
            api_root_dict[prefix] = basename, viewset  # list_name.format(basename=basename)
        router_self = self
        class_name = self.root_view_name.replace('-', '_').capitalize()

        class APIRoot(APIView):
            _ignore_model_permissions = True

            @staticmethod
            def get(request, *args, **kwargs):
                ret = []
                for r in router_self.includeRouterList:
                    try:
                        ret += [{
                            'name': r.root_view_name,
                            'url': reverse_base(request, r.root_view_name, absolute=True),
                        }]
                    except NoReverseMatch:
                        continue

                for pre_fix in api_root_dict:
                    base_name, view_set = api_root_dict[pre_fix]
                    for urlx, mapping, name, detail, initkwargs in router_self.get_routes(view_set):
                        correct_name = name.format(basename=base_name)
                        reverse_kwagrs = {}

                        if 'lookup' in urlx:
                            reverse_kwagrs = {'pk': 1}

                        try:
                            ret += [{
                                'name': correct_name,
                                'methods': mapping,
                                'url': reverse_base(request, correct_name, absolute=True, kwargs=reverse_kwagrs),
                            }]
                        except NoReverseMatch:
                            continue

                return Response(ret)

        APIRoot.__name__ = self.root_view_name
        return APIRoot.as_view()

    def get_urls(self):
        urlpatterns = super(self.__class__, self).get_urls()
        for r in self.includeRouterList:
            urlpatterns.append(url('^' + r.root_view_name + '/', include(r.urls)))

        return urlpatterns

    def include(self, r):
        """ Include another router to this route

        :param r: an instance of DefaultRouter
        :return:
        """
        self.includeRouterList.append(r)


class NestedRouter(DefaultRouter):
    def __init__(self, parent_router, parent_prefix, *args, **kwargs):
        """ Create a NestedRouter nested within `parent_router`
        Args:

        parent_router: Parent router. Mayb be a simple router or another nested
            router.

        parent_prefix: The url prefix within parent_router under which the
            routes from this router should be nested.

        lookup:
            The regex variable that matches an instance of the parent-resource
            will be called '<lookup>_<parent-viewset.lookup_field>'
            In the example above, lookup=domain and the parent viewset looks up
            on 'pk' so the parent lookup regex will be 'domain_pk'.
            Default: 'nested_<n>' where <n> is 1+parent_router.nest_count

        """
        self.parent_router = parent_router
        self.parent_prefix = parent_prefix
        self.nest_count = getattr(parent_router, 'nest_count', 0) + 1
        self.nest_prefix = kwargs.pop('lookup', 'nested_%i' % self.nest_count) + '_'
        super(NestedRouter, self).__init__(*args, **kwargs)
        parent_registry = [registered for registered in self.parent_router.registry if
                           registered[0] == self.parent_prefix]
        try:
            parent_registry = parent_registry[0]
            parent_prefix, parent_viewset, parent_basename = parent_registry
        except:
            raise RuntimeError('parent registered resource not found')

        nested_routes = []
        parent_lookup_regex = parent_router.get_lookup_regex(parent_viewset, self.nest_prefix)

        self.parent_regex = '{parent_prefix}/{parent_lookup_regex}/'.format(
            parent_prefix=parent_prefix,
            parent_lookup_regex=parent_lookup_regex
        )
        if hasattr(parent_router, 'parent_regex'):
            self.parent_regex = parent_router.parent_regex + self.parent_regex

        for route in self.routes:
            route_contents = route._asdict()

            # This will get passed through .format in a little bit, so we need
            # to escape it
            escaped_parent_regex = self.parent_regex.replace('{', '{{').replace('}', '}}')

            route_contents['url'] = route.url.replace('^', '^' + escaped_parent_regex)
            nested_routes.append(type(route)(**route_contents))

        self.routes = nested_routes
