from channels.layers import get_channel_layer
from asgiref.sync import async_to_sync


channel_layer = get_channel_layer()


def send_to_chanel(username, data):
    # channel_layer.group_send('default_group', {"type": "group_message", "message": data, "username": username})
    async_to_sync(channel_layer.group_send)('default_group',
                                            {"type": "group_message", "message": data, "username": username})
