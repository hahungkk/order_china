#!/bin/bash

echo "Building alodathang app ..."
sed -i.back 's/\/static\/alodathang\/bower_components/bower_components/g; s/\/static\/alodathang\/app\///g' app/index.html  \
&& grunt build --force \
&& sed -i.back 's/styles\//\/static\/alodathang\/dist\/styles\//g; s/scripts\//\/static\/alodathang\/dist\/scripts\//g;s/images\//\/static\/alodathang\/dist\/images\//g' dist/index.html  \
&& mv app/index.html.back app/index.html \
&& echo "GRUNT BUILD COMPLETE"