'use strict';

describe('Service: ProxyService', function () {

  // load the service's module
  beforeEach(module('alodathangApp'));

  // instantiate service
  var ProxyService;
  beforeEach(inject(function (_ProxyService_) {
    ProxyService = _ProxyService_;
  }));

  it('should do something', function () {
    expect(!!ProxyService).toBe(true);
  });

});
