'use strict';

/**
 * @ngdoc service
 * @name alodathangApp.UserService
 * @description
 * # UserService
 * Factory in the alodathangApp.
 */
angular.module('alodathangApp')
    .factory('UserService', ['$http', "$rootScope", '$location', function ($http, $rootScope, $location) {
        var factory = {};

        var defaultErrorCallBack = function (method_name, data, status) {
            if (status === 403) {
                alert('Error: you must login first to see documents.');
                console.log(data);
            } else {
                console.log(method_name + ": error_status=" + status + " data=" + data);
            }
        };

        // Service logic
        // ...

        // Public API here

        factory.ChangePassword = function (POST, callback, errorCallback) {
            var req = {
                method: 'PUT',
                url: '/api/user/auth/change_password/',
                headers: {
                    //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: {
                    current_password: POST.password,
                    new_password: POST.newPassword
                }
            };

            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('changePassword', data.data, data.status);
            });
        };

        factory.ChangeEmail = function (POST, callback, errorCallback) {
            var req = {
                method: 'POST',
                url: '/api/user/auth/resend_activate_email/',
                headers: {
                    //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: {
                    username: POST.username,
                    password: POST.password,
                    email: POST.new_email
                }
            };

            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('changeEmail', data.data, data.status);
            });
        };

        factory.ResetPasswordViaEmail = function (POST, callback, errorCallback) {
            var req = {
                method: 'POST',
                url: '/api/user/auth/reset_password_via_email/',
                headers: {
                    //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: {
                    email: POST.email
                }
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('resetPasswordViaEmail', data.data, data.status);
            });
        };

        factory.ViewUserProfile = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/user/myProfile/show/'
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewProfile', data.data, data.status);
            });
        };

        factory.UpdateUserProfile = function (POST, callback, errorCallback) {
            // POST.dob = $('#dob').val()+'-01-01';
            var req = {
                method: 'PATCH',
                url: '/api/user/myProfile/edit/',
                headers: {
                    //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: {
                    "full_name": POST.full_name,
                    "email": POST.email,
                    "address": POST.address,
                    "user_facebook": POST.user_facebook,
                    "phone_number": POST.phone_number,
                    "dob_year": POST.dob_year
                }
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('updateProfile', data.data, data.status);
            });
        };

        factory.ViewCustomerProfile = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/user/myProfile/show/'
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewProfile', data.data, data.status);
            });
        };

        factory.UpdateCustomerProfile = function (POST, callback, errorCallback) {
            var req = {
                method: 'PATCH',
                url: '/api/shop_module/customer/' + POST.id + "/",
                headers: {
                    //'Content-Type': 'application/x-www-form-urlencoded; charset=UTF-8'
                },
                data: POST
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('updateProfile', data.data, data.status);
            });
        };

        factory.GetPurchasesList = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/payment/'
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewPurchaseHistory', data.data, data.status);
            });
        };

        factory.GetWalletTo = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/payment/?transaction__to_balance=' + POST
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewWalletIn', data.data, data.status);
            });
        };

        factory.GetWalletFrom = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/payment/?transaction__from_balance=' + POST
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewWalletOut', data.data, data.status);
            });
        };

        factory.GetBalance = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/balance/'
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewPurchaseHistory', data.data, data.status);
            });
        };

        factory.GetOrders = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/order/?page_size=1000'
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewPurchaseHistory', data.data, data.status);
            });
        };

        factory.GetOrderByID = function (POST, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/order/' + POST + '/'
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('viewPurchaseHistory', data.data, data.status);
            });
        };

        return factory;
    }]);
