'use strict';

/**
 * @ngdoc service
 * @name alodathangApp.CartService
 * @description
 * # CartService
 * Factory in the alodathangApp.
 */
angular.module('alodathangApp')
    .factory('CartService', ['$http', '$rootScope', function ($http, $rootScope) {
        var factory = {};
        factory.AddCartItem = function (POST, callback, errorCallback) {
            var req = {
                method: 'POST',
                url: '/api/shop_module/cart/',
                headers: {},
                data: POST
            };
            $http(req).then(function (data) {
                $rootScope.cart_count++;
                callback(data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else console.log(data);
            });
        };

        factory.GetCartItems = function (GET, callback, errorCallback) {
            var page_size = 1000;
            if (GET != null && GET.page_size !== undefined && GET.page_size != null) page_size = GET.page_size;
            var req = {
                method: 'GET',
                url: '/api/shop_module/cart/?page_size=' + page_size,
                headers: {}
            };

            $http(req).then(function (data) {
                $rootScope.cart_count = data.data.count; //save cart_count
                for (var i in data.data.results) {
                    data.data.results[i].price = parseFloat(data.data.results[i].price);
                    data.data.results[i].shipping = parseFloat(data.data.results[i].shipping);
                    data.data.results[i].weight = parseFloat(data.data.results[i].weight)
                }
                if (callback) callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else console.log(data);
            });
        };

        factory.RemoveCartItem = function (item_id, callback, errorCallback) {
            var req = {
                method: 'DELETE',
                url: '/api/shop_module/cart/' + item_id + "/",
                headers: {}
            };

            $http(req).then(function (data) {
                $rootScope.cart_count--;
                callback(data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else console.log(data);
            });
        };

        factory.UpdateCartItem = function (PUT, callback, errorCallback) {
            var req = {
                method: 'PUT',
                url: '/api/shop_module/cart/' + PUT.id + "/",
                data: PUT,
                headers: {}
            };

            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else console.log(data.data);
            });
        };

        factory.SendOrder = function (POST, callback, errorCallback) {
            var req = {
                method: 'POST',
                url: '/api/shop_module/order/',
                headers: {},
                data: POST
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else console.log(data.data);
            });
        };


        return factory;
    }]);
