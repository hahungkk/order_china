'use strict';

/**
 * @ngdoc service
 * @name alodathangApp.AuthenticationService
 * @description
 * # AuthenticationService
 * Factory in the alodathangApp.
 */
angular.module('alodathangApp')
    .factory('AuthenticationService', ['$http', "$rootScope", "$cookies", function ($http, $rootScope, $cookies) {
        var factory = {};
        var DEFAULT_DOMAIN = "order_china.vn";
        // Service logic
        // ...
        var defaultErrorCallBack = function (method_name, data, status) {
            if (status === 403) {
                alert('Error: you must login first to see documents.');
                console.log(data);
            } else {
                console.log(method_name + ": error_status=" + status + " data=" + data);
            }
        };

        // Public API here

        factory.CreateCredential = function (data, password) {
            if ($cookies.get("prxydomain") === null || $cookies.get("prxydomain") === undefined)
                $cookies.put("prxydomain", DEFAULT_DOMAIN);
            $cookies.putObject('globals', data);
        };

        factory.RemoveCredential = function () {
            $rootScope.globals = null;
            $cookies.remove('globals');
        };

        factory.Login = function (POST, callback, errorCallback) {
            var req = {
                method: 'POST',
                url: '/api/user/auth/login/',
                headers: {},
                data: {
                    username: POST.username,
                    password: POST.password
                }
            };

            $http(req).then(function (data) {
                factory.CreateCredential(data.data);
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('login', data.data, data.status);
            });
        };

        factory.Logout = function (POST, callback, errorCallback) {
            var req = {
                method: 'POST',
                url: '/api/user/auth/logout/'
            };

            $http(req).then(function (data) {
                factory.RemoveCredential();
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('logout', data.data, data.status);
            });
        };

        factory.SignUp = function (POST, callback, errorCallback) {
            var req = {
                method: 'POST',
                url: '/api/user/auth/signup/',
                data: {
                    username: POST.username,
                    password: POST.password,
                    email: POST.email,
                    referer: POST.referer
                }
            };

            $http(req).then(function (data) {
                factory.CreateCredential(data.data);
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('signup', data.data, data.status);
            });
        };

        return factory;
    }])
;
