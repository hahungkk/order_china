'use strict';

/**
 * @ngdoc service
 * @name alodathangApp.ProxyService
 * @description
 * # ProxyService
 * Factory in the alodathangApp.
 */
angular.module('alodathangApp')
    .factory('ProxyService', ['$http', function ($http) {
        var factory = {};
        // Service logic
        // ...

        // Public API here
        factory.getHTML = function (url, prxydomain) {
            var headers = {};
            if (prxydomain != null) headers.prxydomain = prxydomain;
            var req = {
                method: 'GET',
                url: url,
                headers: headers
            };
            return $http(req);
        };

        return factory;
    }]);
