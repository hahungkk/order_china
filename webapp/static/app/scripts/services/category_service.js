/**
 * Created by TVA on 11/19/15.
 */

'use strict';

/**
 * @ngdoc service
 * @name alodathangApp.AuthenticationService
 * @description
 * # AuthenticationService
 * Factory in the alodathangApp.
 */
angular.module('alodathangApp')
    .factory('CategoryService', function ($http, $cookies, $rootScope, $resource) {
        var factory = {};
        // Service logic
        // ...
        var defaultErrorCallBack = function (method_name, data, status) {
            if (status === 403) {
                alert('Error: you must login first to see documents.');
                console.log(data);
            } else {
                console.log(method_name + ": error_status=" + status + " data=" + data);
            }
        };

        // Public API here

        factory.GetCategoryList = function (GET, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/category/?page_size=20',
                headers: {},
                params: GET
            };
            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else console.log(data.data);
            });
        };

        factory.GetCategoryDetail = function (GET, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/category/' + GET.pk + '/?page_size=50&child_level=' + GET.child_level,
                headers: {},
                params: GET
            };
            $http(req).then(function (data) {
                try {
                    data.metadata = JSON.parse(data.metadata)
                } catch (err) {
                    data.metadata = null
                }
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else console.log(data.data);
            });
        };

        factory.transformDRFResponse = function (raw_data, headersGetter) {
            var data = angular.fromJson(raw_data);
            this.$metadata = {
                "count": data.count,
                "next": data.next,
                "previous": data.previous,
            };
            return data.results;
        };

        factory.interceptorDRFResponse = function (response) {
            response.resource.$metadata = this.$metadata;
            return response.resource;
        };

        function convertMetadataJson(data) {
            try {
                data.metadata = angular.fromJson(data.metadata)
            } catch (err) {
                data.metadata = {}
            }
        }

        function recursiveConvertMetadataJsonWithChildren(data) {
            convertMetadataJson(data);
            if (data.children && data.children.length > 0) {
                for (var i in data.children) {
                    recursiveConvertMetadataJsonWithChildren(data.children[i]);
                }
            }
        }

        function convertMetadataYaml(data) {
            try {
                data.metadata = yaml.load(data.metadata);
                // console.log("yaml.load",data.metadata);
            } catch (err) {
                data.metadata = {}
            }
        }

        function recursiveConvertMetadataYamlWithChildren(data) {
            convertMetadataYaml(data);
            if (data.children && data.children.length > 0) {
                for (var i in data.children) {
                    recursiveConvertMetadataYamlWithChildren(data.children[i]);
                }
            }
        }

        factory.transformCategoryResponse = function (raw_data, headersGetter) {
            var data = angular.fromJson(raw_data);
            recursiveConvertMetadataYamlWithChildren(data);
            return data;
        };

        factory.Category = $resource('/api/shop_module/category/:pk/', {pk: '@id'}, {
            get: {
                method: 'GET',
                url: '/api/shop_module/category/:pk/',
                isArray: false,
                params: {pk: '@code'}, //value of :pk will be replaced by object.code
                transformResponse: factory.transformCategoryResponse
            },
            query: {
                method: 'GET',
                url: '/api/shop_module/category/',
                isArray: true,
                transformResponse: factory.transformDRFResponse,
                interceptor: {
                    response: factory.interceptorDRFResponse
                }
            },
        });


        return factory;
    })
;
