'use strict';

/**
 * @ngdoc service
 * @name alodathangApp.NotificationService
 * @description
 * # NotificationService
 * Factory in the alodathangApp.
 */
angular.module('alodathangApp')
    .factory('NotificationService', ['$http', "$rootScope", function ($http, $rootScope) {
        var factory = {};

        var defaultErrorCallBack = function (method_name, data, status) {
            if (status === 403) {
                alert('Error: you must login first to see documents.');
                console.log(data);
            } else {
                console.log(method_name + ": error_status=" + status + " data=" + data);
            }
        };

        factory.ListNotifications = function (GET, callback, errorCallback) {
            var page_params = "";

            if (GET != null) {
                page_params = "?page=" + GET
            }
            var _url = '/api/notification_module/notification/' + page_params;
            var req = {
                method: 'GET',
                url: _url
            };

            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('ListNotifications', data.data, data.status);
            });
        };

        factory.ListUnreadNotifications = function (GET, callback, errorCallback) {

            var _url = '/inbox/notifications/api/unread_list/';
            var req = {
                method: 'GET',
                url: _url
            };

            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('ListUnreadNotifications', data.data, data.status);
            });
        };

        factory.MarkAsReadNotification = function (PATCH, callback, errorCallback) {
            var req = {
                method: 'PATCH',
                url: '/api/notification_module/notification/' + PATCH.id + '/mark_as_read/',
            };

            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('MarkAsUnreadNotification', data.data, data.status);
            });
        };

        factory.MarkAllAsReadNotification = function (PUT, callback, errorCallback) {
            var req = {
                method: 'PUT',
                url: '/api/notification_module/notification/mark_all_as_read/',
            };

            $http(req).then(function (data) {
                callback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
                else defaultErrorCallBack('MarkAllAsUnreadNotification', data.data, data.status);
            });
        };


        return factory;
    }]);
