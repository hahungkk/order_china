'use strict';

/**
 * @ngdoc service
 * @name alodathangApp.ShareService
 * @description
 * # ShareService
 * Factory in the alodathangApp.
 */
angular.module('alodathangApp')
    .factory('ShareService', ['$http', '$cookies', '$rootScope', function ($http, $cookies, $rootScope) {
        var factory = {};
        // Service logic
        // ...

        // Private function

        function extractOptionSelected(node, domain) {
            var selected = {};
            if (domain == "www.amazon.com") {
                $(node).find(".a-section .a-spacing-small").each(function (index, el) {
                    var optionRoot = $(el).find(".a-row>label");
                    var optionName = $(optionRoot).html().replace(/(\r\n|\n|\r|:)/gm, "");
                    var optionValue = $(optionRoot).next().html().replace(/(\r\n|\n|\r|:)/gm, "");
                    selected[optionName] = optionValue;
                });
            } else if (domain == "www.ebay.com") {
                $(node).find("form .vi-msku-cntr").each(function (index, el) {
                    var optionName = $(el).find(".vi-bbox-dspn label").html().replace(/(\r\n|\n|\r|:)/gm, "");
                    var optionValue = $(el).find("select").find("option:selected").text().replace(/(\r\n|\n|\r|:)/gm, "");
                    selected[optionName] = optionValue;
                });
            }
            return selected;
        }

        // TODO: load data from systemconfig
        // function get_modal_data(key) {
        //     $http.get()
        // }

        function extractOptionMetadata(node, domain) {
            var metadata = {};
            if (domain == "www.amazon.com") {
                $(node).find(".a-section .a-spacing-small").each(function (index, el) {
                    var optionRoot = $(el).find(".a-row>label");
                    var optionName = $(optionRoot).html().replace(/(\r\n|\n|\r|:)/gm, "");
                    metadata[optionName] = [];
                    $(optionRoot).parent().next().children('li').each(function (index, el) {
                        metadata[optionName].push(el.textContent.replace(/(\r\n|\n|\r|:)/g, ""));
                    });
                });
            } else if (domain == "www.ebay.com") {
                $(node).find("form .vi-msku-cntr").each(function (index, el) {
                    var optionName = $(el).find(".vi-bbox-dspn label").html().replace(/(\r\n|\n|\r|:)/gm, "");
                    metadata[optionName] = [];
                    var optionValue = $(el).find("select").children("option").each(function (index, el) {
                        if (el.textContent.replace(/(\r\n|\n|\r|:)/gm, "") != "- Select -")
                            metadata[optionName].push(el.textContent.replace(/(\r\n|\n|\r|:)/gm, ""))
                    });
                    ;
                });
            }
            return metadata;
        }


        // Public API here

        factory.GetVNDPrice = function (prod) {
            var shipping = 0;
            var rawPrice = 0;
            var serviceCharge = 0;
            // var externalShipping = 0; //Shipping fee outside vietnam
            if (prod.currency === "USD") {
                rawPrice = factory.convertUSDToVND(prod.price) * prod.quantity;
                // externalShipping = factory.convertUSDToVND(prod.shipping) * prod.weight * prod.quantity;
                serviceCharge = factory.convertUSDToVND(prod.service_charge) || 0;
                shipping = factory.convertUSDToVND(prod.shipping) || 0;
            } else if (prod.currency === "CNY") {
                rawPrice = factory.convertCNYToVND(prod.price) * prod.quantity;
                // externalShipping = factory.convertCNYToVND(prod.shipping) * prod.weight * prod.quantity;
                serviceCharge = factory.convertCNYToVND(prod.service_charge) || 0;
                shipping = factory.convertCNYToVND(prod.shipping) || 0;
            } else if (prod.currency === "VND") {
                rawPrice = prod.price * prod.quantity;
                // externalShipping = prod.shipping * prod.weight * prod.quantity;
                serviceCharge = 10000; //fixed 10K
                shipping = prod.shipping;
            }
            // var internalShipping = prod.quantity * prod.weight * 25000;
            // if (prod.insurance) serviceCharge += rawPrice*0.03; // insurance charge = 3% of item cost
            var totalPrice = rawPrice + serviceCharge + shipping;
            return {
                totalPrice: totalPrice,
                rawPrice: rawPrice,
                shipping: shipping,
                serviceCharge: totalPrice - rawPrice + shipping
            };
        };


        factory.convertUSDToVND = function (oldPrice) {
            var USDRate = $.grep($cookies.getObject("currency"), function (e) {
                return e.code.toUpperCase() === "USD";
            })[0];

            return oldPrice * USDRate.exchange_rate;
        };

        factory.convertCNYToVND = function (oldPrice) {
            var CNYRate = $.grep($cookies.getObject("currency"), function (e) {
                return e.code.toUpperCase() === "CNY";
            })[0];
            return oldPrice * CNYRate.exchange_rate;
        };

        factory.getCurrencyRate = function (callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '/api/shop_module/currency/'
            };

            $http(req).then(function (data) {
                for (var i in data.data.results) {
                    data.data.results[i].exchange_rate = parseFloat(data.data.results[i].exchange_rate)
                }
                var cDate = new Date();
                cDate.setDate(cDate.getDate() + 1);
                $cookies.putObject("currency", data.data.results, {
                    'expries': cDate
                });
                $rootScope.rates = $cookies.getObject('currency');
                callback(data.data.results);
            }, function (data) {
                alert("Không thể lấy được tỉ giá mới nhất, sử dụng tỉ giá cũ có thể không chính xác");
            });
        };

        factory.translateText = function (text, callback, errorCallback) {
            var req = {
                method: 'GET',
                url: '//api.mymemory.translated.net/get',
                params: {
                    q: text,
                    langpair: 'vi|en'
                }
            };

            $http(req).then(function (data) {
                var result = data.responseData.translatedText;
                if (result) {
                    callback(result);
                } else {
                    callback(null);
                }
            }, function (data) {
                alert("Lỗi ko thể dịch từ khoá bằng dịch vụ");
                errorCallback();
            });
        };

        factory.changeDomain = function (domain) {
            $rootScope.prxydomain = domain;
            $cookies.put("prxydomain", domain);
        };

        factory.getDomainOfSite = function (site) {
            var prxydomain = 'www.amazon.com';
            if (site == 'ebay') {
                prxydomain = 'www.ebay.com'
            }
            return prxydomain
        };

        factory.getSiteOfDomain = function (domain) {
            var site = 'amazon';
            if (domain == 'www.ebay.com') {
                site = 'ebay';
            }
            return site;
        };

        factory.classifyURL = function (url, domain) {
            if (domain == 'www.amazon.com' || domain == null) {
                if (url.match(new RegExp('/dp/\\w+')) || url.match(new RegExp('/gp/slredirect/'))) {
                    return "product"
                } else if (url.match(new RegExp('[sb]\\?ie|s\\?rh|s/ref|gp/search/|gp/most-gifted/'))) { // handle query
                    return "search";
                } else {
                }
            } else if (domain == "www.ebay.com") {
                if (url.match(new RegExp('/itm/'))) {
                    return "product"
                } else if (url.match(new RegExp('/sch/'))) { // handle query
                    return "search";
                } else {
                }
            }
        };

        factory.extractProductData = function (html, domain, url) {
            var obj = {};

            if (domain == 'www.amazon.com') {
                obj.shopping_domain = domain;
                obj.vendor = $(html).find("a#brand").html();
                ;
                obj.detail_url = 'http://' + domain + '/' + url;
                obj.name = $(html).find("#productTitle").text();
                if ($(html).find("#priceblock_ourprice").length > 0) {
                    obj.price = parseFloat($(html).find("#priceblock_ourprice").html().replace("$", "").replace(",", ""));
                } else if ($(html).find("#priceblock_saleprice").length > 0) { // saled item
                    obj.price = parseFloat($(html).find("#priceblock_saleprice").html().replace("$", "").replace(",", ""));
                } else if ($(html).find("#olp_feature_div").length > 0) { // used item
                    obj.price = parseFloat($(html).find("#olp_feature_div span.a-color-price").html().replace("$", "").replace(",", ""));
                }
                obj.short_description = $(html).find("#feature-bullets ul").html().replace(/(\r\n\t|\n\t|\r)/gm, "");
                if (url.match(/dp\/\w+\/?/g))
                    obj.sku = url.match(/dp\/\w+\/?/g)[0].replace("dp/", "").replace("/", "");
                else {
                    obj.sku = $($(html).find("#detail-bullets table b:contains(ASIN: )")
                        .parent()[0])
                        .children()
                        .remove()
                        .end()
                        .html();
                    console.log($($(html).find("#detail-bullets table b:contains(ASIN: )")));
                }
                // obj.image_url = $(html).find("#imgTagWrapperId>img").attr('src');
                obj.image_url = Object.keys($(html).find("#imgTagWrapperId>img").data('aDynamicImage'))[0];
                if (($(html).find("#detail-bullets table b:contains(Shipping Weight:)").length > 0)) {
                    obj.weight = Math.round(parseFloat($($(html).find("#detail-bullets table b:contains(Shipping Weight:)")
                        .parent()[0])
                        .children()
                        .remove()
                        .end()
                        .html()
                        .match(/\d+[.]?\d*/)[0]) * 0.454);
                } else
                    obj.weight = 0;
                obj.shipping = 0;
                obj.quantity = 1;
                obj.options_selected = extractOptionSelected($(html).find("#twisterContainer"), domain);
                obj.options_metadata = extractOptionMetadata($(html).find("#twisterContainer"), domain);
                obj.category_list = $(html).find("#wayfinding-breadcrumbs_feature_div ul li").last().text().replace(/(\r\n|\n|\r)/gm, "");
                obj.fragile = false;
                obj.insurance = false;
                obj.note = "";
                obj.currency = "USD";
            } else if (domain == "www.ebay.com") {
                obj.shopping_domain = domain;
                obj.vendor = $(html).find("#RightSummaryPanel span.mbg-nw").html();
                ;
                obj.detail_url = 'http://' + domain + '/' + url;
                obj.name = $(html).find("#itemTitle").children().remove().end().html();
                if ($(html).find("#prcIsum").length > 0) {
                    obj.price = parseFloat($(html).find("#prcIsum").children().remove().end().html().replace("US $", "").replace(/,/g, ""));
                } else {
                    obj.price = parseFloat($(html).find("#prcIsum_bidPrice").children().remove().end().html().replace("US $", "").replace(/,/g, ""));
                }
                obj.short_description = $(html).find(".itemAttr table").html().replace(/(\r\n\t|\n\t|\r)/gm, "");
                obj.sku = url.match(/\/\w+\?/g)[0].replace("/", "").replace("?", "");
                obj.image_url = $(html).find("#icImg").attr('src');
                obj.weight = 0; //TODO: get weight
                obj.shipping = 0;
                obj.quantity = 1;
                obj.options_selected = extractOptionSelected($(html).find("#mainContent"), domain);
                obj.options_metadata = extractOptionMetadata($(html).find("#mainContent"), domain);
                obj.category_list = $(html).find(".vi-bc-topM table li").last().text().replace(/(\r\n|\n|\r)/gm, "");
                obj.fragile = false;
                obj.insurance = false;
                obj.note = "";

                obj.currency = "USD";
            }
            return obj;
        }

        factory.GetStaticHTML = function (key, successCallback, errorCallback) {
            var req = {
                method: 'GET',
                url: "/api/system_configure/template/" + key + "/",
            };
            $http(req).then(function (data) {
                successCallback(data.data);
            }, function (data) {
                if (errorCallback) errorCallback(data.data, data.status, data.headers, data.config);
            });
        };

        return factory;
    }]);
