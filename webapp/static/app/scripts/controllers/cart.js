'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:CartCtrl
 * @description
 * # CartCtrl
 * Controller of the alodathangApp
 */
angular.module('alodathangApp').controller('CartCtrl',
    function ($scope, CartService, $sce, $location, ShareService,
              UserService, AuthenticationService, $rootScope, $cookies, $q,
              swangular, $http) {
        $http.get('/api/system_configure/order-min-price/').then(function (data) {
            $scope.minPrice = parseFloat(data.data.value);
        });
        $scope.disableSend = false;
        var confirmOptions = {
            title: "Thông Báo",
            showCancelButton: true,
            confirmButtonText: 'Ok',
            cancelButtonText: 'Hủy'
        };
        $scope.cartItems = [];
        $scope.register = false;
        $scope.checkAll = true;
        $scope.error = {};

        $scope.getCustomerCart = function () {
            CartService.GetCartItems(null,
                function (data) {
                    $scope.cartItems = data.results;
                    // var checkAll = $scope.cartItems.filter(function (item) {
                    //     return item.checked = false;
                    // });
                    // $scope.checkAll = checkAll.length === 0;
                },
                function (error) {
                console.log(error);
                    toastr.error("Không thể lấy dữ liệu giỏ hàng, hãy thử lại!", "Có lỗi xảy ra");
                });
        };

        $rootScope.$on('wsMessage', function (event, args) {
            var data = JSON.parse(args.data);
            if (data.message === 'CartItemAdded' && data.username === $rootScope.globals.username) {
                $scope.getCustomerCart();
            }
        });

        $scope.getTotalPrice = function () {
            var totalOrderPrice = 0;
            angular.forEach($scope.cartItems, function (item, index) {
                if (item.checked) {
                    totalOrderPrice += ShareService.GetVNDPrice(item).totalPrice;
                }
            });
            return totalOrderPrice;
        };

        $scope.removeCartItem = function (item) {
            swangular.confirm("Bạn có chắc muốn xóa món hàng này khỏi giỏ hàng?",
                confirmOptions).then(function (yes) {
                if (yes.value !== undefined) {
                    CartService.RemoveCartItem(item.id,
                        function (data) {
                            toastr.success("Xóa đơn hàng thành công");
                            $scope.getCustomerCart();
                        }, function (error) {
                            console.log(error);
                            toastr.error("Không thể xóa đơn hàng", "Có lỗi xảy ra");
                        });
                }
            });
        };

        $scope.removeAll = function () {
            swangular.confirm("Bạn có chắc muốn xóa tất cả các món hàng?",
                confirmOptions).then(function (yes) {
                if (yes.value !== undefined) {
                    var promises = [];
                    angular.forEach($scope.cartItems, function (item) {
                        var deferred = $q.defer();
                        if (item.checked) {
                            CartService.RemoveCartItem(item.id, function (data) {
                                deferred.resolve(data);
                            }, function (err) {
                                console.log(err);
                                deferred.reject();
                            });
                            promises.push(deferred);
                        }
                    });
                    $q.all(promises).then(
                        window.location.reload()
                    );
                }
            });
        };

        $scope.signInUser = function () {
            var post = {
                username: $scope.loginForm.username,
                password: $scope.loginForm.password
            };
            if (!$scope.register) {
                AuthenticationService.Login(post, function (data) {
                        AuthenticationService.CreateCredential(data, post.password);
                        $rootScope.globals = data;
                        $rootScope.prxydomain = $cookies.get("prxydomain");
                        $location.path("/cart").search({});
                        $scope.getDefaultOrderInfo();
                    },
                    function (error) {
                        toastr.error("Không thể đăng nhập, vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra")
                        $scope.error = error.error;
                    });

            } else {
                post['email'] = $scope.signUpForm.email;
                AuthenticationService.SignUp(post, function (data) {
                        AuthenticationService.Login(post, function (data) {
                                AuthenticationService.CreateCredential(data, post.password);
                                $rootScope.globals = data;
                                $rootScope.prxydomain = $cookies.get("prxydomain");
                                $location.path("/cart").search({});
                                $scope.getDefaultOrderInfo();
                            },
                            function (error) {
                                toastr.error("Không thể đăng nhập, vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                                $scope.error = error.error;
                            });


                    },
                    function (error) {
                        toastr.error("Vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                        $scope.error = error.error;
                    });
            }
        };

        $scope.doNextTab = function () {
            if (!$scope.order.facebook_name) {
                toastr.error("Vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                $scope.error = {'facebook_name': ['Trường này không được bỏ trống !']};
                return false;
            }
            $scope.error = {};
            doNextTab();
        };

        $scope.sendOrder = function () {
            $scope.disableSend = true;
            var params = $scope.order;
            var totalPrice = $scope.getTotalPrice();
            if (totalPrice < $scope.minPrice) {
                swangular.swal('Thông báo', 'Đơn hàng trên 100.000vnd mới đủ điều kiện để đặt hàng', 'error')
                $scope.disableSend = false;
            } else {
                CartService.SendOrder(params, function (data) {
                    toastr.success("Gửi đơn hàng thành công");
                    $scope.getCustomerCart();
                    doNextTab();
                    $scope.disableSend = false;
                    $scope.totalOrderAmount = 0;
                    angular.forEach(data.orderitem_set, function (item, index) {
                        $scope.totalOrderAmount += ShareService.GetVNDPrice(item).totalPrice;
                    });
                    $scope.prepaid_amount = $scope.totalOrderAmount * (data.prepaid_percent / 100);
                    $scope.sentOrder = data;
                }, function (error) {
                    $scope.error = error.error;
                    console.log($scope.error);
                    toastr.error("Không thể gửi order, hãy thử lại!", "Có lỗi xảy ra");
                });
            }

        };

        $scope.updateCart = function (moveNext) {
            var error = false;
            angular.forEach($scope.cartItems, function (item, index) {
                // check if user want to keep order item
                CartService.UpdateCartItem(item, function () {
                    if (index === $scope.cartItems.length - 1) {
                        $scope.getCustomerCart();
                        if (!error) {
                            if (!moveNext) {
                                toastr.success("Cập nhật giỏ hàng thành công, hãy tiếp tục mua sắm!");
                            } else {
                                if (!$rootScope.globals)
                                    toastr.info("Bạn chưa đăng nhập, hãy đăng nhập hoặc đăng ký tài khoản để có thể đặt hàng");
                                doNextTab();
                            }
                        }
                    }
                }, function (error) {
                    toastr.error("Kiểm tra lại thông tin giỏ hàng", "Có lỗi xảy ra");
                    error = true;
                });
            });
        };

        $scope.getDefaultOrderInfo = function () {
            UserService.ViewCustomerProfile(null, function (data) {
                    $scope.order = data || {};
                    $scope.order.receiver_name = data.full_name;
                    $scope.order.receiver_phone = data.phone_number;
                    $scope.order.facebook_name = data.user_facebook;
                    $scope.order.delivery_address = data.address;
                    $scope.order.prepaid_percent = "70";
                    $scope.order.delivery_method = "0";
                    $scope.order.status = 0;
                },
                function (error) {
                    console.log(error);
                });

        };

        $scope.toggleChecked = function () {
            angular.forEach($scope.cartItems, function (item) {
                item.checked = $scope.checkAll;
            })
        };

        $scope.getCustomerCart();
        $scope.getDefaultOrderInfo();

        $(document).ready(function () {

            //Wizard
            $('a[data-toggle="tab"]').on('show.bs.tab', function (e) {

                var $target = $(e.target);

                if ($target.parent().hasClass('disabled')) {
                    return false;
                }
            });

            $(".next-step").click(function (e) {
                var $active = $('.wizard .nav-tabs li.active');
                $active.next().removeClass('disabled');
                nextTab($active);
            });

            $(".prev-step").click(function (e) {
                var $active = $('.wizard .nav-tabs li.active');
                prevTab($active);

            });


        });

        function doNextTab() {
            var $active = $('.wizard .nav-tabs li.active');
            $active.next().removeClass('disabled');
            nextTab($active);
        }

        function nextTab(elem) {
            $(elem).next().find('a[data-toggle="tab"]').click();
        }

        function prevTab(elem) {
            $(elem).prev().find('a[data-toggle="tab"]').click();
        }


        $(function () {

            $('#login-form-link').click(function (e) {
                $("#login-form").delay(100).fadeIn(100);
                $("#register-form").fadeOut(100);
                $('#register-form-link').removeClass('active');
                $(this).addClass('active');
                $scope.$apply(function () {
                    $scope.register = false;
                });

                e.preventDefault();
            });
            $('#register-form-link').click(function (e) {
                $("#register-form").delay(100).fadeIn(100);
                $("#login-form").fadeOut(100);
                $('#login-form-link').removeClass('active');
                $(this).addClass('active');
                $scope.$apply(function () {
                    $scope.register = true;
                });
                e.preventDefault();
            });


        });

    });
