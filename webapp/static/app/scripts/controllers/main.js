'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('PriceController', function ($scope, ShareService) {
        $scope.price = 0.0;
        $scope.pricePerItem = 0.0;
        $scope.weight = 0.0;
        $scope.total = 0.0;
        $scope.quantity = 0;
        $scope.service_charge = 0.0;
        $scope.insurance = false;
        $scope.shippingToVN = 0.0;
        $scope.rate = 0;
        ShareService.getCurrencyRate(function (data) {
            angular.forEach(data, function (value, key) {
                if (value.code === 'CNY') {
                    $scope.rate = value.exchange_rate;
                }
            });
        });

        $scope.calculate = function () {
            if($scope.weight <= 0.5){
                $scope.shippingToVN = 12500;
            }
            else if(0.5 <= $scope.weight < 20){
                $scope.shippingToVN = 25000 * $scope.weight;
            } else if(20 < $scope.weight < 50){
                $scope.shippingToVN = 20000 * $scope.weight;
            } else if(50 <= $scope.weight < 100){
                $scope.shippingToVN = 19000 * $scope.weight;
            } else if(100 <= $scope.weight < 300){
                $scope.shippingToVN = 18000 * $scope.weight;
            } else if(300 < $scope.weight < 600) {
                $scope.shippingToVN = 16000 * $scope.weight;
            } else if($scope.weight >= 600) {
                $scope.shippingToVN = 14000 * $scope.weight;
            }
            $scope.service_charge = $scope.calculate_service_charge() * $scope.rate;
            $scope.total = ($scope.price*$scope.quantity*$scope.rate) + $scope.shippingToVN + $scope.service_charge;
            $scope.pricePerItem = $scope.total/$scope.quantity;
        };

        $scope.calculate_service_charge = function () {
            if ($scope.quantity >= 50) {
                return 0
            } else if ($scope.price >= 350) {
                return $scope.price * 0.05;
            } else if ($scope.price < 12) {
                return $scope.price * 0.1;
            } else if (12 <= $scope.price < 350) {
                var service_charge_quantity = 1.5;
                var total_item_cost = $scope.price * $scope.quantity * $scope.rate;
                // big hand here because missing context
                if (1000000 <= total_item_cost <= 10000000) {
                    service_charge_quantity = 1.2;
                } else if (10000000 <= total_item_cost <= 20000000) {
                    service_charge_quantity = 1.0;
                } else if (20000000 <= total_item_cost <= 60000000) {
                    service_charge_quantity = 0.5;
                } else if (total_item_cost > 60000000) {
                    return 0
                }
                return $scope.quantity * service_charge_quantity
            }
        }
})
    .controller('ManualAddController', function ($scope, $http) {
        $scope.loading = false;
        $scope.options = {
            "color": "",
            "size": ""
        };
        $scope.item = {
            "shopping_domain": "",
            "vendor": "",
            "detail_url": "",
            "name": "",
            "sku": "",
            "image_url": "",
            "short_description": "",
            "price": 0,
            "shipping": 0,
            "weight": 0,
            "quantity": 1,
            "options_selected": "",
            "options_metadata": "",
            "category_list": "",
            "fragile": false,
            "insurance": false,
            "note": "",
            "html": "",
            "currency": "CNY",
            "http_referer": "http://alo68.vn/"
        };
        $scope.add = function () {
            $('#spinner').show();
            $scope.item.options_selected = JSON.stringify($scope.options);
            $http.post('/api/shop_module/cart-manual/', $scope.item).then(function (data) {
                window.location = '/cart';
            }, function (error) {
                $('#spinner').hide();
                console.log(error.data.detail);
                if(data.status === 403){
                    toastr.error('Bạn chưa đăng nhập! Vui lòng đăng nhập trước khi thêm.')
                } else {
                    toastr.error('Có lỗi xảy ra! Vui lòng lên hệ trực tiếp với order_china.')
                }
            })
        }
})
    .controller('MainCtrl', function($scope, $http, $sce, $rootScope, $location, $cookies, ShareService, CategoryService) {
        $scope.doSearch = function() {
            $location.path('/product/search').search({
                'q': $scope.keyword,
                'l': $rootScope.searchLang
            });
        };
        $scope.baogias = [];
        $scope.disPlay = 'none';
        $scope.changeDomain = function(domain) {
            ShareService.changeDomain(domain);
        };

        $scope.toggleSearchLang = function() {
            if ($rootScope.searchLang == "en") {
                $rootScope.searchLang = "vi";
                $cookies.put('searchLang', "vi");
            } else {
                $rootScope.searchLang = "en";
                $cookies.put('searchLang', "en");
            }
        };

        $scope.checkActiveSubPage = function (value) {
            var path = $location.path();
            return path == value;
        };

        ShareService.getCurrencyRate(function (data) {
            angular.forEach(data, function (value, key) {
                if (value.code === 'CNY') {
                    $scope.rate = value.exchange_rate + " " + value.code + "/VNĐ";
                }
            });
        });

        $scope.carousel = {
            options: {
                nav: true,
                items: 4,
                margin: 10,
                navText: ["<span class='label label-info'><i class='glyphicon-chevron-left glyphicon'></i></span>",
                    "<span class='label label-info'><i class='glyphicon-chevron-right glyphicon'></i></span>"
                ],
                rewind: true,
                autoplay: true
            }
        };

        $scope.initCarousels = function(data) {
            $scope.carouselVisibility = []
            angular.forEach(data.children, function(granpa, granpaIndex){
                var row = [];
                angular.forEach(granpa.children, function(dat,dadIndex){
                    row.push((0==dadIndex));
                });
                $scope.carouselVisibility.push(row) ;
            });
            console.log($scope.carouselVisibility);
        };

        $scope.onHoverMenuItem = function(granpaIndex, dadIndex) {
            angular.forEach($scope.carouselVisibility[granpaIndex], function(dad,index) {
                $scope.carouselVisibility[granpaIndex][index] = (dadIndex == index);
            });
        };

        $scope.modal = angular.element('#myModal');

        $scope.close = function(){
            $scope.disPlay = "none";
        };

        $http.get('/api/system_configure/template/thong-bao-main/').then(function (data) {
            // console.log(data);
            $scope.value = data.data.body;
            $scope.disPlay = "block";
        });

    })
    .directive("owlCarousel", function() {
        return {
            restrict: 'E',
            transclude: false,
            link: function(scope) {
                scope.initCarousel = function(element) {
                    // provide any default options you want
                    var defaultOptions = {};
                    var customOptions = scope.$eval($(element).attr('data-options'));
                    // combine the two options objects
                    for (var key in customOptions) {
                        defaultOptions[key] = customOptions[key];
                    }
                    // init carousel
                    $(element).owlCarousel(defaultOptions);
                };
            }
        };
    })
    .directive('owlCarouselItem', [function() {
        return {
            restrict: 'A',
            transclude: false,
            link: function(scope, element) {
                // wait for the last item in the ng-repeat then call init
                if (scope.$last) {
                    scope.initCarousel(element.parent());
                }
            }
        };
    }]);

