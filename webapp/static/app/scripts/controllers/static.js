/**
 * Created by QQ on 12/16/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:NavBarCtrl
 * @description
 *
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('StaticCtrl', function($scope, ShareService, $sce, $routeParams, $location) {
        $scope.key = $routeParams.page_name;

        $scope.getStaticHTML = function(key) {
            ShareService.GetStaticHTML(key, function(data) {
                $scope.title = data.description;
                $scope.html = $sce.trustAsHtml(data.body);
            }, function(error,status) {
                if (status == 404) {
                    toastr.error("Không tìm thấy trang bạn cần tìm", "Lỗi 404");
                    $location.path("/").search({});
                }
            })
        };
        $scope.getStaticHTML($scope.key);

    });

