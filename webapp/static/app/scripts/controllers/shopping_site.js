/** JavaScript on WebKit
 *
 *  amazon.js
 *
 *
 *  Created by TVA on 12/5/15.
 *  Copyright (c) 2015 order_china. All rights reserved.
 */

'use strict';

angular.module('alodathangApp')
    .controller('SiteProductCtrl', function($routeParams, $location, $sce, ProxyService, $scope, $rootScope, ShareService, CartService) {
        var prxydomain = ShareService.getDomainOfSite($routeParams.site);
        ShareService.changeDomain(prxydomain);

        if ($location.search().path != undefined) $scope.url = $location.search().path.replace("%2F", "/");
        $scope.loading = true;

        $scope.getHTML = function(url) {
            ProxyService.getHTML(url, prxydomain).then(function(response) {
                    if (response.status != 200) {
                        toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + response.status);
                        $scope.getHTMLError = true;
                        $scope.loading = false;
                        return;
                    }
                    var newData = response.data.split("document.write").join("");
                    $scope.html = $sce.trustAsHtml(newData);
                    $scope.loading = false;
                    $scope.extractProductData();

                    if ($routeParams.site == 'amazon') {
                        $(document).on("click", '#altImages input', function(env) {
                            $("#altImages").children("span.a-button-thumbnail").removeClass('a-button-secected').removeClass('a-button-focus');
                            var img_id = new RegExp('images/I/(\\w+)\\.').exec(this.nextSibling.children[0].src)[1];
                            $('#landingImage').attr('src', 'http://ecx.images-amazon.com/images/I/' + img_id + '._SX355_.jpg');
                        });
                    } else if ($routeParams.site == 'ebay') window.scrollTo(0, 0); //some product of ebay trigger scroll to bot =.='
                },
                function(error) {
                    toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + error.status);
                    $scope.getHTMLError = true;
                    $scope.loading = false;
                });
        };

        $scope.extractProductData = function() {
            try {
                $scope.productData = ShareService.extractProductData($scope.html.toString(), prxydomain, $scope.url);
                $scope.parseError = false;
            } catch (e) {
                $scope.parseError = true;
                console.log(e);
            }
        }

        $scope.addToCart = function() {
            $scope.productData.html = $(document).find("#html-content").html(); //insert HTML for caching
            CartService.AddCartItem($scope.productData,
                function(data) {
                    toastr.success("Đã thêm vào giỏ hàng thành công");
                },
                function(data) {
                    toastr.success("Không thể thêm sản phẩm vào giỏ hàng", "Có lỗi xảy ra");
                });
        }

        $scope.expandBar = function() {
            if ($(".add-to-cart-btn").data("submit") == true) {
                $scope.addToCart();
            } else {
                $(".cart-bar-item-info").slideDown("400", function() {
                    $(".add-to-cart-btn").removeClass("btn-info").addClass("btn-success");
                    $(".add-to-cart-btn").data("submit", true);
                });
            }
        }


        $("span.minimize").click(function() {
            if ($(".cart-bar-fixed").hasClass("cart-bar-collapsed")) {
                $(".cart-bar-fixed").animate({
                        bottom: 0
                    },
                    400,
                    function() {
                        $(".cart-bar-fixed").removeClass("cart-bar-collapsed");
                        $("span.minimize span").removeClass("glyphicon-menu-up");
                        $("span.minimize span").addClass("glyphicon-menu-down");
                    });
            } else if ($(".add-to-cart-btn").data("submit")) {
                $(".cart-bar-item-info").slideUp("400", function() {
                    $(".add-to-cart-btn").addClass("btn-info").removeClass("btn-success");
                    $(".add-to-cart-btn").data("submit", false);
                });
            } else {
                var offset = $(".cart-bar-fixed").height();
                $(".cart-bar-fixed").animate({
                        bottom: "-" + offset
                    },
                    400,
                    function() {
                        $(".cart-bar-fixed").addClass("cart-bar-collapsed");
                        $("span.minimize span").removeClass("glyphicon-menu-down");
                        $("span.minimize span").addClass("glyphicon-menu-up");
                    });
            }
        })

        $(document).on("click", '#html-content a', function(event) {
            event.preventDefault();
            var newUrl = event.currentTarget.href.split($rootScope.rootURL)[1];
            var urlType = ShareService.classifyURL(newUrl, prxydomain);
            if (urlType == 'product') { //Check amazon url
                $scope.$apply(function() {
                    $location.search({
                        path: newUrl.replace("/", "%2F")
                    });
                });
            } else if (urlType == 'search') { // handle query
                $scope.$apply(function() {
                    $location.path('/' + $routeParams.site + '/s').search({
                        'path': newUrl.replace("/", "%2F")
                    });
                });
            } else {}
        });

        $("div#html-content").on("change", "select", function() {
            $scope.$apply(function() {
                $scope.productData = ShareService.extractProductData($(".product-content"), prxydomain, $scope.url);
            });
        });

        $scope.getHTML($scope.url, prxydomain);

    })
    //angular.module('alodathangApp')
    .controller('SiteSearchCtrl', function($routeParams, $scope, ProxyService, $sce, $location, $rootScope, ShareService) {
        var prxydomain = ShareService.getDomainOfSite($routeParams.site);
        ShareService.changeDomain(prxydomain);

        if ($location.search().path != undefined) $scope.url = $location.search().path.replace("%2F", "/");
        $scope.loading = true;

        $scope.getHTML = function(url) {
            ProxyService.getHTML(url, prxydomain).then(function(response) {
                    if (response.status != 200) {
                        toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + response.status);
                        $scope.getHTMLError = true;
                        $scope.loading = false;
                        return;
                    }
                    if (prxydomain == "www.amazon.com") response.data = stripScripts(response.data); // hot fix amazon
                    var newData = response.data.split("document.write").join("");
                    // var wnd = window.open("about:blank", "", "_blank");
                    // wnd.document.write(newData);
                    $scope.html = $sce.trustAsHtml(newData);
                    $scope.loading = false;
                },
                function(error) {
                    toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + error.status);
                    $scope.getHTMLError = true;
                    $scope.loading = false;
                });
        };

        function stripScripts(s) {
            var div = document.createElement('div');
            div.innerHTML = s;
            var scripts = div.getElementsByTagName('script');
            var i = scripts.length;
            while (i--) {
                // if(i<23){
                // console.log(scripts[i]);
                scripts[i].parentNode.removeChild(scripts[i]);
                // }
            }
            return div.innerHTML;
        }

        $scope.getHTML($scope.url);

        $("div#html-content").on("click", 'a', function(event) {
            event.preventDefault();
            var newUrl = event.currentTarget.href.split($rootScope.rootURL)[1];
            var urlType = ShareService.classifyURL(newUrl, prxydomain);

            if (urlType == 'product') { //Check amazon url
                $scope.$apply(function() {
                    $location.path('/' + $routeParams.site + '/p').search({
                        'path': newUrl.replace("/", "%2F")
                    });
                });
            } else if (urlType == 'search') { // handle query
                $scope.$apply(function() {
                    $location.search({
                        path: newUrl.replace("/", "%2F")
                    });
                });
            } else {}
            // TODO: another url
        });

    });
