'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:ProductCtrl
 * @description
 * # ProductCtrl
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('ProductCtrl', function($location, $sce, ProxyService, $scope, ShareService, CartService, $rootScope) {
        var rootURL = location.protocol + '//' + location.host + "/";

        if ($location.search().path != undefined) $scope.url = $location.search().path.replace("%2F", "/");
        $scope.loading = true;

        $scope.getHTML = function(url) {
            ProxyService.getHTML(url).then(function(response) {
                    if (response.status != 200) {
                        toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + response.status);
                        $scope.getHTMLError = true;
                        $scope.loading = false;
                        return;
                    }
                    var newData = response.data.split("document.write").join("");
                    $scope.html = $sce.trustAsHtml(newData);
                    $scope.loading = false;
                    $scope.extractProductData();
                },
                function(error) {
                    toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + error.status);
                    $scope.getHTMLError = true;
                    $scope.loading = false;
                });
        }

        $scope.extractProductData = function() {
            try {
                $scope.productData = ShareService.extractProductData($scope.html.toString(), $rootScope.prxydomain, $scope.url);
                $scope.parseError = false;
            } catch (e) {
                $scope.parseError = true;
            }
        }

        $scope.addToCart = function() {
            CartService.AddCartItem($scope.productData,
                function(data) {
                    toastr.success("Đã thêm vào giỏ hàng thành công");
                },
                function(data) {
                    toastr.success("Không thể thêm sản phẩm vào giỏ hàng", "Có lỗi xảy ra");
                });
        }

        $scope.expandBar = function() {
            if ($(".add-to-cart-btn").data("submit") == true) {
                $scope.addToCart();
            } else {
                $(".cart-bar-item-info").slideDown("400", function() {
                    $(".add-to-cart-btn").removeClass("btn-info").addClass("btn-success");
                    $(".add-to-cart-btn").data("submit", true);
                });
            }
        }


        $("span.minimize").click(function() {
            if ($(".cart-bar-fixed").hasClass("cart-bar-collapsed")) {
                $(".cart-bar-fixed").animate({
                        bottom: 0
                    },
                    400,
                    function() {
                        $(".cart-bar-fixed").removeClass("cart-bar-collapsed");
                        $("span.minimize span").removeClass("glyphicon-menu-up");
                        $("span.minimize span").addClass("glyphicon-menu-down");
                    });
            } else if ($(".add-to-cart-btn").data("submit")) {
                $(".cart-bar-item-info").slideUp("400", function() {
                    $(".add-to-cart-btn").addClass("btn-info").removeClass("btn-success");
                    $(".add-to-cart-btn").data("submit", false);
                });
            } else {
                var offset = $(".cart-bar-fixed").height();
                $(".cart-bar-fixed").animate({
                        bottom: "-" + offset
                    },
                    400,
                    function() {
                        $(".cart-bar-fixed").addClass("cart-bar-collapsed");
                        $("span.minimize span").removeClass("glyphicon-menu-down");
                        $("span.minimize span").addClass("glyphicon-menu-up");
                    });
            }
        })

        $(".product-content").on("click", 'a', function(event) {
            event.preventDefault();
            var newUrl = event.currentTarget.href;
            if (newUrl.match("/dp/")) { //Check amazon url 
                $scope.loading = true;
                $scope.getHTMLError = false;
                $scope.getHTML(newUrl);
            }
            // else if(newUrl.match(/\/[s|b]\?ie/)){ // handle query
            //     $scope.loading = true;
            // }
            else {}
        });

        $(".product-content").on("change", "select", function() {
            $scope.$apply(function() {
                $scope.productData = ShareService.extractProductData($(".product-content"), $rootScope.prxydomain, $scope.url);
            });
        })

        $scope.getHTML($scope.url);

    });
