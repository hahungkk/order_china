'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:SearchCtrl
 * @description
 * # SearchCtrl
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('SearchCtrl', function($scope, $sce, $location, $rootScope, ProxyService, ShareService, CategoryService, $window) {
        var queryStringBase = {
            "www.amazon.com": "/s/ref=nb_sb_noss?url=search-alias%3Daps&field-keywords={{q}}",
            "www.ebay.com": "/sch/i.html?_odkw=macbook&_osacat=0&_from=R40&_trksid=p2045573.m570.l1313.TR0.TRC0.H0.X{{q}}.TRS0&_nkw={{q}}&_sacat=0",
            "taobao": "https://world.taobao.com/search/search.htm?_ksTS=1464110669624_41&spm=a21bp.7806943.20151106.1&_input_charset=utf-8&navigator=all&json=on&q={{q}}&callback=__jsonp_cb&cna=SnzLD12B5A8CAXEWOjfwU7xp&abtest=_AB-LR517-LR854-LR895-PR517-PR854-PV895_2462"
        };

        $scope.keyword = $location.search().q;
        $scope.lang = $location.search().l;

        console.log('SearchCtrl: lang=' + $scope.lang + ' keywords=' + $scope.keyword);


        $scope.searchProcuctByQueryStringOnCurrentDomain = function() {
            var url = queryStringBase["taobao"].split("{{q}}").join($scope.keyword);
            window.location = url;

        };

        $scope.loading = true;

        $scope.translateKeywordThenSearch = function() {
            console.log("Translating:" + $scope.keyword);
            //TODO: try translate $scope.keyword using google translate api
            //$scope.searchProcuctByQueryStringOnCurrentDomain()
            ShareService.translateText($scope.keyword, function(translated_text) {
                $scope.keyword = translated_text;
                $scope.searchProcuctByQueryStringOnCurrentDomain()
            }, function() { //fail to transalte
                $scope.searchProcuctByQueryStringOnCurrentDomain()
            });
        };

        if ($scope.lang == 'vi') {
            CategoryService.GetCategoryList({
                    search: $scope.keyword
                },
                function(data) {
                    // console.log(data);
                    if (data.results && data.results.length >= 1) {
                        var cat = data.results[0];
                        //$location.path('/cat/'+cat.code).replace()
                        $scope.keyword = cat.code; //quick solution
                        $scope.searchProcuctByQueryStringOnCurrentDomain()
                    } else {
                        $scope.translateKeywordThenSearch();
                    }
                },
                function(data) {
                    $scope.translateKeywordThenSearch();
                }
            );
        } else { //Other language (assume its english)
            $scope.searchProcuctByQueryStringOnCurrentDomain()
        }

        //$scope.getSearchResultHTML = function(keyword) {
        //    var url = queryStringBase[$rootScope.prxydomain].split("{{q}}").join(keyword);
        //
        //    ProxyService.getHTML(url).then(function(response) {
        //            if (response.status != 200) {
        //                toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + response.status);
        //                $scope.getHTMLError=true;
        //                $scope.loading=false;
        //                return;
        //            }
        //            var newData = response.data.split("document.write").join("");
        //            $scope.html = $sce.trustAsHtml(newData);
        //            $scope.loading = false;
        //        },
        //        function(error) {
        //            toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + error.status);
        //            $scope.getHTMLError = true;
        //            $scope.loading = false;
        //        });
        //};
        //
        //$scope.getSearchResultHTMLOnPage = function(url) {
        //    ProxyService.getHTML(url).then(function(response) {
        //            if (response.status != 200) {
        //                toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + response.status);
        //                $scope.getHTMLError=true;
        //                $scope.loading=false;
        //                return;
        //            }
        //            var newData = response.data.split("document.write").join("");
        //            $scope.html = $sce.trustAsHtml(newData);
        //            $scope.loading = false;
        //        },
        //        function(error) {
        //            toastr.error("Có lỗi xảy ra, vui lòng thử lại", "ERROR: " + error.status);
        //            $scope.getHTMLError = true;
        //            $scope.loading = false;
        //        });
        //};

        //$scope.getSearchResultHTML($scope.keyword);
        //
        //$(".product-content").on("click", 'a', function(event) {
        //    event.preventDefault();
        //    var newUrl = event.currentTarget.href;
        //    if (newUrl.match("/dp/")) { //Check amazon url
        //        $scope.$apply(function() {
        //            $location.path('/amazon').search({
        //                'path': newUrl.split($rootScope.rootURL)[1].replace("/", "%2F")
        //            });
        //            //$location.path('/product').search({
        //            //    'path': newUrl.split(rootUrl)[1].replace("/", "%2F")
        //            //});
        //        });
        //    } else if (newUrl.match(/\/s\?ie/)|| newUrl.match(/gp\/search\//) ) { // handle query
        //        $scope.loading = true;
        //        $scope.getHTMLError=false;
        //        $scope.getSearchResultHTMLOnPage(newUrl)
        //    } else {}
        //    // TODO: another url
        //});

    })
    .controller('ChinaCheckerCtrl', ['$scope', '$http', 'moment', function($scope, $http, moment) {
        $scope.tracking_code = null;
        $scope.search_code = null;
        $scope.error = null;
        $scope.info = null;
        $scope.tracking_data = null;
        $scope.disPlay = "none";
        $scope.list_date = [];
        $scope.list_data = [];
        $scope.code = null;
        $scope.upDate = "none";
        $scope.update_data = {};
        $scope.today = moment();

        var url = '/api/shop_module/smp/';

        $scope.addCode = function() {
            if ($scope.tracking_code) {
                if($scope.update_data[$scope.tracking_code] > 3){
                    $scope.upDate = 'block';
                    return false;
                } else {
                    $scope.add();
                }
            } else {
                $scope.error = "Please enter one number !";
            }
        };

        $scope.add = function(){
            $scope.cancel();
            var postData = {'tracking_code': $scope.tracking_code };
            $http.post(url, postData)
                .then(function successCallback(data) {
                    $scope.info = data.data.info;
                    $scope.getData();
                }, function errorCallback(msg) {
                    $scope.error = msg.data[0];
                });
            $scope.tracking_code = null;
        };

        $scope.getData = function() {
            $http.get(url).then(function(data) {
                $scope.tracking_data = data.data;
                $scope.extractShipmentPackData();
            });
        };

        $scope.extractShipmentPackData = function() {
            //create list 15 day
            $scope.data = {};
            $scope.list_date = [];
            for (var i = 0; i < 15; i++) {
                var date = moment().subtract(i, 'days').format("DD-MM-YYYY");
                $scope.list_date.push(date);
                $scope.data[date] = [];
            }
            //extract data
            angular.forEach($scope.tracking_data.results, function(track) {
                var date = moment(track.created).format('DD-MM-YYYY');
                angular.forEach($scope.list_date, function(dt) {
                    $scope.update_data[track.tracking_code] = track.latest_version;
                    if (date === dt) {
                        $scope.data[date].push(track);
                    }
                });
            });
        };

        $scope.delete = function(code) {
            $http.delete(url + code + '/')
                .then(function successCallback(data) {
                    console.log(data);
                    $scope.getData();
                }, function errorCallback(msg) {
                    console.log(msg);
                });
            $scope.cancel();
        };

        $scope.search = function() {
            $scope.search_results = [];
            angular.forEach($scope.tracking_data.results, function(track) {
                if (track.tracking_code === $scope.search_code) {
                    $scope.search_results.push(track);
                }
            });
            $scope.search_code = null;
        };

        $scope.cancel = function() {
            $scope.disPlay = "none";
            $scope.upDate = "none";
        };

        $scope.removeTrackingCode = function(code, tracking_code) {
            $scope.tracking_code = tracking_code;
            $scope.code = code;
            $scope.disPlay = "block";
        };

        $scope.getData();

        $scope.removeEror = function() {
            $scope.error = null;
            $scope.info = null;
            $scope.search_results = [];
        };

    }])
    .controller('StorageCtrl', ['$scope', '$http', 'moment', function($scope, $http, moment) {
        $scope.tracking_code = null;
        $scope.search_code = null;
        $scope.error = null;
        $scope.info = null;
        $scope.tracking_data = null;
        $scope.disPlay = "none";
        $scope.list_date = [];
        $scope.list_data = [];
        $scope.code = null;
        $scope.weight = 0.0;
        $scope.length = 0.0;
        $scope.width = 0.0;
        $scope.height = 0.0;
        $scope.upDate = "none";
        $scope.update_data = {};
        $scope.today = moment();
        $scope.remain = [];

        var url = '/api/shop_module/tracking/';
        var export_url = '/api/shop_module/tracking/export/';
        var remain_url = '/api/shop_module/smp/?checked=false';

        $scope.addCode = function() {
            if ($scope.tracking_code) {
                if($scope.update_data[$scope.tracking_code] > 3){
                    $scope.upDate = 'block';
                    return false;
                } else {
                    $scope.add();
                }
            } else {
                $scope.error = "Please enter one number !";
            }
        };

        $scope.add = function(){
            $scope.cancel();
            var postData = {
                    'tracking_code': $scope.tracking_code,
                    'weight': $scope.weight,
                    'length': $scope.length,
                    'width': $scope.width,
                    'height': $scope.height
            };
            $http.post(url, postData)
                .then(function successCallback(data) {
                    $scope.info = data.data.info;
                    $scope.getTrackingData();
                }, function errorCallback(msg) {
                    $scope.error = msg.data[0];
                });
            $scope.tracking_code = null;
        };

        $scope.getTrackingData = function() {
            $http.get(url).then(function(data) {
                $http.get(remain_url).then(function (remain) {
                    $scope.tracking_data = data.data;
                    console.log($scope.tracking_data);
                    $scope.remain = remain.data.results;
                    $scope.extractTrackingData();
                });
            });
        };

        $scope.extractTrackingData = function() {
            //create list 15 day
            $scope.data = {};
            $scope.remain_data = {};
            $scope.list_date = [];
            for (var i = 0; i < 15; i++) {
                var date = moment().subtract(i, 'days').format("DD-MM-YYYY");
                $scope.list_date.push(date);
                $scope.data[date] = [];
                $scope.remain_data[date] = [];
            }

            //extract data
            angular.forEach($scope.tracking_data.results, function(track) {
                var date = moment(track.created).format('DD-MM-YYYY');
                angular.forEach($scope.list_date, function(dt) {
                    $scope.update_data[track.tracking_code] = track.latest_version;
                    if (date === dt) {
                        $scope.data[date].push(track);
                    }
                });
            });

            //extract remain data
            angular.forEach($scope.remain, function(remain) {
                var date = moment(remain.created).format('DD-MM-YYYY');
                angular.forEach($scope.list_date, function(dt) {
                    if (date === dt) {
                        $scope.remain_data[date].push(remain);
                    }
                });
            });
        };

        $scope.delete = function(code) {
            $http.delete(url + code + '/')
                .then(function successCallback(data) {
                    console.log(data);
                    $scope.getTrackingData();
                }, function errorCallback(msg) {
                    console.log(msg);
                });
            $scope.cancel();
        };

        $scope.search = function() {
            $scope.search_results = [];
            angular.forEach($scope.tracking_data.results, function(track) {
                if (track.tracking_code === $scope.search_code) {
                    $scope.search_results.push(track);
                }
            });
            $scope.search_code = null;
        };

        $scope.cancel = function() {
            $scope.disPlay = "none";
            $scope.upDate = "none";
        };

        $scope.removeTrackingCode = function(code, tracking_code) {
            $scope.tracking_code = tracking_code;
            $scope.code = code;
            $scope.disPlay = "block";
        };

        $scope.getTrackingData();

        $scope.export = function () {
            var today = moment().format("DD-MM-YYYY");
            $http.get(export_url, { responseType: 'arraybuffer'}).then(function (data) {
                var blob = new Blob([data.data], {
                    type: "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",
                });
                saveAs(blob, today+".xlsx");
            })
        };

        $scope.removeEror = function() {
            $scope.error = null;
            $scope.info = null;
            $scope.weight = 0.0;
            $scope.length = 0.0;
            $scope.width = 0.0;
            $scope.height = 0.0;
            $scope.hugePackage = false;
            $scope.search_results = [];
        };

    }]);
