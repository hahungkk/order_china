/**
 * Created by TVA on 11/19/15.
 */
'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:NavBarCtrl
 * @description
 *
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('NavBarCtrl',function($scope, CategoryService, $sce, $location, $rootScope) {

        $scope.categoryList = [];
        var update_success_callback = function($data) {
            $scope.categoryList = $data.results;
        };

        var update_error_callback = function($data) {
            toastr.error("Không thể lấy dữ liệu", "Có lỗi xảy ra");
        };

        $scope.getCategories = function() {
            CategoryService.GetCategoryList(null, update_success_callback, update_error_callback);
        };

        $scope.getCategories();

        $scope.logout = function(){
            alert("AAA");
        }

    });
;