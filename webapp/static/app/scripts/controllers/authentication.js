'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:AuthenticationCtrl
 * @description
 * # AuthenticationCtrl
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('AuthenticationCtrl',
        function($scope, AuthenticationService, $sce, $location,
                 $rootScope, $cookies, $window, $timeout) {
        $scope.referer = null;
        $scope.login = function(byRegistration) {
            var post;

            if (byRegistration) {
                post = {
                    username: $scope.signUpForm.username,
                    password: $scope.signUpForm.password
                }
            } else {
                post = {
                    username: $scope.loginForm.username,
                    password: $scope.loginForm.password
                }
            }

            AuthenticationService.Login(post, function (data) {
                $scope.setData(data);
            }, function (error) {
                var firtLetter = post.username.charAt(0);
                if(firtLetter === firtLetter.toUpperCase()){
                    post.username = jsLcfirst(post.username);
                } else if(firtLetter === firtLetter.toLowerCase()){
                    post.username = jsUcfirst(post.username);
                }
                AuthenticationService.Login(post, function (data) {
                    $scope.setData(data);
                }, function (error) {
                    toastr.error("Vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                    $scope.error = error.error
                });
            });
        };

        function jsUcfirst(string) {
            return string.charAt(0).toUpperCase() + string.slice(1);
        }
        function jsLcfirst(string) {
            return string.charAt(0).toLowerCase() + string.slice(1);
        }

        $scope.setData = function(data){
            $rootScope.globals = data;
            $rootScope.prxydomain = $cookies.get("prxydomain");
            $location.path('/').search({});
            toastr.success("Bạn đã đăng nhập thành công");
            // WebSocketService.connect();
        };

        $scope.oauthFacebook = function () {
            $window.open('/auth/login/facebook', '_self');
        };

        $scope.oauthGoogle = function () {
            $window.open('/auth/login/google-oauth2', '_self');
        };

        $scope.logout = function() {
            var c = confirm("Bạn có chắc muốn đăng xuất khỏi hệ thống?");
            if (c) {
                AuthenticationService.Logout(null, function() {
                    $location.path('/').search({});
                }, null);
            }
        };

        $(document).ready(function() {
            $('#login-form-link').click(function(e) {
                $("#login-form").delay(100).fadeIn(100);
                $("#register-form").fadeOut(100);
                $('#register-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });
            $('#register-form-link').click(function(e) {
                $("#register-form").delay(100).fadeIn(100);
                $("#login-form").fadeOut(100);
                $('#login-form-link').removeClass('active');
                $(this).addClass('active');
                e.preventDefault();
            });
        });

        if($location.search().referer){
            $scope.referer = $location.search().referer;
        }

        var reg = new RegExp('[a-z]+');
        $scope.signUp = function() {

            if($location.search().referer){
                $scope.signUpForm.referer = $scope.referer;
            }

            var pw = $scope.signUpForm.password;
            var username = $scope.signUpForm.username;
            var rpw = $scope.signUpForm.repassword;
            if (pw === undefined || rpw === undefined) {
                toastr.error("Vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                $scope.error = {
                    confirm_password: "Vui lòng điền đẩy đủ mật khẩu và nhập lại mật khẩu"
                };
                console.log($scope.error);
                return;
            } else if (pw !== rpw) {
                toastr.error("Vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                $scope.error = {
                    confirm_password: "Mật khẩu không khớp, hãy thử lại"
                };
                console.log($scope.error);
                return;
            }
            if (!reg.test(username)) {
                toastr.error("Vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                $scope.error = {
                    username_number: "Tài khoản yêu cầu phải chứa ký tự latin !"
                };
                console.log($scope.error);
                return;
            }
            var post = {
                username: $scope.signUpForm.username,
                password: $scope.signUpForm.password,
                email: $scope.signUpForm.email,
                referer: $scope.signUpForm.referer
            };
            AuthenticationService.SignUp(post, function(data) {
                    $scope.login(true);
                },
                function(error) {
                    toastr.error("Vui lòng kiểm tra lại thông tin", "Có lỗi xảy ra");
                    if(error.error.referer){
                        $scope.error = {
                            referer: ["Người giới thiệu ko tồn tại !"]
                        };
                        $timeout(function () {
                            window.location = "/";
                        }, 3000);
                    }else{
                        $scope.error = error.error;
                        console.log($scope.error);
                    }

                });
        }

    });
