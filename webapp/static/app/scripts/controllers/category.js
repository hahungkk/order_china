/**
 * Created by TVA on 11/19/15.
 */
/**
 * @ngdoc function
 * @name alodathangApp.controller:CategoryCtrl
 * @description
 *
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('CategoryCtrl',function($scope,$routeParams, CategoryService, $sce, $location, $rootScope) {

        var update_success_callback = function(cat) {
            if(cat.metadata){
                if(cat.metadata.amazon_url){
                    $location.path('/amazon/s').search({path: cat.metadata.amazon_url}).replace();
                    return
                }else if(cat.metadata.ebay_url){
                    $location.path('/ebay/s').search({path: cat.metadata.amazon_url}).replace();
                    return
                }
            }
            //redirect but don't push browser history stack.
            $location.path('/product/search').search({q: cat.code}).replace();
        };

        var update_error_callback = function($data) {
            console.log("Category not found, redirect to product search with category code");
            $location.path('/product/search').search({q: $routeParams.pk}).replace();
            //toastr.error("Không thể lấy dữ liệu", "Có lỗi xảy ra");
        };

        $scope.GetCategoryDetail = function() {
            CategoryService.GetCategoryDetail({pk:$routeParams.pk}, update_success_callback, update_error_callback);
        };

        $scope.GetCategoryDetail();


    });
;