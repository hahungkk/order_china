'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:UserCtrl
 * @description
 * # UserCtrl
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
    .controller('UserProfileCtrl', function($scope, $rootScope, $location, UserService) {
        $scope.error = {};
        $scope.page = "profile";
        var update_success_callback = function($data) {
            toastr.success("Profile đã được cập nhật");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().removeClass("has-error")
            });
            $scope.error = {};
        };

        var update_error_callback = function($data) {
            toastr.error("Vui lòng kiểm tra lại", "Có lỗi xảy ra");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().addClass("has-error");
            });
            $scope.error = $data;
        };


        $scope.updateProfile = function() {
            UserService.UpdateUserProfile($scope.user, update_success_callback, update_error_callback);
        };

        var get_success_callback = function($data) {
            $scope.user = $data;
        };

        var get_error_callback = function($data) {
            $scope.user = $data;
        };

        UserService.ViewUserProfile(null, get_success_callback, get_error_callback);

        $scope.resetField = function() {
            UserService.ViewUserProfile(null, get_success_callback, get_error_callback);
        }
    })
    .controller('CustomerProfileCtrl', function($scope, $rootScope, $location, UserService) {
        $scope.error = {};
        $scope.page = "customer_profile";

        var update_success_callback = function($data) {
            toastr.success("Profile đã được cập nhật");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().removeClass("has-error")
            });
            $scope.error = {};
        };

        var update_error_callback = function($data) {
            toastr.error("Vui lòng kiểm tra lại", "Có lỗi xảy ra");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().addClass("has-error");
            });
            $scope.error = $data;
        };


        $scope.updateProfile = function() {
            UserService.UpdateCustomerProfile($scope.user, update_success_callback, update_error_callback);
        };

        var get_success_callback = function($data) {
            $scope.user = $data;
        };

        var get_error_callback = function($data) {
            $scope.user = $data;
        };

        UserService.ViewCustomerProfile(null, get_success_callback, get_error_callback);

        $scope.resetField = function() {
            UserService.ViewCustomerProfile(null, get_success_callback, get_error_callback);
        }
    })
    .controller('UserChangePasswordCtrl', function($scope, $rootScope, $location, UserService) {
        $scope.error = {};
        $scope.page = "change_password";
        $scope.changePassword = function() {
            $("#change-password-btn").button('loading');

            if ($scope.user.newPassword !== $scope.user.reNewPassword) {
                angular.element("#reNewPassword").parent().parent().addClass("has-error");
                $scope.passwordnotmatch = true;
                $("#change-password-btn").button('reset');
                return;
            }

            angular.element("#reNewPassword").parent().parent().removeClass("has-error");
            $scope.passwordnotmatch = false;
            UserService.ChangePassword($scope.user, update_success_callback, update_error_callback);
        };

        $scope.resetField = function() {
            $scope.user.password = "";
            $scope.user.newPassword = "";
            $scope.user.reNewPassword = "";
        };

        var update_success_callback = function($data) {
            toastr.success("Password đã được đổi");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().removeClass("has-error")
            });
            $scope.error = {};
            $("#change-password-btn").button('reset');
        };
        var update_error_callback = function($data) {
            toastr.error("Vui lòng kiểm tra lại: ", "Có lỗi xảy ra");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().addClass("has-error");
            });
            $scope.error = $data;
            $("#change-password-btn").button('reset');
        };
    })
    .controller('UserChangeEmailCtrl', function($scope, $rootScope, $location, UserService) {
        $scope.error = {};
        $scope.user = {
            username: $rootScope.globals.username
        };
        $scope.page = "change_email";

        $scope.changeEmail = function() {
            $("#change-email-btn").button('loading');
            angular.element("#new_email").parent().parent().removeClass("has-error");
            $scope.email_match = false;
            UserService.ChangeEmail($scope.user, update_success_callback, update_error_callback);
        };

        $scope.resetField = function() {
            $scope.user.password = "";
            $scope.user.new_email = "";
        };

        var update_success_callback = function($data) {
            toastr.success("Vui lòng kiểm tra email xác nhận để đổi email thành công.");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().removeClass("has-error")
            });
            $scope.error = {};
            $("#change-email-btn").button('reset');
        };

        var update_error_callback = function($data) {
            toastr.error("Vui lòng kiểm tra lại: ", "Có lỗi xảy ra");
            angular.forEach($data, function(value, key) {
                angular.element("#" + key).parent().parent().addClass("has-error");
            });
            $scope.error = $data;
            $("#change-email-btn").button('reset');
        };

    })
    .controller('UserMyOrderCtrl', function($scope, $rootScope, $location, UserService) {
        $scope.orders = [];
        $scope.page = "my_order";
        var update_success_callback = function($data) {
            $scope.orders = $data.results;
        };
        var update_error_callback = function($data) {
            toastr.error("Không thể lấy dữ liệu", "Có lỗi xảy ra");
        };

        $scope.getOrders = function() {
            UserService.GetOrders(null, update_success_callback, update_error_callback);
        };

        $scope.getOrders();
    })
    .controller('UserMyOrderDetailCtrl', function($scope, $rootScope, $location, UserService, $routeParams, $cookies) {
        $scope.orders = [];
        $scope.page = "my_order";
        $scope.orderId = $routeParams.id;
        $scope.tracking_status = [];
        $scope.showAll = false;

        var update_success_callback = function($data) {
            $scope.order = $data;
            $scope.vendors = partitionVendors($scope.order);
        };

        function partitionVendors(order) {
            var ret = {quantity: 0};
            angular.forEach(order.orderitem_set, function(value, key) {
                var vendor_name = value.shopping_domain + " | " + value.vendor;
                ret.quantity += value.quantity;
                if (ret.hasOwnProperty(vendor_name)) { // found vendor, add to array
                    ret[vendor_name].push(value);
                } else { //new vendor, create new arra then push
                    ret[vendor_name] = [];
                    ret[vendor_name].push(value);
                }
            });
            return ret;
        }

        $scope.shipment_all = null;
        $scope.display = 'none';

        $scope.showModal = function (id) {
            angular.forEach($scope.order.orderitem_set, function (item) {
                if(item.id === id){
                    $scope.shipment_all = item.shipment_all;
                }
                $scope.display = 'block';
            })
        };

        $scope.close = function () {
            $scope.display = 'none';
        };

        // function partitionCurrency(order) {
        //     var prices = {};
        //     angular.forEach(order.orderitem_set, function(item, index) {
        //         if(item.status_value !== 'Failed'){
        //             if (prices[item.currency] === undefined){
        //                 prices[item.currency] = {};
        //                 prices[item.currency].exchange_rate = $.grep($cookies.getObject("currency"), function(e) {
        //                     return e.code.toUpperCase() === item.currency;
        //                 })[0].exchange_rate;
        //
        //                 prices[item.currency].item_cost = 0;
        //                 prices[item.currency].shipping = 0;
        //                 prices[item.currency].service_charge = 0;
        //                 prices[item.currency].insurance_charge = 0;
        //                 prices[item.currency].fast_order_charge = 0;
        //                 prices[item.currency].tax_cost = 0;
        //                 prices[item.currency].total = 0;
        //             }
        //             prices[item.currency].item_cost += parseFloat(item.price)*item.quantity;
        //             prices[item.currency].shipping += parseFloat(item.shipping);
        //             prices[item.currency].service_charge += parseFloat(item.service_charge);
        //             prices[item.currency].insurance_charge += parseFloat(item.insurance_charge);
        //             prices[item.currency].fast_order_charge += parseFloat(item.fast_order_charge);
        //             prices[item.currency].tax_cost += parseFloat(item.tax_cost);
        //             prices[item.currency].total += parseFloat(item.price)*item.quantity
        //                 + parseFloat(item.shipping)
        //                 + parseFloat(item.service_charge)
        //                 + parseFloat(item.insurance_charge)
        //                 + parseFloat(item.fast_order_charge)
        //                 + parseFloat(item.tax_cost);
        //         }});
        //     return prices;
        // }

        var update_error_callback = function($data) {
            toastr.error("Không thể lấy dữ liệu", "Có lỗi xảy ra");
        };

        $scope.getOrderByID = function(id) {
            UserService.GetOrderByID(id, update_success_callback, update_error_callback);
        };

        $scope.getOrderByID($scope.orderId);
    })
    .controller('UserMyPurchasesCtrl', function($scope, $rootScope, $location, UserService) {
        $scope.payments = [];
        $scope.page = "my_purchase";
        var update_success_callback = function($data) {
            $scope.payments = $data.results;
            for (var i = 0; i < $scope.payments.length; i++) {
                var tempDate = new Date($scope.payments[i].created);
                $scope.payments[i].modified_date_parsed = tempDate.getFullYear() + "-" + (tempDate.getMonth() + 1) + "-" + tempDate.getDate() + " " + tempDate.getHours() + ":" + tempDate.getMinutes() + ":" + tempDate.getSeconds();
            }
        };

        var update_error_callback = function($data) {
            toastr.error("Không thể lấy dữ liệu", "Có lỗi xảy ra");
        };

        $scope.getResultsPage = function() {
            UserService.GetPurchasesList(null, update_success_callback, update_error_callback);
        };

        $scope.getResultsPage();
    })
    .controller('UserMyWalletCtrl', function($scope, $rootScope, $location, UserService) {
        $scope.walletFromItems = [];
        $scope.walletToItems = [];
        $scope.page = "my_wallet";
        var update_success_callback_1 = function($data) {
            $scope.walletFromItems += $data.results;
            for (var i = 0; i < $scope.walletFromItems.length; i++) {
                var tempDate = new Date($scope.walletFromItems[i].created);
                $scope.walletFromItems[i].modified_date_parsed = tempDate.getFullYear() + "-" + (tempDate.getMonth() + 1) + "-" + tempDate.getDate() + " " + tempDate.getHours() + ":" + tempDate.getMinutes() + ":" + tempDate.getSeconds();
            }
        };

        var update_success_callback_2 = function($data) {
            $scope.walletToItems = $data.results;
            for (var i = 0; i < $scope.walletToItems.length; i++) {
                var tempDate = new Date($scope.walletToItems[i].created);
                $scope.walletToItems[i].modified_date_parsed = tempDate.getFullYear() + "-" + (tempDate.getMonth() + 1) + "-" + tempDate.getDate() + " " + tempDate.getHours() + ":" + tempDate.getMinutes() + ":" + tempDate.getSeconds();
            }
        };

        var update_error_callback = function($data) {
            toastr.error("Không thể lấy dữ liệu", "Có lỗi xảy ra");
        };

        $scope.getResultsPage = function() {
            UserService.GetWalletFrom($rootScope.globals.id, update_success_callback_1, update_error_callback);
            UserService.GetWalletTo($rootScope.globals.id, update_success_callback_2, update_error_callback);
        };

        $scope.getCurrentBalance = function(){
            UserService.GetBalance(null, function(data){
                $scope.user_balance = data.results[0];
            },
            null)
        };

        $scope.getResultsPage();
        $scope.getCurrentBalance();
    });
