'use strict';

/**
 * @ngdoc function
 * @name alodathangApp.controller:NotificationCtrl
 * @description
 * # NotificationCtrl
 * Controller of the alodathangApp
 */
angular.module('alodathangApp')
	.controller('NotificationCtrl', function ($scope, $rootScope, $sce, NotificationService, $timeout, $window) {
		var defaultErrorCallback = function (error) {
			toastr.error("Có lỗi xảy ra, vui lòng thử lại.");
			$scope.error = error;
		};

		$scope.notifications = {};
		$scope.notifications.counter = 0;

		$scope.loadListUnreadNotifications = function () {
			NotificationService.ListUnreadNotifications(null,
				function (data) {
					$scope.notifications.list = [];
					$scope.notifications.counter = data.unread_count;
					angular.forEach(data.unread_list, function (item, index) {
						var listNotification = {
							image: '/static/blur-admin/dev-release/assets/img/shopping-cart.svg',
							template: item.description,
							time: item.timestamp,
							unread: item.unread,
							actor_content_type: item.actor_content_type,
							actor_object_id: item.actor_object_id,
							id: item.id
						};
						$scope.notifications.list.push(listNotification);
					});

				}, defaultErrorCallback)
		};

		$rootScope.$on('reloadNotifications', function (event) {
			$timeout(function () {
				$scope.loadListUnreadNotifications();
			}, 1000);
		});

		$scope.getMessage = function (msg) {
			var text = msg.template;
			return $sce.trustAsHtml(text);
		};

		$scope.clickMarkAsRead = function (item) {
			if (item.unread) {
				var PATCH = {id: item.id};
				NotificationService.MarkAsReadNotification(PATCH,
					function (data) {
					}, defaultErrorCallback);

				$scope.$emit('reloadNotifications');
			}

			if (item.actor_content_type == 'order') {
				$window.location.href = '/user/my-order/' + item.actor_object_id;
			}
		};

		$scope.clickMarkAllAsRead = function () {
			NotificationService.MarkAllAsReadNotification(null,
				function (data) {
				}, defaultErrorCallback);
			$scope.notifications = {};
		};

		$scope.loadListUnreadNotifications();

	})

	.controller('NotificationPageCtrl', function ($scope, $rootScope, $sce, NotificationService, $timeout, $window) {
		var defaultErrorCallback = function (error) {
			toastr.error("Có lỗi xảy ra, vui lòng thử lại.");
			$scope.error = error;
		};

		$scope.error = {};
		$scope.user = {};
		$scope.page = "notification_page";

		NotificationService.ListNotifications(null,
			function (data) {
				$scope.notifications = data.results;
			}, defaultErrorCallback);

		$scope.clickMarkAsReadNotification = function (item) {
			if (item.unread) {
				var PATCH = {id: item.id};
				NotificationService.MarkAsReadNotification(PATCH,
					function (data) {
					}, defaultErrorCallback);

				$scope.$emit('reloadNotifications');
			}
			if (item.actor_content_type == 'order') {
				$window.location.href = '/user/my-order/' + item.actor_object_id;
			}
		}

	})
;
