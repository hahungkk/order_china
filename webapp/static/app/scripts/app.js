'use strict';

/**
 * @ngdoc overview
 * @name alodathangApp
 * @description
 * # alodathangApp
 *
 * Main module of the application.
 */
angular.module('alodathangApp', [
        'ngAnimate',
        'ngCookies',
        'ngResource',
        'ngRoute',
        'ngSanitize',
        'ngTouch',
        'angularMoment',
        'swangular',
        'angular-websocket'
    ])
    .config(function($routeProvider, $httpProvider, $resourceProvider, $locationProvider) {
        $routeProvider
            .when('/', {
                templateUrl: '/static/app/views/main.html',
                controller: 'MainCtrl'
            })
            .when('/product', {
                templateUrl: '/static/app/views/product.html',
                controller: 'ProductCtrl'
            })
            .when('/product/search', {
                templateUrl: '/static/app/views/search.html',
                controller: 'SearchCtrl'
            })
            .when('/:site/p', {
                templateUrl: '/static/app/views/product.html',
                controller: 'SiteProductCtrl'
            })
            .when('/:site/s', {
                templateUrl: '/static/app/views/search.html',
                controller: 'SiteSearchCtrl'
            })
            .when('/login', {
                templateUrl: '/static/app/views/login.html',
                controller: 'AuthenticationCtrl'
            })
            .when('/signup', {
                templateUrl: '/static/app/views/signup.html',
                controller: 'AuthenticationCtrl'
            })
            .when('/user/user-profile/', {
                templateUrl: '/static/app/views/user-profile.html',
                controller: 'UserProfileCtrl'
            })
            .when('/user/customer-profile/', {
                templateUrl: '/static/app/views/customer-profile.html',
                controller: 'CustomerProfileCtrl'
            })
            .when('/user/change-password', {
                templateUrl: '/static/app/views/change-password.html',
                controller: 'UserChangePasswordCtrl'
            })
            .when('/user/change-email', {
                templateUrl: '/static/app/views/change-email.html',
                controller: 'UserChangeEmailCtrl'
            })
            .when('/user/my-order', {
                templateUrl: '/static/app/views/my-order.html',
                controller: 'UserMyOrderCtrl'
            })
            .when('/user/my-order/:id/', {
                templateUrl: '/static/app/views/my-order-detail.html',
                controller: 'UserMyOrderDetailCtrl'
            })
            .when('/user/payment-history', {
                templateUrl: '/static/app/views/payment-history.html',
                controller: 'UserMyPurchasesCtrl'
            })
            .when('/user/wallet-history', {
                templateUrl: '/static/app/views/wallet-history.html',
                controller: 'UserMyWalletCtrl'
            })
            .when('/user/notification', {
                templateUrl: '/static/app/views/notification-page.html',
                controller: 'NotificationPageCtrl'
            })
            .when('/cart', {
                templateUrl: '/static/app/views/cart.html',
                controller: 'CartCtrl'
            })
            .when('/cat/:pk/', {
                templateUrl: '/static/app/views/main.html',
                controller: 'CategoryCtrl'
            })
            .when('/taobao-addon/', {
                templateUrl: '/static/app/views/taobao-addon.html',
                // controller: 'CategoryCtrl'
            })
            .when('/taobao-addon-instruction/', {
                templateUrl: '/static/app/views/taobao-addon-instruction.html',
                // controller: 'CategoryCtrl'
            })
            .when('/page/:page_name', {
                templateUrl: '/static/app/views/static.html',
                controller: 'StaticCtrl'
            })
            .when('/huong-dan', {
                templateUrl: '/static/app/views/huong-dan.html',
                controller: 'AuthenticationCtrl'
            })
            .when('/checker/', {
                templateUrl: '/static/app/views/checker.html',
                controller: 'ChinaCheckerCtrl'
            }).when('/kho/', {
                templateUrl: '/static/app/views/kho.html',
                controller: 'StorageCtrl'
            }).when('/tam-tinh/', {
                templateUrl: '/static/app/views/tam-tinh.html',
                controller: 'PriceController'
            }).when('/them-san-pham/', {
                templateUrl: '/static/app/views/manual-item.html',
                controller: 'ManualAddController'
            })
            .otherwise({
                redirectTo: '/'
            });

        // Don't strip trailing slashes from calculated URLs
        $resourceProvider.defaults.stripTrailingSlashes = false;

        // Remove hashbang symbol
        $locationProvider.html5Mode(true);
        $locationProvider.hashPrefix('');

        // TODO: this should be moved some where else
        $(document).on("click", "a.note-toggle", function(e) {
            $($(e.target).parent().next()).slideToggle("400");
        });

        $(document).on("click", "img.odmx-image", function(e) {
            var $fullimage = $("<div class='full-image-hovered'><img src='" + $(this).attr("src") + "'/></div>");
            var offset = $(this).offset();
            $fullimage.css({
                "position": "absolute",
                "top": offset.top,
                "left": offset.left
            });
            $("body").append($fullimage);
            $fullimage.mouseout(function(event) {
                $(this).remove();
            });
        });

        $(document).on("mouseenter", ".odmx-price", function(e) {
            var fullcontent = $(this).next().html();
            var offset = $(this).offset();
            var $hoverDiv = $("<div class='odmx-price-hover'></div>");
            $hoverDiv.append(fullcontent);
            $hoverDiv.css({
                "position": "absolute",
                "top": offset.top + 20,
                "left": offset.left
            });
            $("body").append($hoverDiv);
            $(this).mouseout(function(event) {
                $hoverDiv.remove();
            });
        });

    })
    .run(['$rootScope', '$location', '$cookies', '$http', 'ShareService',
        'AuthenticationService', 'CartService', "UserService",
        function($rootScope, $location, $cookies, $http, ShareService,
                 AuthenticationService, CartService, UserService) {
            // keep user logged in after page refresh
            $rootScope.globals = $cookies.getObject('globals') || {};

            UserService.ViewUserProfile(null, function(data, status) {
                AuthenticationService.CreateCredential(data);
            }, function(data, status) { //handle when cookie expired
                if (status === 403) {
                    toastr.error("Phiên đăng nhập đã hết hạn. Mời đăng nhập lại", "Thông báo");
                    AuthenticationService.RemoveCredential();
                }
            });

            $rootScope.rootURL = location.protocol + '//' + location.host;
            $rootScope.appURL = location.protocol + '//' + location.host + location.pathname;

            $http.defaults.xsrfHeaderName = 'X-CSRFToken';
            $http.defaults.xsrfCookieName = 'csrftoken';

            $rootScope.cart_count = 0;

            CartService.GetCartItems({
                page_size: 0
            }, null, null);

            $rootScope.$on('wsMessage', function (event, args) {
                var data = JSON.parse(args.data);
                if(data.message === 'CartItemAdded' && data.username === $rootScope.globals.username){
                    CartService.GetCartItems({
                        page_size: 0
                    }, null, null);
                } else if (data.message === 'NewNotifications'){
                    $rootScope.$broadcast('reloadNotifications');
                }
            });

            toastr.options = {
                "closeButton": true,
                "debug": false,
                "newestOnTop": false,
                "progressBar": false,
                "positionClass": "toast-top-right",
                "preventDuplicates": false,
                "onclick": null,
                "showDuration": "300",
                "hideDuration": "1000",
                "timeOut": "5000",
                "extendedTimeOut": "1000",
                "showEasing": "swing",
                "hideEasing": "linear",
                "showMethod": "fadeIn",
                "hideMethod": "fadeOut"
            };
        }
    ])
    .filter('getVNDPrice', function (ShareService) {
        return function (obj) {
            if (obj !== undefined) {
                return ShareService.GetVNDPrice(obj).totalPrice;
            }
        };
    })
    .filter('getVNDRawPrice', function (ShareService) {
        return function (obj) {
            if (obj !== undefined) {
                return ShareService.GetVNDPrice(obj).rawPrice;
            }
        };
    })
    .filter('getVNDServiceCharge', function (ShareService) {
        return function (obj) {
            if (obj !== undefined) {
                return ShareService.GetVNDPrice(obj).serviceCharge;
            }
        };
    })
    .filter('getTotalPrice', function() {
        return function(obj) {
            return parseFloat(obj.price) * obj.quantity + parseFloat(obj.service_charge) + parseFloat(obj.shipping);
        };
    })
    .filter('getPricesByCurrency', function($sce, $filter, getCurrencySymbolFilter) {
        return function(items) {
            if (items === undefined) return;
            var prices = {};
            angular.forEach(items, function(item, index) {
                if (item.checked !== undefined && item.checked){
                    if (prices[item.currency] === undefined) {
                        prices[item.currency] = {};
                        prices[item.currency].item_cost = 0;
                        prices[item.currency].shipping = 0;
                        prices[item.currency].service_charge = 0;
                        prices[item.currency].tax_cost = 0;
                        prices[item.currency].total = 0;
                    }
                    prices[item.currency].item_cost += parseFloat(item.price) * item.quantity;
                    prices[item.currency].shipping += parseFloat(item.shipping);
                    prices[item.currency].service_charge += parseFloat(item.service_charge);
                    prices[item.currency].total += parseFloat(item.price) * item.quantity + parseFloat(item.shipping) + parseFloat(item.service_charge);
                }
            });

            var s = "<ul class='prices-by-currency'>";
            angular.forEach(prices, function(sum_data, currency_code) {
                s += "<li> Sản phẩm: <strong class='text-danger'>" + $filter('currency')(sum_data.item_cost, ' ', 2) + getCurrencySymbolFilter(currency_code) + "</strong></li>";
                s += "<li> VCND: <strong class='text-danger'>" + $filter('currency')(sum_data.shipping, ' ', 2) + getCurrencySymbolFilter(currency_code) + "</strong></li>";
                s += "<li> Phí DV: <strong class='text-danger'>" + $filter('currency')(sum_data.service_charge, ' ', 2) + getCurrencySymbolFilter(currency_code) + "</strong></li>";
                s += "<li> <h4>Tổng đơn: <strong class='text-danger'>" + $filter('currency')(sum_data.total, ' ', 2) + getCurrencySymbolFilter(currency_code) + "</strong></h4></li>";
            });
            s += "</ul>";
            return $sce.trustAsHtml(s);
        }
    })
    .filter('getDeliveryMethod', function() {
        return function(value) {
            if (value === 0) return "Nhận hàng tại công ty";
            else if (value === 1) return "Nhận hàng tại nhà";
        };
    })
    .filter('parseItemOption', function($sce) {
        return function(value) {
            if (value === undefined || value == {}) return;
            var html = "";
            var v = value.replace(/\s?u'\s?/g, "\"").replace(/\s?'/g, "\"");
            var obj = JSON.parse(v);
            html += "<ul>";
            angular.forEach(obj, function(item) {
                html += "<li><strong>" + item + ": </strong></li>";
            });
            html += "</ul>";
            return $sce.trustAsHtml(html);
        }
    })
    .filter('formatDate', function() {
        return function(date) {
            var tempDate = new Date(date);
            return tempDate.getFullYear() + "-" + (tempDate.getMonth() + 1) + "-" + tempDate.getDate() + " " + tempDate.getHours() + ":" + tempDate.getMinutes() + ":" + tempDate.getSeconds();
        }
    })
    .filter('getCurrencySymbol', function($cookies) {
        return function(code) {
            if (code === undefined) return;
            var rate = $.grep($cookies.getObject("currency"), function(e) {
                return e.code.toUpperCase() === code;
            })[0];
            return rate.label;
        }
    })
    .filter('currentDate', function($filter) {
        return function() {
            return $filter('date')(new Date(), 'dd-MM-yyyy');
        }
    })
    .filter('truncate', function() {
        return function(text, length, end) {
            if (isNaN(length))
                length = 10;

            if (end === undefined)
                end = "...";

            if (text.length <= length || text.length - end.length <= length) {
                return text;
            } else {
                return String(text).substring(0, length - end.length) + end;
            }

        };
    })
    .factory('WebSocketService', function($websocket, $rootScope) {
      // Open a WebSocket connection
        var factory = {};
        factory.connect = function (){
            var url = 'ws://';
            if(window.location.host === 'order_china.vn') {
                url = 'wss://';
            }
            var dataStream = $websocket(url + window.location.host + '/ws');
            dataStream.onMessage(function (message) {
                $rootScope.$broadcast('wsMessage', message);
            });
        };
        return factory;
    });

