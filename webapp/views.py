from django import shortcuts
from system_configure.models import TemplateCategory


def index(request):
    categories = TemplateCategory.objects.all()
    return shortcuts.render_to_response('index.html', {'categories': categories})
