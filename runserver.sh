#!/usr/bin/env bash
python manage.py collectstatic --no-input
#daphne -b 0.0.0.0 -p 8000 order_china.asgi:application
gunicorn order_china.wsgi:application -w 4 -k gevent --threads 100 --max-requests 100000 --bind 0.0.0.0:8000
